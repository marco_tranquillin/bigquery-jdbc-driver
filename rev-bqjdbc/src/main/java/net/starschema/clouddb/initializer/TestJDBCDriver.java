package net.starschema.clouddb.initializer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Properties;

/**
 * This is a sample use case for BigQuery jdbc driver
 * 
 * @author Horváth Attila (horvatha@starschema.net)
 * 
 */
public class TestJDBCDriver {

        /**
         * @param args
         */
        public static void main(String[] args) {
                Connection con = null;
                ResultSetMetaData rs=null;

                try {
                        Class.forName("net.starschema.clouddb.jdbc.BQDriver");
                        String URL="jdbc:BQDriver:aodocs-kpis-poc?withServiceAccount=true&limitRow=1&transformQuery=true";
                        //String user="394579788165-5brhodne2betair58mo3q193v05o24qg@developer.gserviceaccount.com";
                        //String password="/Users/macbook/Downloads/AODocsKey.p12";
                        String user="394579788165-7dchgv5srf7acj64jig5kh8il7eunke4@developer.gserviceaccount.com";
                        String password="/Users/macbook/Downloads/MyOwnKey.p12";

                        con = DriverManager.getConnection(URL, user,password);
                } catch (Exception e) {
                        System.out.println("Failed to initialize Connection to BigQuery:");
                        e.printStackTrace();
                }
                String samplesql = "SELECT * FROM [KPIS_POC.AUDIT_LOG];";
                ResultSet TheResult = null;

                try {
                        TheResult = con.createStatement().executeQuery(samplesql);
                        PreparedStatement ps= con.prepareStatement(samplesql);
                        rs=ps.getMetaData();
                        rs= con.createStatement().executeQuery(samplesql).getMetaData();
                        
                } catch (SQLException e) {
                        System.out.println("Failed to run Query:");
                        e.printStackTrace();
                }
                try {
                        //TheResult.first();
                        ResultSetMetaData metadata = TheResult.getMetaData();
                        int ColumnCount = metadata.getColumnCount();
                        // Print Out Column Names
                        String Line = "";
                        for (int i = 0; i < ColumnCount; i++) {
                                Line += String.format("%-32s", metadata.getColumnName(i + 1));
                        }
                        System.out.println(Line + "\n");

                        // Print out Column Values
                        while (TheResult.next()) {
                                Line = "";
                                for (int i = 0; i < ColumnCount; i++) {
                                        Line += String.format("%-32s", TheResult.getString(i + 1));
                                }
                                System.out.println(Line);
                        }
                } catch (SQLException e) {
                        e.printStackTrace();
                }

        }

}

// $ANTLR 3.5.2 JdbcGrammar.g 2015-05-16 13:18:12
/**
 * Starschema Big Query JDBC Driver
 * Copyright (C) 2012, Starschema Ltd.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * This grammar implement the sql select statement
 *    @author Horvath Attila
 *    @author Balazs Gunics
 */
  package net.starschema.clouddb.jdbc;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import org.antlr.runtime.tree.*;


/**
 * Starschema Big Query JDBC Driver
 * Copyright (C) 2012, Starschema Ltd.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * any later version.
 * </p><p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * </p><p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * </p><p>
 * This grammar implement the sql select statement
 * </p>
 *    @author Attila Horvath
 *    @author Balazs Gunics
 */
@SuppressWarnings("all")
public class JdbcGrammarParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "A", "ALIAS", "AND", "ASC", "ASKEYWORD", 
		"B", "BACKQUOTE", "BACKSINGLEQUOTE", "BOOLEANEXPRESSION", "BOOLEANEXPRESSIONITEM", 
		"BOOLEANEXPRESSIONITEMLEFT", "BOOLEANEXPRESSIONITEMRIGHT", "BYKEYWORD", 
		"C", "COLON", "COLUMN", "COMMA", "COMMENT", "COMPARISONOPERATOR", "CONDITION", 
		"CONDITIONLEFT", "CONDITIONRIGHT", "CONJUNCTION", "D", "DATASETNAME", 
		"DATASOURCE", "DESC", "DIGIT", "DISJUNCTION", "DISTINCT", "DIVIDER", "DOUBLEQUOTE", 
		"E", "EACH", "ESCAPEDDOUBLEQUOTE", "ESCAPEDSINGLEQUOTE", "EXPRESSION", 
		"EXPRESSIONTEXT", "End_Comment", "F", "FROMEXPRESSION", "FROMKEYWORD", 
		"FULl_KEYWORD", "FUNCTIONCALL", "FUNCTIONPARAMETERS", "G", "GROUPBYEXPRESSION", 
		"GROUPKEYWORD", "H", "HAVINGEXPRESSION", "HAVINGKEYWORD", "HIGHCHAR", 
		"I", "IDENTIFIER", "INNERKEYWORD", "INTEGERPARAM", "J", "JOINEXPRESSION", 
		"JOINKEYWORD", "JOINTYPE", "JOKER", "JOKERCALL", "K", "KEYWORD", "L", 
		"LEFTEXPR", "LEFT_KEYWORD", "LIKEEXPRESSION", "LIKEKEYWORD", "LIMITEXPRESSION", 
		"LIMITKEYWORD", "LINE_COMMENT", "LOGICALOPERATOR", "LOWCHAR", "LPARAM", 
		"Line_Comment", "M", "MULTIJOINEXPRESSION", "MULTIPLECALL", "N", "NAME", 
		"NEGATION", "NL", "NOTKEYWORD", "NUMBER", "O", "ONCLAUSE", "ONKEYWORD", 
		"OR", "ORDERBYCLAUSE", "ORDERBYEXPRESSION", "ORDERBYORDER", "ORDERKEYWORD", 
		"P", "PARENJOIN", "PROJECTNAME", "PUNCTUATION", "Q", "R", "RIGHTEXPR", 
		"RIGHT_KEYWORD", "RPARAM", "S", "SCOPE", "SELECTKEYWORD", "SELECTSTATEMENT", 
		"SEMICOLON", "SINGLEQUOTE", "SOURCETABLE", "STRINGLIT", "SUBQUERY", "Start_Comment", 
		"T", "TABLENAME", "TEXT", "U", "V", "W", "WHEREEXPRESSION", "WHEREKEYWORD", 
		"WS", "X", "Y", "Z", "'!='", "'*'", "'0'", "'1'", "'<'", "'<='", "'<>'", 
		"'='", "'>'", "'>='", "'['", "']'"
	};
	public static final int EOF=-1;
	public static final int T__128=128;
	public static final int T__129=129;
	public static final int T__130=130;
	public static final int T__131=131;
	public static final int T__132=132;
	public static final int T__133=133;
	public static final int T__134=134;
	public static final int T__135=135;
	public static final int T__136=136;
	public static final int T__137=137;
	public static final int T__138=138;
	public static final int T__139=139;
	public static final int A=4;
	public static final int ALIAS=5;
	public static final int AND=6;
	public static final int ASC=7;
	public static final int ASKEYWORD=8;
	public static final int B=9;
	public static final int BACKQUOTE=10;
	public static final int BACKSINGLEQUOTE=11;
	public static final int BOOLEANEXPRESSION=12;
	public static final int BOOLEANEXPRESSIONITEM=13;
	public static final int BOOLEANEXPRESSIONITEMLEFT=14;
	public static final int BOOLEANEXPRESSIONITEMRIGHT=15;
	public static final int BYKEYWORD=16;
	public static final int C=17;
	public static final int COLON=18;
	public static final int COLUMN=19;
	public static final int COMMA=20;
	public static final int COMMENT=21;
	public static final int COMPARISONOPERATOR=22;
	public static final int CONDITION=23;
	public static final int CONDITIONLEFT=24;
	public static final int CONDITIONRIGHT=25;
	public static final int CONJUNCTION=26;
	public static final int D=27;
	public static final int DATASETNAME=28;
	public static final int DATASOURCE=29;
	public static final int DESC=30;
	public static final int DIGIT=31;
	public static final int DISJUNCTION=32;
	public static final int DISTINCT=33;
	public static final int DIVIDER=34;
	public static final int DOUBLEQUOTE=35;
	public static final int E=36;
	public static final int EACH=37;
	public static final int ESCAPEDDOUBLEQUOTE=38;
	public static final int ESCAPEDSINGLEQUOTE=39;
	public static final int EXPRESSION=40;
	public static final int EXPRESSIONTEXT=41;
	public static final int End_Comment=42;
	public static final int F=43;
	public static final int FROMEXPRESSION=44;
	public static final int FROMKEYWORD=45;
	public static final int FULl_KEYWORD=46;
	public static final int FUNCTIONCALL=47;
	public static final int FUNCTIONPARAMETERS=48;
	public static final int G=49;
	public static final int GROUPBYEXPRESSION=50;
	public static final int GROUPKEYWORD=51;
	public static final int H=52;
	public static final int HAVINGEXPRESSION=53;
	public static final int HAVINGKEYWORD=54;
	public static final int HIGHCHAR=55;
	public static final int I=56;
	public static final int IDENTIFIER=57;
	public static final int INNERKEYWORD=58;
	public static final int INTEGERPARAM=59;
	public static final int J=60;
	public static final int JOINEXPRESSION=61;
	public static final int JOINKEYWORD=62;
	public static final int JOINTYPE=63;
	public static final int JOKER=64;
	public static final int JOKERCALL=65;
	public static final int K=66;
	public static final int KEYWORD=67;
	public static final int L=68;
	public static final int LEFTEXPR=69;
	public static final int LEFT_KEYWORD=70;
	public static final int LIKEEXPRESSION=71;
	public static final int LIKEKEYWORD=72;
	public static final int LIMITEXPRESSION=73;
	public static final int LIMITKEYWORD=74;
	public static final int LINE_COMMENT=75;
	public static final int LOGICALOPERATOR=76;
	public static final int LOWCHAR=77;
	public static final int LPARAM=78;
	public static final int Line_Comment=79;
	public static final int M=80;
	public static final int MULTIJOINEXPRESSION=81;
	public static final int MULTIPLECALL=82;
	public static final int N=83;
	public static final int NAME=84;
	public static final int NEGATION=85;
	public static final int NL=86;
	public static final int NOTKEYWORD=87;
	public static final int NUMBER=88;
	public static final int O=89;
	public static final int ONCLAUSE=90;
	public static final int ONKEYWORD=91;
	public static final int OR=92;
	public static final int ORDERBYCLAUSE=93;
	public static final int ORDERBYEXPRESSION=94;
	public static final int ORDERBYORDER=95;
	public static final int ORDERKEYWORD=96;
	public static final int P=97;
	public static final int PARENJOIN=98;
	public static final int PROJECTNAME=99;
	public static final int PUNCTUATION=100;
	public static final int Q=101;
	public static final int R=102;
	public static final int RIGHTEXPR=103;
	public static final int RIGHT_KEYWORD=104;
	public static final int RPARAM=105;
	public static final int S=106;
	public static final int SCOPE=107;
	public static final int SELECTKEYWORD=108;
	public static final int SELECTSTATEMENT=109;
	public static final int SEMICOLON=110;
	public static final int SINGLEQUOTE=111;
	public static final int SOURCETABLE=112;
	public static final int STRINGLIT=113;
	public static final int SUBQUERY=114;
	public static final int Start_Comment=115;
	public static final int T=116;
	public static final int TABLENAME=117;
	public static final int TEXT=118;
	public static final int U=119;
	public static final int V=120;
	public static final int W=121;
	public static final int WHEREEXPRESSION=122;
	public static final int WHEREKEYWORD=123;
	public static final int WS=124;
	public static final int X=125;
	public static final int Y=126;
	public static final int Z=127;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public JdbcGrammarParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public JdbcGrammarParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	protected TreeAdaptor adaptor = new CommonTreeAdaptor();

	public void setTreeAdaptor(TreeAdaptor adaptor) {
		this.adaptor = adaptor;
	}
	public TreeAdaptor getTreeAdaptor() {
		return adaptor;
	}
	@Override public String[] getTokenNames() { return JdbcGrammarParser.tokenNames; }
	@Override public String getGrammarFileName() { return "JdbcGrammar.g"; }


	public static class statement_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "statement"
	// JdbcGrammar.g:82:1: statement : e= selectstatement ^ ( EOF | ';' ) !;
	public final JdbcGrammarParser.statement_return statement() throws RecognitionException {
		JdbcGrammarParser.statement_return retval = new JdbcGrammarParser.statement_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token set1=null;
		ParserRuleReturnScope e =null;

		Object set1_tree=null;

		try {
			// JdbcGrammar.g:83:3: (e= selectstatement ^ ( EOF | ';' ) !)
			// JdbcGrammar.g:84:3: e= selectstatement ^ ( EOF | ';' ) !
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_selectstatement_in_statement60);
			e=selectstatement();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) root_0 = (Object)adaptor.becomeRoot(e.getTree(), root_0);
			set1=input.LT(1);
			if ( input.LA(1)==EOF||input.LA(1)==SEMICOLON ) {
				input.consume();
				state.errorRecovery=false;
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "statement"


	public static class selectstatement_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "selectstatement"
	// JdbcGrammar.g:90:1: selectstatement : SELECTKEYWORD ( DISTINCT )? expression fromexpression ( whereexpression )? ( groupbyexpression )? ( havingexpression )? ( orderbyexpression )? ( limitexpression )? -> ^( SELECTSTATEMENT SELECTKEYWORD expression fromexpression ( whereexpression )? ( groupbyexpression )? ( havingexpression )? ( orderbyexpression )? ( limitexpression )? ) ;
	public final JdbcGrammarParser.selectstatement_return selectstatement() throws RecognitionException {
		JdbcGrammarParser.selectstatement_return retval = new JdbcGrammarParser.selectstatement_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token SELECTKEYWORD2=null;
		Token DISTINCT3=null;
		ParserRuleReturnScope expression4 =null;
		ParserRuleReturnScope fromexpression5 =null;
		ParserRuleReturnScope whereexpression6 =null;
		ParserRuleReturnScope groupbyexpression7 =null;
		ParserRuleReturnScope havingexpression8 =null;
		ParserRuleReturnScope orderbyexpression9 =null;
		ParserRuleReturnScope limitexpression10 =null;

		Object SELECTKEYWORD2_tree=null;
		Object DISTINCT3_tree=null;
		RewriteRuleTokenStream stream_SELECTKEYWORD=new RewriteRuleTokenStream(adaptor,"token SELECTKEYWORD");
		RewriteRuleTokenStream stream_DISTINCT=new RewriteRuleTokenStream(adaptor,"token DISTINCT");
		RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");
		RewriteRuleSubtreeStream stream_fromexpression=new RewriteRuleSubtreeStream(adaptor,"rule fromexpression");
		RewriteRuleSubtreeStream stream_havingexpression=new RewriteRuleSubtreeStream(adaptor,"rule havingexpression");
		RewriteRuleSubtreeStream stream_orderbyexpression=new RewriteRuleSubtreeStream(adaptor,"rule orderbyexpression");
		RewriteRuleSubtreeStream stream_limitexpression=new RewriteRuleSubtreeStream(adaptor,"rule limitexpression");
		RewriteRuleSubtreeStream stream_groupbyexpression=new RewriteRuleSubtreeStream(adaptor,"rule groupbyexpression");
		RewriteRuleSubtreeStream stream_whereexpression=new RewriteRuleSubtreeStream(adaptor,"rule whereexpression");

		try {
			// JdbcGrammar.g:91:5: ( SELECTKEYWORD ( DISTINCT )? expression fromexpression ( whereexpression )? ( groupbyexpression )? ( havingexpression )? ( orderbyexpression )? ( limitexpression )? -> ^( SELECTSTATEMENT SELECTKEYWORD expression fromexpression ( whereexpression )? ( groupbyexpression )? ( havingexpression )? ( orderbyexpression )? ( limitexpression )? ) )
			// JdbcGrammar.g:91:5: SELECTKEYWORD ( DISTINCT )? expression fromexpression ( whereexpression )? ( groupbyexpression )? ( havingexpression )? ( orderbyexpression )? ( limitexpression )?
			{
			SELECTKEYWORD2=(Token)match(input,SELECTKEYWORD,FOLLOW_SELECTKEYWORD_in_selectstatement84); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_SELECTKEYWORD.add(SELECTKEYWORD2);

			// JdbcGrammar.g:91:19: ( DISTINCT )?
			int alt1=2;
			int LA1_0 = input.LA(1);
			if ( (LA1_0==DISTINCT) ) {
				alt1=1;
			}
			switch (alt1) {
				case 1 :
					// JdbcGrammar.g:91:19: DISTINCT
					{
					DISTINCT3=(Token)match(input,DISTINCT,FOLLOW_DISTINCT_in_selectstatement86); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_DISTINCT.add(DISTINCT3);

					}
					break;

			}

			pushFollow(FOLLOW_expression_in_selectstatement89);
			expression4=expression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_expression.add(expression4.getTree());
			pushFollow(FOLLOW_fromexpression_in_selectstatement91);
			fromexpression5=fromexpression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_fromexpression.add(fromexpression5.getTree());
			// JdbcGrammar.g:91:55: ( whereexpression )?
			int alt2=2;
			int LA2_0 = input.LA(1);
			if ( (LA2_0==WHEREKEYWORD) ) {
				alt2=1;
			}
			switch (alt2) {
				case 1 :
					// JdbcGrammar.g:91:55: whereexpression
					{
					pushFollow(FOLLOW_whereexpression_in_selectstatement93);
					whereexpression6=whereexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_whereexpression.add(whereexpression6.getTree());
					}
					break;

			}

			// JdbcGrammar.g:91:72: ( groupbyexpression )?
			int alt3=2;
			int LA3_0 = input.LA(1);
			if ( (LA3_0==GROUPKEYWORD) ) {
				alt3=1;
			}
			switch (alt3) {
				case 1 :
					// JdbcGrammar.g:91:72: groupbyexpression
					{
					pushFollow(FOLLOW_groupbyexpression_in_selectstatement96);
					groupbyexpression7=groupbyexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_groupbyexpression.add(groupbyexpression7.getTree());
					}
					break;

			}

			// JdbcGrammar.g:91:91: ( havingexpression )?
			int alt4=2;
			int LA4_0 = input.LA(1);
			if ( (LA4_0==HAVINGKEYWORD) ) {
				alt4=1;
			}
			switch (alt4) {
				case 1 :
					// JdbcGrammar.g:91:91: havingexpression
					{
					pushFollow(FOLLOW_havingexpression_in_selectstatement99);
					havingexpression8=havingexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_havingexpression.add(havingexpression8.getTree());
					}
					break;

			}

			// JdbcGrammar.g:91:109: ( orderbyexpression )?
			int alt5=2;
			int LA5_0 = input.LA(1);
			if ( (LA5_0==ORDERKEYWORD) ) {
				alt5=1;
			}
			switch (alt5) {
				case 1 :
					// JdbcGrammar.g:91:109: orderbyexpression
					{
					pushFollow(FOLLOW_orderbyexpression_in_selectstatement102);
					orderbyexpression9=orderbyexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_orderbyexpression.add(orderbyexpression9.getTree());
					}
					break;

			}

			// JdbcGrammar.g:91:128: ( limitexpression )?
			int alt6=2;
			int LA6_0 = input.LA(1);
			if ( (LA6_0==LIMITKEYWORD) ) {
				alt6=1;
			}
			switch (alt6) {
				case 1 :
					// JdbcGrammar.g:91:128: limitexpression
					{
					pushFollow(FOLLOW_limitexpression_in_selectstatement105);
					limitexpression10=limitexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_limitexpression.add(limitexpression10.getTree());
					}
					break;

			}

			// AST REWRITE
			// elements: limitexpression, havingexpression, expression, SELECTKEYWORD, orderbyexpression, fromexpression, whereexpression, groupbyexpression
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 92:5: -> ^( SELECTSTATEMENT SELECTKEYWORD expression fromexpression ( whereexpression )? ( groupbyexpression )? ( havingexpression )? ( orderbyexpression )? ( limitexpression )? )
			{
				// JdbcGrammar.g:92:7: ^( SELECTSTATEMENT SELECTKEYWORD expression fromexpression ( whereexpression )? ( groupbyexpression )? ( havingexpression )? ( orderbyexpression )? ( limitexpression )? )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(SELECTSTATEMENT, "SELECTSTATEMENT"), root_1);
				adaptor.addChild(root_1, stream_SELECTKEYWORD.nextNode());
				adaptor.addChild(root_1, stream_expression.nextTree());
				adaptor.addChild(root_1, stream_fromexpression.nextTree());
				// JdbcGrammar.g:92:65: ( whereexpression )?
				if ( stream_whereexpression.hasNext() ) {
					adaptor.addChild(root_1, stream_whereexpression.nextTree());
				}
				stream_whereexpression.reset();

				// JdbcGrammar.g:92:82: ( groupbyexpression )?
				if ( stream_groupbyexpression.hasNext() ) {
					adaptor.addChild(root_1, stream_groupbyexpression.nextTree());
				}
				stream_groupbyexpression.reset();

				// JdbcGrammar.g:92:101: ( havingexpression )?
				if ( stream_havingexpression.hasNext() ) {
					adaptor.addChild(root_1, stream_havingexpression.nextTree());
				}
				stream_havingexpression.reset();

				// JdbcGrammar.g:92:119: ( orderbyexpression )?
				if ( stream_orderbyexpression.hasNext() ) {
					adaptor.addChild(root_1, stream_orderbyexpression.nextTree());
				}
				stream_orderbyexpression.reset();

				// JdbcGrammar.g:92:138: ( limitexpression )?
				if ( stream_limitexpression.hasNext() ) {
					adaptor.addChild(root_1, stream_limitexpression.nextTree());
				}
				stream_limitexpression.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "selectstatement"


	public static class limitexpression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "limitexpression"
	// JdbcGrammar.g:100:1: limitexpression : LIMITKEYWORD NUMBER -> ^( LIMITEXPRESSION NUMBER ) ;
	public final JdbcGrammarParser.limitexpression_return limitexpression() throws RecognitionException {
		JdbcGrammarParser.limitexpression_return retval = new JdbcGrammarParser.limitexpression_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token LIMITKEYWORD11=null;
		Token NUMBER12=null;

		Object LIMITKEYWORD11_tree=null;
		Object NUMBER12_tree=null;
		RewriteRuleTokenStream stream_LIMITKEYWORD=new RewriteRuleTokenStream(adaptor,"token LIMITKEYWORD");
		RewriteRuleTokenStream stream_NUMBER=new RewriteRuleTokenStream(adaptor,"token NUMBER");

		try {
			// JdbcGrammar.g:101:5: ( LIMITKEYWORD NUMBER -> ^( LIMITEXPRESSION NUMBER ) )
			// JdbcGrammar.g:102:5: LIMITKEYWORD NUMBER
			{
			LIMITKEYWORD11=(Token)match(input,LIMITKEYWORD,FOLLOW_LIMITKEYWORD_in_limitexpression162); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_LIMITKEYWORD.add(LIMITKEYWORD11);

			NUMBER12=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_limitexpression164); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_NUMBER.add(NUMBER12);

			// AST REWRITE
			// elements: NUMBER
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 102:25: -> ^( LIMITEXPRESSION NUMBER )
			{
				// JdbcGrammar.g:102:27: ^( LIMITEXPRESSION NUMBER )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(LIMITEXPRESSION, "LIMITEXPRESSION"), root_1);
				adaptor.addChild(root_1, stream_NUMBER.nextNode());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "limitexpression"


	public static class orderbyexpression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "orderbyexpression"
	// JdbcGrammar.g:108:1: orderbyexpression : ( ORDERKEYWORD BYKEYWORD orderbycondition ( COMMA orderbycondition )* ) -> ^( ORDERBYEXPRESSION ( orderbycondition )* ) ;
	public final JdbcGrammarParser.orderbyexpression_return orderbyexpression() throws RecognitionException {
		JdbcGrammarParser.orderbyexpression_return retval = new JdbcGrammarParser.orderbyexpression_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token ORDERKEYWORD13=null;
		Token BYKEYWORD14=null;
		Token COMMA16=null;
		ParserRuleReturnScope orderbycondition15 =null;
		ParserRuleReturnScope orderbycondition17 =null;

		Object ORDERKEYWORD13_tree=null;
		Object BYKEYWORD14_tree=null;
		Object COMMA16_tree=null;
		RewriteRuleTokenStream stream_BYKEYWORD=new RewriteRuleTokenStream(adaptor,"token BYKEYWORD");
		RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
		RewriteRuleTokenStream stream_ORDERKEYWORD=new RewriteRuleTokenStream(adaptor,"token ORDERKEYWORD");
		RewriteRuleSubtreeStream stream_orderbycondition=new RewriteRuleSubtreeStream(adaptor,"rule orderbycondition");

		try {
			// JdbcGrammar.g:109:5: ( ( ORDERKEYWORD BYKEYWORD orderbycondition ( COMMA orderbycondition )* ) -> ^( ORDERBYEXPRESSION ( orderbycondition )* ) )
			// JdbcGrammar.g:110:5: ( ORDERKEYWORD BYKEYWORD orderbycondition ( COMMA orderbycondition )* )
			{
			// JdbcGrammar.g:110:5: ( ORDERKEYWORD BYKEYWORD orderbycondition ( COMMA orderbycondition )* )
			// JdbcGrammar.g:111:5: ORDERKEYWORD BYKEYWORD orderbycondition ( COMMA orderbycondition )*
			{
			ORDERKEYWORD13=(Token)match(input,ORDERKEYWORD,FOLLOW_ORDERKEYWORD_in_orderbyexpression192); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_ORDERKEYWORD.add(ORDERKEYWORD13);

			BYKEYWORD14=(Token)match(input,BYKEYWORD,FOLLOW_BYKEYWORD_in_orderbyexpression194); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_BYKEYWORD.add(BYKEYWORD14);

			pushFollow(FOLLOW_orderbycondition_in_orderbyexpression196);
			orderbycondition15=orderbycondition();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_orderbycondition.add(orderbycondition15.getTree());
			// JdbcGrammar.g:111:44: ( COMMA orderbycondition )*
			loop7:
			while (true) {
				int alt7=2;
				int LA7_0 = input.LA(1);
				if ( (LA7_0==COMMA) ) {
					alt7=1;
				}

				switch (alt7) {
				case 1 :
					// JdbcGrammar.g:111:45: COMMA orderbycondition
					{
					COMMA16=(Token)match(input,COMMA,FOLLOW_COMMA_in_orderbyexpression198); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_COMMA.add(COMMA16);

					pushFollow(FOLLOW_orderbycondition_in_orderbyexpression200);
					orderbycondition17=orderbycondition();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_orderbycondition.add(orderbycondition17.getTree());
					}
					break;

				default :
					break loop7;
				}
			}

			}

			// AST REWRITE
			// elements: orderbycondition
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 112:6: -> ^( ORDERBYEXPRESSION ( orderbycondition )* )
			{
				// JdbcGrammar.g:112:8: ^( ORDERBYEXPRESSION ( orderbycondition )* )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(ORDERBYEXPRESSION, "ORDERBYEXPRESSION"), root_1);
				// JdbcGrammar.g:112:28: ( orderbycondition )*
				while ( stream_orderbycondition.hasNext() ) {
					adaptor.addChild(root_1, stream_orderbycondition.nextTree());
				}
				stream_orderbycondition.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "orderbyexpression"


	public static class orderbycondition_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "orderbycondition"
	// JdbcGrammar.g:120:1: orderbycondition : column ( DESC | ASC )? -> ^( ORDERBYCLAUSE ( column )* ^( ORDERBYORDER ( DESC )? ( ASC )? ) ) ;
	public final JdbcGrammarParser.orderbycondition_return orderbycondition() throws RecognitionException {
		JdbcGrammarParser.orderbycondition_return retval = new JdbcGrammarParser.orderbycondition_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token DESC19=null;
		Token ASC20=null;
		ParserRuleReturnScope column18 =null;

		Object DESC19_tree=null;
		Object ASC20_tree=null;
		RewriteRuleTokenStream stream_DESC=new RewriteRuleTokenStream(adaptor,"token DESC");
		RewriteRuleTokenStream stream_ASC=new RewriteRuleTokenStream(adaptor,"token ASC");
		RewriteRuleSubtreeStream stream_column=new RewriteRuleSubtreeStream(adaptor,"rule column");

		try {
			// JdbcGrammar.g:121:5: ( column ( DESC | ASC )? -> ^( ORDERBYCLAUSE ( column )* ^( ORDERBYORDER ( DESC )? ( ASC )? ) ) )
			// JdbcGrammar.g:122:5: column ( DESC | ASC )?
			{
			pushFollow(FOLLOW_column_in_orderbycondition242);
			column18=column();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_column.add(column18.getTree());
			// JdbcGrammar.g:122:12: ( DESC | ASC )?
			int alt8=3;
			int LA8_0 = input.LA(1);
			if ( (LA8_0==DESC) ) {
				alt8=1;
			}
			else if ( (LA8_0==ASC) ) {
				alt8=2;
			}
			switch (alt8) {
				case 1 :
					// JdbcGrammar.g:122:13: DESC
					{
					DESC19=(Token)match(input,DESC,FOLLOW_DESC_in_orderbycondition245); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_DESC.add(DESC19);

					}
					break;
				case 2 :
					// JdbcGrammar.g:122:18: ASC
					{
					ASC20=(Token)match(input,ASC,FOLLOW_ASC_in_orderbycondition247); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_ASC.add(ASC20);

					}
					break;

			}

			// AST REWRITE
			// elements: ASC, DESC, column
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 122:24: -> ^( ORDERBYCLAUSE ( column )* ^( ORDERBYORDER ( DESC )? ( ASC )? ) )
			{
				// JdbcGrammar.g:122:26: ^( ORDERBYCLAUSE ( column )* ^( ORDERBYORDER ( DESC )? ( ASC )? ) )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(ORDERBYCLAUSE, "ORDERBYCLAUSE"), root_1);
				// JdbcGrammar.g:122:42: ( column )*
				while ( stream_column.hasNext() ) {
					adaptor.addChild(root_1, stream_column.nextTree());
				}
				stream_column.reset();

				// JdbcGrammar.g:122:50: ^( ORDERBYORDER ( DESC )? ( ASC )? )
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(ORDERBYORDER, "ORDERBYORDER"), root_2);
				// JdbcGrammar.g:122:65: ( DESC )?
				if ( stream_DESC.hasNext() ) {
					adaptor.addChild(root_2, stream_DESC.nextNode());
				}
				stream_DESC.reset();

				// JdbcGrammar.g:122:71: ( ASC )?
				if ( stream_ASC.hasNext() ) {
					adaptor.addChild(root_2, stream_ASC.nextNode());
				}
				stream_ASC.reset();

				adaptor.addChild(root_1, root_2);
				}

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "orderbycondition"


	public static class fromexpression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "fromexpression"
	// JdbcGrammar.g:128:1: fromexpression : FROMKEYWORD datasource ( COMMA datasource )* -> ^( FROMEXPRESSION ( datasource )* ^( TEXT TEXT[$fromexpression.text] ) ) ;
	public final JdbcGrammarParser.fromexpression_return fromexpression() throws RecognitionException {
		JdbcGrammarParser.fromexpression_return retval = new JdbcGrammarParser.fromexpression_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token FROMKEYWORD21=null;
		Token COMMA23=null;
		ParserRuleReturnScope datasource22 =null;
		ParserRuleReturnScope datasource24 =null;

		Object FROMKEYWORD21_tree=null;
		Object COMMA23_tree=null;
		RewriteRuleTokenStream stream_FROMKEYWORD=new RewriteRuleTokenStream(adaptor,"token FROMKEYWORD");
		RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
		RewriteRuleSubtreeStream stream_datasource=new RewriteRuleSubtreeStream(adaptor,"rule datasource");

		try {
			// JdbcGrammar.g:129:5: ( FROMKEYWORD datasource ( COMMA datasource )* -> ^( FROMEXPRESSION ( datasource )* ^( TEXT TEXT[$fromexpression.text] ) ) )
			// JdbcGrammar.g:130:5: FROMKEYWORD datasource ( COMMA datasource )*
			{
			FROMKEYWORD21=(Token)match(input,FROMKEYWORD,FOLLOW_FROMKEYWORD_in_fromexpression282); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_FROMKEYWORD.add(FROMKEYWORD21);

			pushFollow(FOLLOW_datasource_in_fromexpression288);
			datasource22=datasource();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_datasource.add(datasource22.getTree());
			// JdbcGrammar.g:132:5: ( COMMA datasource )*
			loop9:
			while (true) {
				int alt9=2;
				int LA9_0 = input.LA(1);
				if ( (LA9_0==COMMA) ) {
					alt9=1;
				}

				switch (alt9) {
				case 1 :
					// JdbcGrammar.g:133:5: COMMA datasource
					{
					COMMA23=(Token)match(input,COMMA,FOLLOW_COMMA_in_fromexpression300); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_COMMA.add(COMMA23);

					pushFollow(FOLLOW_datasource_in_fromexpression315);
					datasource24=datasource();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_datasource.add(datasource24.getTree());
					}
					break;

				default :
					break loop9;
				}
			}

			// AST REWRITE
			// elements: datasource
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 136:5: -> ^( FROMEXPRESSION ( datasource )* ^( TEXT TEXT[$fromexpression.text] ) )
			{
				// JdbcGrammar.g:136:7: ^( FROMEXPRESSION ( datasource )* ^( TEXT TEXT[$fromexpression.text] ) )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(FROMEXPRESSION, "FROMEXPRESSION"), root_1);
				// JdbcGrammar.g:136:24: ( datasource )*
				while ( stream_datasource.hasNext() ) {
					adaptor.addChild(root_1, stream_datasource.nextTree());
				}
				stream_datasource.reset();

				// JdbcGrammar.g:136:36: ^( TEXT TEXT[$fromexpression.text] )
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(TEXT, "TEXT"), root_2);
				adaptor.addChild(root_2, (Object)adaptor.create(TEXT, input.toString(retval.start,input.LT(-1))));
				adaptor.addChild(root_1, root_2);
				}

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "fromexpression"


	public static class logicaloperator_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "logicaloperator"
	// JdbcGrammar.g:142:1: logicaloperator : ( andoperator | oroperator ) -> ^( LOGICALOPERATOR ( andoperator )? ( oroperator )? ^( TEXT TEXT[$logicaloperator.text] ) ) ;
	public final JdbcGrammarParser.logicaloperator_return logicaloperator() throws RecognitionException {
		JdbcGrammarParser.logicaloperator_return retval = new JdbcGrammarParser.logicaloperator_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope andoperator25 =null;
		ParserRuleReturnScope oroperator26 =null;

		RewriteRuleSubtreeStream stream_andoperator=new RewriteRuleSubtreeStream(adaptor,"rule andoperator");
		RewriteRuleSubtreeStream stream_oroperator=new RewriteRuleSubtreeStream(adaptor,"rule oroperator");

		try {
			// JdbcGrammar.g:143:5: ( ( andoperator | oroperator ) -> ^( LOGICALOPERATOR ( andoperator )? ( oroperator )? ^( TEXT TEXT[$logicaloperator.text] ) ) )
			// JdbcGrammar.g:144:5: ( andoperator | oroperator )
			{
			// JdbcGrammar.g:144:5: ( andoperator | oroperator )
			int alt10=2;
			int LA10_0 = input.LA(1);
			if ( (LA10_0==AND) ) {
				alt10=1;
			}
			else if ( (LA10_0==OR) ) {
				alt10=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 10, 0, input);
				throw nvae;
			}

			switch (alt10) {
				case 1 :
					// JdbcGrammar.g:144:6: andoperator
					{
					pushFollow(FOLLOW_andoperator_in_logicaloperator357);
					andoperator25=andoperator();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_andoperator.add(andoperator25.getTree());
					}
					break;
				case 2 :
					// JdbcGrammar.g:144:18: oroperator
					{
					pushFollow(FOLLOW_oroperator_in_logicaloperator359);
					oroperator26=oroperator();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_oroperator.add(oroperator26.getTree());
					}
					break;

			}

			// AST REWRITE
			// elements: andoperator, oroperator
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 144:29: -> ^( LOGICALOPERATOR ( andoperator )? ( oroperator )? ^( TEXT TEXT[$logicaloperator.text] ) )
			{
				// JdbcGrammar.g:144:31: ^( LOGICALOPERATOR ( andoperator )? ( oroperator )? ^( TEXT TEXT[$logicaloperator.text] ) )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(LOGICALOPERATOR, "LOGICALOPERATOR"), root_1);
				// JdbcGrammar.g:144:49: ( andoperator )?
				if ( stream_andoperator.hasNext() ) {
					adaptor.addChild(root_1, stream_andoperator.nextTree());
				}
				stream_andoperator.reset();

				// JdbcGrammar.g:144:62: ( oroperator )?
				if ( stream_oroperator.hasNext() ) {
					adaptor.addChild(root_1, stream_oroperator.nextTree());
				}
				stream_oroperator.reset();

				// JdbcGrammar.g:144:74: ^( TEXT TEXT[$logicaloperator.text] )
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(TEXT, "TEXT"), root_2);
				adaptor.addChild(root_2, (Object)adaptor.create(TEXT, input.toString(retval.start,input.LT(-1))));
				adaptor.addChild(root_1, root_2);
				}

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "logicaloperator"


	public static class andoperator_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "andoperator"
	// JdbcGrammar.g:147:1: andoperator : AND ;
	public final JdbcGrammarParser.andoperator_return andoperator() throws RecognitionException {
		JdbcGrammarParser.andoperator_return retval = new JdbcGrammarParser.andoperator_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token AND27=null;

		Object AND27_tree=null;

		try {
			// JdbcGrammar.g:148:3: ( AND )
			// JdbcGrammar.g:149:3: AND
			{
			root_0 = (Object)adaptor.nil();


			AND27=(Token)match(input,AND,FOLLOW_AND_in_andoperator388); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			AND27_tree = (Object)adaptor.create(AND27);
			adaptor.addChild(root_0, AND27_tree);
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "andoperator"


	public static class oroperator_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "oroperator"
	// JdbcGrammar.g:152:1: oroperator : OR ;
	public final JdbcGrammarParser.oroperator_return oroperator() throws RecognitionException {
		JdbcGrammarParser.oroperator_return retval = new JdbcGrammarParser.oroperator_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token OR28=null;

		Object OR28_tree=null;

		try {
			// JdbcGrammar.g:153:3: ( OR )
			// JdbcGrammar.g:154:3: OR
			{
			root_0 = (Object)adaptor.nil();


			OR28=(Token)match(input,OR,FOLLOW_OR_in_oroperator399); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			OR28_tree = (Object)adaptor.create(OR28);
			adaptor.addChild(root_0, OR28_tree);
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "oroperator"


	public static class whereexpression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "whereexpression"
	// JdbcGrammar.g:161:1: whereexpression : ( ( WHEREKEYWORD '1' '=' '1' ) | ( WHEREKEYWORD '1' '=' '0' ) -> ^( LIMITEXPRESSION ) | ( WHEREKEYWORD expr ) -> ^( WHEREEXPRESSION expr ^( TEXT TEXT[$whereexpression.text] ) ) );
	public final JdbcGrammarParser.whereexpression_return whereexpression() throws RecognitionException {
		JdbcGrammarParser.whereexpression_return retval = new JdbcGrammarParser.whereexpression_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token WHEREKEYWORD29=null;
		Token char_literal30=null;
		Token char_literal31=null;
		Token char_literal32=null;
		Token WHEREKEYWORD33=null;
		Token char_literal34=null;
		Token char_literal35=null;
		Token char_literal36=null;
		Token WHEREKEYWORD37=null;
		ParserRuleReturnScope expr38 =null;

		Object WHEREKEYWORD29_tree=null;
		Object char_literal30_tree=null;
		Object char_literal31_tree=null;
		Object char_literal32_tree=null;
		Object WHEREKEYWORD33_tree=null;
		Object char_literal34_tree=null;
		Object char_literal35_tree=null;
		Object char_literal36_tree=null;
		Object WHEREKEYWORD37_tree=null;
		RewriteRuleTokenStream stream_135=new RewriteRuleTokenStream(adaptor,"token 135");
		RewriteRuleTokenStream stream_WHEREKEYWORD=new RewriteRuleTokenStream(adaptor,"token WHEREKEYWORD");
		RewriteRuleTokenStream stream_131=new RewriteRuleTokenStream(adaptor,"token 131");
		RewriteRuleTokenStream stream_130=new RewriteRuleTokenStream(adaptor,"token 130");
		RewriteRuleSubtreeStream stream_expr=new RewriteRuleSubtreeStream(adaptor,"rule expr");

		try {
			// JdbcGrammar.g:162:5: ( ( WHEREKEYWORD '1' '=' '1' ) | ( WHEREKEYWORD '1' '=' '0' ) -> ^( LIMITEXPRESSION ) | ( WHEREKEYWORD expr ) -> ^( WHEREEXPRESSION expr ^( TEXT TEXT[$whereexpression.text] ) ) )
			int alt11=3;
			int LA11_0 = input.LA(1);
			if ( (LA11_0==WHEREKEYWORD) ) {
				int LA11_1 = input.LA(2);
				if ( (LA11_1==131) ) {
					int LA11_2 = input.LA(3);
					if ( (LA11_2==135) ) {
						int LA11_4 = input.LA(4);
						if ( (LA11_4==131) ) {
							alt11=1;
						}
						else if ( (LA11_4==130) ) {
							alt11=2;
						}

						else {
							if (state.backtracking>0) {state.failed=true; return retval;}
							int nvaeMark = input.mark();
							try {
								for (int nvaeConsume = 0; nvaeConsume < 4 - 1; nvaeConsume++) {
									input.consume();
								}
								NoViableAltException nvae =
									new NoViableAltException("", 11, 4, input);
								throw nvae;
							} finally {
								input.rewind(nvaeMark);
							}
						}

					}

					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 11, 2, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}
				else if ( (LA11_1==BACKSINGLEQUOTE||LA11_1==DOUBLEQUOTE||LA11_1==IDENTIFIER||LA11_1==LPARAM||LA11_1==NOTKEYWORD||LA11_1==SINGLEQUOTE) ) {
					alt11=3;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 11, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 11, 0, input);
				throw nvae;
			}

			switch (alt11) {
				case 1 :
					// JdbcGrammar.g:163:5: ( WHEREKEYWORD '1' '=' '1' )
					{
					root_0 = (Object)adaptor.nil();


					// JdbcGrammar.g:163:5: ( WHEREKEYWORD '1' '=' '1' )
					// JdbcGrammar.g:163:6: WHEREKEYWORD '1' '=' '1'
					{
					WHEREKEYWORD29=(Token)match(input,WHEREKEYWORD,FOLLOW_WHEREKEYWORD_in_whereexpression416); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					WHEREKEYWORD29_tree = (Object)adaptor.create(WHEREKEYWORD29);
					adaptor.addChild(root_0, WHEREKEYWORD29_tree);
					}

					char_literal30=(Token)match(input,131,FOLLOW_131_in_whereexpression418); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal30_tree = (Object)adaptor.create(char_literal30);
					adaptor.addChild(root_0, char_literal30_tree);
					}

					char_literal31=(Token)match(input,135,FOLLOW_135_in_whereexpression420); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal31_tree = (Object)adaptor.create(char_literal31);
					adaptor.addChild(root_0, char_literal31_tree);
					}

					char_literal32=(Token)match(input,131,FOLLOW_131_in_whereexpression422); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal32_tree = (Object)adaptor.create(char_literal32);
					adaptor.addChild(root_0, char_literal32_tree);
					}

					}

					}
					break;
				case 2 :
					// JdbcGrammar.g:164:7: ( WHEREKEYWORD '1' '=' '0' )
					{
					// JdbcGrammar.g:164:7: ( WHEREKEYWORD '1' '=' '0' )
					// JdbcGrammar.g:164:8: WHEREKEYWORD '1' '=' '0'
					{
					WHEREKEYWORD33=(Token)match(input,WHEREKEYWORD,FOLLOW_WHEREKEYWORD_in_whereexpression433); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_WHEREKEYWORD.add(WHEREKEYWORD33);

					char_literal34=(Token)match(input,131,FOLLOW_131_in_whereexpression435); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_131.add(char_literal34);

					char_literal35=(Token)match(input,135,FOLLOW_135_in_whereexpression437); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_135.add(char_literal35);

					char_literal36=(Token)match(input,130,FOLLOW_130_in_whereexpression439); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_130.add(char_literal36);

					}

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 164:34: -> ^( LIMITEXPRESSION )
					{
						// JdbcGrammar.g:164:36: ^( LIMITEXPRESSION )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(LIMITEXPRESSION, "LIMITEXPRESSION"), root_1);
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 3 :
					// JdbcGrammar.g:165:7: ( WHEREKEYWORD expr )
					{
					// JdbcGrammar.g:165:7: ( WHEREKEYWORD expr )
					// JdbcGrammar.g:165:8: WHEREKEYWORD expr
					{
					WHEREKEYWORD37=(Token)match(input,WHEREKEYWORD,FOLLOW_WHEREKEYWORD_in_whereexpression456); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_WHEREKEYWORD.add(WHEREKEYWORD37);

					pushFollow(FOLLOW_expr_in_whereexpression458);
					expr38=expr();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_expr.add(expr38.getTree());
					}

					// AST REWRITE
					// elements: expr
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 166:6: -> ^( WHEREEXPRESSION expr ^( TEXT TEXT[$whereexpression.text] ) )
					{
						// JdbcGrammar.g:166:8: ^( WHEREEXPRESSION expr ^( TEXT TEXT[$whereexpression.text] ) )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(WHEREEXPRESSION, "WHEREEXPRESSION"), root_1);
						adaptor.addChild(root_1, stream_expr.nextTree());
						// JdbcGrammar.g:168:13: ^( TEXT TEXT[$whereexpression.text] )
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(TEXT, "TEXT"), root_2);
						adaptor.addChild(root_2, (Object)adaptor.create(TEXT, input.toString(retval.start,input.LT(-1))));
						adaptor.addChild(root_1, root_2);
						}

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "whereexpression"


	public static class expr_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "expr"
	// JdbcGrammar.g:172:1: expr : ( ( disjunction )=> disjunction | ( conjunction )=> conjunction | ( subconjunction )=> subconjunction | ( subdisjunction )=> subdisjunction | ( exprlvl0 )=> exprlvl0 );
	public final JdbcGrammarParser.expr_return expr() throws RecognitionException {
		JdbcGrammarParser.expr_return retval = new JdbcGrammarParser.expr_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope disjunction39 =null;
		ParserRuleReturnScope conjunction40 =null;
		ParserRuleReturnScope subconjunction41 =null;
		ParserRuleReturnScope subdisjunction42 =null;
		ParserRuleReturnScope exprlvl043 =null;


		try {
			// JdbcGrammar.g:173:3: ( ( disjunction )=> disjunction | ( conjunction )=> conjunction | ( subconjunction )=> subconjunction | ( subdisjunction )=> subdisjunction | ( exprlvl0 )=> exprlvl0 )
			int alt12=5;
			switch ( input.LA(1) ) {
			case NOTKEYWORD:
				{
				int LA12_1 = input.LA(2);
				if ( (synpred1_JdbcGrammar()) ) {
					alt12=1;
				}
				else if ( (synpred2_JdbcGrammar()) ) {
					alt12=2;
				}
				else if ( (synpred3_JdbcGrammar()) ) {
					alt12=3;
				}
				else if ( (synpred4_JdbcGrammar()) ) {
					alt12=4;
				}
				else if ( (synpred5_JdbcGrammar()) ) {
					alt12=5;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 12, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case LPARAM:
				{
				int LA12_2 = input.LA(2);
				if ( (synpred1_JdbcGrammar()) ) {
					alt12=1;
				}
				else if ( (synpred2_JdbcGrammar()) ) {
					alt12=2;
				}
				else if ( (synpred3_JdbcGrammar()) ) {
					alt12=3;
				}
				else if ( (synpred4_JdbcGrammar()) ) {
					alt12=4;
				}
				else if ( (synpred5_JdbcGrammar()) ) {
					alt12=5;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 12, 2, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case DOUBLEQUOTE:
				{
				int LA12_3 = input.LA(2);
				if ( (synpred1_JdbcGrammar()) ) {
					alt12=1;
				}
				else if ( (synpred2_JdbcGrammar()) ) {
					alt12=2;
				}
				else if ( (synpred5_JdbcGrammar()) ) {
					alt12=5;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 12, 3, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case IDENTIFIER:
				{
				int LA12_4 = input.LA(2);
				if ( (synpred1_JdbcGrammar()) ) {
					alt12=1;
				}
				else if ( (synpred2_JdbcGrammar()) ) {
					alt12=2;
				}
				else if ( (synpred5_JdbcGrammar()) ) {
					alt12=5;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 12, 4, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case BACKSINGLEQUOTE:
				{
				int LA12_5 = input.LA(2);
				if ( (synpred1_JdbcGrammar()) ) {
					alt12=1;
				}
				else if ( (synpred2_JdbcGrammar()) ) {
					alt12=2;
				}
				else if ( (synpred5_JdbcGrammar()) ) {
					alt12=5;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 12, 5, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case SINGLEQUOTE:
				{
				int LA12_6 = input.LA(2);
				if ( (synpred1_JdbcGrammar()) ) {
					alt12=1;
				}
				else if ( (synpred2_JdbcGrammar()) ) {
					alt12=2;
				}
				else if ( (synpred5_JdbcGrammar()) ) {
					alt12=5;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 12, 6, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 12, 0, input);
				throw nvae;
			}
			switch (alt12) {
				case 1 :
					// JdbcGrammar.g:174:3: ( disjunction )=> disjunction
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_disjunction_in_expr533);
					disjunction39=disjunction();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, disjunction39.getTree());

					}
					break;
				case 2 :
					// JdbcGrammar.g:175:4: ( conjunction )=> conjunction
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_conjunction_in_expr542);
					conjunction40=conjunction();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, conjunction40.getTree());

					}
					break;
				case 3 :
					// JdbcGrammar.g:176:4: ( subconjunction )=> subconjunction
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_subconjunction_in_expr551);
					subconjunction41=subconjunction();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, subconjunction41.getTree());

					}
					break;
				case 4 :
					// JdbcGrammar.g:177:4: ( subdisjunction )=> subdisjunction
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_subdisjunction_in_expr560);
					subdisjunction42=subdisjunction();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, subdisjunction42.getTree());

					}
					break;
				case 5 :
					// JdbcGrammar.g:178:4: ( exprlvl0 )=> exprlvl0
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_exprlvl0_in_expr569);
					exprlvl043=exprlvl0();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, exprlvl043.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "expr"


	public static class subdisjunction_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "subdisjunction"
	// JdbcGrammar.g:181:1: subdisjunction : ( ( NOTKEYWORD LPARAM subdisjunction RPARAM )=> ( NOTKEYWORD LPARAM subdisjunction RPARAM ) -> ^( NEGATION subdisjunction ) | ( NOTKEYWORD LPARAM disjunction RPARAM )=> ( NOTKEYWORD LPARAM disjunction RPARAM ) -> ^( NEGATION disjunction ) | ( LPARAM ! subdisjunction RPARAM !)=> ( LPARAM ! subdisjunction RPARAM !) | ( LPARAM ! disjunction RPARAM !)=> ( LPARAM ! disjunction RPARAM !) );
	public final JdbcGrammarParser.subdisjunction_return subdisjunction() throws RecognitionException {
		JdbcGrammarParser.subdisjunction_return retval = new JdbcGrammarParser.subdisjunction_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token NOTKEYWORD44=null;
		Token LPARAM45=null;
		Token RPARAM47=null;
		Token NOTKEYWORD48=null;
		Token LPARAM49=null;
		Token RPARAM51=null;
		Token LPARAM52=null;
		Token RPARAM54=null;
		Token LPARAM55=null;
		Token RPARAM57=null;
		ParserRuleReturnScope subdisjunction46 =null;
		ParserRuleReturnScope disjunction50 =null;
		ParserRuleReturnScope subdisjunction53 =null;
		ParserRuleReturnScope disjunction56 =null;

		Object NOTKEYWORD44_tree=null;
		Object LPARAM45_tree=null;
		Object RPARAM47_tree=null;
		Object NOTKEYWORD48_tree=null;
		Object LPARAM49_tree=null;
		Object RPARAM51_tree=null;
		Object LPARAM52_tree=null;
		Object RPARAM54_tree=null;
		Object LPARAM55_tree=null;
		Object RPARAM57_tree=null;
		RewriteRuleTokenStream stream_LPARAM=new RewriteRuleTokenStream(adaptor,"token LPARAM");
		RewriteRuleTokenStream stream_RPARAM=new RewriteRuleTokenStream(adaptor,"token RPARAM");
		RewriteRuleTokenStream stream_NOTKEYWORD=new RewriteRuleTokenStream(adaptor,"token NOTKEYWORD");
		RewriteRuleSubtreeStream stream_disjunction=new RewriteRuleSubtreeStream(adaptor,"rule disjunction");
		RewriteRuleSubtreeStream stream_subdisjunction=new RewriteRuleSubtreeStream(adaptor,"rule subdisjunction");

		try {
			// JdbcGrammar.g:182:3: ( ( NOTKEYWORD LPARAM subdisjunction RPARAM )=> ( NOTKEYWORD LPARAM subdisjunction RPARAM ) -> ^( NEGATION subdisjunction ) | ( NOTKEYWORD LPARAM disjunction RPARAM )=> ( NOTKEYWORD LPARAM disjunction RPARAM ) -> ^( NEGATION disjunction ) | ( LPARAM ! subdisjunction RPARAM !)=> ( LPARAM ! subdisjunction RPARAM !) | ( LPARAM ! disjunction RPARAM !)=> ( LPARAM ! disjunction RPARAM !) )
			int alt13=4;
			int LA13_0 = input.LA(1);
			if ( (LA13_0==NOTKEYWORD) ) {
				int LA13_1 = input.LA(2);
				if ( (synpred6_JdbcGrammar()) ) {
					alt13=1;
				}
				else if ( (synpred7_JdbcGrammar()) ) {
					alt13=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 13, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}
			else if ( (LA13_0==LPARAM) ) {
				int LA13_2 = input.LA(2);
				if ( (synpred8_JdbcGrammar()) ) {
					alt13=3;
				}
				else if ( (synpred9_JdbcGrammar()) ) {
					alt13=4;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 13, 2, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 13, 0, input);
				throw nvae;
			}

			switch (alt13) {
				case 1 :
					// JdbcGrammar.g:183:3: ( NOTKEYWORD LPARAM subdisjunction RPARAM )=> ( NOTKEYWORD LPARAM subdisjunction RPARAM )
					{
					// JdbcGrammar.g:183:46: ( NOTKEYWORD LPARAM subdisjunction RPARAM )
					// JdbcGrammar.g:183:47: NOTKEYWORD LPARAM subdisjunction RPARAM
					{
					NOTKEYWORD44=(Token)match(input,NOTKEYWORD,FOLLOW_NOTKEYWORD_in_subdisjunction591); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_NOTKEYWORD.add(NOTKEYWORD44);

					LPARAM45=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_subdisjunction593); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM45);

					pushFollow(FOLLOW_subdisjunction_in_subdisjunction595);
					subdisjunction46=subdisjunction();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_subdisjunction.add(subdisjunction46.getTree());
					RPARAM47=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_subdisjunction597); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM47);

					}

					// AST REWRITE
					// elements: subdisjunction
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 183:87: -> ^( NEGATION subdisjunction )
					{
						// JdbcGrammar.g:183:89: ^( NEGATION subdisjunction )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(NEGATION, "NEGATION"), root_1);
						adaptor.addChild(root_1, stream_subdisjunction.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// JdbcGrammar.g:184:4: ( NOTKEYWORD LPARAM disjunction RPARAM )=> ( NOTKEYWORD LPARAM disjunction RPARAM )
					{
					// JdbcGrammar.g:184:44: ( NOTKEYWORD LPARAM disjunction RPARAM )
					// JdbcGrammar.g:184:45: NOTKEYWORD LPARAM disjunction RPARAM
					{
					NOTKEYWORD48=(Token)match(input,NOTKEYWORD,FOLLOW_NOTKEYWORD_in_subdisjunction620); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_NOTKEYWORD.add(NOTKEYWORD48);

					LPARAM49=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_subdisjunction622); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM49);

					pushFollow(FOLLOW_disjunction_in_subdisjunction624);
					disjunction50=disjunction();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_disjunction.add(disjunction50.getTree());
					RPARAM51=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_subdisjunction626); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM51);

					}

					// AST REWRITE
					// elements: disjunction
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 184:82: -> ^( NEGATION disjunction )
					{
						// JdbcGrammar.g:184:84: ^( NEGATION disjunction )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(NEGATION, "NEGATION"), root_1);
						adaptor.addChild(root_1, stream_disjunction.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 3 :
					// JdbcGrammar.g:185:4: ( LPARAM ! subdisjunction RPARAM !)=> ( LPARAM ! subdisjunction RPARAM !)
					{
					root_0 = (Object)adaptor.nil();


					// JdbcGrammar.g:185:38: ( LPARAM ! subdisjunction RPARAM !)
					// JdbcGrammar.g:185:39: LPARAM ! subdisjunction RPARAM !
					{
					LPARAM52=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_subdisjunction649); if (state.failed) return retval;
					pushFollow(FOLLOW_subdisjunction_in_subdisjunction652);
					subdisjunction53=subdisjunction();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, subdisjunction53.getTree());

					RPARAM54=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_subdisjunction654); if (state.failed) return retval;
					}

					}
					break;
				case 4 :
					// JdbcGrammar.g:186:4: ( LPARAM ! disjunction RPARAM !)=> ( LPARAM ! disjunction RPARAM !)
					{
					root_0 = (Object)adaptor.nil();


					// JdbcGrammar.g:186:35: ( LPARAM ! disjunction RPARAM !)
					// JdbcGrammar.g:186:36: LPARAM ! disjunction RPARAM !
					{
					LPARAM55=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_subdisjunction672); if (state.failed) return retval;
					pushFollow(FOLLOW_disjunction_in_subdisjunction675);
					disjunction56=disjunction();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, disjunction56.getTree());

					RPARAM57=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_subdisjunction677); if (state.failed) return retval;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "subdisjunction"


	public static class disjunctionpart1_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "disjunctionpart1"
	// JdbcGrammar.g:189:1: disjunctionpart1 : ( ( conjunction )=> conjunction | ( subconjunction )=> subconjunction | ( subdisjunction )=> subdisjunction | exprlvl0 );
	public final JdbcGrammarParser.disjunctionpart1_return disjunctionpart1() throws RecognitionException {
		JdbcGrammarParser.disjunctionpart1_return retval = new JdbcGrammarParser.disjunctionpart1_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope conjunction58 =null;
		ParserRuleReturnScope subconjunction59 =null;
		ParserRuleReturnScope subdisjunction60 =null;
		ParserRuleReturnScope exprlvl061 =null;


		try {
			// JdbcGrammar.g:190:3: ( ( conjunction )=> conjunction | ( subconjunction )=> subconjunction | ( subdisjunction )=> subdisjunction | exprlvl0 )
			int alt14=4;
			switch ( input.LA(1) ) {
			case NOTKEYWORD:
				{
				int LA14_1 = input.LA(2);
				if ( (synpred10_JdbcGrammar()) ) {
					alt14=1;
				}
				else if ( (synpred11_JdbcGrammar()) ) {
					alt14=2;
				}
				else if ( (synpred12_JdbcGrammar()) ) {
					alt14=3;
				}
				else if ( (true) ) {
					alt14=4;
				}

				}
				break;
			case LPARAM:
				{
				int LA14_2 = input.LA(2);
				if ( (synpred10_JdbcGrammar()) ) {
					alt14=1;
				}
				else if ( (synpred11_JdbcGrammar()) ) {
					alt14=2;
				}
				else if ( (synpred12_JdbcGrammar()) ) {
					alt14=3;
				}
				else if ( (true) ) {
					alt14=4;
				}

				}
				break;
			case DOUBLEQUOTE:
				{
				int LA14_3 = input.LA(2);
				if ( (synpred10_JdbcGrammar()) ) {
					alt14=1;
				}
				else if ( (true) ) {
					alt14=4;
				}

				}
				break;
			case IDENTIFIER:
				{
				int LA14_4 = input.LA(2);
				if ( (synpred10_JdbcGrammar()) ) {
					alt14=1;
				}
				else if ( (true) ) {
					alt14=4;
				}

				}
				break;
			case BACKSINGLEQUOTE:
				{
				int LA14_5 = input.LA(2);
				if ( (synpred10_JdbcGrammar()) ) {
					alt14=1;
				}
				else if ( (true) ) {
					alt14=4;
				}

				}
				break;
			case SINGLEQUOTE:
				{
				int LA14_6 = input.LA(2);
				if ( (synpred10_JdbcGrammar()) ) {
					alt14=1;
				}
				else if ( (true) ) {
					alt14=4;
				}

				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 14, 0, input);
				throw nvae;
			}
			switch (alt14) {
				case 1 :
					// JdbcGrammar.g:191:3: ( conjunction )=> conjunction
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_conjunction_in_disjunctionpart1694);
					conjunction58=conjunction();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, conjunction58.getTree());

					}
					break;
				case 2 :
					// JdbcGrammar.g:192:4: ( subconjunction )=> subconjunction
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_subconjunction_in_disjunctionpart1703);
					subconjunction59=subconjunction();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, subconjunction59.getTree());

					}
					break;
				case 3 :
					// JdbcGrammar.g:193:4: ( subdisjunction )=> subdisjunction
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_subdisjunction_in_disjunctionpart1712);
					subdisjunction60=subdisjunction();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, subdisjunction60.getTree());

					}
					break;
				case 4 :
					// JdbcGrammar.g:194:4: exprlvl0
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_exprlvl0_in_disjunctionpart1717);
					exprlvl061=exprlvl0();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, exprlvl061.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "disjunctionpart1"


	public static class disjunction_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "disjunction"
	// JdbcGrammar.g:198:1: disjunction : ( disjunctionpart1 ( disjunctionpart2 )+ ) -> ^( DISJUNCTION disjunctionpart1 ( disjunctionpart2 )* ) ;
	public final JdbcGrammarParser.disjunction_return disjunction() throws RecognitionException {
		JdbcGrammarParser.disjunction_return retval = new JdbcGrammarParser.disjunction_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope disjunctionpart162 =null;
		ParserRuleReturnScope disjunctionpart263 =null;

		RewriteRuleSubtreeStream stream_disjunctionpart2=new RewriteRuleSubtreeStream(adaptor,"rule disjunctionpart2");
		RewriteRuleSubtreeStream stream_disjunctionpart1=new RewriteRuleSubtreeStream(adaptor,"rule disjunctionpart1");

		try {
			// JdbcGrammar.g:199:3: ( ( disjunctionpart1 ( disjunctionpart2 )+ ) -> ^( DISJUNCTION disjunctionpart1 ( disjunctionpart2 )* ) )
			// JdbcGrammar.g:200:3: ( disjunctionpart1 ( disjunctionpart2 )+ )
			{
			// JdbcGrammar.g:200:3: ( disjunctionpart1 ( disjunctionpart2 )+ )
			// JdbcGrammar.g:200:4: disjunctionpart1 ( disjunctionpart2 )+
			{
			pushFollow(FOLLOW_disjunctionpart1_in_disjunction735);
			disjunctionpart162=disjunctionpart1();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_disjunctionpart1.add(disjunctionpart162.getTree());
			// JdbcGrammar.g:200:21: ( disjunctionpart2 )+
			int cnt15=0;
			loop15:
			while (true) {
				int alt15=2;
				int LA15_0 = input.LA(1);
				if ( (LA15_0==OR) ) {
					alt15=1;
				}

				switch (alt15) {
				case 1 :
					// JdbcGrammar.g:200:21: disjunctionpart2
					{
					pushFollow(FOLLOW_disjunctionpart2_in_disjunction737);
					disjunctionpart263=disjunctionpart2();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_disjunctionpart2.add(disjunctionpart263.getTree());
					}
					break;

				default :
					if ( cnt15 >= 1 ) break loop15;
					if (state.backtracking>0) {state.failed=true; return retval;}
					EarlyExitException eee = new EarlyExitException(15, input);
					throw eee;
				}
				cnt15++;
			}

			}

			// AST REWRITE
			// elements: disjunctionpart1, disjunctionpart2
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 200:39: -> ^( DISJUNCTION disjunctionpart1 ( disjunctionpart2 )* )
			{
				// JdbcGrammar.g:200:41: ^( DISJUNCTION disjunctionpart1 ( disjunctionpart2 )* )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(DISJUNCTION, "DISJUNCTION"), root_1);
				adaptor.addChild(root_1, stream_disjunctionpart1.nextTree());
				// JdbcGrammar.g:200:72: ( disjunctionpart2 )*
				while ( stream_disjunctionpart2.hasNext() ) {
					adaptor.addChild(root_1, stream_disjunctionpart2.nextTree());
				}
				stream_disjunctionpart2.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "disjunction"


	public static class disjunctionpart2_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "disjunctionpart2"
	// JdbcGrammar.g:203:1: disjunctionpart2 : oroperator disjunctionpart1 ;
	public final JdbcGrammarParser.disjunctionpart2_return disjunctionpart2() throws RecognitionException {
		JdbcGrammarParser.disjunctionpart2_return retval = new JdbcGrammarParser.disjunctionpart2_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope oroperator64 =null;
		ParserRuleReturnScope disjunctionpart165 =null;


		try {
			// JdbcGrammar.g:204:3: ( oroperator disjunctionpart1 )
			// JdbcGrammar.g:205:3: oroperator disjunctionpart1
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_oroperator_in_disjunctionpart2759);
			oroperator64=oroperator();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, oroperator64.getTree());

			pushFollow(FOLLOW_disjunctionpart1_in_disjunctionpart2761);
			disjunctionpart165=disjunctionpart1();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, disjunctionpart165.getTree());

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "disjunctionpart2"


	public static class conjunctionpart1_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "conjunctionpart1"
	// JdbcGrammar.g:208:1: conjunctionpart1 : ( ( subconjunction )=> subconjunction | ( subdisjunction )=> subdisjunction | exprlvl0 );
	public final JdbcGrammarParser.conjunctionpart1_return conjunctionpart1() throws RecognitionException {
		JdbcGrammarParser.conjunctionpart1_return retval = new JdbcGrammarParser.conjunctionpart1_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope subconjunction66 =null;
		ParserRuleReturnScope subdisjunction67 =null;
		ParserRuleReturnScope exprlvl068 =null;


		try {
			// JdbcGrammar.g:209:3: ( ( subconjunction )=> subconjunction | ( subdisjunction )=> subdisjunction | exprlvl0 )
			int alt16=3;
			switch ( input.LA(1) ) {
			case NOTKEYWORD:
				{
				int LA16_1 = input.LA(2);
				if ( (synpred13_JdbcGrammar()) ) {
					alt16=1;
				}
				else if ( (synpred14_JdbcGrammar()) ) {
					alt16=2;
				}
				else if ( (true) ) {
					alt16=3;
				}

				}
				break;
			case LPARAM:
				{
				int LA16_2 = input.LA(2);
				if ( (synpred13_JdbcGrammar()) ) {
					alt16=1;
				}
				else if ( (synpred14_JdbcGrammar()) ) {
					alt16=2;
				}
				else if ( (true) ) {
					alt16=3;
				}

				}
				break;
			case BACKSINGLEQUOTE:
			case DOUBLEQUOTE:
			case IDENTIFIER:
			case SINGLEQUOTE:
				{
				alt16=3;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 16, 0, input);
				throw nvae;
			}
			switch (alt16) {
				case 1 :
					// JdbcGrammar.g:210:3: ( subconjunction )=> subconjunction
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_subconjunction_in_conjunctionpart1776);
					subconjunction66=subconjunction();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, subconjunction66.getTree());

					}
					break;
				case 2 :
					// JdbcGrammar.g:211:4: ( subdisjunction )=> subdisjunction
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_subdisjunction_in_conjunctionpart1785);
					subdisjunction67=subdisjunction();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, subdisjunction67.getTree());

					}
					break;
				case 3 :
					// JdbcGrammar.g:212:4: exprlvl0
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_exprlvl0_in_conjunctionpart1790);
					exprlvl068=exprlvl0();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, exprlvl068.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "conjunctionpart1"


	public static class subconjunction_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "subconjunction"
	// JdbcGrammar.g:215:1: subconjunction : ( ( NOTKEYWORD LPARAM subconjunction RPARAM )=> ( NOTKEYWORD LPARAM subconjunction RPARAM ) -> ^( NEGATION subconjunction ) | ( NOTKEYWORD LPARAM conjunction RPARAM )=> ( NOTKEYWORD LPARAM conjunction RPARAM ) -> ^( NEGATION conjunction ) | ( LPARAM ! subconjunction RPARAM !)=> ( LPARAM ! subconjunction RPARAM !) | ( LPARAM ! conjunction RPARAM !)=> ( LPARAM ! conjunction RPARAM !) );
	public final JdbcGrammarParser.subconjunction_return subconjunction() throws RecognitionException {
		JdbcGrammarParser.subconjunction_return retval = new JdbcGrammarParser.subconjunction_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token NOTKEYWORD69=null;
		Token LPARAM70=null;
		Token RPARAM72=null;
		Token NOTKEYWORD73=null;
		Token LPARAM74=null;
		Token RPARAM76=null;
		Token LPARAM77=null;
		Token RPARAM79=null;
		Token LPARAM80=null;
		Token RPARAM82=null;
		ParserRuleReturnScope subconjunction71 =null;
		ParserRuleReturnScope conjunction75 =null;
		ParserRuleReturnScope subconjunction78 =null;
		ParserRuleReturnScope conjunction81 =null;

		Object NOTKEYWORD69_tree=null;
		Object LPARAM70_tree=null;
		Object RPARAM72_tree=null;
		Object NOTKEYWORD73_tree=null;
		Object LPARAM74_tree=null;
		Object RPARAM76_tree=null;
		Object LPARAM77_tree=null;
		Object RPARAM79_tree=null;
		Object LPARAM80_tree=null;
		Object RPARAM82_tree=null;
		RewriteRuleTokenStream stream_LPARAM=new RewriteRuleTokenStream(adaptor,"token LPARAM");
		RewriteRuleTokenStream stream_RPARAM=new RewriteRuleTokenStream(adaptor,"token RPARAM");
		RewriteRuleTokenStream stream_NOTKEYWORD=new RewriteRuleTokenStream(adaptor,"token NOTKEYWORD");
		RewriteRuleSubtreeStream stream_conjunction=new RewriteRuleSubtreeStream(adaptor,"rule conjunction");
		RewriteRuleSubtreeStream stream_subconjunction=new RewriteRuleSubtreeStream(adaptor,"rule subconjunction");

		try {
			// JdbcGrammar.g:216:3: ( ( NOTKEYWORD LPARAM subconjunction RPARAM )=> ( NOTKEYWORD LPARAM subconjunction RPARAM ) -> ^( NEGATION subconjunction ) | ( NOTKEYWORD LPARAM conjunction RPARAM )=> ( NOTKEYWORD LPARAM conjunction RPARAM ) -> ^( NEGATION conjunction ) | ( LPARAM ! subconjunction RPARAM !)=> ( LPARAM ! subconjunction RPARAM !) | ( LPARAM ! conjunction RPARAM !)=> ( LPARAM ! conjunction RPARAM !) )
			int alt17=4;
			int LA17_0 = input.LA(1);
			if ( (LA17_0==NOTKEYWORD) ) {
				int LA17_1 = input.LA(2);
				if ( (synpred15_JdbcGrammar()) ) {
					alt17=1;
				}
				else if ( (synpred16_JdbcGrammar()) ) {
					alt17=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 17, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}
			else if ( (LA17_0==LPARAM) ) {
				int LA17_2 = input.LA(2);
				if ( (synpred17_JdbcGrammar()) ) {
					alt17=3;
				}
				else if ( (synpred18_JdbcGrammar()) ) {
					alt17=4;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 17, 2, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 17, 0, input);
				throw nvae;
			}

			switch (alt17) {
				case 1 :
					// JdbcGrammar.g:217:3: ( NOTKEYWORD LPARAM subconjunction RPARAM )=> ( NOTKEYWORD LPARAM subconjunction RPARAM )
					{
					// JdbcGrammar.g:217:46: ( NOTKEYWORD LPARAM subconjunction RPARAM )
					// JdbcGrammar.g:217:47: NOTKEYWORD LPARAM subconjunction RPARAM
					{
					NOTKEYWORD69=(Token)match(input,NOTKEYWORD,FOLLOW_NOTKEYWORD_in_subconjunction812); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_NOTKEYWORD.add(NOTKEYWORD69);

					LPARAM70=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_subconjunction814); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM70);

					pushFollow(FOLLOW_subconjunction_in_subconjunction816);
					subconjunction71=subconjunction();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_subconjunction.add(subconjunction71.getTree());
					RPARAM72=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_subconjunction818); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM72);

					}

					// AST REWRITE
					// elements: subconjunction
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 217:87: -> ^( NEGATION subconjunction )
					{
						// JdbcGrammar.g:217:89: ^( NEGATION subconjunction )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(NEGATION, "NEGATION"), root_1);
						adaptor.addChild(root_1, stream_subconjunction.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// JdbcGrammar.g:218:4: ( NOTKEYWORD LPARAM conjunction RPARAM )=> ( NOTKEYWORD LPARAM conjunction RPARAM )
					{
					// JdbcGrammar.g:218:44: ( NOTKEYWORD LPARAM conjunction RPARAM )
					// JdbcGrammar.g:218:45: NOTKEYWORD LPARAM conjunction RPARAM
					{
					NOTKEYWORD73=(Token)match(input,NOTKEYWORD,FOLLOW_NOTKEYWORD_in_subconjunction841); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_NOTKEYWORD.add(NOTKEYWORD73);

					LPARAM74=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_subconjunction843); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM74);

					pushFollow(FOLLOW_conjunction_in_subconjunction845);
					conjunction75=conjunction();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_conjunction.add(conjunction75.getTree());
					RPARAM76=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_subconjunction847); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM76);

					}

					// AST REWRITE
					// elements: conjunction
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 218:82: -> ^( NEGATION conjunction )
					{
						// JdbcGrammar.g:218:84: ^( NEGATION conjunction )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(NEGATION, "NEGATION"), root_1);
						adaptor.addChild(root_1, stream_conjunction.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 3 :
					// JdbcGrammar.g:219:4: ( LPARAM ! subconjunction RPARAM !)=> ( LPARAM ! subconjunction RPARAM !)
					{
					root_0 = (Object)adaptor.nil();


					// JdbcGrammar.g:219:38: ( LPARAM ! subconjunction RPARAM !)
					// JdbcGrammar.g:219:39: LPARAM ! subconjunction RPARAM !
					{
					LPARAM77=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_subconjunction870); if (state.failed) return retval;
					pushFollow(FOLLOW_subconjunction_in_subconjunction873);
					subconjunction78=subconjunction();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, subconjunction78.getTree());

					RPARAM79=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_subconjunction875); if (state.failed) return retval;
					}

					}
					break;
				case 4 :
					// JdbcGrammar.g:220:4: ( LPARAM ! conjunction RPARAM !)=> ( LPARAM ! conjunction RPARAM !)
					{
					root_0 = (Object)adaptor.nil();


					// JdbcGrammar.g:220:35: ( LPARAM ! conjunction RPARAM !)
					// JdbcGrammar.g:220:36: LPARAM ! conjunction RPARAM !
					{
					LPARAM80=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_subconjunction893); if (state.failed) return retval;
					pushFollow(FOLLOW_conjunction_in_subconjunction896);
					conjunction81=conjunction();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, conjunction81.getTree());

					RPARAM82=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_subconjunction898); if (state.failed) return retval;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "subconjunction"


	public static class conjunction_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "conjunction"
	// JdbcGrammar.g:223:1: conjunction : ( conjunctionpart1 ( conjunctionpart2 )+ ) -> ^( CONJUNCTION conjunctionpart1 ( conjunctionpart2 )* ) ;
	public final JdbcGrammarParser.conjunction_return conjunction() throws RecognitionException {
		JdbcGrammarParser.conjunction_return retval = new JdbcGrammarParser.conjunction_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope conjunctionpart183 =null;
		ParserRuleReturnScope conjunctionpart284 =null;

		RewriteRuleSubtreeStream stream_conjunctionpart2=new RewriteRuleSubtreeStream(adaptor,"rule conjunctionpart2");
		RewriteRuleSubtreeStream stream_conjunctionpart1=new RewriteRuleSubtreeStream(adaptor,"rule conjunctionpart1");

		try {
			// JdbcGrammar.g:224:3: ( ( conjunctionpart1 ( conjunctionpart2 )+ ) -> ^( CONJUNCTION conjunctionpart1 ( conjunctionpart2 )* ) )
			// JdbcGrammar.g:225:3: ( conjunctionpart1 ( conjunctionpart2 )+ )
			{
			// JdbcGrammar.g:225:3: ( conjunctionpart1 ( conjunctionpart2 )+ )
			// JdbcGrammar.g:225:4: conjunctionpart1 ( conjunctionpart2 )+
			{
			pushFollow(FOLLOW_conjunctionpart1_in_conjunction917);
			conjunctionpart183=conjunctionpart1();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_conjunctionpart1.add(conjunctionpart183.getTree());
			// JdbcGrammar.g:225:21: ( conjunctionpart2 )+
			int cnt18=0;
			loop18:
			while (true) {
				int alt18=2;
				int LA18_0 = input.LA(1);
				if ( (LA18_0==AND) ) {
					alt18=1;
				}

				switch (alt18) {
				case 1 :
					// JdbcGrammar.g:225:21: conjunctionpart2
					{
					pushFollow(FOLLOW_conjunctionpart2_in_conjunction919);
					conjunctionpart284=conjunctionpart2();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_conjunctionpart2.add(conjunctionpart284.getTree());
					}
					break;

				default :
					if ( cnt18 >= 1 ) break loop18;
					if (state.backtracking>0) {state.failed=true; return retval;}
					EarlyExitException eee = new EarlyExitException(18, input);
					throw eee;
				}
				cnt18++;
			}

			}

			// AST REWRITE
			// elements: conjunctionpart2, conjunctionpart1
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 225:39: -> ^( CONJUNCTION conjunctionpart1 ( conjunctionpart2 )* )
			{
				// JdbcGrammar.g:225:41: ^( CONJUNCTION conjunctionpart1 ( conjunctionpart2 )* )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(CONJUNCTION, "CONJUNCTION"), root_1);
				adaptor.addChild(root_1, stream_conjunctionpart1.nextTree());
				// JdbcGrammar.g:225:72: ( conjunctionpart2 )*
				while ( stream_conjunctionpart2.hasNext() ) {
					adaptor.addChild(root_1, stream_conjunctionpart2.nextTree());
				}
				stream_conjunctionpart2.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "conjunction"


	public static class conjunctionpart2_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "conjunctionpart2"
	// JdbcGrammar.g:228:1: conjunctionpart2 : andoperator conjunctionpart1 ;
	public final JdbcGrammarParser.conjunctionpart2_return conjunctionpart2() throws RecognitionException {
		JdbcGrammarParser.conjunctionpart2_return retval = new JdbcGrammarParser.conjunctionpart2_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope andoperator85 =null;
		ParserRuleReturnScope conjunctionpart186 =null;


		try {
			// JdbcGrammar.g:229:3: ( andoperator conjunctionpart1 )
			// JdbcGrammar.g:230:3: andoperator conjunctionpart1
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_andoperator_in_conjunctionpart2941);
			andoperator85=andoperator();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, andoperator85.getTree());

			pushFollow(FOLLOW_conjunctionpart1_in_conjunctionpart2943);
			conjunctionpart186=conjunctionpart1();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, conjunctionpart186.getTree());

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "conjunctionpart2"


	public static class exprlvl0_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "exprlvl0"
	// JdbcGrammar.g:234:1: exprlvl0 : ( ( NOTKEYWORD LPARAM exprlvl0 RPARAM )=> ( NOTKEYWORD LPARAM exprlvl0 RPARAM ) -> ^( NEGATION exprlvl0 ) | ( NOTKEYWORD LPARAM exprlvl1 RPARAM )=> ( NOTKEYWORD LPARAM exprlvl1 RPARAM ) -> ^( NEGATION exprlvl1 ) | ( LPARAM ! exprlvl0 RPARAM !)=> ( LPARAM ! exprlvl0 RPARAM !) | ( LPARAM ! exprlvl1 RPARAM !)=> ( LPARAM ! exprlvl1 RPARAM !) | ( exprlvl1 ) );
	public final JdbcGrammarParser.exprlvl0_return exprlvl0() throws RecognitionException {
		JdbcGrammarParser.exprlvl0_return retval = new JdbcGrammarParser.exprlvl0_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token NOTKEYWORD87=null;
		Token LPARAM88=null;
		Token RPARAM90=null;
		Token NOTKEYWORD91=null;
		Token LPARAM92=null;
		Token RPARAM94=null;
		Token LPARAM95=null;
		Token RPARAM97=null;
		Token LPARAM98=null;
		Token RPARAM100=null;
		ParserRuleReturnScope exprlvl089 =null;
		ParserRuleReturnScope exprlvl193 =null;
		ParserRuleReturnScope exprlvl096 =null;
		ParserRuleReturnScope exprlvl199 =null;
		ParserRuleReturnScope exprlvl1101 =null;

		Object NOTKEYWORD87_tree=null;
		Object LPARAM88_tree=null;
		Object RPARAM90_tree=null;
		Object NOTKEYWORD91_tree=null;
		Object LPARAM92_tree=null;
		Object RPARAM94_tree=null;
		Object LPARAM95_tree=null;
		Object RPARAM97_tree=null;
		Object LPARAM98_tree=null;
		Object RPARAM100_tree=null;
		RewriteRuleTokenStream stream_LPARAM=new RewriteRuleTokenStream(adaptor,"token LPARAM");
		RewriteRuleTokenStream stream_RPARAM=new RewriteRuleTokenStream(adaptor,"token RPARAM");
		RewriteRuleTokenStream stream_NOTKEYWORD=new RewriteRuleTokenStream(adaptor,"token NOTKEYWORD");
		RewriteRuleSubtreeStream stream_exprlvl0=new RewriteRuleSubtreeStream(adaptor,"rule exprlvl0");
		RewriteRuleSubtreeStream stream_exprlvl1=new RewriteRuleSubtreeStream(adaptor,"rule exprlvl1");

		try {
			// JdbcGrammar.g:235:3: ( ( NOTKEYWORD LPARAM exprlvl0 RPARAM )=> ( NOTKEYWORD LPARAM exprlvl0 RPARAM ) -> ^( NEGATION exprlvl0 ) | ( NOTKEYWORD LPARAM exprlvl1 RPARAM )=> ( NOTKEYWORD LPARAM exprlvl1 RPARAM ) -> ^( NEGATION exprlvl1 ) | ( LPARAM ! exprlvl0 RPARAM !)=> ( LPARAM ! exprlvl0 RPARAM !) | ( LPARAM ! exprlvl1 RPARAM !)=> ( LPARAM ! exprlvl1 RPARAM !) | ( exprlvl1 ) )
			int alt19=5;
			switch ( input.LA(1) ) {
			case NOTKEYWORD:
				{
				int LA19_1 = input.LA(2);
				if ( (synpred19_JdbcGrammar()) ) {
					alt19=1;
				}
				else if ( (synpred20_JdbcGrammar()) ) {
					alt19=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 19, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case LPARAM:
				{
				int LA19_2 = input.LA(2);
				if ( (synpred21_JdbcGrammar()) ) {
					alt19=3;
				}
				else if ( (synpred22_JdbcGrammar()) ) {
					alt19=4;
				}
				else if ( (true) ) {
					alt19=5;
				}

				}
				break;
			case BACKSINGLEQUOTE:
			case DOUBLEQUOTE:
			case IDENTIFIER:
			case SINGLEQUOTE:
				{
				alt19=5;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 19, 0, input);
				throw nvae;
			}
			switch (alt19) {
				case 1 :
					// JdbcGrammar.g:236:3: ( NOTKEYWORD LPARAM exprlvl0 RPARAM )=> ( NOTKEYWORD LPARAM exprlvl0 RPARAM )
					{
					// JdbcGrammar.g:236:40: ( NOTKEYWORD LPARAM exprlvl0 RPARAM )
					// JdbcGrammar.g:236:41: NOTKEYWORD LPARAM exprlvl0 RPARAM
					{
					NOTKEYWORD87=(Token)match(input,NOTKEYWORD,FOLLOW_NOTKEYWORD_in_exprlvl0971); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_NOTKEYWORD.add(NOTKEYWORD87);

					LPARAM88=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_exprlvl0973); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM88);

					pushFollow(FOLLOW_exprlvl0_in_exprlvl0975);
					exprlvl089=exprlvl0();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_exprlvl0.add(exprlvl089.getTree());
					RPARAM90=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_exprlvl0977); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM90);

					}

					// AST REWRITE
					// elements: exprlvl0
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 236:75: -> ^( NEGATION exprlvl0 )
					{
						// JdbcGrammar.g:236:77: ^( NEGATION exprlvl0 )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(NEGATION, "NEGATION"), root_1);
						adaptor.addChild(root_1, stream_exprlvl0.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// JdbcGrammar.g:237:4: ( NOTKEYWORD LPARAM exprlvl1 RPARAM )=> ( NOTKEYWORD LPARAM exprlvl1 RPARAM )
					{
					// JdbcGrammar.g:237:41: ( NOTKEYWORD LPARAM exprlvl1 RPARAM )
					// JdbcGrammar.g:237:42: NOTKEYWORD LPARAM exprlvl1 RPARAM
					{
					NOTKEYWORD91=(Token)match(input,NOTKEYWORD,FOLLOW_NOTKEYWORD_in_exprlvl01000); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_NOTKEYWORD.add(NOTKEYWORD91);

					LPARAM92=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_exprlvl01002); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM92);

					pushFollow(FOLLOW_exprlvl1_in_exprlvl01004);
					exprlvl193=exprlvl1();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_exprlvl1.add(exprlvl193.getTree());
					RPARAM94=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_exprlvl01006); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM94);

					}

					// AST REWRITE
					// elements: exprlvl1
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 237:76: -> ^( NEGATION exprlvl1 )
					{
						// JdbcGrammar.g:237:78: ^( NEGATION exprlvl1 )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(NEGATION, "NEGATION"), root_1);
						adaptor.addChild(root_1, stream_exprlvl1.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 3 :
					// JdbcGrammar.g:238:4: ( LPARAM ! exprlvl0 RPARAM !)=> ( LPARAM ! exprlvl0 RPARAM !)
					{
					root_0 = (Object)adaptor.nil();


					// JdbcGrammar.g:238:32: ( LPARAM ! exprlvl0 RPARAM !)
					// JdbcGrammar.g:238:33: LPARAM ! exprlvl0 RPARAM !
					{
					LPARAM95=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_exprlvl01029); if (state.failed) return retval;
					pushFollow(FOLLOW_exprlvl0_in_exprlvl01032);
					exprlvl096=exprlvl0();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, exprlvl096.getTree());

					RPARAM97=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_exprlvl01034); if (state.failed) return retval;
					}

					}
					break;
				case 4 :
					// JdbcGrammar.g:239:4: ( LPARAM ! exprlvl1 RPARAM !)=> ( LPARAM ! exprlvl1 RPARAM !)
					{
					root_0 = (Object)adaptor.nil();


					// JdbcGrammar.g:239:32: ( LPARAM ! exprlvl1 RPARAM !)
					// JdbcGrammar.g:239:33: LPARAM ! exprlvl1 RPARAM !
					{
					LPARAM98=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_exprlvl01052); if (state.failed) return retval;
					pushFollow(FOLLOW_exprlvl1_in_exprlvl01055);
					exprlvl199=exprlvl1();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, exprlvl199.getTree());

					RPARAM100=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_exprlvl01057); if (state.failed) return retval;
					}

					}
					break;
				case 5 :
					// JdbcGrammar.g:240:4: ( exprlvl1 )
					{
					root_0 = (Object)adaptor.nil();


					// JdbcGrammar.g:240:4: ( exprlvl1 )
					// JdbcGrammar.g:240:5: exprlvl1
					{
					pushFollow(FOLLOW_exprlvl1_in_exprlvl01065);
					exprlvl1101=exprlvl1();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, exprlvl1101.getTree());

					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "exprlvl0"


	public static class exprlvl1_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "exprlvl1"
	// JdbcGrammar.g:243:1: exprlvl1 : exprcondition ;
	public final JdbcGrammarParser.exprlvl1_return exprlvl1() throws RecognitionException {
		JdbcGrammarParser.exprlvl1_return retval = new JdbcGrammarParser.exprlvl1_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope exprcondition102 =null;


		try {
			// JdbcGrammar.g:244:4: ( exprcondition )
			// JdbcGrammar.g:245:4: exprcondition
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_exprcondition_in_exprlvl11078);
			exprcondition102=exprcondition();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, exprcondition102.getTree());

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "exprlvl1"


	public static class exprcondition_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "exprcondition"
	// JdbcGrammar.g:256:1: exprcondition : (s1= exprconditioncore ( ( LIKEKEYWORD likeclause ) | ( comparisonoperator (s2= exprconditioncore |s3= number ) ) ) ) -> ^( BOOLEANEXPRESSIONITEM ^( BOOLEANEXPRESSIONITEMLEFT $s1) ( comparisonoperator )? ^( BOOLEANEXPRESSIONITEMRIGHT ( likeclause )? ( $s2)? ( $s3)? ) ^( TEXT TEXT[$exprcondition.text] ) ) ;
	public final JdbcGrammarParser.exprcondition_return exprcondition() throws RecognitionException {
		JdbcGrammarParser.exprcondition_return retval = new JdbcGrammarParser.exprcondition_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token LIKEKEYWORD103=null;
		ParserRuleReturnScope s1 =null;
		ParserRuleReturnScope s2 =null;
		ParserRuleReturnScope s3 =null;
		ParserRuleReturnScope likeclause104 =null;
		ParserRuleReturnScope comparisonoperator105 =null;

		Object LIKEKEYWORD103_tree=null;
		RewriteRuleTokenStream stream_LIKEKEYWORD=new RewriteRuleTokenStream(adaptor,"token LIKEKEYWORD");
		RewriteRuleSubtreeStream stream_comparisonoperator=new RewriteRuleSubtreeStream(adaptor,"rule comparisonoperator");
		RewriteRuleSubtreeStream stream_likeclause=new RewriteRuleSubtreeStream(adaptor,"rule likeclause");
		RewriteRuleSubtreeStream stream_number=new RewriteRuleSubtreeStream(adaptor,"rule number");
		RewriteRuleSubtreeStream stream_exprconditioncore=new RewriteRuleSubtreeStream(adaptor,"rule exprconditioncore");

		try {
			// JdbcGrammar.g:257:5: ( (s1= exprconditioncore ( ( LIKEKEYWORD likeclause ) | ( comparisonoperator (s2= exprconditioncore |s3= number ) ) ) ) -> ^( BOOLEANEXPRESSIONITEM ^( BOOLEANEXPRESSIONITEMLEFT $s1) ( comparisonoperator )? ^( BOOLEANEXPRESSIONITEMRIGHT ( likeclause )? ( $s2)? ( $s3)? ) ^( TEXT TEXT[$exprcondition.text] ) ) )
			// JdbcGrammar.g:258:5: (s1= exprconditioncore ( ( LIKEKEYWORD likeclause ) | ( comparisonoperator (s2= exprconditioncore |s3= number ) ) ) )
			{
			// JdbcGrammar.g:258:5: (s1= exprconditioncore ( ( LIKEKEYWORD likeclause ) | ( comparisonoperator (s2= exprconditioncore |s3= number ) ) ) )
			// JdbcGrammar.g:259:7: s1= exprconditioncore ( ( LIKEKEYWORD likeclause ) | ( comparisonoperator (s2= exprconditioncore |s3= number ) ) )
			{
			pushFollow(FOLLOW_exprconditioncore_in_exprcondition1128);
			s1=exprconditioncore();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_exprconditioncore.add(s1.getTree());
			// JdbcGrammar.g:260:7: ( ( LIKEKEYWORD likeclause ) | ( comparisonoperator (s2= exprconditioncore |s3= number ) ) )
			int alt21=2;
			int LA21_0 = input.LA(1);
			if ( (LA21_0==LIKEKEYWORD) ) {
				alt21=1;
			}
			else if ( (LA21_0==128||(LA21_0 >= 132 && LA21_0 <= 137)) ) {
				alt21=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 21, 0, input);
				throw nvae;
			}

			switch (alt21) {
				case 1 :
					// JdbcGrammar.g:261:11: ( LIKEKEYWORD likeclause )
					{
					// JdbcGrammar.g:261:11: ( LIKEKEYWORD likeclause )
					// JdbcGrammar.g:261:12: LIKEKEYWORD likeclause
					{
					LIKEKEYWORD103=(Token)match(input,LIKEKEYWORD,FOLLOW_LIKEKEYWORD_in_exprcondition1149); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LIKEKEYWORD.add(LIKEKEYWORD103);

					pushFollow(FOLLOW_likeclause_in_exprcondition1151);
					likeclause104=likeclause();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_likeclause.add(likeclause104.getTree());
					}

					}
					break;
				case 2 :
					// JdbcGrammar.g:262:11: ( comparisonoperator (s2= exprconditioncore |s3= number ) )
					{
					// JdbcGrammar.g:262:11: ( comparisonoperator (s2= exprconditioncore |s3= number ) )
					// JdbcGrammar.g:262:12: comparisonoperator (s2= exprconditioncore |s3= number )
					{
					pushFollow(FOLLOW_comparisonoperator_in_exprcondition1165);
					comparisonoperator105=comparisonoperator();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_comparisonoperator.add(comparisonoperator105.getTree());
					// JdbcGrammar.g:263:15: (s2= exprconditioncore |s3= number )
					int alt20=2;
					int LA20_0 = input.LA(1);
					if ( (LA20_0==BACKSINGLEQUOTE||LA20_0==DOUBLEQUOTE||LA20_0==IDENTIFIER||LA20_0==LPARAM||LA20_0==SINGLEQUOTE) ) {
						alt20=1;
					}
					else if ( (LA20_0==NUMBER) ) {
						alt20=2;
					}

					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						NoViableAltException nvae =
							new NoViableAltException("", 20, 0, input);
						throw nvae;
					}

					switch (alt20) {
						case 1 :
							// JdbcGrammar.g:263:16: s2= exprconditioncore
							{
							pushFollow(FOLLOW_exprconditioncore_in_exprcondition1184);
							s2=exprconditioncore();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_exprconditioncore.add(s2.getTree());
							}
							break;
						case 2 :
							// JdbcGrammar.g:263:37: s3= number
							{
							pushFollow(FOLLOW_number_in_exprcondition1188);
							s3=number();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_number.add(s3.getTree());
							}
							break;

					}

					}

					}
					break;

			}

			}

			// AST REWRITE
			// elements: likeclause, s3, s1, s2, comparisonoperator
			// token labels: 
			// rule labels: retval, s2, s1, s3
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
			RewriteRuleSubtreeStream stream_s2=new RewriteRuleSubtreeStream(adaptor,"rule s2",s2!=null?s2.getTree():null);
			RewriteRuleSubtreeStream stream_s1=new RewriteRuleSubtreeStream(adaptor,"rule s1",s1!=null?s1.getTree():null);
			RewriteRuleSubtreeStream stream_s3=new RewriteRuleSubtreeStream(adaptor,"rule s3",s3!=null?s3.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 267:4: -> ^( BOOLEANEXPRESSIONITEM ^( BOOLEANEXPRESSIONITEMLEFT $s1) ( comparisonoperator )? ^( BOOLEANEXPRESSIONITEMRIGHT ( likeclause )? ( $s2)? ( $s3)? ) ^( TEXT TEXT[$exprcondition.text] ) )
			{
				// JdbcGrammar.g:267:6: ^( BOOLEANEXPRESSIONITEM ^( BOOLEANEXPRESSIONITEMLEFT $s1) ( comparisonoperator )? ^( BOOLEANEXPRESSIONITEMRIGHT ( likeclause )? ( $s2)? ( $s3)? ) ^( TEXT TEXT[$exprcondition.text] ) )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(BOOLEANEXPRESSIONITEM, "BOOLEANEXPRESSIONITEM"), root_1);
				// JdbcGrammar.g:267:30: ^( BOOLEANEXPRESSIONITEMLEFT $s1)
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(BOOLEANEXPRESSIONITEMLEFT, "BOOLEANEXPRESSIONITEMLEFT"), root_2);
				adaptor.addChild(root_2, stream_s1.nextTree());
				adaptor.addChild(root_1, root_2);
				}

				// JdbcGrammar.g:267:63: ( comparisonoperator )?
				if ( stream_comparisonoperator.hasNext() ) {
					adaptor.addChild(root_1, stream_comparisonoperator.nextTree());
				}
				stream_comparisonoperator.reset();

				// JdbcGrammar.g:267:84: ^( BOOLEANEXPRESSIONITEMRIGHT ( likeclause )? ( $s2)? ( $s3)? )
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(BOOLEANEXPRESSIONITEMRIGHT, "BOOLEANEXPRESSIONITEMRIGHT"), root_2);
				// JdbcGrammar.g:267:113: ( likeclause )?
				if ( stream_likeclause.hasNext() ) {
					adaptor.addChild(root_2, stream_likeclause.nextTree());
				}
				stream_likeclause.reset();

				// JdbcGrammar.g:267:126: ( $s2)?
				if ( stream_s2.hasNext() ) {
					adaptor.addChild(root_2, stream_s2.nextTree());
				}
				stream_s2.reset();

				// JdbcGrammar.g:267:131: ( $s3)?
				if ( stream_s3.hasNext() ) {
					adaptor.addChild(root_2, stream_s3.nextTree());
				}
				stream_s3.reset();

				adaptor.addChild(root_1, root_2);
				}

				// JdbcGrammar.g:267:136: ^( TEXT TEXT[$exprcondition.text] )
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(TEXT, "TEXT"), root_2);
				adaptor.addChild(root_2, (Object)adaptor.create(TEXT, input.toString(retval.start,input.LT(-1))));
				adaptor.addChild(root_1, root_2);
				}

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "exprcondition"


	public static class exprconditioncore_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "exprconditioncore"
	// JdbcGrammar.g:270:1: exprconditioncore : ( subquery | columnforexpression | stringliteral );
	public final JdbcGrammarParser.exprconditioncore_return exprconditioncore() throws RecognitionException {
		JdbcGrammarParser.exprconditioncore_return retval = new JdbcGrammarParser.exprconditioncore_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope subquery106 =null;
		ParserRuleReturnScope columnforexpression107 =null;
		ParserRuleReturnScope stringliteral108 =null;


		try {
			// JdbcGrammar.g:270:18: ( subquery | columnforexpression | stringliteral )
			int alt22=3;
			alt22 = dfa22.predict(input);
			switch (alt22) {
				case 1 :
					// JdbcGrammar.g:271:1: subquery
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_subquery_in_exprconditioncore1273);
					subquery106=subquery();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, subquery106.getTree());

					}
					break;
				case 2 :
					// JdbcGrammar.g:271:10: columnforexpression
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_columnforexpression_in_exprconditioncore1275);
					columnforexpression107=columnforexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, columnforexpression107.getTree());

					}
					break;
				case 3 :
					// JdbcGrammar.g:271:30: stringliteral
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_stringliteral_in_exprconditioncore1277);
					stringliteral108=stringliteral();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, stringliteral108.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "exprconditioncore"


	public static class columnforexpression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "columnforexpression"
	// JdbcGrammar.g:274:1: columnforexpression : ( ( razoralias COLON )? ( scopealias ( COLON | PUNCTUATION ) )* ( name ) ( alias )? ) -> ^( COLUMN ( scopealias )* name ( alias )? ^( TEXT TEXT[$columnforexpression.text] ) ) ;
	public final JdbcGrammarParser.columnforexpression_return columnforexpression() throws RecognitionException {
		JdbcGrammarParser.columnforexpression_return retval = new JdbcGrammarParser.columnforexpression_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token COLON110=null;
		Token COLON112=null;
		Token PUNCTUATION113=null;
		ParserRuleReturnScope razoralias109 =null;
		ParserRuleReturnScope scopealias111 =null;
		ParserRuleReturnScope name114 =null;
		ParserRuleReturnScope alias115 =null;

		Object COLON110_tree=null;
		Object COLON112_tree=null;
		Object PUNCTUATION113_tree=null;
		RewriteRuleTokenStream stream_COLON=new RewriteRuleTokenStream(adaptor,"token COLON");
		RewriteRuleTokenStream stream_PUNCTUATION=new RewriteRuleTokenStream(adaptor,"token PUNCTUATION");
		RewriteRuleSubtreeStream stream_scopealias=new RewriteRuleSubtreeStream(adaptor,"rule scopealias");
		RewriteRuleSubtreeStream stream_razoralias=new RewriteRuleSubtreeStream(adaptor,"rule razoralias");
		RewriteRuleSubtreeStream stream_alias=new RewriteRuleSubtreeStream(adaptor,"rule alias");
		RewriteRuleSubtreeStream stream_name=new RewriteRuleSubtreeStream(adaptor,"rule name");

		try {
			// JdbcGrammar.g:275:3: ( ( ( razoralias COLON )? ( scopealias ( COLON | PUNCTUATION ) )* ( name ) ( alias )? ) -> ^( COLUMN ( scopealias )* name ( alias )? ^( TEXT TEXT[$columnforexpression.text] ) ) )
			// JdbcGrammar.g:276:3: ( ( razoralias COLON )? ( scopealias ( COLON | PUNCTUATION ) )* ( name ) ( alias )? )
			{
			// JdbcGrammar.g:276:3: ( ( razoralias COLON )? ( scopealias ( COLON | PUNCTUATION ) )* ( name ) ( alias )? )
			// JdbcGrammar.g:277:4: ( razoralias COLON )? ( scopealias ( COLON | PUNCTUATION ) )* ( name ) ( alias )?
			{
			// JdbcGrammar.g:277:4: ( razoralias COLON )?
			int alt23=2;
			int LA23_0 = input.LA(1);
			if ( (LA23_0==DOUBLEQUOTE) ) {
				int LA23_1 = input.LA(2);
				if ( (LA23_1==IDENTIFIER) ) {
					int LA23_3 = input.LA(3);
					if ( (LA23_3==COLON||LA23_3==PUNCTUATION) ) {
						alt23=1;
					}
				}
			}
			switch (alt23) {
				case 1 :
					// JdbcGrammar.g:277:5: razoralias COLON
					{
					pushFollow(FOLLOW_razoralias_in_columnforexpression1294);
					razoralias109=razoralias();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_razoralias.add(razoralias109.getTree());
					COLON110=(Token)match(input,COLON,FOLLOW_COLON_in_columnforexpression1296); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_COLON.add(COLON110);

					}
					break;

			}

			// JdbcGrammar.g:278:4: ( scopealias ( COLON | PUNCTUATION ) )*
			loop25:
			while (true) {
				int alt25=2;
				int LA25_0 = input.LA(1);
				if ( (LA25_0==IDENTIFIER) ) {
					int LA25_1 = input.LA(2);
					if ( (LA25_1==COLON||LA25_1==PUNCTUATION) ) {
						alt25=1;
					}

				}
				else if ( (LA25_0==BACKSINGLEQUOTE||LA25_0==DOUBLEQUOTE) ) {
					alt25=1;
				}

				switch (alt25) {
				case 1 :
					// JdbcGrammar.g:278:5: scopealias ( COLON | PUNCTUATION )
					{
					pushFollow(FOLLOW_scopealias_in_columnforexpression1304);
					scopealias111=scopealias();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_scopealias.add(scopealias111.getTree());
					// JdbcGrammar.g:278:16: ( COLON | PUNCTUATION )
					int alt24=2;
					int LA24_0 = input.LA(1);
					if ( (LA24_0==COLON) ) {
						alt24=1;
					}
					else if ( (LA24_0==PUNCTUATION) ) {
						alt24=2;
					}

					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						NoViableAltException nvae =
							new NoViableAltException("", 24, 0, input);
						throw nvae;
					}

					switch (alt24) {
						case 1 :
							// JdbcGrammar.g:278:17: COLON
							{
							COLON112=(Token)match(input,COLON,FOLLOW_COLON_in_columnforexpression1307); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_COLON.add(COLON112);

							}
							break;
						case 2 :
							// JdbcGrammar.g:278:25: PUNCTUATION
							{
							PUNCTUATION113=(Token)match(input,PUNCTUATION,FOLLOW_PUNCTUATION_in_columnforexpression1311); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_PUNCTUATION.add(PUNCTUATION113);

							}
							break;

					}

					}
					break;

				default :
					break loop25;
				}
			}

			// JdbcGrammar.g:279:4: ( name )
			// JdbcGrammar.g:279:5: name
			{
			pushFollow(FOLLOW_name_in_columnforexpression1320);
			name114=name();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_name.add(name114.getTree());
			}

			// JdbcGrammar.g:280:4: ( alias )?
			int alt26=2;
			int LA26_0 = input.LA(1);
			if ( (LA26_0==ASKEYWORD||LA26_0==DOUBLEQUOTE||LA26_0==IDENTIFIER||LA26_0==SINGLEQUOTE) ) {
				alt26=1;
			}
			switch (alt26) {
				case 1 :
					// JdbcGrammar.g:280:6: alias
					{
					pushFollow(FOLLOW_alias_in_columnforexpression1329);
					alias115=alias();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_alias.add(alias115.getTree());
					}
					break;

			}

			}

			// AST REWRITE
			// elements: alias, name, scopealias
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 281:5: -> ^( COLUMN ( scopealias )* name ( alias )? ^( TEXT TEXT[$columnforexpression.text] ) )
			{
				// JdbcGrammar.g:281:7: ^( COLUMN ( scopealias )* name ( alias )? ^( TEXT TEXT[$columnforexpression.text] ) )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(COLUMN, "COLUMN"), root_1);
				// JdbcGrammar.g:281:16: ( scopealias )*
				while ( stream_scopealias.hasNext() ) {
					adaptor.addChild(root_1, stream_scopealias.nextTree());
				}
				stream_scopealias.reset();

				adaptor.addChild(root_1, stream_name.nextTree());
				// JdbcGrammar.g:281:33: ( alias )?
				if ( stream_alias.hasNext() ) {
					adaptor.addChild(root_1, stream_alias.nextTree());
				}
				stream_alias.reset();

				// JdbcGrammar.g:281:40: ^( TEXT TEXT[$columnforexpression.text] )
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(TEXT, "TEXT"), root_2);
				adaptor.addChild(root_2, (Object)adaptor.create(TEXT, input.toString(retval.start,input.LT(-1))));
				adaptor.addChild(root_1, root_2);
				}

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "columnforexpression"


	public static class havingexpression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "havingexpression"
	// JdbcGrammar.g:287:1: havingexpression : ( HAVINGKEYWORD expr ) -> ^( HAVINGEXPRESSION expr ^( TEXT TEXT[$havingexpression.text] ) ) ;
	public final JdbcGrammarParser.havingexpression_return havingexpression() throws RecognitionException {
		JdbcGrammarParser.havingexpression_return retval = new JdbcGrammarParser.havingexpression_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token HAVINGKEYWORD116=null;
		ParserRuleReturnScope expr117 =null;

		Object HAVINGKEYWORD116_tree=null;
		RewriteRuleTokenStream stream_HAVINGKEYWORD=new RewriteRuleTokenStream(adaptor,"token HAVINGKEYWORD");
		RewriteRuleSubtreeStream stream_expr=new RewriteRuleSubtreeStream(adaptor,"rule expr");

		try {
			// JdbcGrammar.g:288:5: ( ( HAVINGKEYWORD expr ) -> ^( HAVINGEXPRESSION expr ^( TEXT TEXT[$havingexpression.text] ) ) )
			// JdbcGrammar.g:289:5: ( HAVINGKEYWORD expr )
			{
			// JdbcGrammar.g:289:5: ( HAVINGKEYWORD expr )
			// JdbcGrammar.g:290:5: HAVINGKEYWORD expr
			{
			HAVINGKEYWORD116=(Token)match(input,HAVINGKEYWORD,FOLLOW_HAVINGKEYWORD_in_havingexpression1376); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_HAVINGKEYWORD.add(HAVINGKEYWORD116);

			pushFollow(FOLLOW_expr_in_havingexpression1378);
			expr117=expr();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_expr.add(expr117.getTree());
			}

			// AST REWRITE
			// elements: expr
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 291:6: -> ^( HAVINGEXPRESSION expr ^( TEXT TEXT[$havingexpression.text] ) )
			{
				// JdbcGrammar.g:291:8: ^( HAVINGEXPRESSION expr ^( TEXT TEXT[$havingexpression.text] ) )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(HAVINGEXPRESSION, "HAVINGEXPRESSION"), root_1);
				adaptor.addChild(root_1, stream_expr.nextTree());
				// JdbcGrammar.g:293:21: ^( TEXT TEXT[$havingexpression.text] )
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(TEXT, "TEXT"), root_2);
				adaptor.addChild(root_2, (Object)adaptor.create(TEXT, input.toString(retval.start,input.LT(-1))));
				adaptor.addChild(root_1, root_2);
				}

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "havingexpression"


	public static class groupbyexpression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "groupbyexpression"
	// JdbcGrammar.g:302:1: groupbyexpression : ( GROUPKEYWORD BYKEYWORD column ( COMMA column )* ) -> ^( GROUPBYEXPRESSION ( column )+ ^( TEXT TEXT[$groupbyexpression.text] ) ) ;
	public final JdbcGrammarParser.groupbyexpression_return groupbyexpression() throws RecognitionException {
		JdbcGrammarParser.groupbyexpression_return retval = new JdbcGrammarParser.groupbyexpression_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token GROUPKEYWORD118=null;
		Token BYKEYWORD119=null;
		Token COMMA121=null;
		ParserRuleReturnScope column120 =null;
		ParserRuleReturnScope column122 =null;

		Object GROUPKEYWORD118_tree=null;
		Object BYKEYWORD119_tree=null;
		Object COMMA121_tree=null;
		RewriteRuleTokenStream stream_BYKEYWORD=new RewriteRuleTokenStream(adaptor,"token BYKEYWORD");
		RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
		RewriteRuleTokenStream stream_GROUPKEYWORD=new RewriteRuleTokenStream(adaptor,"token GROUPKEYWORD");
		RewriteRuleSubtreeStream stream_column=new RewriteRuleSubtreeStream(adaptor,"rule column");

		try {
			// JdbcGrammar.g:303:4: ( ( GROUPKEYWORD BYKEYWORD column ( COMMA column )* ) -> ^( GROUPBYEXPRESSION ( column )+ ^( TEXT TEXT[$groupbyexpression.text] ) ) )
			// JdbcGrammar.g:304:4: ( GROUPKEYWORD BYKEYWORD column ( COMMA column )* )
			{
			// JdbcGrammar.g:304:4: ( GROUPKEYWORD BYKEYWORD column ( COMMA column )* )
			// JdbcGrammar.g:304:5: GROUPKEYWORD BYKEYWORD column ( COMMA column )*
			{
			GROUPKEYWORD118=(Token)match(input,GROUPKEYWORD,FOLLOW_GROUPKEYWORD_in_groupbyexpression1463); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_GROUPKEYWORD.add(GROUPKEYWORD118);

			BYKEYWORD119=(Token)match(input,BYKEYWORD,FOLLOW_BYKEYWORD_in_groupbyexpression1465); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_BYKEYWORD.add(BYKEYWORD119);

			pushFollow(FOLLOW_column_in_groupbyexpression1467);
			column120=column();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_column.add(column120.getTree());
			// JdbcGrammar.g:304:35: ( COMMA column )*
			loop27:
			while (true) {
				int alt27=2;
				int LA27_0 = input.LA(1);
				if ( (LA27_0==COMMA) ) {
					alt27=1;
				}

				switch (alt27) {
				case 1 :
					// JdbcGrammar.g:304:36: COMMA column
					{
					COMMA121=(Token)match(input,COMMA,FOLLOW_COMMA_in_groupbyexpression1470); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_COMMA.add(COMMA121);

					pushFollow(FOLLOW_column_in_groupbyexpression1472);
					column122=column();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_column.add(column122.getTree());
					}
					break;

				default :
					break loop27;
				}
			}

			}

			// AST REWRITE
			// elements: column
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 304:52: -> ^( GROUPBYEXPRESSION ( column )+ ^( TEXT TEXT[$groupbyexpression.text] ) )
			{
				// JdbcGrammar.g:304:54: ^( GROUPBYEXPRESSION ( column )+ ^( TEXT TEXT[$groupbyexpression.text] ) )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(GROUPBYEXPRESSION, "GROUPBYEXPRESSION"), root_1);
				if ( !(stream_column.hasNext()) ) {
					throw new RewriteEarlyExitException();
				}
				while ( stream_column.hasNext() ) {
					adaptor.addChild(root_1, stream_column.nextTree());
				}
				stream_column.reset();

				// JdbcGrammar.g:304:82: ^( TEXT TEXT[$groupbyexpression.text] )
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(TEXT, "TEXT"), root_2);
				adaptor.addChild(root_2, (Object)adaptor.create(TEXT, input.toString(retval.start,input.LT(-1))));
				adaptor.addChild(root_1, root_2);
				}

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "groupbyexpression"


	public static class condition_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "condition"
	// JdbcGrammar.g:314:1: condition : (s1= column comparisonoperator s2= column ) -> ^( CONDITION ^( CONDITIONLEFT $s1) comparisonoperator ^( CONDITIONRIGHT $s2) ^( TEXT TEXT[$condition.text] ) ) ;
	public final JdbcGrammarParser.condition_return condition() throws RecognitionException {
		JdbcGrammarParser.condition_return retval = new JdbcGrammarParser.condition_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope s1 =null;
		ParserRuleReturnScope s2 =null;
		ParserRuleReturnScope comparisonoperator123 =null;

		RewriteRuleSubtreeStream stream_comparisonoperator=new RewriteRuleSubtreeStream(adaptor,"rule comparisonoperator");
		RewriteRuleSubtreeStream stream_column=new RewriteRuleSubtreeStream(adaptor,"rule column");

		try {
			// JdbcGrammar.g:315:2: ( (s1= column comparisonoperator s2= column ) -> ^( CONDITION ^( CONDITIONLEFT $s1) comparisonoperator ^( CONDITIONRIGHT $s2) ^( TEXT TEXT[$condition.text] ) ) )
			// JdbcGrammar.g:316:2: (s1= column comparisonoperator s2= column )
			{
			// JdbcGrammar.g:316:2: (s1= column comparisonoperator s2= column )
			// JdbcGrammar.g:317:5: s1= column comparisonoperator s2= column
			{
			pushFollow(FOLLOW_column_in_condition1524);
			s1=column();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_column.add(s1.getTree());
			pushFollow(FOLLOW_comparisonoperator_in_condition1526);
			comparisonoperator123=comparisonoperator();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_comparisonoperator.add(comparisonoperator123.getTree());
			pushFollow(FOLLOW_column_in_condition1530);
			s2=column();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_column.add(s2.getTree());
			}

			// AST REWRITE
			// elements: comparisonoperator, s2, s1
			// token labels: 
			// rule labels: retval, s2, s1
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
			RewriteRuleSubtreeStream stream_s2=new RewriteRuleSubtreeStream(adaptor,"rule s2",s2!=null?s2.getTree():null);
			RewriteRuleSubtreeStream stream_s1=new RewriteRuleSubtreeStream(adaptor,"rule s1",s1!=null?s1.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 318:3: -> ^( CONDITION ^( CONDITIONLEFT $s1) comparisonoperator ^( CONDITIONRIGHT $s2) ^( TEXT TEXT[$condition.text] ) )
			{
				// JdbcGrammar.g:318:5: ^( CONDITION ^( CONDITIONLEFT $s1) comparisonoperator ^( CONDITIONRIGHT $s2) ^( TEXT TEXT[$condition.text] ) )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(CONDITION, "CONDITION"), root_1);
				// JdbcGrammar.g:318:17: ^( CONDITIONLEFT $s1)
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(CONDITIONLEFT, "CONDITIONLEFT"), root_2);
				adaptor.addChild(root_2, stream_s1.nextTree());
				adaptor.addChild(root_1, root_2);
				}

				adaptor.addChild(root_1, stream_comparisonoperator.nextTree());
				// JdbcGrammar.g:318:57: ^( CONDITIONRIGHT $s2)
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(CONDITIONRIGHT, "CONDITIONRIGHT"), root_2);
				adaptor.addChild(root_2, stream_s2.nextTree());
				adaptor.addChild(root_1, root_2);
				}

				// JdbcGrammar.g:318:79: ^( TEXT TEXT[$condition.text] )
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(TEXT, "TEXT"), root_2);
				adaptor.addChild(root_2, (Object)adaptor.create(TEXT, input.toString(retval.start,input.LT(-1))));
				adaptor.addChild(root_1, root_2);
				}

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "condition"


	public static class subquery_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "subquery"
	// JdbcGrammar.g:325:1: subquery : ( LPARAM selectstatement RPARAM ( alias )? ) -> ^( SUBQUERY selectstatement ( alias )? ^( TEXT TEXT[$subquery.text] ) ) ;
	public final JdbcGrammarParser.subquery_return subquery() throws RecognitionException {
		JdbcGrammarParser.subquery_return retval = new JdbcGrammarParser.subquery_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token LPARAM124=null;
		Token RPARAM126=null;
		ParserRuleReturnScope selectstatement125 =null;
		ParserRuleReturnScope alias127 =null;

		Object LPARAM124_tree=null;
		Object RPARAM126_tree=null;
		RewriteRuleTokenStream stream_LPARAM=new RewriteRuleTokenStream(adaptor,"token LPARAM");
		RewriteRuleTokenStream stream_RPARAM=new RewriteRuleTokenStream(adaptor,"token RPARAM");
		RewriteRuleSubtreeStream stream_selectstatement=new RewriteRuleSubtreeStream(adaptor,"rule selectstatement");
		RewriteRuleSubtreeStream stream_alias=new RewriteRuleSubtreeStream(adaptor,"rule alias");

		try {
			// JdbcGrammar.g:326:4: ( ( LPARAM selectstatement RPARAM ( alias )? ) -> ^( SUBQUERY selectstatement ( alias )? ^( TEXT TEXT[$subquery.text] ) ) )
			// JdbcGrammar.g:327:4: ( LPARAM selectstatement RPARAM ( alias )? )
			{
			// JdbcGrammar.g:327:4: ( LPARAM selectstatement RPARAM ( alias )? )
			// JdbcGrammar.g:328:5: LPARAM selectstatement RPARAM ( alias )?
			{
			LPARAM124=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_subquery1581); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM124);

			pushFollow(FOLLOW_selectstatement_in_subquery1590);
			selectstatement125=selectstatement();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_selectstatement.add(selectstatement125.getTree());
			RPARAM126=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_subquery1596); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM126);

			// JdbcGrammar.g:331:5: ( alias )?
			int alt28=2;
			int LA28_0 = input.LA(1);
			if ( (LA28_0==ASKEYWORD||LA28_0==DOUBLEQUOTE||LA28_0==IDENTIFIER||LA28_0==SINGLEQUOTE) ) {
				alt28=1;
			}
			switch (alt28) {
				case 1 :
					// JdbcGrammar.g:331:6: alias
					{
					pushFollow(FOLLOW_alias_in_subquery1604);
					alias127=alias();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_alias.add(alias127.getTree());
					}
					break;

			}

			}

			// AST REWRITE
			// elements: alias, selectstatement
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 333:4: -> ^( SUBQUERY selectstatement ( alias )? ^( TEXT TEXT[$subquery.text] ) )
			{
				// JdbcGrammar.g:333:6: ^( SUBQUERY selectstatement ( alias )? ^( TEXT TEXT[$subquery.text] ) )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(SUBQUERY, "SUBQUERY"), root_1);
				adaptor.addChild(root_1, stream_selectstatement.nextTree());
				// JdbcGrammar.g:333:33: ( alias )?
				if ( stream_alias.hasNext() ) {
					adaptor.addChild(root_1, stream_alias.nextTree());
				}
				stream_alias.reset();

				// JdbcGrammar.g:333:40: ^( TEXT TEXT[$subquery.text] )
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(TEXT, "TEXT"), root_2);
				adaptor.addChild(root_2, (Object)adaptor.create(TEXT, input.toString(retval.start,input.LT(-1))));
				adaptor.addChild(root_1, root_2);
				}

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "subquery"


	public static class datasourceelement_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "datasourceelement"
	// JdbcGrammar.g:341:1: datasourceelement : ( subquery | sourcetable ) ;
	public final JdbcGrammarParser.datasourceelement_return datasourceelement() throws RecognitionException {
		JdbcGrammarParser.datasourceelement_return retval = new JdbcGrammarParser.datasourceelement_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope subquery128 =null;
		ParserRuleReturnScope sourcetable129 =null;


		try {
			// JdbcGrammar.g:342:3: ( ( subquery | sourcetable ) )
			// JdbcGrammar.g:343:3: ( subquery | sourcetable )
			{
			root_0 = (Object)adaptor.nil();


			// JdbcGrammar.g:343:3: ( subquery | sourcetable )
			int alt29=2;
			int LA29_0 = input.LA(1);
			if ( (LA29_0==LPARAM) ) {
				alt29=1;
			}
			else if ( (LA29_0==BACKQUOTE||LA29_0==DOUBLEQUOTE||LA29_0==ESCAPEDDOUBLEQUOTE||LA29_0==IDENTIFIER||LA29_0==SINGLEQUOTE||LA29_0==138) ) {
				alt29=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 29, 0, input);
				throw nvae;
			}

			switch (alt29) {
				case 1 :
					// JdbcGrammar.g:343:4: subquery
					{
					pushFollow(FOLLOW_subquery_in_datasourceelement1668);
					subquery128=subquery();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, subquery128.getTree());

					}
					break;
				case 2 :
					// JdbcGrammar.g:343:15: sourcetable
					{
					pushFollow(FOLLOW_sourcetable_in_datasourceelement1672);
					sourcetable129=sourcetable();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, sourcetable129.getTree());

					}
					break;

			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "datasourceelement"


	public static class datasource_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "datasource"
	// JdbcGrammar.g:346:1: datasource : ( datasourcenoparen | datasourceparen );
	public final JdbcGrammarParser.datasource_return datasource() throws RecognitionException {
		JdbcGrammarParser.datasource_return retval = new JdbcGrammarParser.datasource_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope datasourcenoparen130 =null;
		ParserRuleReturnScope datasourceparen131 =null;


		try {
			// JdbcGrammar.g:347:3: ( datasourcenoparen | datasourceparen )
			int alt30=2;
			int LA30_0 = input.LA(1);
			if ( (LA30_0==LPARAM) ) {
				int LA30_1 = input.LA(2);
				if ( (LA30_1==BACKQUOTE||LA30_1==DOUBLEQUOTE||LA30_1==ESCAPEDDOUBLEQUOTE||LA30_1==IDENTIFIER||LA30_1==LPARAM||LA30_1==SINGLEQUOTE||LA30_1==138) ) {
					alt30=2;
				}
				else if ( (LA30_1==SELECTKEYWORD) ) {
					alt30=1;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 30, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}
			else if ( (LA30_0==BACKQUOTE||LA30_0==DOUBLEQUOTE||LA30_0==ESCAPEDDOUBLEQUOTE||LA30_0==IDENTIFIER||LA30_0==SINGLEQUOTE||LA30_0==138) ) {
				alt30=1;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 30, 0, input);
				throw nvae;
			}

			switch (alt30) {
				case 1 :
					// JdbcGrammar.g:348:3: datasourcenoparen
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_datasourcenoparen_in_datasource1684);
					datasourcenoparen130=datasourcenoparen();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, datasourcenoparen130.getTree());

					}
					break;
				case 2 :
					// JdbcGrammar.g:351:3: datasourceparen
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_datasourceparen_in_datasource1695);
					datasourceparen131=datasourceparen();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, datasourceparen131.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "datasource"


	public static class datasourcenoparen_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "datasourcenoparen"
	// JdbcGrammar.g:357:1: datasourcenoparen : (s1= datasourceelement (joinclause1= joinclause joinelement1= datasourceelement onclause1= onclause ( multijoinexpression )* )? ) -> {joinexpr}? ^( JOINEXPRESSION ^( LEFTEXPR ( $s1)? ) ( $joinclause1)? ^( RIGHTEXPR ( $joinelement1)? ) ( $onclause1)? ( multijoinexpression )* ) -> $s1;
	public final JdbcGrammarParser.datasourcenoparen_return datasourcenoparen() throws RecognitionException {
		JdbcGrammarParser.datasourcenoparen_return retval = new JdbcGrammarParser.datasourcenoparen_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope s1 =null;
		ParserRuleReturnScope joinclause1 =null;
		ParserRuleReturnScope joinelement1 =null;
		ParserRuleReturnScope onclause1 =null;
		ParserRuleReturnScope multijoinexpression132 =null;

		RewriteRuleSubtreeStream stream_joinclause=new RewriteRuleSubtreeStream(adaptor,"rule joinclause");
		RewriteRuleSubtreeStream stream_onclause=new RewriteRuleSubtreeStream(adaptor,"rule onclause");
		RewriteRuleSubtreeStream stream_datasourceelement=new RewriteRuleSubtreeStream(adaptor,"rule datasourceelement");
		RewriteRuleSubtreeStream stream_multijoinexpression=new RewriteRuleSubtreeStream(adaptor,"rule multijoinexpression");

		boolean joinexpr = false;
		try {
			// JdbcGrammar.g:359:4: ( (s1= datasourceelement (joinclause1= joinclause joinelement1= datasourceelement onclause1= onclause ( multijoinexpression )* )? ) -> {joinexpr}? ^( JOINEXPRESSION ^( LEFTEXPR ( $s1)? ) ( $joinclause1)? ^( RIGHTEXPR ( $joinelement1)? ) ( $onclause1)? ( multijoinexpression )* ) -> $s1)
			// JdbcGrammar.g:360:4: (s1= datasourceelement (joinclause1= joinclause joinelement1= datasourceelement onclause1= onclause ( multijoinexpression )* )? )
			{
			// JdbcGrammar.g:360:4: (s1= datasourceelement (joinclause1= joinclause joinelement1= datasourceelement onclause1= onclause ( multijoinexpression )* )? )
			// JdbcGrammar.g:360:5: s1= datasourceelement (joinclause1= joinclause joinelement1= datasourceelement onclause1= onclause ( multijoinexpression )* )?
			{
			pushFollow(FOLLOW_datasourceelement_in_datasourcenoparen1719);
			s1=datasourceelement();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_datasourceelement.add(s1.getTree());
			// JdbcGrammar.g:360:26: (joinclause1= joinclause joinelement1= datasourceelement onclause1= onclause ( multijoinexpression )* )?
			int alt32=2;
			int LA32_0 = input.LA(1);
			if ( (LA32_0==FULl_KEYWORD||LA32_0==INNERKEYWORD||LA32_0==JOINKEYWORD||LA32_0==LEFT_KEYWORD||LA32_0==RIGHT_KEYWORD) ) {
				alt32=1;
			}
			switch (alt32) {
				case 1 :
					// JdbcGrammar.g:360:27: joinclause1= joinclause joinelement1= datasourceelement onclause1= onclause ( multijoinexpression )*
					{
					pushFollow(FOLLOW_joinclause_in_datasourcenoparen1724);
					joinclause1=joinclause();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_joinclause.add(joinclause1.getTree());
					pushFollow(FOLLOW_datasourceelement_in_datasourcenoparen1728);
					joinelement1=datasourceelement();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_datasourceelement.add(joinelement1.getTree());
					pushFollow(FOLLOW_onclause_in_datasourcenoparen1732);
					onclause1=onclause();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_onclause.add(onclause1.getTree());
					// JdbcGrammar.g:360:100: ( multijoinexpression )*
					loop31:
					while (true) {
						int alt31=2;
						int LA31_0 = input.LA(1);
						if ( (LA31_0==FULl_KEYWORD||LA31_0==INNERKEYWORD||LA31_0==JOINKEYWORD||LA31_0==LEFT_KEYWORD||LA31_0==RIGHT_KEYWORD) ) {
							alt31=1;
						}

						switch (alt31) {
						case 1 :
							// JdbcGrammar.g:360:101: multijoinexpression
							{
							pushFollow(FOLLOW_multijoinexpression_in_datasourcenoparen1735);
							multijoinexpression132=multijoinexpression();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression132.getTree());
							}
							break;

						default :
							break loop31;
						}
					}

					if ( state.backtracking==0 ) {joinexpr=true;}
					}
					break;

			}

			}

			// AST REWRITE
			// elements: joinclause1, joinelement1, s1, multijoinexpression, s1, onclause1
			// token labels: 
			// rule labels: retval, s1, onclause1, joinelement1, joinclause1
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
			RewriteRuleSubtreeStream stream_s1=new RewriteRuleSubtreeStream(adaptor,"rule s1",s1!=null?s1.getTree():null);
			RewriteRuleSubtreeStream stream_onclause1=new RewriteRuleSubtreeStream(adaptor,"rule onclause1",onclause1!=null?onclause1.getTree():null);
			RewriteRuleSubtreeStream stream_joinelement1=new RewriteRuleSubtreeStream(adaptor,"rule joinelement1",joinelement1!=null?joinelement1.getTree():null);
			RewriteRuleSubtreeStream stream_joinclause1=new RewriteRuleSubtreeStream(adaptor,"rule joinclause1",joinclause1!=null?joinclause1.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 361:4: -> {joinexpr}? ^( JOINEXPRESSION ^( LEFTEXPR ( $s1)? ) ( $joinclause1)? ^( RIGHTEXPR ( $joinelement1)? ) ( $onclause1)? ( multijoinexpression )* )
			if (joinexpr) {
				// JdbcGrammar.g:361:20: ^( JOINEXPRESSION ^( LEFTEXPR ( $s1)? ) ( $joinclause1)? ^( RIGHTEXPR ( $joinelement1)? ) ( $onclause1)? ( multijoinexpression )* )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(JOINEXPRESSION, "JOINEXPRESSION"), root_1);
				// JdbcGrammar.g:362:25: ^( LEFTEXPR ( $s1)? )
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(LEFTEXPR, "LEFTEXPR"), root_2);
				// JdbcGrammar.g:362:37: ( $s1)?
				if ( stream_s1.hasNext() ) {
					adaptor.addChild(root_2, stream_s1.nextTree());
				}
				stream_s1.reset();

				adaptor.addChild(root_1, root_2);
				}

				// JdbcGrammar.g:363:26: ( $joinclause1)?
				if ( stream_joinclause1.hasNext() ) {
					adaptor.addChild(root_1, stream_joinclause1.nextTree());
				}
				stream_joinclause1.reset();

				// JdbcGrammar.g:364:25: ^( RIGHTEXPR ( $joinelement1)? )
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(RIGHTEXPR, "RIGHTEXPR"), root_2);
				// JdbcGrammar.g:364:38: ( $joinelement1)?
				if ( stream_joinelement1.hasNext() ) {
					adaptor.addChild(root_2, stream_joinelement1.nextTree());
				}
				stream_joinelement1.reset();

				adaptor.addChild(root_1, root_2);
				}

				// JdbcGrammar.g:364:54: ( $onclause1)?
				if ( stream_onclause1.hasNext() ) {
					adaptor.addChild(root_1, stream_onclause1.nextTree());
				}
				stream_onclause1.reset();

				// JdbcGrammar.g:364:65: ( multijoinexpression )*
				while ( stream_multijoinexpression.hasNext() ) {
					adaptor.addChild(root_1, stream_multijoinexpression.nextTree());
				}
				stream_multijoinexpression.reset();

				adaptor.addChild(root_0, root_1);
				}

			}

			else // 366:4: -> $s1
			{
				adaptor.addChild(root_0, stream_s1.nextTree());
			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "datasourcenoparen"


	public static class datasourceparen_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "datasourceparen"
	// JdbcGrammar.g:370:1: datasourceparen : ( ( LPARAM simplejoin RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? ) -> ^( JOINEXPRESSION simplejoin ( multijoinexpression )* ) ;
	public final JdbcGrammarParser.datasourceparen_return datasourceparen() throws RecognitionException {
		JdbcGrammarParser.datasourceparen_return retval = new JdbcGrammarParser.datasourceparen_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token LPARAM133=null;
		Token RPARAM135=null;
		Token LPARAM137=null;
		Token LPARAM138=null;
		Token RPARAM140=null;
		Token RPARAM142=null;
		Token LPARAM144=null;
		Token LPARAM145=null;
		Token LPARAM146=null;
		Token RPARAM148=null;
		Token RPARAM150=null;
		Token RPARAM152=null;
		Token LPARAM154=null;
		Token LPARAM155=null;
		Token LPARAM156=null;
		Token LPARAM157=null;
		Token RPARAM159=null;
		Token RPARAM161=null;
		Token RPARAM163=null;
		Token RPARAM165=null;
		Token LPARAM167=null;
		Token LPARAM168=null;
		Token LPARAM169=null;
		Token LPARAM170=null;
		Token LPARAM171=null;
		Token RPARAM173=null;
		Token RPARAM175=null;
		Token RPARAM177=null;
		Token RPARAM179=null;
		Token RPARAM181=null;
		Token LPARAM183=null;
		Token LPARAM184=null;
		Token LPARAM185=null;
		Token LPARAM186=null;
		Token LPARAM187=null;
		Token LPARAM188=null;
		Token RPARAM190=null;
		Token RPARAM192=null;
		Token RPARAM194=null;
		Token RPARAM196=null;
		Token RPARAM198=null;
		Token RPARAM200=null;
		Token LPARAM202=null;
		Token LPARAM203=null;
		Token LPARAM204=null;
		Token LPARAM205=null;
		Token LPARAM206=null;
		Token LPARAM207=null;
		Token LPARAM208=null;
		Token RPARAM210=null;
		Token RPARAM212=null;
		Token RPARAM214=null;
		Token RPARAM216=null;
		Token RPARAM218=null;
		Token RPARAM220=null;
		Token RPARAM222=null;
		Token LPARAM224=null;
		Token LPARAM225=null;
		Token LPARAM226=null;
		Token LPARAM227=null;
		Token LPARAM228=null;
		Token LPARAM229=null;
		Token LPARAM230=null;
		Token LPARAM231=null;
		Token RPARAM233=null;
		Token RPARAM235=null;
		Token RPARAM237=null;
		Token RPARAM239=null;
		Token RPARAM241=null;
		Token RPARAM243=null;
		Token RPARAM245=null;
		Token RPARAM247=null;
		Token LPARAM249=null;
		Token LPARAM250=null;
		Token LPARAM251=null;
		Token LPARAM252=null;
		Token LPARAM253=null;
		Token LPARAM254=null;
		Token LPARAM255=null;
		Token LPARAM256=null;
		Token LPARAM257=null;
		Token RPARAM259=null;
		Token RPARAM261=null;
		Token RPARAM263=null;
		Token RPARAM265=null;
		Token RPARAM267=null;
		Token RPARAM269=null;
		Token RPARAM271=null;
		Token RPARAM273=null;
		Token RPARAM275=null;
		Token LPARAM277=null;
		Token LPARAM278=null;
		Token LPARAM279=null;
		Token LPARAM280=null;
		Token LPARAM281=null;
		Token LPARAM282=null;
		Token LPARAM283=null;
		Token LPARAM284=null;
		Token LPARAM285=null;
		Token LPARAM286=null;
		Token RPARAM288=null;
		Token RPARAM290=null;
		Token RPARAM292=null;
		Token RPARAM294=null;
		Token RPARAM296=null;
		Token RPARAM298=null;
		Token RPARAM300=null;
		Token RPARAM302=null;
		Token RPARAM304=null;
		Token RPARAM306=null;
		ParserRuleReturnScope simplejoin134 =null;
		ParserRuleReturnScope multijoinexpression136 =null;
		ParserRuleReturnScope simplejoin139 =null;
		ParserRuleReturnScope multijoinexpression141 =null;
		ParserRuleReturnScope multijoinexpression143 =null;
		ParserRuleReturnScope simplejoin147 =null;
		ParserRuleReturnScope multijoinexpression149 =null;
		ParserRuleReturnScope multijoinexpression151 =null;
		ParserRuleReturnScope multijoinexpression153 =null;
		ParserRuleReturnScope simplejoin158 =null;
		ParserRuleReturnScope multijoinexpression160 =null;
		ParserRuleReturnScope multijoinexpression162 =null;
		ParserRuleReturnScope multijoinexpression164 =null;
		ParserRuleReturnScope multijoinexpression166 =null;
		ParserRuleReturnScope simplejoin172 =null;
		ParserRuleReturnScope multijoinexpression174 =null;
		ParserRuleReturnScope multijoinexpression176 =null;
		ParserRuleReturnScope multijoinexpression178 =null;
		ParserRuleReturnScope multijoinexpression180 =null;
		ParserRuleReturnScope multijoinexpression182 =null;
		ParserRuleReturnScope simplejoin189 =null;
		ParserRuleReturnScope multijoinexpression191 =null;
		ParserRuleReturnScope multijoinexpression193 =null;
		ParserRuleReturnScope multijoinexpression195 =null;
		ParserRuleReturnScope multijoinexpression197 =null;
		ParserRuleReturnScope multijoinexpression199 =null;
		ParserRuleReturnScope multijoinexpression201 =null;
		ParserRuleReturnScope simplejoin209 =null;
		ParserRuleReturnScope multijoinexpression211 =null;
		ParserRuleReturnScope multijoinexpression213 =null;
		ParserRuleReturnScope multijoinexpression215 =null;
		ParserRuleReturnScope multijoinexpression217 =null;
		ParserRuleReturnScope multijoinexpression219 =null;
		ParserRuleReturnScope multijoinexpression221 =null;
		ParserRuleReturnScope multijoinexpression223 =null;
		ParserRuleReturnScope simplejoin232 =null;
		ParserRuleReturnScope multijoinexpression234 =null;
		ParserRuleReturnScope multijoinexpression236 =null;
		ParserRuleReturnScope multijoinexpression238 =null;
		ParserRuleReturnScope multijoinexpression240 =null;
		ParserRuleReturnScope multijoinexpression242 =null;
		ParserRuleReturnScope multijoinexpression244 =null;
		ParserRuleReturnScope multijoinexpression246 =null;
		ParserRuleReturnScope multijoinexpression248 =null;
		ParserRuleReturnScope simplejoin258 =null;
		ParserRuleReturnScope multijoinexpression260 =null;
		ParserRuleReturnScope multijoinexpression262 =null;
		ParserRuleReturnScope multijoinexpression264 =null;
		ParserRuleReturnScope multijoinexpression266 =null;
		ParserRuleReturnScope multijoinexpression268 =null;
		ParserRuleReturnScope multijoinexpression270 =null;
		ParserRuleReturnScope multijoinexpression272 =null;
		ParserRuleReturnScope multijoinexpression274 =null;
		ParserRuleReturnScope multijoinexpression276 =null;
		ParserRuleReturnScope simplejoin287 =null;
		ParserRuleReturnScope multijoinexpression289 =null;
		ParserRuleReturnScope multijoinexpression291 =null;
		ParserRuleReturnScope multijoinexpression293 =null;
		ParserRuleReturnScope multijoinexpression295 =null;
		ParserRuleReturnScope multijoinexpression297 =null;
		ParserRuleReturnScope multijoinexpression299 =null;
		ParserRuleReturnScope multijoinexpression301 =null;
		ParserRuleReturnScope multijoinexpression303 =null;
		ParserRuleReturnScope multijoinexpression305 =null;
		ParserRuleReturnScope multijoinexpression307 =null;

		Object LPARAM133_tree=null;
		Object RPARAM135_tree=null;
		Object LPARAM137_tree=null;
		Object LPARAM138_tree=null;
		Object RPARAM140_tree=null;
		Object RPARAM142_tree=null;
		Object LPARAM144_tree=null;
		Object LPARAM145_tree=null;
		Object LPARAM146_tree=null;
		Object RPARAM148_tree=null;
		Object RPARAM150_tree=null;
		Object RPARAM152_tree=null;
		Object LPARAM154_tree=null;
		Object LPARAM155_tree=null;
		Object LPARAM156_tree=null;
		Object LPARAM157_tree=null;
		Object RPARAM159_tree=null;
		Object RPARAM161_tree=null;
		Object RPARAM163_tree=null;
		Object RPARAM165_tree=null;
		Object LPARAM167_tree=null;
		Object LPARAM168_tree=null;
		Object LPARAM169_tree=null;
		Object LPARAM170_tree=null;
		Object LPARAM171_tree=null;
		Object RPARAM173_tree=null;
		Object RPARAM175_tree=null;
		Object RPARAM177_tree=null;
		Object RPARAM179_tree=null;
		Object RPARAM181_tree=null;
		Object LPARAM183_tree=null;
		Object LPARAM184_tree=null;
		Object LPARAM185_tree=null;
		Object LPARAM186_tree=null;
		Object LPARAM187_tree=null;
		Object LPARAM188_tree=null;
		Object RPARAM190_tree=null;
		Object RPARAM192_tree=null;
		Object RPARAM194_tree=null;
		Object RPARAM196_tree=null;
		Object RPARAM198_tree=null;
		Object RPARAM200_tree=null;
		Object LPARAM202_tree=null;
		Object LPARAM203_tree=null;
		Object LPARAM204_tree=null;
		Object LPARAM205_tree=null;
		Object LPARAM206_tree=null;
		Object LPARAM207_tree=null;
		Object LPARAM208_tree=null;
		Object RPARAM210_tree=null;
		Object RPARAM212_tree=null;
		Object RPARAM214_tree=null;
		Object RPARAM216_tree=null;
		Object RPARAM218_tree=null;
		Object RPARAM220_tree=null;
		Object RPARAM222_tree=null;
		Object LPARAM224_tree=null;
		Object LPARAM225_tree=null;
		Object LPARAM226_tree=null;
		Object LPARAM227_tree=null;
		Object LPARAM228_tree=null;
		Object LPARAM229_tree=null;
		Object LPARAM230_tree=null;
		Object LPARAM231_tree=null;
		Object RPARAM233_tree=null;
		Object RPARAM235_tree=null;
		Object RPARAM237_tree=null;
		Object RPARAM239_tree=null;
		Object RPARAM241_tree=null;
		Object RPARAM243_tree=null;
		Object RPARAM245_tree=null;
		Object RPARAM247_tree=null;
		Object LPARAM249_tree=null;
		Object LPARAM250_tree=null;
		Object LPARAM251_tree=null;
		Object LPARAM252_tree=null;
		Object LPARAM253_tree=null;
		Object LPARAM254_tree=null;
		Object LPARAM255_tree=null;
		Object LPARAM256_tree=null;
		Object LPARAM257_tree=null;
		Object RPARAM259_tree=null;
		Object RPARAM261_tree=null;
		Object RPARAM263_tree=null;
		Object RPARAM265_tree=null;
		Object RPARAM267_tree=null;
		Object RPARAM269_tree=null;
		Object RPARAM271_tree=null;
		Object RPARAM273_tree=null;
		Object RPARAM275_tree=null;
		Object LPARAM277_tree=null;
		Object LPARAM278_tree=null;
		Object LPARAM279_tree=null;
		Object LPARAM280_tree=null;
		Object LPARAM281_tree=null;
		Object LPARAM282_tree=null;
		Object LPARAM283_tree=null;
		Object LPARAM284_tree=null;
		Object LPARAM285_tree=null;
		Object LPARAM286_tree=null;
		Object RPARAM288_tree=null;
		Object RPARAM290_tree=null;
		Object RPARAM292_tree=null;
		Object RPARAM294_tree=null;
		Object RPARAM296_tree=null;
		Object RPARAM298_tree=null;
		Object RPARAM300_tree=null;
		Object RPARAM302_tree=null;
		Object RPARAM304_tree=null;
		Object RPARAM306_tree=null;
		RewriteRuleTokenStream stream_LPARAM=new RewriteRuleTokenStream(adaptor,"token LPARAM");
		RewriteRuleTokenStream stream_RPARAM=new RewriteRuleTokenStream(adaptor,"token RPARAM");
		RewriteRuleSubtreeStream stream_simplejoin=new RewriteRuleSubtreeStream(adaptor,"rule simplejoin");
		RewriteRuleSubtreeStream stream_multijoinexpression=new RewriteRuleSubtreeStream(adaptor,"rule multijoinexpression");

		try {
			// JdbcGrammar.g:371:17: ( ( ( LPARAM simplejoin RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? ) -> ^( JOINEXPRESSION simplejoin ( multijoinexpression )* ) )
			// JdbcGrammar.g:372:17: ( ( LPARAM simplejoin RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? )
			{
			// JdbcGrammar.g:372:17: ( ( LPARAM simplejoin RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? | ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )? )
			int alt43=10;
			int LA43_0 = input.LA(1);
			if ( (LA43_0==LPARAM) ) {
				int LA43_1 = input.LA(2);
				if ( (LA43_1==LPARAM) ) {
					switch ( input.LA(3) ) {
					case LPARAM:
						{
						switch ( input.LA(4) ) {
						case LPARAM:
							{
							switch ( input.LA(5) ) {
							case LPARAM:
								{
								switch ( input.LA(6) ) {
								case LPARAM:
									{
									switch ( input.LA(7) ) {
									case LPARAM:
										{
										switch ( input.LA(8) ) {
										case LPARAM:
											{
											switch ( input.LA(9) ) {
											case LPARAM:
												{
												switch ( input.LA(10) ) {
												case LPARAM:
													{
													int LA43_18 = input.LA(11);
													if ( (LA43_18==BACKQUOTE||LA43_18==DOUBLEQUOTE||LA43_18==ESCAPEDDOUBLEQUOTE||LA43_18==IDENTIFIER||LA43_18==LPARAM||LA43_18==SINGLEQUOTE||LA43_18==138) ) {
														alt43=10;
													}
													else if ( (LA43_18==SELECTKEYWORD) ) {
														alt43=9;
													}

													else {
														if (state.backtracking>0) {state.failed=true; return retval;}
														int nvaeMark = input.mark();
														try {
															for (int nvaeConsume = 0; nvaeConsume < 11 - 1; nvaeConsume++) {
																input.consume();
															}
															NoViableAltException nvae =
																new NoViableAltException("", 43, 18, input);
															throw nvae;
														} finally {
															input.rewind(nvaeMark);
														}
													}

													}
													break;
												case BACKQUOTE:
												case DOUBLEQUOTE:
												case ESCAPEDDOUBLEQUOTE:
												case IDENTIFIER:
												case SINGLEQUOTE:
												case 138:
													{
													alt43=9;
													}
													break;
												case SELECTKEYWORD:
													{
													alt43=8;
													}
													break;
												default:
													if (state.backtracking>0) {state.failed=true; return retval;}
													int nvaeMark = input.mark();
													try {
														for (int nvaeConsume = 0; nvaeConsume < 10 - 1; nvaeConsume++) {
															input.consume();
														}
														NoViableAltException nvae =
															new NoViableAltException("", 43, 16, input);
														throw nvae;
													} finally {
														input.rewind(nvaeMark);
													}
												}
												}
												break;
											case BACKQUOTE:
											case DOUBLEQUOTE:
											case ESCAPEDDOUBLEQUOTE:
											case IDENTIFIER:
											case SINGLEQUOTE:
											case 138:
												{
												alt43=8;
												}
												break;
											case SELECTKEYWORD:
												{
												alt43=7;
												}
												break;
											default:
												if (state.backtracking>0) {state.failed=true; return retval;}
												int nvaeMark = input.mark();
												try {
													for (int nvaeConsume = 0; nvaeConsume < 9 - 1; nvaeConsume++) {
														input.consume();
													}
													NoViableAltException nvae =
														new NoViableAltException("", 43, 14, input);
													throw nvae;
												} finally {
													input.rewind(nvaeMark);
												}
											}
											}
											break;
										case BACKQUOTE:
										case DOUBLEQUOTE:
										case ESCAPEDDOUBLEQUOTE:
										case IDENTIFIER:
										case SINGLEQUOTE:
										case 138:
											{
											alt43=7;
											}
											break;
										case SELECTKEYWORD:
											{
											alt43=6;
											}
											break;
										default:
											if (state.backtracking>0) {state.failed=true; return retval;}
											int nvaeMark = input.mark();
											try {
												for (int nvaeConsume = 0; nvaeConsume < 8 - 1; nvaeConsume++) {
													input.consume();
												}
												NoViableAltException nvae =
													new NoViableAltException("", 43, 12, input);
												throw nvae;
											} finally {
												input.rewind(nvaeMark);
											}
										}
										}
										break;
									case BACKQUOTE:
									case DOUBLEQUOTE:
									case ESCAPEDDOUBLEQUOTE:
									case IDENTIFIER:
									case SINGLEQUOTE:
									case 138:
										{
										alt43=6;
										}
										break;
									case SELECTKEYWORD:
										{
										alt43=5;
										}
										break;
									default:
										if (state.backtracking>0) {state.failed=true; return retval;}
										int nvaeMark = input.mark();
										try {
											for (int nvaeConsume = 0; nvaeConsume < 7 - 1; nvaeConsume++) {
												input.consume();
											}
											NoViableAltException nvae =
												new NoViableAltException("", 43, 10, input);
											throw nvae;
										} finally {
											input.rewind(nvaeMark);
										}
									}
									}
									break;
								case BACKQUOTE:
								case DOUBLEQUOTE:
								case ESCAPEDDOUBLEQUOTE:
								case IDENTIFIER:
								case SINGLEQUOTE:
								case 138:
									{
									alt43=5;
									}
									break;
								case SELECTKEYWORD:
									{
									alt43=4;
									}
									break;
								default:
									if (state.backtracking>0) {state.failed=true; return retval;}
									int nvaeMark = input.mark();
									try {
										for (int nvaeConsume = 0; nvaeConsume < 6 - 1; nvaeConsume++) {
											input.consume();
										}
										NoViableAltException nvae =
											new NoViableAltException("", 43, 8, input);
										throw nvae;
									} finally {
										input.rewind(nvaeMark);
									}
								}
								}
								break;
							case BACKQUOTE:
							case DOUBLEQUOTE:
							case ESCAPEDDOUBLEQUOTE:
							case IDENTIFIER:
							case SINGLEQUOTE:
							case 138:
								{
								alt43=4;
								}
								break;
							case SELECTKEYWORD:
								{
								alt43=3;
								}
								break;
							default:
								if (state.backtracking>0) {state.failed=true; return retval;}
								int nvaeMark = input.mark();
								try {
									for (int nvaeConsume = 0; nvaeConsume < 5 - 1; nvaeConsume++) {
										input.consume();
									}
									NoViableAltException nvae =
										new NoViableAltException("", 43, 6, input);
									throw nvae;
								} finally {
									input.rewind(nvaeMark);
								}
							}
							}
							break;
						case BACKQUOTE:
						case DOUBLEQUOTE:
						case ESCAPEDDOUBLEQUOTE:
						case IDENTIFIER:
						case SINGLEQUOTE:
						case 138:
							{
							alt43=3;
							}
							break;
						case SELECTKEYWORD:
							{
							alt43=2;
							}
							break;
						default:
							if (state.backtracking>0) {state.failed=true; return retval;}
							int nvaeMark = input.mark();
							try {
								for (int nvaeConsume = 0; nvaeConsume < 4 - 1; nvaeConsume++) {
									input.consume();
								}
								NoViableAltException nvae =
									new NoViableAltException("", 43, 4, input);
								throw nvae;
							} finally {
								input.rewind(nvaeMark);
							}
						}
						}
						break;
					case BACKQUOTE:
					case DOUBLEQUOTE:
					case ESCAPEDDOUBLEQUOTE:
					case IDENTIFIER:
					case SINGLEQUOTE:
					case 138:
						{
						alt43=2;
						}
						break;
					case SELECTKEYWORD:
						{
						alt43=1;
						}
						break;
					default:
						if (state.backtracking>0) {state.failed=true; return retval;}
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 43, 2, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}
				}
				else if ( (LA43_1==BACKQUOTE||LA43_1==DOUBLEQUOTE||LA43_1==ESCAPEDDOUBLEQUOTE||LA43_1==IDENTIFIER||LA43_1==SINGLEQUOTE||LA43_1==138) ) {
					alt43=1;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 43, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 43, 0, input);
				throw nvae;
			}

			switch (alt43) {
				case 1 :
					// JdbcGrammar.g:374:17: ( LPARAM simplejoin RPARAM ) ( multijoinexpression )?
					{
					// JdbcGrammar.g:374:17: ( LPARAM simplejoin RPARAM )
					// JdbcGrammar.g:374:18: LPARAM simplejoin RPARAM
					{
					LPARAM133=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen1956); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM133);

					pushFollow(FOLLOW_simplejoin_in_datasourceparen1958);
					simplejoin134=simplejoin();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_simplejoin.add(simplejoin134.getTree());
					RPARAM135=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen1960); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM135);

					}

					// JdbcGrammar.g:374:44: ( multijoinexpression )?
					int alt33=2;
					int LA33_0 = input.LA(1);
					if ( (LA33_0==FULl_KEYWORD||LA33_0==INNERKEYWORD||LA33_0==JOINKEYWORD||LA33_0==LEFT_KEYWORD||LA33_0==RIGHT_KEYWORD) ) {
						alt33=1;
					}
					switch (alt33) {
						case 1 :
							// JdbcGrammar.g:374:44: multijoinexpression
							{
							pushFollow(FOLLOW_multijoinexpression_in_datasourceparen1963);
							multijoinexpression136=multijoinexpression();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression136.getTree());
							}
							break;

					}

					}
					break;
				case 2 :
					// JdbcGrammar.g:376:9: ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )?
					{
					// JdbcGrammar.g:376:9: ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:376:10: LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM
					{
					LPARAM137=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen1978); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM137);

					// JdbcGrammar.g:376:17: ( LPARAM simplejoin RPARAM )
					// JdbcGrammar.g:376:18: LPARAM simplejoin RPARAM
					{
					LPARAM138=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen1981); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM138);

					pushFollow(FOLLOW_simplejoin_in_datasourceparen1983);
					simplejoin139=simplejoin();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_simplejoin.add(simplejoin139.getTree());
					RPARAM140=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen1985); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM140);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen1988);
					multijoinexpression141=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression141.getTree());
					RPARAM142=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen1990); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM142);

					}

					// JdbcGrammar.g:376:73: ( multijoinexpression )?
					int alt34=2;
					int LA34_0 = input.LA(1);
					if ( (LA34_0==FULl_KEYWORD||LA34_0==INNERKEYWORD||LA34_0==JOINKEYWORD||LA34_0==LEFT_KEYWORD||LA34_0==RIGHT_KEYWORD) ) {
						alt34=1;
					}
					switch (alt34) {
						case 1 :
							// JdbcGrammar.g:376:73: multijoinexpression
							{
							pushFollow(FOLLOW_multijoinexpression_in_datasourceparen1994);
							multijoinexpression143=multijoinexpression();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression143.getTree());
							}
							break;

					}

					}
					break;
				case 3 :
					// JdbcGrammar.g:378:2: ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )?
					{
					// JdbcGrammar.g:378:2: ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:378:2: LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM144=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2002); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM144);

					// JdbcGrammar.g:378:9: ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:378:10: LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM
					{
					LPARAM145=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2005); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM145);

					// JdbcGrammar.g:378:17: ( LPARAM simplejoin RPARAM )
					// JdbcGrammar.g:378:18: LPARAM simplejoin RPARAM
					{
					LPARAM146=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2008); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM146);

					pushFollow(FOLLOW_simplejoin_in_datasourceparen2010);
					simplejoin147=simplejoin();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_simplejoin.add(simplejoin147.getTree());
					RPARAM148=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2012); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM148);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2015);
					multijoinexpression149=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression149.getTree());
					RPARAM150=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2017); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM150);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2021);
					multijoinexpression151=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression151.getTree());
					RPARAM152=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2023); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM152);

					}

					// JdbcGrammar.g:379:1: ( multijoinexpression )?
					int alt35=2;
					int LA35_0 = input.LA(1);
					if ( (LA35_0==FULl_KEYWORD||LA35_0==INNERKEYWORD||LA35_0==JOINKEYWORD||LA35_0==LEFT_KEYWORD||LA35_0==RIGHT_KEYWORD) ) {
						alt35=1;
					}
					switch (alt35) {
						case 1 :
							// JdbcGrammar.g:379:1: multijoinexpression
							{
							pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2027);
							multijoinexpression153=multijoinexpression();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression153.getTree());
							}
							break;

					}

					}
					break;
				case 4 :
					// JdbcGrammar.g:381:2: ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )?
					{
					// JdbcGrammar.g:381:2: ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:381:2: LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM154=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2034); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM154);

					// JdbcGrammar.g:382:2: ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:382:2: LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM155=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2038); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM155);

					// JdbcGrammar.g:382:9: ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:382:10: LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM
					{
					LPARAM156=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2041); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM156);

					// JdbcGrammar.g:382:17: ( LPARAM simplejoin RPARAM )
					// JdbcGrammar.g:382:18: LPARAM simplejoin RPARAM
					{
					LPARAM157=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2044); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM157);

					pushFollow(FOLLOW_simplejoin_in_datasourceparen2046);
					simplejoin158=simplejoin();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_simplejoin.add(simplejoin158.getTree());
					RPARAM159=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2048); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM159);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2051);
					multijoinexpression160=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression160.getTree());
					RPARAM161=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2053); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM161);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2057);
					multijoinexpression162=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression162.getTree());
					RPARAM163=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2059); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM163);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2063);
					multijoinexpression164=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression164.getTree());
					RPARAM165=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2065); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM165);

					}

					// JdbcGrammar.g:383:29: ( multijoinexpression )?
					int alt36=2;
					int LA36_0 = input.LA(1);
					if ( (LA36_0==FULl_KEYWORD||LA36_0==INNERKEYWORD||LA36_0==JOINKEYWORD||LA36_0==LEFT_KEYWORD||LA36_0==RIGHT_KEYWORD) ) {
						alt36=1;
					}
					switch (alt36) {
						case 1 :
							// JdbcGrammar.g:383:29: multijoinexpression
							{
							pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2068);
							multijoinexpression166=multijoinexpression();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression166.getTree());
							}
							break;

					}

					}
					break;
				case 5 :
					// JdbcGrammar.g:385:2: ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )?
					{
					// JdbcGrammar.g:385:2: ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:385:2: LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM167=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2075); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM167);

					// JdbcGrammar.g:385:9: ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:385:10: LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM168=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2078); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM168);

					// JdbcGrammar.g:386:2: ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:386:2: LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM169=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2082); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM169);

					// JdbcGrammar.g:386:9: ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:386:10: LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM
					{
					LPARAM170=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2085); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM170);

					// JdbcGrammar.g:386:17: ( LPARAM simplejoin RPARAM )
					// JdbcGrammar.g:386:18: LPARAM simplejoin RPARAM
					{
					LPARAM171=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2088); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM171);

					pushFollow(FOLLOW_simplejoin_in_datasourceparen2090);
					simplejoin172=simplejoin();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_simplejoin.add(simplejoin172.getTree());
					RPARAM173=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2092); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM173);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2095);
					multijoinexpression174=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression174.getTree());
					RPARAM175=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2097); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM175);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2101);
					multijoinexpression176=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression176.getTree());
					RPARAM177=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2103); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM177);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2107);
					multijoinexpression178=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression178.getTree());
					RPARAM179=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2109); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM179);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2112);
					multijoinexpression180=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression180.getTree());
					RPARAM181=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2114); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM181);

					}

					// JdbcGrammar.g:387:57: ( multijoinexpression )?
					int alt37=2;
					int LA37_0 = input.LA(1);
					if ( (LA37_0==FULl_KEYWORD||LA37_0==INNERKEYWORD||LA37_0==JOINKEYWORD||LA37_0==LEFT_KEYWORD||LA37_0==RIGHT_KEYWORD) ) {
						alt37=1;
					}
					switch (alt37) {
						case 1 :
							// JdbcGrammar.g:387:57: multijoinexpression
							{
							pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2117);
							multijoinexpression182=multijoinexpression();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression182.getTree());
							}
							break;

					}

					}
					break;
				case 6 :
					// JdbcGrammar.g:389:2: ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )?
					{
					// JdbcGrammar.g:389:2: ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:389:2: LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM183=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2124); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM183);

					// JdbcGrammar.g:389:9: ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:389:10: LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM184=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2127); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM184);

					// JdbcGrammar.g:389:17: ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:389:18: LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM185=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2130); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM185);

					// JdbcGrammar.g:390:2: ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:390:2: LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM186=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2134); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM186);

					// JdbcGrammar.g:390:9: ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:390:10: LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM
					{
					LPARAM187=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2137); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM187);

					// JdbcGrammar.g:390:17: ( LPARAM simplejoin RPARAM )
					// JdbcGrammar.g:390:18: LPARAM simplejoin RPARAM
					{
					LPARAM188=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2140); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM188);

					pushFollow(FOLLOW_simplejoin_in_datasourceparen2142);
					simplejoin189=simplejoin();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_simplejoin.add(simplejoin189.getTree());
					RPARAM190=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2144); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM190);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2147);
					multijoinexpression191=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression191.getTree());
					RPARAM192=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2149); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM192);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2153);
					multijoinexpression193=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression193.getTree());
					RPARAM194=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2155); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM194);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2159);
					multijoinexpression195=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression195.getTree());
					RPARAM196=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2161); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM196);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2164);
					multijoinexpression197=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression197.getTree());
					RPARAM198=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2166); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM198);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2169);
					multijoinexpression199=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression199.getTree());
					RPARAM200=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2171); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM200);

					}

					// JdbcGrammar.g:391:85: ( multijoinexpression )?
					int alt38=2;
					int LA38_0 = input.LA(1);
					if ( (LA38_0==FULl_KEYWORD||LA38_0==INNERKEYWORD||LA38_0==JOINKEYWORD||LA38_0==LEFT_KEYWORD||LA38_0==RIGHT_KEYWORD) ) {
						alt38=1;
					}
					switch (alt38) {
						case 1 :
							// JdbcGrammar.g:391:85: multijoinexpression
							{
							pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2174);
							multijoinexpression201=multijoinexpression();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression201.getTree());
							}
							break;

					}

					}
					break;
				case 7 :
					// JdbcGrammar.g:393:2: ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )?
					{
					// JdbcGrammar.g:393:2: ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:393:2: LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM202=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2181); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM202);

					// JdbcGrammar.g:393:9: ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:393:10: LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM203=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2184); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM203);

					// JdbcGrammar.g:393:17: ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:393:18: LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM204=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2187); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM204);

					// JdbcGrammar.g:393:25: ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:393:26: LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM205=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2190); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM205);

					// JdbcGrammar.g:394:2: ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:394:2: LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM206=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2194); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM206);

					// JdbcGrammar.g:394:9: ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:394:10: LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM
					{
					LPARAM207=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2197); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM207);

					// JdbcGrammar.g:394:17: ( LPARAM simplejoin RPARAM )
					// JdbcGrammar.g:394:18: LPARAM simplejoin RPARAM
					{
					LPARAM208=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2200); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM208);

					pushFollow(FOLLOW_simplejoin_in_datasourceparen2202);
					simplejoin209=simplejoin();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_simplejoin.add(simplejoin209.getTree());
					RPARAM210=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2204); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM210);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2207);
					multijoinexpression211=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression211.getTree());
					RPARAM212=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2209); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM212);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2213);
					multijoinexpression213=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression213.getTree());
					RPARAM214=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2215); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM214);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2219);
					multijoinexpression215=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression215.getTree());
					RPARAM216=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2221); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM216);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2224);
					multijoinexpression217=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression217.getTree());
					RPARAM218=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2226); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM218);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2229);
					multijoinexpression219=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression219.getTree());
					RPARAM220=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2231); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM220);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2234);
					multijoinexpression221=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression221.getTree());
					RPARAM222=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2236); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM222);

					}

					// JdbcGrammar.g:396:1: ( multijoinexpression )?
					int alt39=2;
					int LA39_0 = input.LA(1);
					if ( (LA39_0==FULl_KEYWORD||LA39_0==INNERKEYWORD||LA39_0==JOINKEYWORD||LA39_0==LEFT_KEYWORD||LA39_0==RIGHT_KEYWORD) ) {
						alt39=1;
					}
					switch (alt39) {
						case 1 :
							// JdbcGrammar.g:396:1: multijoinexpression
							{
							pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2240);
							multijoinexpression223=multijoinexpression();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression223.getTree());
							}
							break;

					}

					}
					break;
				case 8 :
					// JdbcGrammar.g:398:2: ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )?
					{
					// JdbcGrammar.g:398:2: ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:398:2: LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM224=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2247); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM224);

					// JdbcGrammar.g:398:9: ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:398:10: LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM225=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2250); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM225);

					// JdbcGrammar.g:398:17: ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:398:18: LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM226=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2253); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM226);

					// JdbcGrammar.g:398:25: ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:398:26: LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM227=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2256); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM227);

					// JdbcGrammar.g:398:33: ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:398:34: LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM228=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2259); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM228);

					// JdbcGrammar.g:399:2: ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:399:2: LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM229=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2263); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM229);

					// JdbcGrammar.g:399:9: ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:399:10: LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM
					{
					LPARAM230=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2266); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM230);

					// JdbcGrammar.g:399:17: ( LPARAM simplejoin RPARAM )
					// JdbcGrammar.g:399:18: LPARAM simplejoin RPARAM
					{
					LPARAM231=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2269); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM231);

					pushFollow(FOLLOW_simplejoin_in_datasourceparen2271);
					simplejoin232=simplejoin();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_simplejoin.add(simplejoin232.getTree());
					RPARAM233=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2273); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM233);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2276);
					multijoinexpression234=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression234.getTree());
					RPARAM235=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2278); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM235);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2282);
					multijoinexpression236=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression236.getTree());
					RPARAM237=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2284); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM237);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2288);
					multijoinexpression238=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression238.getTree());
					RPARAM239=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2290); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM239);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2293);
					multijoinexpression240=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression240.getTree());
					RPARAM241=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2295); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM241);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2298);
					multijoinexpression242=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression242.getTree());
					RPARAM243=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2300); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM243);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2303);
					multijoinexpression244=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression244.getTree());
					RPARAM245=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2305); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM245);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2309);
					multijoinexpression246=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression246.getTree());
					RPARAM247=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2311); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM247);

					}

					// JdbcGrammar.g:401:29: ( multijoinexpression )?
					int alt40=2;
					int LA40_0 = input.LA(1);
					if ( (LA40_0==FULl_KEYWORD||LA40_0==INNERKEYWORD||LA40_0==JOINKEYWORD||LA40_0==LEFT_KEYWORD||LA40_0==RIGHT_KEYWORD) ) {
						alt40=1;
					}
					switch (alt40) {
						case 1 :
							// JdbcGrammar.g:401:29: multijoinexpression
							{
							pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2314);
							multijoinexpression248=multijoinexpression();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression248.getTree());
							}
							break;

					}

					}
					break;
				case 9 :
					// JdbcGrammar.g:403:2: ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )?
					{
					// JdbcGrammar.g:403:2: ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:403:2: LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM249=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2321); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM249);

					// JdbcGrammar.g:403:9: ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:403:10: LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM250=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2324); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM250);

					// JdbcGrammar.g:403:17: ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:403:18: LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM251=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2327); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM251);

					// JdbcGrammar.g:403:25: ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:403:26: LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM252=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2330); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM252);

					// JdbcGrammar.g:403:33: ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:403:34: LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM253=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2333); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM253);

					// JdbcGrammar.g:403:41: ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:403:42: LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM254=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2336); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM254);

					// JdbcGrammar.g:404:2: ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:404:2: LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM255=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2340); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM255);

					// JdbcGrammar.g:404:9: ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:404:10: LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM
					{
					LPARAM256=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2343); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM256);

					// JdbcGrammar.g:404:17: ( LPARAM simplejoin RPARAM )
					// JdbcGrammar.g:404:18: LPARAM simplejoin RPARAM
					{
					LPARAM257=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2346); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM257);

					pushFollow(FOLLOW_simplejoin_in_datasourceparen2348);
					simplejoin258=simplejoin();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_simplejoin.add(simplejoin258.getTree());
					RPARAM259=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2350); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM259);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2353);
					multijoinexpression260=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression260.getTree());
					RPARAM261=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2355); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM261);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2359);
					multijoinexpression262=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression262.getTree());
					RPARAM263=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2361); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM263);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2365);
					multijoinexpression264=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression264.getTree());
					RPARAM265=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2367); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM265);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2370);
					multijoinexpression266=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression266.getTree());
					RPARAM267=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2372); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM267);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2375);
					multijoinexpression268=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression268.getTree());
					RPARAM269=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2377); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM269);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2380);
					multijoinexpression270=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression270.getTree());
					RPARAM271=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2382); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM271);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2386);
					multijoinexpression272=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression272.getTree());
					RPARAM273=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2388); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM273);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2391);
					multijoinexpression274=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression274.getTree());
					RPARAM275=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2393); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM275);

					}

					// JdbcGrammar.g:406:57: ( multijoinexpression )?
					int alt41=2;
					int LA41_0 = input.LA(1);
					if ( (LA41_0==FULl_KEYWORD||LA41_0==INNERKEYWORD||LA41_0==JOINKEYWORD||LA41_0==LEFT_KEYWORD||LA41_0==RIGHT_KEYWORD) ) {
						alt41=1;
					}
					switch (alt41) {
						case 1 :
							// JdbcGrammar.g:406:57: multijoinexpression
							{
							pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2396);
							multijoinexpression276=multijoinexpression();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression276.getTree());
							}
							break;

					}

					}
					break;
				case 10 :
					// JdbcGrammar.g:408:2: ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) ( multijoinexpression )?
					{
					// JdbcGrammar.g:408:2: ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:408:2: LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM277=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2403); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM277);

					// JdbcGrammar.g:408:9: ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:408:10: LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM278=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2406); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM278);

					// JdbcGrammar.g:408:17: ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:408:18: LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM279=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2409); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM279);

					// JdbcGrammar.g:408:25: ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:408:26: LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM280=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2412); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM280);

					// JdbcGrammar.g:408:33: ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:408:34: LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM281=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2415); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM281);

					// JdbcGrammar.g:408:41: ( LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:408:42: LPARAM ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM282=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2418); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM282);

					// JdbcGrammar.g:408:49: ( LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:408:50: LPARAM ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM283=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2421); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM283);

					// JdbcGrammar.g:409:2: ( LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:409:2: LPARAM ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM ) multijoinexpression RPARAM
					{
					LPARAM284=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2425); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM284);

					// JdbcGrammar.g:409:9: ( LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM )
					// JdbcGrammar.g:409:10: LPARAM ( LPARAM simplejoin RPARAM ) multijoinexpression RPARAM
					{
					LPARAM285=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2428); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM285);

					// JdbcGrammar.g:409:17: ( LPARAM simplejoin RPARAM )
					// JdbcGrammar.g:409:18: LPARAM simplejoin RPARAM
					{
					LPARAM286=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourceparen2431); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM286);

					pushFollow(FOLLOW_simplejoin_in_datasourceparen2433);
					simplejoin287=simplejoin();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_simplejoin.add(simplejoin287.getTree());
					RPARAM288=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2435); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM288);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2438);
					multijoinexpression289=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression289.getTree());
					RPARAM290=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2440); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM290);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2444);
					multijoinexpression291=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression291.getTree());
					RPARAM292=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2446); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM292);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2450);
					multijoinexpression293=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression293.getTree());
					RPARAM294=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2452); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM294);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2455);
					multijoinexpression295=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression295.getTree());
					RPARAM296=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2457); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM296);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2460);
					multijoinexpression297=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression297.getTree());
					RPARAM298=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2462); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM298);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2465);
					multijoinexpression299=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression299.getTree());
					RPARAM300=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2467); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM300);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2471);
					multijoinexpression301=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression301.getTree());
					RPARAM302=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2473); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM302);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2476);
					multijoinexpression303=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression303.getTree());
					RPARAM304=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2478); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM304);

					}

					pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2481);
					multijoinexpression305=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression305.getTree());
					RPARAM306=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourceparen2483); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM306);

					}

					// JdbcGrammar.g:411:85: ( multijoinexpression )?
					int alt42=2;
					int LA42_0 = input.LA(1);
					if ( (LA42_0==FULl_KEYWORD||LA42_0==INNERKEYWORD||LA42_0==JOINKEYWORD||LA42_0==LEFT_KEYWORD||LA42_0==RIGHT_KEYWORD) ) {
						alt42=1;
					}
					switch (alt42) {
						case 1 :
							// JdbcGrammar.g:411:85: multijoinexpression
							{
							pushFollow(FOLLOW_multijoinexpression_in_datasourceparen2486);
							multijoinexpression307=multijoinexpression();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_multijoinexpression.add(multijoinexpression307.getTree());
							}
							break;

					}

					}
					break;

			}

			// AST REWRITE
			// elements: simplejoin, multijoinexpression
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 414:5: -> ^( JOINEXPRESSION simplejoin ( multijoinexpression )* )
			{
				// JdbcGrammar.g:414:7: ^( JOINEXPRESSION simplejoin ( multijoinexpression )* )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(JOINEXPRESSION, "JOINEXPRESSION"), root_1);
				adaptor.addChild(root_1, stream_simplejoin.nextTree());
				// JdbcGrammar.g:414:35: ( multijoinexpression )*
				while ( stream_multijoinexpression.hasNext() ) {
					adaptor.addChild(root_1, stream_multijoinexpression.nextTree());
				}
				stream_multijoinexpression.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "datasourceparen"


	public static class simplejoin_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "simplejoin"
	// JdbcGrammar.g:417:1: simplejoin : (s1= datasourceelement joinclause1= joinclause joinelement1= datasourceelement onclause1= onclause ) -> ^( LEFTEXPR ( $s1)? ) ( $joinclause1)? ^( RIGHTEXPR ( $joinelement1)? ) ( $onclause1)? ;
	public final JdbcGrammarParser.simplejoin_return simplejoin() throws RecognitionException {
		JdbcGrammarParser.simplejoin_return retval = new JdbcGrammarParser.simplejoin_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope s1 =null;
		ParserRuleReturnScope joinclause1 =null;
		ParserRuleReturnScope joinelement1 =null;
		ParserRuleReturnScope onclause1 =null;

		RewriteRuleSubtreeStream stream_joinclause=new RewriteRuleSubtreeStream(adaptor,"rule joinclause");
		RewriteRuleSubtreeStream stream_onclause=new RewriteRuleSubtreeStream(adaptor,"rule onclause");
		RewriteRuleSubtreeStream stream_datasourceelement=new RewriteRuleSubtreeStream(adaptor,"rule datasourceelement");

		try {
			// JdbcGrammar.g:418:3: ( (s1= datasourceelement joinclause1= joinclause joinelement1= datasourceelement onclause1= onclause ) -> ^( LEFTEXPR ( $s1)? ) ( $joinclause1)? ^( RIGHTEXPR ( $joinelement1)? ) ( $onclause1)? )
			// JdbcGrammar.g:419:3: (s1= datasourceelement joinclause1= joinclause joinelement1= datasourceelement onclause1= onclause )
			{
			// JdbcGrammar.g:419:3: (s1= datasourceelement joinclause1= joinclause joinelement1= datasourceelement onclause1= onclause )
			// JdbcGrammar.g:419:4: s1= datasourceelement joinclause1= joinclause joinelement1= datasourceelement onclause1= onclause
			{
			pushFollow(FOLLOW_datasourceelement_in_simplejoin2540);
			s1=datasourceelement();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_datasourceelement.add(s1.getTree());
			pushFollow(FOLLOW_joinclause_in_simplejoin2544);
			joinclause1=joinclause();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_joinclause.add(joinclause1.getTree());
			pushFollow(FOLLOW_datasourceelement_in_simplejoin2548);
			joinelement1=datasourceelement();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_datasourceelement.add(joinelement1.getTree());
			pushFollow(FOLLOW_onclause_in_simplejoin2552);
			onclause1=onclause();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_onclause.add(onclause1.getTree());
			}

			// AST REWRITE
			// elements: s1, onclause1, joinelement1, joinclause1
			// token labels: 
			// rule labels: retval, s1, onclause1, joinelement1, joinclause1
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
			RewriteRuleSubtreeStream stream_s1=new RewriteRuleSubtreeStream(adaptor,"rule s1",s1!=null?s1.getTree():null);
			RewriteRuleSubtreeStream stream_onclause1=new RewriteRuleSubtreeStream(adaptor,"rule onclause1",onclause1!=null?onclause1.getTree():null);
			RewriteRuleSubtreeStream stream_joinelement1=new RewriteRuleSubtreeStream(adaptor,"rule joinelement1",joinelement1!=null?joinelement1.getTree():null);
			RewriteRuleSubtreeStream stream_joinclause1=new RewriteRuleSubtreeStream(adaptor,"rule joinclause1",joinclause1!=null?joinclause1.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 420:3: -> ^( LEFTEXPR ( $s1)? ) ( $joinclause1)? ^( RIGHTEXPR ( $joinelement1)? ) ( $onclause1)?
			{
				// JdbcGrammar.g:420:6: ^( LEFTEXPR ( $s1)? )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(LEFTEXPR, "LEFTEXPR"), root_1);
				// JdbcGrammar.g:420:18: ( $s1)?
				if ( stream_s1.hasNext() ) {
					adaptor.addChild(root_1, stream_s1.nextTree());
				}
				stream_s1.reset();

				adaptor.addChild(root_0, root_1);
				}

				// JdbcGrammar.g:420:24: ( $joinclause1)?
				if ( stream_joinclause1.hasNext() ) {
					adaptor.addChild(root_0, stream_joinclause1.nextTree());
				}
				stream_joinclause1.reset();

				// JdbcGrammar.g:420:37: ^( RIGHTEXPR ( $joinelement1)? )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RIGHTEXPR, "RIGHTEXPR"), root_1);
				// JdbcGrammar.g:420:50: ( $joinelement1)?
				if ( stream_joinelement1.hasNext() ) {
					adaptor.addChild(root_1, stream_joinelement1.nextTree());
				}
				stream_joinelement1.reset();

				adaptor.addChild(root_0, root_1);
				}

				// JdbcGrammar.g:420:66: ( $onclause1)?
				if ( stream_onclause1.hasNext() ) {
					adaptor.addChild(root_0, stream_onclause1.nextTree());
				}
				stream_onclause1.reset();

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "simplejoin"


	public static class datasourcerecur_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "datasourcerecur"
	// JdbcGrammar.g:423:1: datasourcerecur : ( datasourceelement | LPARAM datasourcerecur multijoinexpression RPARAM );
	public final JdbcGrammarParser.datasourcerecur_return datasourcerecur() throws RecognitionException {
		JdbcGrammarParser.datasourcerecur_return retval = new JdbcGrammarParser.datasourcerecur_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token LPARAM309=null;
		Token RPARAM312=null;
		ParserRuleReturnScope datasourceelement308 =null;
		ParserRuleReturnScope datasourcerecur310 =null;
		ParserRuleReturnScope multijoinexpression311 =null;

		Object LPARAM309_tree=null;
		Object RPARAM312_tree=null;

		try {
			// JdbcGrammar.g:424:3: ( datasourceelement | LPARAM datasourcerecur multijoinexpression RPARAM )
			int alt44=2;
			int LA44_0 = input.LA(1);
			if ( (LA44_0==LPARAM) ) {
				int LA44_1 = input.LA(2);
				if ( (LA44_1==SELECTKEYWORD) ) {
					alt44=1;
				}
				else if ( (LA44_1==BACKQUOTE||LA44_1==DOUBLEQUOTE||LA44_1==ESCAPEDDOUBLEQUOTE||LA44_1==IDENTIFIER||LA44_1==LPARAM||LA44_1==SINGLEQUOTE||LA44_1==138) ) {
					alt44=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 44, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}
			else if ( (LA44_0==BACKQUOTE||LA44_0==DOUBLEQUOTE||LA44_0==ESCAPEDDOUBLEQUOTE||LA44_0==IDENTIFIER||LA44_0==SINGLEQUOTE||LA44_0==138) ) {
				alt44=1;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 44, 0, input);
				throw nvae;
			}

			switch (alt44) {
				case 1 :
					// JdbcGrammar.g:425:3: datasourceelement
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_datasourceelement_in_datasourcerecur2601);
					datasourceelement308=datasourceelement();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, datasourceelement308.getTree());

					}
					break;
				case 2 :
					// JdbcGrammar.g:427:3: LPARAM datasourcerecur multijoinexpression RPARAM
					{
					root_0 = (Object)adaptor.nil();


					LPARAM309=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_datasourcerecur2611); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					LPARAM309_tree = (Object)adaptor.create(LPARAM309);
					adaptor.addChild(root_0, LPARAM309_tree);
					}

					pushFollow(FOLLOW_datasourcerecur_in_datasourcerecur2613);
					datasourcerecur310=datasourcerecur();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, datasourcerecur310.getTree());

					pushFollow(FOLLOW_multijoinexpression_in_datasourcerecur2615);
					multijoinexpression311=multijoinexpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, multijoinexpression311.getTree());

					RPARAM312=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_datasourcerecur2617); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					RPARAM312_tree = (Object)adaptor.create(RPARAM312);
					adaptor.addChild(root_0, RPARAM312_tree);
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "datasourcerecur"


	public static class multijoinexpression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "multijoinexpression"
	// JdbcGrammar.g:431:1: multijoinexpression : joinclause datasourceelement onclause -> ^( MULTIJOINEXPRESSION joinclause ^( RIGHTEXPR datasourceelement ) onclause ) ;
	public final JdbcGrammarParser.multijoinexpression_return multijoinexpression() throws RecognitionException {
		JdbcGrammarParser.multijoinexpression_return retval = new JdbcGrammarParser.multijoinexpression_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope joinclause313 =null;
		ParserRuleReturnScope datasourceelement314 =null;
		ParserRuleReturnScope onclause315 =null;

		RewriteRuleSubtreeStream stream_joinclause=new RewriteRuleSubtreeStream(adaptor,"rule joinclause");
		RewriteRuleSubtreeStream stream_onclause=new RewriteRuleSubtreeStream(adaptor,"rule onclause");
		RewriteRuleSubtreeStream stream_datasourceelement=new RewriteRuleSubtreeStream(adaptor,"rule datasourceelement");

		try {
			// JdbcGrammar.g:432:3: ( joinclause datasourceelement onclause -> ^( MULTIJOINEXPRESSION joinclause ^( RIGHTEXPR datasourceelement ) onclause ) )
			// JdbcGrammar.g:433:3: joinclause datasourceelement onclause
			{
			pushFollow(FOLLOW_joinclause_in_multijoinexpression2629);
			joinclause313=joinclause();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_joinclause.add(joinclause313.getTree());
			pushFollow(FOLLOW_datasourceelement_in_multijoinexpression2631);
			datasourceelement314=datasourceelement();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_datasourceelement.add(datasourceelement314.getTree());
			pushFollow(FOLLOW_onclause_in_multijoinexpression2633);
			onclause315=onclause();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_onclause.add(onclause315.getTree());
			// AST REWRITE
			// elements: onclause, datasourceelement, joinclause
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 434:3: -> ^( MULTIJOINEXPRESSION joinclause ^( RIGHTEXPR datasourceelement ) onclause )
			{
				// JdbcGrammar.g:435:3: ^( MULTIJOINEXPRESSION joinclause ^( RIGHTEXPR datasourceelement ) onclause )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(MULTIJOINEXPRESSION, "MULTIJOINEXPRESSION"), root_1);
				adaptor.addChild(root_1, stream_joinclause.nextTree());
				// JdbcGrammar.g:437:25: ^( RIGHTEXPR datasourceelement )
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(RIGHTEXPR, "RIGHTEXPR"), root_2);
				adaptor.addChild(root_2, stream_datasourceelement.nextTree());
				adaptor.addChild(root_1, root_2);
				}

				adaptor.addChild(root_1, stream_onclause.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "multijoinexpression"


	public static class joinclause_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "joinclause"
	// JdbcGrammar.g:444:1: joinclause : ( ( jointypes )? JOINKEYWORD ( EACH )? ) -> ^( JOINTYPE ( jointypes )? JOINKEYWORD ( EACH )? ^( TEXT TEXT[$joinclause.text] ) ) ;
	public final JdbcGrammarParser.joinclause_return joinclause() throws RecognitionException {
		JdbcGrammarParser.joinclause_return retval = new JdbcGrammarParser.joinclause_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token JOINKEYWORD317=null;
		Token EACH318=null;
		ParserRuleReturnScope jointypes316 =null;

		Object JOINKEYWORD317_tree=null;
		Object EACH318_tree=null;
		RewriteRuleTokenStream stream_EACH=new RewriteRuleTokenStream(adaptor,"token EACH");
		RewriteRuleTokenStream stream_JOINKEYWORD=new RewriteRuleTokenStream(adaptor,"token JOINKEYWORD");
		RewriteRuleSubtreeStream stream_jointypes=new RewriteRuleSubtreeStream(adaptor,"rule jointypes");

		try {
			// JdbcGrammar.g:445:5: ( ( ( jointypes )? JOINKEYWORD ( EACH )? ) -> ^( JOINTYPE ( jointypes )? JOINKEYWORD ( EACH )? ^( TEXT TEXT[$joinclause.text] ) ) )
			// JdbcGrammar.g:446:5: ( ( jointypes )? JOINKEYWORD ( EACH )? )
			{
			// JdbcGrammar.g:446:5: ( ( jointypes )? JOINKEYWORD ( EACH )? )
			// JdbcGrammar.g:446:6: ( jointypes )? JOINKEYWORD ( EACH )?
			{
			// JdbcGrammar.g:446:6: ( jointypes )?
			int alt45=2;
			int LA45_0 = input.LA(1);
			if ( (LA45_0==FULl_KEYWORD||LA45_0==INNERKEYWORD||LA45_0==LEFT_KEYWORD||LA45_0==RIGHT_KEYWORD) ) {
				alt45=1;
			}
			switch (alt45) {
				case 1 :
					// JdbcGrammar.g:446:6: jointypes
					{
					pushFollow(FOLLOW_jointypes_in_joinclause2740);
					jointypes316=jointypes();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_jointypes.add(jointypes316.getTree());
					}
					break;

			}

			JOINKEYWORD317=(Token)match(input,JOINKEYWORD,FOLLOW_JOINKEYWORD_in_joinclause2743); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_JOINKEYWORD.add(JOINKEYWORD317);

			// JdbcGrammar.g:446:29: ( EACH )?
			int alt46=2;
			int LA46_0 = input.LA(1);
			if ( (LA46_0==EACH) ) {
				alt46=1;
			}
			switch (alt46) {
				case 1 :
					// JdbcGrammar.g:446:29: EACH
					{
					EACH318=(Token)match(input,EACH,FOLLOW_EACH_in_joinclause2745); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_EACH.add(EACH318);

					}
					break;

			}

			}

			// AST REWRITE
			// elements: EACH, jointypes, JOINKEYWORD
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 446:35: -> ^( JOINTYPE ( jointypes )? JOINKEYWORD ( EACH )? ^( TEXT TEXT[$joinclause.text] ) )
			{
				// JdbcGrammar.g:446:37: ^( JOINTYPE ( jointypes )? JOINKEYWORD ( EACH )? ^( TEXT TEXT[$joinclause.text] ) )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(JOINTYPE, "JOINTYPE"), root_1);
				// JdbcGrammar.g:446:48: ( jointypes )?
				if ( stream_jointypes.hasNext() ) {
					adaptor.addChild(root_1, stream_jointypes.nextTree());
				}
				stream_jointypes.reset();

				adaptor.addChild(root_1, stream_JOINKEYWORD.nextNode());
				// JdbcGrammar.g:446:71: ( EACH )?
				if ( stream_EACH.hasNext() ) {
					adaptor.addChild(root_1, stream_EACH.nextNode());
				}
				stream_EACH.reset();

				// JdbcGrammar.g:446:77: ^( TEXT TEXT[$joinclause.text] )
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(TEXT, "TEXT"), root_2);
				adaptor.addChild(root_2, (Object)adaptor.create(TEXT, input.toString(retval.start,input.LT(-1))));
				adaptor.addChild(root_1, root_2);
				}

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "joinclause"


	public static class onclause_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "onclause"
	// JdbcGrammar.g:453:1: onclause : ( ONKEYWORD ( ( LPARAM condition RPARAM ) | condition ) ) -> ^( ONCLAUSE condition ^( TEXT TEXT[$onclause.text] ) ) ;
	public final JdbcGrammarParser.onclause_return onclause() throws RecognitionException {
		JdbcGrammarParser.onclause_return retval = new JdbcGrammarParser.onclause_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token ONKEYWORD319=null;
		Token LPARAM320=null;
		Token RPARAM322=null;
		ParserRuleReturnScope condition321 =null;
		ParserRuleReturnScope condition323 =null;

		Object ONKEYWORD319_tree=null;
		Object LPARAM320_tree=null;
		Object RPARAM322_tree=null;
		RewriteRuleTokenStream stream_LPARAM=new RewriteRuleTokenStream(adaptor,"token LPARAM");
		RewriteRuleTokenStream stream_RPARAM=new RewriteRuleTokenStream(adaptor,"token RPARAM");
		RewriteRuleTokenStream stream_ONKEYWORD=new RewriteRuleTokenStream(adaptor,"token ONKEYWORD");
		RewriteRuleSubtreeStream stream_condition=new RewriteRuleSubtreeStream(adaptor,"rule condition");

		try {
			// JdbcGrammar.g:454:2: ( ( ONKEYWORD ( ( LPARAM condition RPARAM ) | condition ) ) -> ^( ONCLAUSE condition ^( TEXT TEXT[$onclause.text] ) ) )
			// JdbcGrammar.g:455:2: ( ONKEYWORD ( ( LPARAM condition RPARAM ) | condition ) )
			{
			// JdbcGrammar.g:455:2: ( ONKEYWORD ( ( LPARAM condition RPARAM ) | condition ) )
			// JdbcGrammar.g:455:2: ONKEYWORD ( ( LPARAM condition RPARAM ) | condition )
			{
			ONKEYWORD319=(Token)match(input,ONKEYWORD,FOLLOW_ONKEYWORD_in_onclause2780); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_ONKEYWORD.add(ONKEYWORD319);

			// JdbcGrammar.g:456:13: ( ( LPARAM condition RPARAM ) | condition )
			int alt47=2;
			int LA47_0 = input.LA(1);
			if ( (LA47_0==LPARAM) ) {
				alt47=1;
			}
			else if ( (LA47_0==BACKSINGLEQUOTE||LA47_0==DOUBLEQUOTE||LA47_0==IDENTIFIER) ) {
				alt47=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 47, 0, input);
				throw nvae;
			}

			switch (alt47) {
				case 1 :
					// JdbcGrammar.g:457:17: ( LPARAM condition RPARAM )
					{
					// JdbcGrammar.g:457:17: ( LPARAM condition RPARAM )
					// JdbcGrammar.g:457:18: LPARAM condition RPARAM
					{
					LPARAM320=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_onclause2814); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM320);

					pushFollow(FOLLOW_condition_in_onclause2816);
					condition321=condition();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_condition.add(condition321.getTree());
					RPARAM322=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_onclause2818); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM322);

					}

					}
					break;
				case 2 :
					// JdbcGrammar.g:457:45: condition
					{
					pushFollow(FOLLOW_condition_in_onclause2823);
					condition323=condition();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_condition.add(condition323.getTree());
					}
					break;

			}

			}

			// AST REWRITE
			// elements: condition
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 459:14: -> ^( ONCLAUSE condition ^( TEXT TEXT[$onclause.text] ) )
			{
				// JdbcGrammar.g:459:16: ^( ONCLAUSE condition ^( TEXT TEXT[$onclause.text] ) )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(ONCLAUSE, "ONCLAUSE"), root_1);
				adaptor.addChild(root_1, stream_condition.nextTree());
				// JdbcGrammar.g:459:37: ^( TEXT TEXT[$onclause.text] )
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(TEXT, "TEXT"), root_2);
				adaptor.addChild(root_2, (Object)adaptor.create(TEXT, input.toString(retval.start,input.LT(-1))));
				adaptor.addChild(root_1, root_2);
				}

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "onclause"


	public static class dataset_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "dataset"
	// JdbcGrammar.g:467:1: dataset : ( ( BACKQUOTE name BACKQUOTE ) | ( SINGLEQUOTE name SINGLEQUOTE ) | ( DOUBLEQUOTE name DOUBLEQUOTE ) | ( ESCAPEDDOUBLEQUOTE name ESCAPEDDOUBLEQUOTE ) | name ) -> ^( DATASETNAME name ) ;
	public final JdbcGrammarParser.dataset_return dataset() throws RecognitionException {
		JdbcGrammarParser.dataset_return retval = new JdbcGrammarParser.dataset_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token BACKQUOTE324=null;
		Token BACKQUOTE326=null;
		Token SINGLEQUOTE327=null;
		Token SINGLEQUOTE329=null;
		Token DOUBLEQUOTE330=null;
		Token DOUBLEQUOTE332=null;
		Token ESCAPEDDOUBLEQUOTE333=null;
		Token ESCAPEDDOUBLEQUOTE335=null;
		ParserRuleReturnScope name325 =null;
		ParserRuleReturnScope name328 =null;
		ParserRuleReturnScope name331 =null;
		ParserRuleReturnScope name334 =null;
		ParserRuleReturnScope name336 =null;

		Object BACKQUOTE324_tree=null;
		Object BACKQUOTE326_tree=null;
		Object SINGLEQUOTE327_tree=null;
		Object SINGLEQUOTE329_tree=null;
		Object DOUBLEQUOTE330_tree=null;
		Object DOUBLEQUOTE332_tree=null;
		Object ESCAPEDDOUBLEQUOTE333_tree=null;
		Object ESCAPEDDOUBLEQUOTE335_tree=null;
		RewriteRuleTokenStream stream_BACKQUOTE=new RewriteRuleTokenStream(adaptor,"token BACKQUOTE");
		RewriteRuleTokenStream stream_DOUBLEQUOTE=new RewriteRuleTokenStream(adaptor,"token DOUBLEQUOTE");
		RewriteRuleTokenStream stream_SINGLEQUOTE=new RewriteRuleTokenStream(adaptor,"token SINGLEQUOTE");
		RewriteRuleTokenStream stream_ESCAPEDDOUBLEQUOTE=new RewriteRuleTokenStream(adaptor,"token ESCAPEDDOUBLEQUOTE");
		RewriteRuleSubtreeStream stream_name=new RewriteRuleSubtreeStream(adaptor,"rule name");

		try {
			// JdbcGrammar.g:468:3: ( ( ( BACKQUOTE name BACKQUOTE ) | ( SINGLEQUOTE name SINGLEQUOTE ) | ( DOUBLEQUOTE name DOUBLEQUOTE ) | ( ESCAPEDDOUBLEQUOTE name ESCAPEDDOUBLEQUOTE ) | name ) -> ^( DATASETNAME name ) )
			// JdbcGrammar.g:469:3: ( ( BACKQUOTE name BACKQUOTE ) | ( SINGLEQUOTE name SINGLEQUOTE ) | ( DOUBLEQUOTE name DOUBLEQUOTE ) | ( ESCAPEDDOUBLEQUOTE name ESCAPEDDOUBLEQUOTE ) | name )
			{
			// JdbcGrammar.g:469:3: ( ( BACKQUOTE name BACKQUOTE ) | ( SINGLEQUOTE name SINGLEQUOTE ) | ( DOUBLEQUOTE name DOUBLEQUOTE ) | ( ESCAPEDDOUBLEQUOTE name ESCAPEDDOUBLEQUOTE ) | name )
			int alt48=5;
			switch ( input.LA(1) ) {
			case BACKQUOTE:
				{
				alt48=1;
				}
				break;
			case SINGLEQUOTE:
				{
				alt48=2;
				}
				break;
			case DOUBLEQUOTE:
				{
				alt48=3;
				}
				break;
			case ESCAPEDDOUBLEQUOTE:
				{
				alt48=4;
				}
				break;
			case IDENTIFIER:
				{
				alt48=5;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 48, 0, input);
				throw nvae;
			}
			switch (alt48) {
				case 1 :
					// JdbcGrammar.g:469:6: ( BACKQUOTE name BACKQUOTE )
					{
					// JdbcGrammar.g:469:6: ( BACKQUOTE name BACKQUOTE )
					// JdbcGrammar.g:469:7: BACKQUOTE name BACKQUOTE
					{
					BACKQUOTE324=(Token)match(input,BACKQUOTE,FOLLOW_BACKQUOTE_in_dataset2883); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_BACKQUOTE.add(BACKQUOTE324);

					pushFollow(FOLLOW_name_in_dataset2885);
					name325=name();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_name.add(name325.getTree());
					BACKQUOTE326=(Token)match(input,BACKQUOTE,FOLLOW_BACKQUOTE_in_dataset2887); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_BACKQUOTE.add(BACKQUOTE326);

					}

					}
					break;
				case 2 :
					// JdbcGrammar.g:470:6: ( SINGLEQUOTE name SINGLEQUOTE )
					{
					// JdbcGrammar.g:470:6: ( SINGLEQUOTE name SINGLEQUOTE )
					// JdbcGrammar.g:470:7: SINGLEQUOTE name SINGLEQUOTE
					{
					SINGLEQUOTE327=(Token)match(input,SINGLEQUOTE,FOLLOW_SINGLEQUOTE_in_dataset2896); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_SINGLEQUOTE.add(SINGLEQUOTE327);

					pushFollow(FOLLOW_name_in_dataset2898);
					name328=name();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_name.add(name328.getTree());
					SINGLEQUOTE329=(Token)match(input,SINGLEQUOTE,FOLLOW_SINGLEQUOTE_in_dataset2900); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_SINGLEQUOTE.add(SINGLEQUOTE329);

					}

					}
					break;
				case 3 :
					// JdbcGrammar.g:471:6: ( DOUBLEQUOTE name DOUBLEQUOTE )
					{
					// JdbcGrammar.g:471:6: ( DOUBLEQUOTE name DOUBLEQUOTE )
					// JdbcGrammar.g:471:7: DOUBLEQUOTE name DOUBLEQUOTE
					{
					DOUBLEQUOTE330=(Token)match(input,DOUBLEQUOTE,FOLLOW_DOUBLEQUOTE_in_dataset2910); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_DOUBLEQUOTE.add(DOUBLEQUOTE330);

					pushFollow(FOLLOW_name_in_dataset2912);
					name331=name();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_name.add(name331.getTree());
					DOUBLEQUOTE332=(Token)match(input,DOUBLEQUOTE,FOLLOW_DOUBLEQUOTE_in_dataset2914); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_DOUBLEQUOTE.add(DOUBLEQUOTE332);

					}

					}
					break;
				case 4 :
					// JdbcGrammar.g:472:6: ( ESCAPEDDOUBLEQUOTE name ESCAPEDDOUBLEQUOTE )
					{
					// JdbcGrammar.g:472:6: ( ESCAPEDDOUBLEQUOTE name ESCAPEDDOUBLEQUOTE )
					// JdbcGrammar.g:472:7: ESCAPEDDOUBLEQUOTE name ESCAPEDDOUBLEQUOTE
					{
					ESCAPEDDOUBLEQUOTE333=(Token)match(input,ESCAPEDDOUBLEQUOTE,FOLLOW_ESCAPEDDOUBLEQUOTE_in_dataset2924); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_ESCAPEDDOUBLEQUOTE.add(ESCAPEDDOUBLEQUOTE333);

					pushFollow(FOLLOW_name_in_dataset2926);
					name334=name();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_name.add(name334.getTree());
					ESCAPEDDOUBLEQUOTE335=(Token)match(input,ESCAPEDDOUBLEQUOTE,FOLLOW_ESCAPEDDOUBLEQUOTE_in_dataset2928); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_ESCAPEDDOUBLEQUOTE.add(ESCAPEDDOUBLEQUOTE335);

					}

					}
					break;
				case 5 :
					// JdbcGrammar.g:473:6: name
					{
					pushFollow(FOLLOW_name_in_dataset2937);
					name336=name();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_name.add(name336.getTree());
					}
					break;

			}

			// AST REWRITE
			// elements: name
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 475:3: -> ^( DATASETNAME name )
			{
				// JdbcGrammar.g:475:5: ^( DATASETNAME name )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(DATASETNAME, "DATASETNAME"), root_1);
				adaptor.addChild(root_1, stream_name.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "dataset"


	public static class project_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "project"
	// JdbcGrammar.g:481:1: project : ( options {greedy=true; } : ( SINGLEQUOTE ( name ( projectdivider )? )+ SINGLEQUOTE ) | ( DOUBLEQUOTE ( name ( projectdivider )? )+ DOUBLEQUOTE ) | ( ESCAPEDDOUBLEQUOTE ( name ( projectdivider )? )+ ESCAPEDDOUBLEQUOTE ) | name ) -> ^( PROJECTNAME name ( projectdivider name )* ) ;
	public final JdbcGrammarParser.project_return project() throws RecognitionException {
		JdbcGrammarParser.project_return retval = new JdbcGrammarParser.project_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token SINGLEQUOTE337=null;
		Token SINGLEQUOTE340=null;
		Token DOUBLEQUOTE341=null;
		Token DOUBLEQUOTE344=null;
		Token ESCAPEDDOUBLEQUOTE345=null;
		Token ESCAPEDDOUBLEQUOTE348=null;
		ParserRuleReturnScope name338 =null;
		ParserRuleReturnScope projectdivider339 =null;
		ParserRuleReturnScope name342 =null;
		ParserRuleReturnScope projectdivider343 =null;
		ParserRuleReturnScope name346 =null;
		ParserRuleReturnScope projectdivider347 =null;
		ParserRuleReturnScope name349 =null;

		Object SINGLEQUOTE337_tree=null;
		Object SINGLEQUOTE340_tree=null;
		Object DOUBLEQUOTE341_tree=null;
		Object DOUBLEQUOTE344_tree=null;
		Object ESCAPEDDOUBLEQUOTE345_tree=null;
		Object ESCAPEDDOUBLEQUOTE348_tree=null;
		RewriteRuleTokenStream stream_DOUBLEQUOTE=new RewriteRuleTokenStream(adaptor,"token DOUBLEQUOTE");
		RewriteRuleTokenStream stream_SINGLEQUOTE=new RewriteRuleTokenStream(adaptor,"token SINGLEQUOTE");
		RewriteRuleTokenStream stream_ESCAPEDDOUBLEQUOTE=new RewriteRuleTokenStream(adaptor,"token ESCAPEDDOUBLEQUOTE");
		RewriteRuleSubtreeStream stream_projectdivider=new RewriteRuleSubtreeStream(adaptor,"rule projectdivider");
		RewriteRuleSubtreeStream stream_name=new RewriteRuleSubtreeStream(adaptor,"rule name");

		try {
			// JdbcGrammar.g:482:3: ( ( options {greedy=true; } : ( SINGLEQUOTE ( name ( projectdivider )? )+ SINGLEQUOTE ) | ( DOUBLEQUOTE ( name ( projectdivider )? )+ DOUBLEQUOTE ) | ( ESCAPEDDOUBLEQUOTE ( name ( projectdivider )? )+ ESCAPEDDOUBLEQUOTE ) | name ) -> ^( PROJECTNAME name ( projectdivider name )* ) )
			// JdbcGrammar.g:483:3: ( options {greedy=true; } : ( SINGLEQUOTE ( name ( projectdivider )? )+ SINGLEQUOTE ) | ( DOUBLEQUOTE ( name ( projectdivider )? )+ DOUBLEQUOTE ) | ( ESCAPEDDOUBLEQUOTE ( name ( projectdivider )? )+ ESCAPEDDOUBLEQUOTE ) | name )
			{
			// JdbcGrammar.g:483:3: ( options {greedy=true; } : ( SINGLEQUOTE ( name ( projectdivider )? )+ SINGLEQUOTE ) | ( DOUBLEQUOTE ( name ( projectdivider )? )+ DOUBLEQUOTE ) | ( ESCAPEDDOUBLEQUOTE ( name ( projectdivider )? )+ ESCAPEDDOUBLEQUOTE ) | name )
			int alt55=4;
			switch ( input.LA(1) ) {
			case SINGLEQUOTE:
				{
				alt55=1;
				}
				break;
			case DOUBLEQUOTE:
				{
				alt55=2;
				}
				break;
			case ESCAPEDDOUBLEQUOTE:
				{
				alt55=3;
				}
				break;
			case IDENTIFIER:
				{
				alt55=4;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 55, 0, input);
				throw nvae;
			}
			switch (alt55) {
				case 1 :
					// JdbcGrammar.g:484:5: ( SINGLEQUOTE ( name ( projectdivider )? )+ SINGLEQUOTE )
					{
					// JdbcGrammar.g:484:5: ( SINGLEQUOTE ( name ( projectdivider )? )+ SINGLEQUOTE )
					// JdbcGrammar.g:484:6: SINGLEQUOTE ( name ( projectdivider )? )+ SINGLEQUOTE
					{
					SINGLEQUOTE337=(Token)match(input,SINGLEQUOTE,FOLLOW_SINGLEQUOTE_in_project2979); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_SINGLEQUOTE.add(SINGLEQUOTE337);

					// JdbcGrammar.g:484:18: ( name ( projectdivider )? )+
					int cnt50=0;
					loop50:
					while (true) {
						int alt50=2;
						int LA50_0 = input.LA(1);
						if ( (LA50_0==IDENTIFIER) ) {
							alt50=1;
						}

						switch (alt50) {
						case 1 :
							// JdbcGrammar.g:484:19: name ( projectdivider )?
							{
							pushFollow(FOLLOW_name_in_project2982);
							name338=name();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_name.add(name338.getTree());
							// JdbcGrammar.g:484:24: ( projectdivider )?
							int alt49=2;
							int LA49_0 = input.LA(1);
							if ( (LA49_0==COLON||LA49_0==PUNCTUATION) ) {
								alt49=1;
							}
							switch (alt49) {
								case 1 :
									// JdbcGrammar.g:484:24: projectdivider
									{
									pushFollow(FOLLOW_projectdivider_in_project2984);
									projectdivider339=projectdivider();
									state._fsp--;
									if (state.failed) return retval;
									if ( state.backtracking==0 ) stream_projectdivider.add(projectdivider339.getTree());
									}
									break;

							}

							}
							break;

						default :
							if ( cnt50 >= 1 ) break loop50;
							if (state.backtracking>0) {state.failed=true; return retval;}
							EarlyExitException eee = new EarlyExitException(50, input);
							throw eee;
						}
						cnt50++;
					}

					SINGLEQUOTE340=(Token)match(input,SINGLEQUOTE,FOLLOW_SINGLEQUOTE_in_project2990); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_SINGLEQUOTE.add(SINGLEQUOTE340);

					}

					}
					break;
				case 2 :
					// JdbcGrammar.g:485:6: ( DOUBLEQUOTE ( name ( projectdivider )? )+ DOUBLEQUOTE )
					{
					// JdbcGrammar.g:485:6: ( DOUBLEQUOTE ( name ( projectdivider )? )+ DOUBLEQUOTE )
					// JdbcGrammar.g:485:7: DOUBLEQUOTE ( name ( projectdivider )? )+ DOUBLEQUOTE
					{
					DOUBLEQUOTE341=(Token)match(input,DOUBLEQUOTE,FOLLOW_DOUBLEQUOTE_in_project2999); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_DOUBLEQUOTE.add(DOUBLEQUOTE341);

					// JdbcGrammar.g:485:19: ( name ( projectdivider )? )+
					int cnt52=0;
					loop52:
					while (true) {
						int alt52=2;
						int LA52_0 = input.LA(1);
						if ( (LA52_0==IDENTIFIER) ) {
							alt52=1;
						}

						switch (alt52) {
						case 1 :
							// JdbcGrammar.g:485:20: name ( projectdivider )?
							{
							pushFollow(FOLLOW_name_in_project3002);
							name342=name();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_name.add(name342.getTree());
							// JdbcGrammar.g:485:25: ( projectdivider )?
							int alt51=2;
							int LA51_0 = input.LA(1);
							if ( (LA51_0==COLON||LA51_0==PUNCTUATION) ) {
								alt51=1;
							}
							switch (alt51) {
								case 1 :
									// JdbcGrammar.g:485:25: projectdivider
									{
									pushFollow(FOLLOW_projectdivider_in_project3004);
									projectdivider343=projectdivider();
									state._fsp--;
									if (state.failed) return retval;
									if ( state.backtracking==0 ) stream_projectdivider.add(projectdivider343.getTree());
									}
									break;

							}

							}
							break;

						default :
							if ( cnt52 >= 1 ) break loop52;
							if (state.backtracking>0) {state.failed=true; return retval;}
							EarlyExitException eee = new EarlyExitException(52, input);
							throw eee;
						}
						cnt52++;
					}

					DOUBLEQUOTE344=(Token)match(input,DOUBLEQUOTE,FOLLOW_DOUBLEQUOTE_in_project3010); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_DOUBLEQUOTE.add(DOUBLEQUOTE344);

					}

					}
					break;
				case 3 :
					// JdbcGrammar.g:486:6: ( ESCAPEDDOUBLEQUOTE ( name ( projectdivider )? )+ ESCAPEDDOUBLEQUOTE )
					{
					// JdbcGrammar.g:486:6: ( ESCAPEDDOUBLEQUOTE ( name ( projectdivider )? )+ ESCAPEDDOUBLEQUOTE )
					// JdbcGrammar.g:486:7: ESCAPEDDOUBLEQUOTE ( name ( projectdivider )? )+ ESCAPEDDOUBLEQUOTE
					{
					ESCAPEDDOUBLEQUOTE345=(Token)match(input,ESCAPEDDOUBLEQUOTE,FOLLOW_ESCAPEDDOUBLEQUOTE_in_project3020); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_ESCAPEDDOUBLEQUOTE.add(ESCAPEDDOUBLEQUOTE345);

					// JdbcGrammar.g:486:26: ( name ( projectdivider )? )+
					int cnt54=0;
					loop54:
					while (true) {
						int alt54=2;
						int LA54_0 = input.LA(1);
						if ( (LA54_0==IDENTIFIER) ) {
							alt54=1;
						}

						switch (alt54) {
						case 1 :
							// JdbcGrammar.g:486:27: name ( projectdivider )?
							{
							pushFollow(FOLLOW_name_in_project3023);
							name346=name();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_name.add(name346.getTree());
							// JdbcGrammar.g:486:32: ( projectdivider )?
							int alt53=2;
							int LA53_0 = input.LA(1);
							if ( (LA53_0==COLON||LA53_0==PUNCTUATION) ) {
								alt53=1;
							}
							switch (alt53) {
								case 1 :
									// JdbcGrammar.g:486:32: projectdivider
									{
									pushFollow(FOLLOW_projectdivider_in_project3025);
									projectdivider347=projectdivider();
									state._fsp--;
									if (state.failed) return retval;
									if ( state.backtracking==0 ) stream_projectdivider.add(projectdivider347.getTree());
									}
									break;

							}

							}
							break;

						default :
							if ( cnt54 >= 1 ) break loop54;
							if (state.backtracking>0) {state.failed=true; return retval;}
							EarlyExitException eee = new EarlyExitException(54, input);
							throw eee;
						}
						cnt54++;
					}

					ESCAPEDDOUBLEQUOTE348=(Token)match(input,ESCAPEDDOUBLEQUOTE,FOLLOW_ESCAPEDDOUBLEQUOTE_in_project3031); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_ESCAPEDDOUBLEQUOTE.add(ESCAPEDDOUBLEQUOTE348);

					}

					}
					break;
				case 4 :
					// JdbcGrammar.g:487:6: name
					{
					pushFollow(FOLLOW_name_in_project3039);
					name349=name();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_name.add(name349.getTree());
					}
					break;

			}

			// AST REWRITE
			// elements: name, projectdivider, name
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 489:5: -> ^( PROJECTNAME name ( projectdivider name )* )
			{
				// JdbcGrammar.g:489:7: ^( PROJECTNAME name ( projectdivider name )* )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(PROJECTNAME, "PROJECTNAME"), root_1);
				adaptor.addChild(root_1, stream_name.nextTree());
				// JdbcGrammar.g:489:26: ( projectdivider name )*
				while ( stream_name.hasNext()||stream_projectdivider.hasNext() ) {
					adaptor.addChild(root_1, stream_projectdivider.nextTree());
					adaptor.addChild(root_1, stream_name.nextTree());
				}
				stream_name.reset();
				stream_projectdivider.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "project"


	public static class projectdivider_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "projectdivider"
	// JdbcGrammar.g:496:1: projectdivider : ( COLON | PUNCTUATION ) -> ^( DIVIDER DIVIDER[$projectdivider.text] ) ;
	public final JdbcGrammarParser.projectdivider_return projectdivider() throws RecognitionException {
		JdbcGrammarParser.projectdivider_return retval = new JdbcGrammarParser.projectdivider_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token COLON350=null;
		Token PUNCTUATION351=null;

		Object COLON350_tree=null;
		Object PUNCTUATION351_tree=null;
		RewriteRuleTokenStream stream_COLON=new RewriteRuleTokenStream(adaptor,"token COLON");
		RewriteRuleTokenStream stream_PUNCTUATION=new RewriteRuleTokenStream(adaptor,"token PUNCTUATION");

		try {
			// JdbcGrammar.g:497:3: ( ( COLON | PUNCTUATION ) -> ^( DIVIDER DIVIDER[$projectdivider.text] ) )
			// JdbcGrammar.g:498:3: ( COLON | PUNCTUATION )
			{
			// JdbcGrammar.g:498:3: ( COLON | PUNCTUATION )
			int alt56=2;
			int LA56_0 = input.LA(1);
			if ( (LA56_0==COLON) ) {
				alt56=1;
			}
			else if ( (LA56_0==PUNCTUATION) ) {
				alt56=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 56, 0, input);
				throw nvae;
			}

			switch (alt56) {
				case 1 :
					// JdbcGrammar.g:498:5: COLON
					{
					COLON350=(Token)match(input,COLON,FOLLOW_COLON_in_projectdivider3076); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_COLON.add(COLON350);

					}
					break;
				case 2 :
					// JdbcGrammar.g:498:13: PUNCTUATION
					{
					PUNCTUATION351=(Token)match(input,PUNCTUATION,FOLLOW_PUNCTUATION_in_projectdivider3080); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_PUNCTUATION.add(PUNCTUATION351);

					}
					break;

			}

			// AST REWRITE
			// elements: 
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 498:27: -> ^( DIVIDER DIVIDER[$projectdivider.text] )
			{
				// JdbcGrammar.g:498:29: ^( DIVIDER DIVIDER[$projectdivider.text] )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(DIVIDER, "DIVIDER"), root_1);
				adaptor.addChild(root_1, (Object)adaptor.create(DIVIDER, input.toString(retval.start,input.LT(-1))));
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "projectdivider"


	public static class sourcetable_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "sourcetable"
	// JdbcGrammar.g:504:1: sourcetable : ( ( '[' )? ( ( ( project columndivider )? dataset columndivider )? srctablename ) ( alias )? ( ']' )? ) -> ^( SOURCETABLE ( '[' )? ( project )? ( dataset )? srctablename ( alias )? ( columndivider )* ( ']' )? ^( TEXT TEXT[$sourcetable.text] ) ) ;
	public final JdbcGrammarParser.sourcetable_return sourcetable() throws RecognitionException {
		JdbcGrammarParser.sourcetable_return retval = new JdbcGrammarParser.sourcetable_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal352=null;
		Token char_literal359=null;
		ParserRuleReturnScope project353 =null;
		ParserRuleReturnScope columndivider354 =null;
		ParserRuleReturnScope dataset355 =null;
		ParserRuleReturnScope columndivider356 =null;
		ParserRuleReturnScope srctablename357 =null;
		ParserRuleReturnScope alias358 =null;

		Object char_literal352_tree=null;
		Object char_literal359_tree=null;
		RewriteRuleTokenStream stream_138=new RewriteRuleTokenStream(adaptor,"token 138");
		RewriteRuleTokenStream stream_139=new RewriteRuleTokenStream(adaptor,"token 139");
		RewriteRuleSubtreeStream stream_project=new RewriteRuleSubtreeStream(adaptor,"rule project");
		RewriteRuleSubtreeStream stream_srctablename=new RewriteRuleSubtreeStream(adaptor,"rule srctablename");
		RewriteRuleSubtreeStream stream_dataset=new RewriteRuleSubtreeStream(adaptor,"rule dataset");
		RewriteRuleSubtreeStream stream_alias=new RewriteRuleSubtreeStream(adaptor,"rule alias");
		RewriteRuleSubtreeStream stream_columndivider=new RewriteRuleSubtreeStream(adaptor,"rule columndivider");

		try {
			// JdbcGrammar.g:505:5: ( ( ( '[' )? ( ( ( project columndivider )? dataset columndivider )? srctablename ) ( alias )? ( ']' )? ) -> ^( SOURCETABLE ( '[' )? ( project )? ( dataset )? srctablename ( alias )? ( columndivider )* ( ']' )? ^( TEXT TEXT[$sourcetable.text] ) ) )
			// JdbcGrammar.g:507:5: ( ( '[' )? ( ( ( project columndivider )? dataset columndivider )? srctablename ) ( alias )? ( ']' )? )
			{
			// JdbcGrammar.g:507:5: ( ( '[' )? ( ( ( project columndivider )? dataset columndivider )? srctablename ) ( alias )? ( ']' )? )
			// JdbcGrammar.g:507:6: ( '[' )? ( ( ( project columndivider )? dataset columndivider )? srctablename ) ( alias )? ( ']' )?
			{
			// JdbcGrammar.g:507:6: ( '[' )?
			int alt57=2;
			int LA57_0 = input.LA(1);
			if ( (LA57_0==138) ) {
				alt57=1;
			}
			switch (alt57) {
				case 1 :
					// JdbcGrammar.g:507:6: '['
					{
					char_literal352=(Token)match(input,138,FOLLOW_138_in_sourcetable3112); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_138.add(char_literal352);

					}
					break;

			}

			// JdbcGrammar.g:508:6: ( ( ( project columndivider )? dataset columndivider )? srctablename )
			// JdbcGrammar.g:508:7: ( ( project columndivider )? dataset columndivider )? srctablename
			{
			// JdbcGrammar.g:508:7: ( ( project columndivider )? dataset columndivider )?
			int alt59=2;
			switch ( input.LA(1) ) {
				case SINGLEQUOTE:
					{
					int LA59_1 = input.LA(2);
					if ( (LA59_1==IDENTIFIER) ) {
						int LA59_6 = input.LA(3);
						if ( (LA59_6==COLON||LA59_6==IDENTIFIER||LA59_6==PUNCTUATION) ) {
							alt59=1;
						}
						else if ( (LA59_6==SINGLEQUOTE) ) {
							int LA59_12 = input.LA(4);
							if ( (LA59_12==COLON||LA59_12==PUNCTUATION) ) {
								alt59=1;
							}
						}
					}
					}
					break;
				case DOUBLEQUOTE:
					{
					int LA59_2 = input.LA(2);
					if ( (LA59_2==IDENTIFIER) ) {
						int LA59_7 = input.LA(3);
						if ( (LA59_7==COLON||LA59_7==IDENTIFIER||LA59_7==PUNCTUATION) ) {
							alt59=1;
						}
						else if ( (LA59_7==DOUBLEQUOTE) ) {
							int LA59_13 = input.LA(4);
							if ( (LA59_13==COLON||LA59_13==PUNCTUATION) ) {
								alt59=1;
							}
						}
					}
					}
					break;
				case ESCAPEDDOUBLEQUOTE:
					{
					int LA59_3 = input.LA(2);
					if ( (LA59_3==IDENTIFIER) ) {
						int LA59_8 = input.LA(3);
						if ( (LA59_8==COLON||LA59_8==IDENTIFIER||LA59_8==PUNCTUATION) ) {
							alt59=1;
						}
						else if ( (LA59_8==ESCAPEDDOUBLEQUOTE) ) {
							int LA59_14 = input.LA(4);
							if ( (LA59_14==COLON||LA59_14==PUNCTUATION) ) {
								alt59=1;
							}
						}
					}
					}
					break;
				case IDENTIFIER:
					{
					int LA59_4 = input.LA(2);
					if ( (LA59_4==COLON||LA59_4==PUNCTUATION) ) {
						alt59=1;
					}
					}
					break;
				case BACKQUOTE:
					{
					int LA59_5 = input.LA(2);
					if ( (LA59_5==IDENTIFIER) ) {
						int LA59_11 = input.LA(3);
						if ( (LA59_11==BACKQUOTE) ) {
							int LA59_15 = input.LA(4);
							if ( (LA59_15==COLON||LA59_15==PUNCTUATION) ) {
								alt59=1;
							}
						}
					}
					}
					break;
			}
			switch (alt59) {
				case 1 :
					// JdbcGrammar.g:508:8: ( project columndivider )? dataset columndivider
					{
					// JdbcGrammar.g:508:8: ( project columndivider )?
					int alt58=2;
					switch ( input.LA(1) ) {
						case SINGLEQUOTE:
							{
							int LA58_1 = input.LA(2);
							if ( (LA58_1==IDENTIFIER) ) {
								int LA58_6 = input.LA(3);
								if ( (LA58_6==COLON||LA58_6==IDENTIFIER||LA58_6==PUNCTUATION) ) {
									alt58=1;
								}
								else if ( (LA58_6==SINGLEQUOTE) ) {
									int LA58_11 = input.LA(4);
									if ( (LA58_11==COLON||LA58_11==PUNCTUATION) ) {
										switch ( input.LA(5) ) {
											case BACKQUOTE:
												{
												int LA58_14 = input.LA(6);
												if ( (LA58_14==IDENTIFIER) ) {
													int LA58_19 = input.LA(7);
													if ( (LA58_19==BACKQUOTE) ) {
														int LA58_23 = input.LA(8);
														if ( (LA58_23==COLON||LA58_23==PUNCTUATION) ) {
															alt58=1;
														}
													}
												}
												}
												break;
											case SINGLEQUOTE:
												{
												int LA58_15 = input.LA(6);
												if ( (LA58_15==IDENTIFIER) ) {
													int LA58_20 = input.LA(7);
													if ( (LA58_20==SINGLEQUOTE) ) {
														int LA58_24 = input.LA(8);
														if ( (LA58_24==COLON||LA58_24==PUNCTUATION) ) {
															alt58=1;
														}
													}
												}
												}
												break;
											case DOUBLEQUOTE:
												{
												int LA58_16 = input.LA(6);
												if ( (LA58_16==IDENTIFIER) ) {
													int LA58_21 = input.LA(7);
													if ( (LA58_21==DOUBLEQUOTE) ) {
														int LA58_25 = input.LA(8);
														if ( (LA58_25==COLON||LA58_25==PUNCTUATION) ) {
															alt58=1;
														}
													}
												}
												}
												break;
											case ESCAPEDDOUBLEQUOTE:
												{
												int LA58_17 = input.LA(6);
												if ( (LA58_17==IDENTIFIER) ) {
													int LA58_22 = input.LA(7);
													if ( (LA58_22==ESCAPEDDOUBLEQUOTE) ) {
														int LA58_26 = input.LA(8);
														if ( (LA58_26==COLON||LA58_26==PUNCTUATION) ) {
															alt58=1;
														}
													}
												}
												}
												break;
											case IDENTIFIER:
												{
												int LA58_18 = input.LA(6);
												if ( (LA58_18==COLON||LA58_18==PUNCTUATION) ) {
													alt58=1;
												}
												}
												break;
										}
									}
								}
							}
							}
							break;
						case DOUBLEQUOTE:
							{
							int LA58_2 = input.LA(2);
							if ( (LA58_2==IDENTIFIER) ) {
								int LA58_7 = input.LA(3);
								if ( (LA58_7==COLON||LA58_7==IDENTIFIER||LA58_7==PUNCTUATION) ) {
									alt58=1;
								}
								else if ( (LA58_7==DOUBLEQUOTE) ) {
									int LA58_12 = input.LA(4);
									if ( (LA58_12==COLON||LA58_12==PUNCTUATION) ) {
										switch ( input.LA(5) ) {
											case BACKQUOTE:
												{
												int LA58_14 = input.LA(6);
												if ( (LA58_14==IDENTIFIER) ) {
													int LA58_19 = input.LA(7);
													if ( (LA58_19==BACKQUOTE) ) {
														int LA58_23 = input.LA(8);
														if ( (LA58_23==COLON||LA58_23==PUNCTUATION) ) {
															alt58=1;
														}
													}
												}
												}
												break;
											case SINGLEQUOTE:
												{
												int LA58_15 = input.LA(6);
												if ( (LA58_15==IDENTIFIER) ) {
													int LA58_20 = input.LA(7);
													if ( (LA58_20==SINGLEQUOTE) ) {
														int LA58_24 = input.LA(8);
														if ( (LA58_24==COLON||LA58_24==PUNCTUATION) ) {
															alt58=1;
														}
													}
												}
												}
												break;
											case DOUBLEQUOTE:
												{
												int LA58_16 = input.LA(6);
												if ( (LA58_16==IDENTIFIER) ) {
													int LA58_21 = input.LA(7);
													if ( (LA58_21==DOUBLEQUOTE) ) {
														int LA58_25 = input.LA(8);
														if ( (LA58_25==COLON||LA58_25==PUNCTUATION) ) {
															alt58=1;
														}
													}
												}
												}
												break;
											case ESCAPEDDOUBLEQUOTE:
												{
												int LA58_17 = input.LA(6);
												if ( (LA58_17==IDENTIFIER) ) {
													int LA58_22 = input.LA(7);
													if ( (LA58_22==ESCAPEDDOUBLEQUOTE) ) {
														int LA58_26 = input.LA(8);
														if ( (LA58_26==COLON||LA58_26==PUNCTUATION) ) {
															alt58=1;
														}
													}
												}
												}
												break;
											case IDENTIFIER:
												{
												int LA58_18 = input.LA(6);
												if ( (LA58_18==COLON||LA58_18==PUNCTUATION) ) {
													alt58=1;
												}
												}
												break;
										}
									}
								}
							}
							}
							break;
						case ESCAPEDDOUBLEQUOTE:
							{
							int LA58_3 = input.LA(2);
							if ( (LA58_3==IDENTIFIER) ) {
								int LA58_8 = input.LA(3);
								if ( (LA58_8==COLON||LA58_8==IDENTIFIER||LA58_8==PUNCTUATION) ) {
									alt58=1;
								}
								else if ( (LA58_8==ESCAPEDDOUBLEQUOTE) ) {
									int LA58_13 = input.LA(4);
									if ( (LA58_13==COLON||LA58_13==PUNCTUATION) ) {
										switch ( input.LA(5) ) {
											case BACKQUOTE:
												{
												int LA58_14 = input.LA(6);
												if ( (LA58_14==IDENTIFIER) ) {
													int LA58_19 = input.LA(7);
													if ( (LA58_19==BACKQUOTE) ) {
														int LA58_23 = input.LA(8);
														if ( (LA58_23==COLON||LA58_23==PUNCTUATION) ) {
															alt58=1;
														}
													}
												}
												}
												break;
											case SINGLEQUOTE:
												{
												int LA58_15 = input.LA(6);
												if ( (LA58_15==IDENTIFIER) ) {
													int LA58_20 = input.LA(7);
													if ( (LA58_20==SINGLEQUOTE) ) {
														int LA58_24 = input.LA(8);
														if ( (LA58_24==COLON||LA58_24==PUNCTUATION) ) {
															alt58=1;
														}
													}
												}
												}
												break;
											case DOUBLEQUOTE:
												{
												int LA58_16 = input.LA(6);
												if ( (LA58_16==IDENTIFIER) ) {
													int LA58_21 = input.LA(7);
													if ( (LA58_21==DOUBLEQUOTE) ) {
														int LA58_25 = input.LA(8);
														if ( (LA58_25==COLON||LA58_25==PUNCTUATION) ) {
															alt58=1;
														}
													}
												}
												}
												break;
											case ESCAPEDDOUBLEQUOTE:
												{
												int LA58_17 = input.LA(6);
												if ( (LA58_17==IDENTIFIER) ) {
													int LA58_22 = input.LA(7);
													if ( (LA58_22==ESCAPEDDOUBLEQUOTE) ) {
														int LA58_26 = input.LA(8);
														if ( (LA58_26==COLON||LA58_26==PUNCTUATION) ) {
															alt58=1;
														}
													}
												}
												}
												break;
											case IDENTIFIER:
												{
												int LA58_18 = input.LA(6);
												if ( (LA58_18==COLON||LA58_18==PUNCTUATION) ) {
													alt58=1;
												}
												}
												break;
										}
									}
								}
							}
							}
							break;
						case IDENTIFIER:
							{
							int LA58_4 = input.LA(2);
							if ( (LA58_4==COLON||LA58_4==PUNCTUATION) ) {
								switch ( input.LA(3) ) {
									case BACKQUOTE:
										{
										int LA58_14 = input.LA(4);
										if ( (LA58_14==IDENTIFIER) ) {
											int LA58_19 = input.LA(5);
											if ( (LA58_19==BACKQUOTE) ) {
												int LA58_23 = input.LA(6);
												if ( (LA58_23==COLON||LA58_23==PUNCTUATION) ) {
													alt58=1;
												}
											}
										}
										}
										break;
									case SINGLEQUOTE:
										{
										int LA58_15 = input.LA(4);
										if ( (LA58_15==IDENTIFIER) ) {
											int LA58_20 = input.LA(5);
											if ( (LA58_20==SINGLEQUOTE) ) {
												int LA58_24 = input.LA(6);
												if ( (LA58_24==COLON||LA58_24==PUNCTUATION) ) {
													alt58=1;
												}
											}
										}
										}
										break;
									case DOUBLEQUOTE:
										{
										int LA58_16 = input.LA(4);
										if ( (LA58_16==IDENTIFIER) ) {
											int LA58_21 = input.LA(5);
											if ( (LA58_21==DOUBLEQUOTE) ) {
												int LA58_25 = input.LA(6);
												if ( (LA58_25==COLON||LA58_25==PUNCTUATION) ) {
													alt58=1;
												}
											}
										}
										}
										break;
									case ESCAPEDDOUBLEQUOTE:
										{
										int LA58_17 = input.LA(4);
										if ( (LA58_17==IDENTIFIER) ) {
											int LA58_22 = input.LA(5);
											if ( (LA58_22==ESCAPEDDOUBLEQUOTE) ) {
												int LA58_26 = input.LA(6);
												if ( (LA58_26==COLON||LA58_26==PUNCTUATION) ) {
													alt58=1;
												}
											}
										}
										}
										break;
									case IDENTIFIER:
										{
										int LA58_18 = input.LA(4);
										if ( (LA58_18==COLON||LA58_18==PUNCTUATION) ) {
											alt58=1;
										}
										}
										break;
								}
							}
							}
							break;
					}
					switch (alt58) {
						case 1 :
							// JdbcGrammar.g:508:9: project columndivider
							{
							pushFollow(FOLLOW_project_in_sourcetable3123);
							project353=project();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_project.add(project353.getTree());
							pushFollow(FOLLOW_columndivider_in_sourcetable3125);
							columndivider354=columndivider();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_columndivider.add(columndivider354.getTree());
							}
							break;

					}

					pushFollow(FOLLOW_dataset_in_sourcetable3129);
					dataset355=dataset();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_dataset.add(dataset355.getTree());
					pushFollow(FOLLOW_columndivider_in_sourcetable3131);
					columndivider356=columndivider();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_columndivider.add(columndivider356.getTree());
					}
					break;

			}

			pushFollow(FOLLOW_srctablename_in_sourcetable3135);
			srctablename357=srctablename();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_srctablename.add(srctablename357.getTree());
			}

			// JdbcGrammar.g:509:7: ( alias )?
			int alt60=2;
			int LA60_0 = input.LA(1);
			if ( (LA60_0==ASKEYWORD||LA60_0==DOUBLEQUOTE||LA60_0==IDENTIFIER||LA60_0==SINGLEQUOTE) ) {
				alt60=1;
			}
			switch (alt60) {
				case 1 :
					// JdbcGrammar.g:509:9: alias
					{
					pushFollow(FOLLOW_alias_in_sourcetable3147);
					alias358=alias();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_alias.add(alias358.getTree());
					}
					break;

			}

			// JdbcGrammar.g:509:17: ( ']' )?
			int alt61=2;
			int LA61_0 = input.LA(1);
			if ( (LA61_0==139) ) {
				alt61=1;
			}
			switch (alt61) {
				case 1 :
					// JdbcGrammar.g:509:17: ']'
					{
					char_literal359=(Token)match(input,139,FOLLOW_139_in_sourcetable3151); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_139.add(char_literal359);

					}
					break;

			}

			}

			// AST REWRITE
			// elements: 139, dataset, project, columndivider, 138, srctablename, alias
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 510:6: -> ^( SOURCETABLE ( '[' )? ( project )? ( dataset )? srctablename ( alias )? ( columndivider )* ( ']' )? ^( TEXT TEXT[$sourcetable.text] ) )
			{
				// JdbcGrammar.g:510:8: ^( SOURCETABLE ( '[' )? ( project )? ( dataset )? srctablename ( alias )? ( columndivider )* ( ']' )? ^( TEXT TEXT[$sourcetable.text] ) )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(SOURCETABLE, "SOURCETABLE"), root_1);
				// JdbcGrammar.g:510:22: ( '[' )?
				if ( stream_138.hasNext() ) {
					adaptor.addChild(root_1, stream_138.nextNode());
				}
				stream_138.reset();

				// JdbcGrammar.g:510:27: ( project )?
				if ( stream_project.hasNext() ) {
					adaptor.addChild(root_1, stream_project.nextTree());
				}
				stream_project.reset();

				// JdbcGrammar.g:510:36: ( dataset )?
				if ( stream_dataset.hasNext() ) {
					adaptor.addChild(root_1, stream_dataset.nextTree());
				}
				stream_dataset.reset();

				adaptor.addChild(root_1, stream_srctablename.nextTree());
				// JdbcGrammar.g:510:58: ( alias )?
				if ( stream_alias.hasNext() ) {
					adaptor.addChild(root_1, stream_alias.nextTree());
				}
				stream_alias.reset();

				// JdbcGrammar.g:510:65: ( columndivider )*
				while ( stream_columndivider.hasNext() ) {
					adaptor.addChild(root_1, stream_columndivider.nextTree());
				}
				stream_columndivider.reset();

				// JdbcGrammar.g:510:80: ( ']' )?
				if ( stream_139.hasNext() ) {
					adaptor.addChild(root_1, stream_139.nextNode());
				}
				stream_139.reset();

				// JdbcGrammar.g:510:86: ^( TEXT TEXT[$sourcetable.text] )
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(TEXT, "TEXT"), root_2);
				adaptor.addChild(root_2, (Object)adaptor.create(TEXT, input.toString(retval.start,input.LT(-1))));
				adaptor.addChild(root_1, root_2);
				}

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "sourcetable"


	public static class srctablename_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "srctablename"
	// JdbcGrammar.g:513:1: srctablename : ( ( SINGLEQUOTE name SINGLEQUOTE ) | ( DOUBLEQUOTE name DOUBLEQUOTE ) | ( ESCAPEDDOUBLEQUOTE name ESCAPEDDOUBLEQUOTE ) | ( BACKQUOTE name BACKQUOTE ) | name );
	public final JdbcGrammarParser.srctablename_return srctablename() throws RecognitionException {
		JdbcGrammarParser.srctablename_return retval = new JdbcGrammarParser.srctablename_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token SINGLEQUOTE360=null;
		Token SINGLEQUOTE362=null;
		Token DOUBLEQUOTE363=null;
		Token DOUBLEQUOTE365=null;
		Token ESCAPEDDOUBLEQUOTE366=null;
		Token ESCAPEDDOUBLEQUOTE368=null;
		Token BACKQUOTE369=null;
		Token BACKQUOTE371=null;
		ParserRuleReturnScope name361 =null;
		ParserRuleReturnScope name364 =null;
		ParserRuleReturnScope name367 =null;
		ParserRuleReturnScope name370 =null;
		ParserRuleReturnScope name372 =null;

		Object SINGLEQUOTE360_tree=null;
		Object SINGLEQUOTE362_tree=null;
		Object DOUBLEQUOTE363_tree=null;
		Object DOUBLEQUOTE365_tree=null;
		Object ESCAPEDDOUBLEQUOTE366_tree=null;
		Object ESCAPEDDOUBLEQUOTE368_tree=null;
		Object BACKQUOTE369_tree=null;
		Object BACKQUOTE371_tree=null;

		try {
			// JdbcGrammar.g:513:13: ( ( SINGLEQUOTE name SINGLEQUOTE ) | ( DOUBLEQUOTE name DOUBLEQUOTE ) | ( ESCAPEDDOUBLEQUOTE name ESCAPEDDOUBLEQUOTE ) | ( BACKQUOTE name BACKQUOTE ) | name )
			int alt62=5;
			switch ( input.LA(1) ) {
			case SINGLEQUOTE:
				{
				alt62=1;
				}
				break;
			case DOUBLEQUOTE:
				{
				alt62=2;
				}
				break;
			case ESCAPEDDOUBLEQUOTE:
				{
				alt62=3;
				}
				break;
			case BACKQUOTE:
				{
				alt62=4;
				}
				break;
			case IDENTIFIER:
				{
				alt62=5;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 62, 0, input);
				throw nvae;
			}
			switch (alt62) {
				case 1 :
					// JdbcGrammar.g:514:2: ( SINGLEQUOTE name SINGLEQUOTE )
					{
					root_0 = (Object)adaptor.nil();


					// JdbcGrammar.g:514:2: ( SINGLEQUOTE name SINGLEQUOTE )
					// JdbcGrammar.g:514:2: SINGLEQUOTE name SINGLEQUOTE
					{
					SINGLEQUOTE360=(Token)match(input,SINGLEQUOTE,FOLLOW_SINGLEQUOTE_in_srctablename3199); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					SINGLEQUOTE360_tree = (Object)adaptor.create(SINGLEQUOTE360);
					adaptor.addChild(root_0, SINGLEQUOTE360_tree);
					}

					pushFollow(FOLLOW_name_in_srctablename3201);
					name361=name();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, name361.getTree());

					SINGLEQUOTE362=(Token)match(input,SINGLEQUOTE,FOLLOW_SINGLEQUOTE_in_srctablename3203); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					SINGLEQUOTE362_tree = (Object)adaptor.create(SINGLEQUOTE362);
					adaptor.addChild(root_0, SINGLEQUOTE362_tree);
					}

					}

					}
					break;
				case 2 :
					// JdbcGrammar.g:515:3: ( DOUBLEQUOTE name DOUBLEQUOTE )
					{
					root_0 = (Object)adaptor.nil();


					// JdbcGrammar.g:515:3: ( DOUBLEQUOTE name DOUBLEQUOTE )
					// JdbcGrammar.g:515:4: DOUBLEQUOTE name DOUBLEQUOTE
					{
					DOUBLEQUOTE363=(Token)match(input,DOUBLEQUOTE,FOLLOW_DOUBLEQUOTE_in_srctablename3209); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					DOUBLEQUOTE363_tree = (Object)adaptor.create(DOUBLEQUOTE363);
					adaptor.addChild(root_0, DOUBLEQUOTE363_tree);
					}

					pushFollow(FOLLOW_name_in_srctablename3211);
					name364=name();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, name364.getTree());

					DOUBLEQUOTE365=(Token)match(input,DOUBLEQUOTE,FOLLOW_DOUBLEQUOTE_in_srctablename3213); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					DOUBLEQUOTE365_tree = (Object)adaptor.create(DOUBLEQUOTE365);
					adaptor.addChild(root_0, DOUBLEQUOTE365_tree);
					}

					}

					}
					break;
				case 3 :
					// JdbcGrammar.g:516:3: ( ESCAPEDDOUBLEQUOTE name ESCAPEDDOUBLEQUOTE )
					{
					root_0 = (Object)adaptor.nil();


					// JdbcGrammar.g:516:3: ( ESCAPEDDOUBLEQUOTE name ESCAPEDDOUBLEQUOTE )
					// JdbcGrammar.g:516:4: ESCAPEDDOUBLEQUOTE name ESCAPEDDOUBLEQUOTE
					{
					ESCAPEDDOUBLEQUOTE366=(Token)match(input,ESCAPEDDOUBLEQUOTE,FOLLOW_ESCAPEDDOUBLEQUOTE_in_srctablename3219); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					ESCAPEDDOUBLEQUOTE366_tree = (Object)adaptor.create(ESCAPEDDOUBLEQUOTE366);
					adaptor.addChild(root_0, ESCAPEDDOUBLEQUOTE366_tree);
					}

					pushFollow(FOLLOW_name_in_srctablename3221);
					name367=name();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, name367.getTree());

					ESCAPEDDOUBLEQUOTE368=(Token)match(input,ESCAPEDDOUBLEQUOTE,FOLLOW_ESCAPEDDOUBLEQUOTE_in_srctablename3223); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					ESCAPEDDOUBLEQUOTE368_tree = (Object)adaptor.create(ESCAPEDDOUBLEQUOTE368);
					adaptor.addChild(root_0, ESCAPEDDOUBLEQUOTE368_tree);
					}

					}

					}
					break;
				case 4 :
					// JdbcGrammar.g:517:3: ( BACKQUOTE name BACKQUOTE )
					{
					root_0 = (Object)adaptor.nil();


					// JdbcGrammar.g:517:3: ( BACKQUOTE name BACKQUOTE )
					// JdbcGrammar.g:517:4: BACKQUOTE name BACKQUOTE
					{
					BACKQUOTE369=(Token)match(input,BACKQUOTE,FOLLOW_BACKQUOTE_in_srctablename3229); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					BACKQUOTE369_tree = (Object)adaptor.create(BACKQUOTE369);
					adaptor.addChild(root_0, BACKQUOTE369_tree);
					}

					pushFollow(FOLLOW_name_in_srctablename3231);
					name370=name();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, name370.getTree());

					BACKQUOTE371=(Token)match(input,BACKQUOTE,FOLLOW_BACKQUOTE_in_srctablename3233); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					BACKQUOTE371_tree = (Object)adaptor.create(BACKQUOTE371);
					adaptor.addChild(root_0, BACKQUOTE371_tree);
					}

					}

					}
					break;
				case 5 :
					// JdbcGrammar.g:518:3: name
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_name_in_srctablename3238);
					name372=name();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, name372.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "srctablename"


	public static class name_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "name"
	// JdbcGrammar.g:525:1: name : IDENTIFIER -> ^( NAME IDENTIFIER ) ;
	public final JdbcGrammarParser.name_return name() throws RecognitionException {
		JdbcGrammarParser.name_return retval = new JdbcGrammarParser.name_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token IDENTIFIER373=null;

		Object IDENTIFIER373_tree=null;
		RewriteRuleTokenStream stream_IDENTIFIER=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER");

		try {
			// JdbcGrammar.g:526:2: ( IDENTIFIER -> ^( NAME IDENTIFIER ) )
			// JdbcGrammar.g:527:2: IDENTIFIER
			{
			IDENTIFIER373=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_name3252); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_IDENTIFIER.add(IDENTIFIER373);

			// AST REWRITE
			// elements: IDENTIFIER
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 527:13: -> ^( NAME IDENTIFIER )
			{
				// JdbcGrammar.g:527:15: ^( NAME IDENTIFIER )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(NAME, "NAME"), root_1);
				adaptor.addChild(root_1, stream_IDENTIFIER.nextNode());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "name"


	public static class number_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "number"
	// JdbcGrammar.g:532:1: number : NUMBER -> ^( INTEGERPARAM NUMBER ) ;
	public final JdbcGrammarParser.number_return number() throws RecognitionException {
		JdbcGrammarParser.number_return retval = new JdbcGrammarParser.number_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token NUMBER374=null;

		Object NUMBER374_tree=null;
		RewriteRuleTokenStream stream_NUMBER=new RewriteRuleTokenStream(adaptor,"token NUMBER");

		try {
			// JdbcGrammar.g:533:4: ( NUMBER -> ^( INTEGERPARAM NUMBER ) )
			// JdbcGrammar.g:534:4: NUMBER
			{
			NUMBER374=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_number3281); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_NUMBER.add(NUMBER374);

			// AST REWRITE
			// elements: NUMBER
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 534:10: -> ^( INTEGERPARAM NUMBER )
			{
				// JdbcGrammar.g:534:12: ^( INTEGERPARAM NUMBER )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(INTEGERPARAM, "INTEGERPARAM"), root_1);
				adaptor.addChild(root_1, stream_NUMBER.nextNode());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "number"


	public static class expression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "expression"
	// JdbcGrammar.g:542:1: expression : ( '*' -> ^( EXPRESSION ^( JOKERCALL '*' ) ^( TEXT TEXT[$expression.text] ) ) | ( ( expressionpart ) ( COMMA expressionpart )* ) -> ^( EXPRESSION ( expressionpart )* ^( TEXT TEXT[$expression.text] ) ) );
	public final JdbcGrammarParser.expression_return expression() throws RecognitionException {
		JdbcGrammarParser.expression_return retval = new JdbcGrammarParser.expression_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal375=null;
		Token COMMA377=null;
		ParserRuleReturnScope expressionpart376 =null;
		ParserRuleReturnScope expressionpart378 =null;

		Object char_literal375_tree=null;
		Object COMMA377_tree=null;
		RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
		RewriteRuleTokenStream stream_129=new RewriteRuleTokenStream(adaptor,"token 129");
		RewriteRuleSubtreeStream stream_expressionpart=new RewriteRuleSubtreeStream(adaptor,"rule expressionpart");

		try {
			// JdbcGrammar.g:543:4: ( '*' -> ^( EXPRESSION ^( JOKERCALL '*' ) ^( TEXT TEXT[$expression.text] ) ) | ( ( expressionpart ) ( COMMA expressionpart )* ) -> ^( EXPRESSION ( expressionpart )* ^( TEXT TEXT[$expression.text] ) ) )
			int alt64=2;
			int LA64_0 = input.LA(1);
			if ( (LA64_0==129) ) {
				alt64=1;
			}
			else if ( (LA64_0==BACKSINGLEQUOTE||LA64_0==DOUBLEQUOTE||LA64_0==IDENTIFIER) ) {
				alt64=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 64, 0, input);
				throw nvae;
			}

			switch (alt64) {
				case 1 :
					// JdbcGrammar.g:544:4: '*'
					{
					char_literal375=(Token)match(input,129,FOLLOW_129_in_expression3308); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_129.add(char_literal375);

					// AST REWRITE
					// elements: 129
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 544:8: -> ^( EXPRESSION ^( JOKERCALL '*' ) ^( TEXT TEXT[$expression.text] ) )
					{
						// JdbcGrammar.g:544:10: ^( EXPRESSION ^( JOKERCALL '*' ) ^( TEXT TEXT[$expression.text] ) )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(EXPRESSION, "EXPRESSION"), root_1);
						// JdbcGrammar.g:544:23: ^( JOKERCALL '*' )
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(JOKERCALL, "JOKERCALL"), root_2);
						adaptor.addChild(root_2, stream_129.nextNode());
						adaptor.addChild(root_1, root_2);
						}

						// JdbcGrammar.g:544:40: ^( TEXT TEXT[$expression.text] )
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(TEXT, "TEXT"), root_2);
						adaptor.addChild(root_2, (Object)adaptor.create(TEXT, input.toString(retval.start,input.LT(-1))));
						adaptor.addChild(root_1, root_2);
						}

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// JdbcGrammar.g:546:4: ( ( expressionpart ) ( COMMA expressionpart )* )
					{
					// JdbcGrammar.g:546:4: ( ( expressionpart ) ( COMMA expressionpart )* )
					// JdbcGrammar.g:547:9: ( expressionpart ) ( COMMA expressionpart )*
					{
					// JdbcGrammar.g:547:9: ( expressionpart )
					// JdbcGrammar.g:547:11: expressionpart
					{
					pushFollow(FOLLOW_expressionpart_in_expression3353);
					expressionpart376=expressionpart();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_expressionpart.add(expressionpart376.getTree());
					}

					// JdbcGrammar.g:548:9: ( COMMA expressionpart )*
					loop63:
					while (true) {
						int alt63=2;
						int LA63_0 = input.LA(1);
						if ( (LA63_0==COMMA) ) {
							alt63=1;
						}

						switch (alt63) {
						case 1 :
							// JdbcGrammar.g:548:11: COMMA expressionpart
							{
							COMMA377=(Token)match(input,COMMA,FOLLOW_COMMA_in_expression3368); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_COMMA.add(COMMA377);

							pushFollow(FOLLOW_expressionpart_in_expression3370);
							expressionpart378=expressionpart();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_expressionpart.add(expressionpart378.getTree());
							}
							break;

						default :
							break loop63;
						}
					}

					}

					// AST REWRITE
					// elements: expressionpart
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 549:5: -> ^( EXPRESSION ( expressionpart )* ^( TEXT TEXT[$expression.text] ) )
					{
						// JdbcGrammar.g:549:7: ^( EXPRESSION ( expressionpart )* ^( TEXT TEXT[$expression.text] ) )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(EXPRESSION, "EXPRESSION"), root_1);
						// JdbcGrammar.g:549:20: ( expressionpart )*
						while ( stream_expressionpart.hasNext() ) {
							adaptor.addChild(root_1, stream_expressionpart.nextTree());
						}
						stream_expressionpart.reset();

						// JdbcGrammar.g:549:36: ^( TEXT TEXT[$expression.text] )
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(TEXT, "TEXT"), root_2);
						adaptor.addChild(root_2, (Object)adaptor.create(TEXT, input.toString(retval.start,input.LT(-1))));
						adaptor.addChild(root_1, root_2);
						}

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "expression"


	public static class expressionpart_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "expressionpart"
	// JdbcGrammar.g:552:1: expressionpart : ( column | functioncall | multiplecolumn );
	public final JdbcGrammarParser.expressionpart_return expressionpart() throws RecognitionException {
		JdbcGrammarParser.expressionpart_return retval = new JdbcGrammarParser.expressionpart_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope column379 =null;
		ParserRuleReturnScope functioncall380 =null;
		ParserRuleReturnScope multiplecolumn381 =null;


		try {
			// JdbcGrammar.g:552:15: ( column | functioncall | multiplecolumn )
			int alt65=3;
			alt65 = dfa65.predict(input);
			switch (alt65) {
				case 1 :
					// JdbcGrammar.g:553:1: column
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_column_in_expressionpart3400);
					column379=column();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, column379.getTree());

					}
					break;
				case 2 :
					// JdbcGrammar.g:553:10: functioncall
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_functioncall_in_expressionpart3404);
					functioncall380=functioncall();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, functioncall380.getTree());

					}
					break;
				case 3 :
					// JdbcGrammar.g:553:25: multiplecolumn
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_multiplecolumn_in_expressionpart3408);
					multiplecolumn381=multiplecolumn();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, multiplecolumn381.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "expressionpart"


	public static class multiplecolumn_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "multiplecolumn"
	// JdbcGrammar.g:560:1: multiplecolumn : ( ( scopealias PUNCTUATION )+ '*' ) -> ^( MULTIPLECALL ( scopealias )* ( PUNCTUATION )* '*' ^( TEXT TEXT[$multiplecolumn.text] ) ) ;
	public final JdbcGrammarParser.multiplecolumn_return multiplecolumn() throws RecognitionException {
		JdbcGrammarParser.multiplecolumn_return retval = new JdbcGrammarParser.multiplecolumn_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token PUNCTUATION383=null;
		Token char_literal384=null;
		ParserRuleReturnScope scopealias382 =null;

		Object PUNCTUATION383_tree=null;
		Object char_literal384_tree=null;
		RewriteRuleTokenStream stream_PUNCTUATION=new RewriteRuleTokenStream(adaptor,"token PUNCTUATION");
		RewriteRuleTokenStream stream_129=new RewriteRuleTokenStream(adaptor,"token 129");
		RewriteRuleSubtreeStream stream_scopealias=new RewriteRuleSubtreeStream(adaptor,"rule scopealias");

		try {
			// JdbcGrammar.g:561:5: ( ( ( scopealias PUNCTUATION )+ '*' ) -> ^( MULTIPLECALL ( scopealias )* ( PUNCTUATION )* '*' ^( TEXT TEXT[$multiplecolumn.text] ) ) )
			// JdbcGrammar.g:562:5: ( ( scopealias PUNCTUATION )+ '*' )
			{
			// JdbcGrammar.g:562:5: ( ( scopealias PUNCTUATION )+ '*' )
			// JdbcGrammar.g:563:6: ( scopealias PUNCTUATION )+ '*'
			{
			// JdbcGrammar.g:563:6: ( scopealias PUNCTUATION )+
			int cnt66=0;
			loop66:
			while (true) {
				int alt66=2;
				int LA66_0 = input.LA(1);
				if ( (LA66_0==BACKSINGLEQUOTE||LA66_0==DOUBLEQUOTE||LA66_0==IDENTIFIER) ) {
					alt66=1;
				}

				switch (alt66) {
				case 1 :
					// JdbcGrammar.g:563:7: scopealias PUNCTUATION
					{
					pushFollow(FOLLOW_scopealias_in_multiplecolumn3432);
					scopealias382=scopealias();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_scopealias.add(scopealias382.getTree());
					PUNCTUATION383=(Token)match(input,PUNCTUATION,FOLLOW_PUNCTUATION_in_multiplecolumn3434); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_PUNCTUATION.add(PUNCTUATION383);

					}
					break;

				default :
					if ( cnt66 >= 1 ) break loop66;
					if (state.backtracking>0) {state.failed=true; return retval;}
					EarlyExitException eee = new EarlyExitException(66, input);
					throw eee;
				}
				cnt66++;
			}

			char_literal384=(Token)match(input,129,FOLLOW_129_in_multiplecolumn3443); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_129.add(char_literal384);

			}

			// AST REWRITE
			// elements: 129, PUNCTUATION, scopealias
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 565:6: -> ^( MULTIPLECALL ( scopealias )* ( PUNCTUATION )* '*' ^( TEXT TEXT[$multiplecolumn.text] ) )
			{
				// JdbcGrammar.g:565:8: ^( MULTIPLECALL ( scopealias )* ( PUNCTUATION )* '*' ^( TEXT TEXT[$multiplecolumn.text] ) )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(MULTIPLECALL, "MULTIPLECALL"), root_1);
				// JdbcGrammar.g:565:23: ( scopealias )*
				while ( stream_scopealias.hasNext() ) {
					adaptor.addChild(root_1, stream_scopealias.nextTree());
				}
				stream_scopealias.reset();

				// JdbcGrammar.g:565:37: ( PUNCTUATION )*
				while ( stream_PUNCTUATION.hasNext() ) {
					adaptor.addChild(root_1, stream_PUNCTUATION.nextNode());
				}
				stream_PUNCTUATION.reset();

				adaptor.addChild(root_1, stream_129.nextNode());
				// JdbcGrammar.g:565:54: ^( TEXT TEXT[$multiplecolumn.text] )
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(TEXT, "TEXT"), root_2);
				adaptor.addChild(root_2, (Object)adaptor.create(TEXT, input.toString(retval.start,input.LT(-1))));
				adaptor.addChild(root_1, root_2);
				}

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "multiplecolumn"


	public static class scopealias_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "scopealias"
	// JdbcGrammar.g:568:1: scopealias : ( IDENTIFIER | ( BACKSINGLEQUOTE IDENTIFIER BACKSINGLEQUOTE ) | ( DOUBLEQUOTE IDENTIFIER DOUBLEQUOTE ) ) -> ^( SCOPE IDENTIFIER ) ;
	public final JdbcGrammarParser.scopealias_return scopealias() throws RecognitionException {
		JdbcGrammarParser.scopealias_return retval = new JdbcGrammarParser.scopealias_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token IDENTIFIER385=null;
		Token BACKSINGLEQUOTE386=null;
		Token IDENTIFIER387=null;
		Token BACKSINGLEQUOTE388=null;
		Token DOUBLEQUOTE389=null;
		Token IDENTIFIER390=null;
		Token DOUBLEQUOTE391=null;

		Object IDENTIFIER385_tree=null;
		Object BACKSINGLEQUOTE386_tree=null;
		Object IDENTIFIER387_tree=null;
		Object BACKSINGLEQUOTE388_tree=null;
		Object DOUBLEQUOTE389_tree=null;
		Object IDENTIFIER390_tree=null;
		Object DOUBLEQUOTE391_tree=null;
		RewriteRuleTokenStream stream_BACKSINGLEQUOTE=new RewriteRuleTokenStream(adaptor,"token BACKSINGLEQUOTE");
		RewriteRuleTokenStream stream_DOUBLEQUOTE=new RewriteRuleTokenStream(adaptor,"token DOUBLEQUOTE");
		RewriteRuleTokenStream stream_IDENTIFIER=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER");

		try {
			// JdbcGrammar.g:569:2: ( ( IDENTIFIER | ( BACKSINGLEQUOTE IDENTIFIER BACKSINGLEQUOTE ) | ( DOUBLEQUOTE IDENTIFIER DOUBLEQUOTE ) ) -> ^( SCOPE IDENTIFIER ) )
			// JdbcGrammar.g:570:2: ( IDENTIFIER | ( BACKSINGLEQUOTE IDENTIFIER BACKSINGLEQUOTE ) | ( DOUBLEQUOTE IDENTIFIER DOUBLEQUOTE ) )
			{
			// JdbcGrammar.g:570:2: ( IDENTIFIER | ( BACKSINGLEQUOTE IDENTIFIER BACKSINGLEQUOTE ) | ( DOUBLEQUOTE IDENTIFIER DOUBLEQUOTE ) )
			int alt67=3;
			switch ( input.LA(1) ) {
			case IDENTIFIER:
				{
				alt67=1;
				}
				break;
			case BACKSINGLEQUOTE:
				{
				alt67=2;
				}
				break;
			case DOUBLEQUOTE:
				{
				alt67=3;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 67, 0, input);
				throw nvae;
			}
			switch (alt67) {
				case 1 :
					// JdbcGrammar.g:570:2: IDENTIFIER
					{
					IDENTIFIER385=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_scopealias3485); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IDENTIFIER.add(IDENTIFIER385);

					}
					break;
				case 2 :
					// JdbcGrammar.g:571:2: ( BACKSINGLEQUOTE IDENTIFIER BACKSINGLEQUOTE )
					{
					// JdbcGrammar.g:571:2: ( BACKSINGLEQUOTE IDENTIFIER BACKSINGLEQUOTE )
					// JdbcGrammar.g:571:2: BACKSINGLEQUOTE IDENTIFIER BACKSINGLEQUOTE
					{
					BACKSINGLEQUOTE386=(Token)match(input,BACKSINGLEQUOTE,FOLLOW_BACKSINGLEQUOTE_in_scopealias3491); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_BACKSINGLEQUOTE.add(BACKSINGLEQUOTE386);

					IDENTIFIER387=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_scopealias3493); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IDENTIFIER.add(IDENTIFIER387);

					BACKSINGLEQUOTE388=(Token)match(input,BACKSINGLEQUOTE,FOLLOW_BACKSINGLEQUOTE_in_scopealias3495); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_BACKSINGLEQUOTE.add(BACKSINGLEQUOTE388);

					}

					}
					break;
				case 3 :
					// JdbcGrammar.g:572:2: ( DOUBLEQUOTE IDENTIFIER DOUBLEQUOTE )
					{
					// JdbcGrammar.g:572:2: ( DOUBLEQUOTE IDENTIFIER DOUBLEQUOTE )
					// JdbcGrammar.g:572:2: DOUBLEQUOTE IDENTIFIER DOUBLEQUOTE
					{
					DOUBLEQUOTE389=(Token)match(input,DOUBLEQUOTE,FOLLOW_DOUBLEQUOTE_in_scopealias3501); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_DOUBLEQUOTE.add(DOUBLEQUOTE389);

					IDENTIFIER390=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_scopealias3503); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IDENTIFIER.add(IDENTIFIER390);

					DOUBLEQUOTE391=(Token)match(input,DOUBLEQUOTE,FOLLOW_DOUBLEQUOTE_in_scopealias3505); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_DOUBLEQUOTE.add(DOUBLEQUOTE391);

					}

					}
					break;

			}

			// AST REWRITE
			// elements: IDENTIFIER
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 573:3: -> ^( SCOPE IDENTIFIER )
			{
				// JdbcGrammar.g:573:5: ^( SCOPE IDENTIFIER )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(SCOPE, "SCOPE"), root_1);
				adaptor.addChild(root_1, stream_IDENTIFIER.nextNode());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "scopealias"


	public static class razoralias_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "razoralias"
	// JdbcGrammar.g:577:1: razoralias : DOUBLEQUOTE IDENTIFIER ( ( COLON | PUNCTUATION ) IDENTIFIER )+ DOUBLEQUOTE ;
	public final JdbcGrammarParser.razoralias_return razoralias() throws RecognitionException {
		JdbcGrammarParser.razoralias_return retval = new JdbcGrammarParser.razoralias_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token DOUBLEQUOTE392=null;
		Token IDENTIFIER393=null;
		Token set394=null;
		Token IDENTIFIER395=null;
		Token DOUBLEQUOTE396=null;

		Object DOUBLEQUOTE392_tree=null;
		Object IDENTIFIER393_tree=null;
		Object set394_tree=null;
		Object IDENTIFIER395_tree=null;
		Object DOUBLEQUOTE396_tree=null;

		try {
			// JdbcGrammar.g:578:1: ( DOUBLEQUOTE IDENTIFIER ( ( COLON | PUNCTUATION ) IDENTIFIER )+ DOUBLEQUOTE )
			// JdbcGrammar.g:579:1: DOUBLEQUOTE IDENTIFIER ( ( COLON | PUNCTUATION ) IDENTIFIER )+ DOUBLEQUOTE
			{
			root_0 = (Object)adaptor.nil();


			DOUBLEQUOTE392=(Token)match(input,DOUBLEQUOTE,FOLLOW_DOUBLEQUOTE_in_razoralias3526); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			DOUBLEQUOTE392_tree = (Object)adaptor.create(DOUBLEQUOTE392);
			adaptor.addChild(root_0, DOUBLEQUOTE392_tree);
			}

			IDENTIFIER393=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_razoralias3528); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			IDENTIFIER393_tree = (Object)adaptor.create(IDENTIFIER393);
			adaptor.addChild(root_0, IDENTIFIER393_tree);
			}

			// JdbcGrammar.g:579:24: ( ( COLON | PUNCTUATION ) IDENTIFIER )+
			int cnt68=0;
			loop68:
			while (true) {
				int alt68=2;
				int LA68_0 = input.LA(1);
				if ( (LA68_0==COLON||LA68_0==PUNCTUATION) ) {
					alt68=1;
				}

				switch (alt68) {
				case 1 :
					// JdbcGrammar.g:579:25: ( COLON | PUNCTUATION ) IDENTIFIER
					{
					set394=input.LT(1);
					if ( input.LA(1)==COLON||input.LA(1)==PUNCTUATION ) {
						input.consume();
						if ( state.backtracking==0 ) adaptor.addChild(root_0, (Object)adaptor.create(set394));
						state.errorRecovery=false;
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						throw mse;
					}
					IDENTIFIER395=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_razoralias3539); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					IDENTIFIER395_tree = (Object)adaptor.create(IDENTIFIER395);
					adaptor.addChild(root_0, IDENTIFIER395_tree);
					}

					}
					break;

				default :
					if ( cnt68 >= 1 ) break loop68;
					if (state.backtracking>0) {state.failed=true; return retval;}
					EarlyExitException eee = new EarlyExitException(68, input);
					throw eee;
				}
				cnt68++;
			}

			DOUBLEQUOTE396=(Token)match(input,DOUBLEQUOTE,FOLLOW_DOUBLEQUOTE_in_razoralias3543); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			DOUBLEQUOTE396_tree = (Object)adaptor.create(DOUBLEQUOTE396);
			adaptor.addChild(root_0, DOUBLEQUOTE396_tree);
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "razoralias"


	public static class functioncall_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "functioncall"
	// JdbcGrammar.g:589:1: functioncall : ( name LPARAM ( DISTINCT )? functionparameters RPARAM ( alias )? ) -> ^( FUNCTIONCALL name functionparameters ( alias )? ^( TEXT TEXT[$functioncall.text] ) ) ;
	public final JdbcGrammarParser.functioncall_return functioncall() throws RecognitionException {
		JdbcGrammarParser.functioncall_return retval = new JdbcGrammarParser.functioncall_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token LPARAM398=null;
		Token DISTINCT399=null;
		Token RPARAM401=null;
		ParserRuleReturnScope name397 =null;
		ParserRuleReturnScope functionparameters400 =null;
		ParserRuleReturnScope alias402 =null;

		Object LPARAM398_tree=null;
		Object DISTINCT399_tree=null;
		Object RPARAM401_tree=null;
		RewriteRuleTokenStream stream_LPARAM=new RewriteRuleTokenStream(adaptor,"token LPARAM");
		RewriteRuleTokenStream stream_RPARAM=new RewriteRuleTokenStream(adaptor,"token RPARAM");
		RewriteRuleTokenStream stream_DISTINCT=new RewriteRuleTokenStream(adaptor,"token DISTINCT");
		RewriteRuleSubtreeStream stream_functionparameters=new RewriteRuleSubtreeStream(adaptor,"rule functionparameters");
		RewriteRuleSubtreeStream stream_alias=new RewriteRuleSubtreeStream(adaptor,"rule alias");
		RewriteRuleSubtreeStream stream_name=new RewriteRuleSubtreeStream(adaptor,"rule name");

		try {
			// JdbcGrammar.g:590:3: ( ( name LPARAM ( DISTINCT )? functionparameters RPARAM ( alias )? ) -> ^( FUNCTIONCALL name functionparameters ( alias )? ^( TEXT TEXT[$functioncall.text] ) ) )
			// JdbcGrammar.g:591:3: ( name LPARAM ( DISTINCT )? functionparameters RPARAM ( alias )? )
			{
			// JdbcGrammar.g:591:3: ( name LPARAM ( DISTINCT )? functionparameters RPARAM ( alias )? )
			// JdbcGrammar.g:592:4: name LPARAM ( DISTINCT )? functionparameters RPARAM ( alias )?
			{
			pushFollow(FOLLOW_name_in_functioncall3569);
			name397=name();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_name.add(name397.getTree());
			LPARAM398=(Token)match(input,LPARAM,FOLLOW_LPARAM_in_functioncall3571); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_LPARAM.add(LPARAM398);

			// JdbcGrammar.g:592:16: ( DISTINCT )?
			int alt69=2;
			int LA69_0 = input.LA(1);
			if ( (LA69_0==DISTINCT) ) {
				alt69=1;
			}
			switch (alt69) {
				case 1 :
					// JdbcGrammar.g:592:16: DISTINCT
					{
					DISTINCT399=(Token)match(input,DISTINCT,FOLLOW_DISTINCT_in_functioncall3573); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_DISTINCT.add(DISTINCT399);

					}
					break;

			}

			pushFollow(FOLLOW_functionparameters_in_functioncall3576);
			functionparameters400=functionparameters();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_functionparameters.add(functionparameters400.getTree());
			RPARAM401=(Token)match(input,RPARAM,FOLLOW_RPARAM_in_functioncall3578); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_RPARAM.add(RPARAM401);

			// JdbcGrammar.g:593:4: ( alias )?
			int alt70=2;
			int LA70_0 = input.LA(1);
			if ( (LA70_0==ASKEYWORD||LA70_0==DOUBLEQUOTE||LA70_0==IDENTIFIER||LA70_0==SINGLEQUOTE) ) {
				alt70=1;
			}
			switch (alt70) {
				case 1 :
					// JdbcGrammar.g:593:6: alias
					{
					pushFollow(FOLLOW_alias_in_functioncall3585);
					alias402=alias();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_alias.add(alias402.getTree());
					}
					break;

			}

			}

			// AST REWRITE
			// elements: functionparameters, name, alias
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 594:4: -> ^( FUNCTIONCALL name functionparameters ( alias )? ^( TEXT TEXT[$functioncall.text] ) )
			{
				// JdbcGrammar.g:594:6: ^( FUNCTIONCALL name functionparameters ( alias )? ^( TEXT TEXT[$functioncall.text] ) )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(FUNCTIONCALL, "FUNCTIONCALL"), root_1);
				adaptor.addChild(root_1, stream_name.nextTree());
				adaptor.addChild(root_1, stream_functionparameters.nextTree());
				// JdbcGrammar.g:594:46: ( alias )?
				if ( stream_alias.hasNext() ) {
					adaptor.addChild(root_1, stream_alias.nextTree());
				}
				stream_alias.reset();

				// JdbcGrammar.g:594:53: ^( TEXT TEXT[$functioncall.text] )
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(TEXT, "TEXT"), root_2);
				adaptor.addChild(root_2, (Object)adaptor.create(TEXT, input.toString(retval.start,input.LT(-1))));
				adaptor.addChild(root_1, root_2);
				}

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "functioncall"


	public static class functionparameters_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "functionparameters"
	// JdbcGrammar.g:599:1: functionparameters : (| ( ( functionparameterresume ) ( COMMA functionparameterresume )* ) | joker ) -> ^( FUNCTIONPARAMETERS ( functionparameterresume )* ( joker )? ^( TEXT TEXT[$functionparameters.text] ) ) ;
	public final JdbcGrammarParser.functionparameters_return functionparameters() throws RecognitionException {
		JdbcGrammarParser.functionparameters_return retval = new JdbcGrammarParser.functionparameters_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token COMMA404=null;
		ParserRuleReturnScope functionparameterresume403 =null;
		ParserRuleReturnScope functionparameterresume405 =null;
		ParserRuleReturnScope joker406 =null;

		Object COMMA404_tree=null;
		RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
		RewriteRuleSubtreeStream stream_joker=new RewriteRuleSubtreeStream(adaptor,"rule joker");
		RewriteRuleSubtreeStream stream_functionparameterresume=new RewriteRuleSubtreeStream(adaptor,"rule functionparameterresume");

		try {
			// JdbcGrammar.g:600:5: ( (| ( ( functionparameterresume ) ( COMMA functionparameterresume )* ) | joker ) -> ^( FUNCTIONPARAMETERS ( functionparameterresume )* ( joker )? ^( TEXT TEXT[$functionparameters.text] ) ) )
			// JdbcGrammar.g:601:5: (| ( ( functionparameterresume ) ( COMMA functionparameterresume )* ) | joker )
			{
			// JdbcGrammar.g:601:5: (| ( ( functionparameterresume ) ( COMMA functionparameterresume )* ) | joker )
			int alt72=3;
			switch ( input.LA(1) ) {
			case RPARAM:
				{
				alt72=1;
				}
				break;
			case BACKSINGLEQUOTE:
			case DOUBLEQUOTE:
			case IDENTIFIER:
			case NUMBER:
			case SINGLEQUOTE:
				{
				alt72=2;
				}
				break;
			case 129:
				{
				alt72=3;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 72, 0, input);
				throw nvae;
			}
			switch (alt72) {
				case 1 :
					// JdbcGrammar.g:603:9: 
					{
					}
					break;
				case 2 :
					// JdbcGrammar.g:603:10: ( ( functionparameterresume ) ( COMMA functionparameterresume )* )
					{
					// JdbcGrammar.g:603:10: ( ( functionparameterresume ) ( COMMA functionparameterresume )* )
					// JdbcGrammar.g:604:13: ( functionparameterresume ) ( COMMA functionparameterresume )*
					{
					// JdbcGrammar.g:604:13: ( functionparameterresume )
					// JdbcGrammar.g:604:14: functionparameterresume
					{
					pushFollow(FOLLOW_functionparameterresume_in_functionparameters3663);
					functionparameterresume403=functionparameterresume();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_functionparameterresume.add(functionparameterresume403.getTree());
					}

					// JdbcGrammar.g:605:13: ( COMMA functionparameterresume )*
					loop71:
					while (true) {
						int alt71=2;
						int LA71_0 = input.LA(1);
						if ( (LA71_0==COMMA) ) {
							alt71=1;
						}

						switch (alt71) {
						case 1 :
							// JdbcGrammar.g:605:14: COMMA functionparameterresume
							{
							COMMA404=(Token)match(input,COMMA,FOLLOW_COMMA_in_functionparameters3680); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_COMMA.add(COMMA404);

							pushFollow(FOLLOW_functionparameterresume_in_functionparameters3682);
							functionparameterresume405=functionparameterresume();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_functionparameterresume.add(functionparameterresume405.getTree());
							}
							break;

						default :
							break loop71;
						}
					}

					}

					}
					break;
				case 3 :
					// JdbcGrammar.g:607:11: joker
					{
					pushFollow(FOLLOW_joker_in_functionparameters3708);
					joker406=joker();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_joker.add(joker406.getTree());
					}
					break;

			}

			// AST REWRITE
			// elements: joker, functionparameterresume
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 608:6: -> ^( FUNCTIONPARAMETERS ( functionparameterresume )* ( joker )? ^( TEXT TEXT[$functionparameters.text] ) )
			{
				// JdbcGrammar.g:608:8: ^( FUNCTIONPARAMETERS ( functionparameterresume )* ( joker )? ^( TEXT TEXT[$functionparameters.text] ) )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(FUNCTIONPARAMETERS, "FUNCTIONPARAMETERS"), root_1);
				// JdbcGrammar.g:608:29: ( functionparameterresume )*
				while ( stream_functionparameterresume.hasNext() ) {
					adaptor.addChild(root_1, stream_functionparameterresume.nextTree());
				}
				stream_functionparameterresume.reset();

				// JdbcGrammar.g:608:54: ( joker )?
				if ( stream_joker.hasNext() ) {
					adaptor.addChild(root_1, stream_joker.nextTree());
				}
				stream_joker.reset();

				// JdbcGrammar.g:608:61: ^( TEXT TEXT[$functionparameters.text] )
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(TEXT, "TEXT"), root_2);
				adaptor.addChild(root_2, (Object)adaptor.create(TEXT, input.toString(retval.start,input.LT(-1))));
				adaptor.addChild(root_1, root_2);
				}

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "functionparameters"


	public static class joker_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "joker"
	// JdbcGrammar.g:612:1: joker : ( '*' ) -> ^( JOKER JOKER ) ;
	public final JdbcGrammarParser.joker_return joker() throws RecognitionException {
		JdbcGrammarParser.joker_return retval = new JdbcGrammarParser.joker_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal407=null;

		Object char_literal407_tree=null;
		RewriteRuleTokenStream stream_129=new RewriteRuleTokenStream(adaptor,"token 129");

		try {
			// JdbcGrammar.g:612:6: ( ( '*' ) -> ^( JOKER JOKER ) )
			// JdbcGrammar.g:613:2: ( '*' )
			{
			// JdbcGrammar.g:613:2: ( '*' )
			// JdbcGrammar.g:613:2: '*'
			{
			char_literal407=(Token)match(input,129,FOLLOW_129_in_joker3747); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_129.add(char_literal407);

			}

			// AST REWRITE
			// elements: 
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 613:6: -> ^( JOKER JOKER )
			{
				// JdbcGrammar.g:613:8: ^( JOKER JOKER )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(JOKER, "JOKER"), root_1);
				adaptor.addChild(root_1, (Object)adaptor.create(JOKER, "JOKER"));
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "joker"


	public static class functionparameterresume_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "functionparameterresume"
	// JdbcGrammar.g:616:1: functionparameterresume : ( mystringliteral | column | number ) ;
	public final JdbcGrammarParser.functionparameterresume_return functionparameterresume() throws RecognitionException {
		JdbcGrammarParser.functionparameterresume_return retval = new JdbcGrammarParser.functionparameterresume_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope mystringliteral408 =null;
		ParserRuleReturnScope column409 =null;
		ParserRuleReturnScope number410 =null;


		try {
			// JdbcGrammar.g:617:2: ( ( mystringliteral | column | number ) )
			// JdbcGrammar.g:618:2: ( mystringliteral | column | number )
			{
			root_0 = (Object)adaptor.nil();


			// JdbcGrammar.g:618:2: ( mystringliteral | column | number )
			int alt73=3;
			switch ( input.LA(1) ) {
			case SINGLEQUOTE:
				{
				alt73=1;
				}
				break;
			case BACKSINGLEQUOTE:
			case DOUBLEQUOTE:
			case IDENTIFIER:
				{
				alt73=2;
				}
				break;
			case NUMBER:
				{
				alt73=3;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 73, 0, input);
				throw nvae;
			}
			switch (alt73) {
				case 1 :
					// JdbcGrammar.g:618:2: mystringliteral
					{
					pushFollow(FOLLOW_mystringliteral_in_functionparameterresume3764);
					mystringliteral408=mystringliteral();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, mystringliteral408.getTree());

					}
					break;
				case 2 :
					// JdbcGrammar.g:618:20: column
					{
					pushFollow(FOLLOW_column_in_functionparameterresume3768);
					column409=column();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, column409.getTree());

					}
					break;
				case 3 :
					// JdbcGrammar.g:618:29: number
					{
					pushFollow(FOLLOW_number_in_functionparameterresume3772);
					number410=number();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, number410.getTree());

					}
					break;

			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "functionparameterresume"


	public static class mystringliteral_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "mystringliteral"
	// JdbcGrammar.g:623:1: mystringliteral : literal -> ^( STRINGLIT literal ^( TEXT TEXT[$mystringliteral.text] ) ) ;
	public final JdbcGrammarParser.mystringliteral_return mystringliteral() throws RecognitionException {
		JdbcGrammarParser.mystringliteral_return retval = new JdbcGrammarParser.mystringliteral_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope literal411 =null;

		RewriteRuleSubtreeStream stream_literal=new RewriteRuleSubtreeStream(adaptor,"rule literal");

		try {
			// JdbcGrammar.g:624:1: ( literal -> ^( STRINGLIT literal ^( TEXT TEXT[$mystringliteral.text] ) ) )
			// JdbcGrammar.g:625:1: literal
			{
			pushFollow(FOLLOW_literal_in_mystringliteral3785);
			literal411=literal();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_literal.add(literal411.getTree());
			// AST REWRITE
			// elements: literal
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 625:8: -> ^( STRINGLIT literal ^( TEXT TEXT[$mystringliteral.text] ) )
			{
				// JdbcGrammar.g:625:10: ^( STRINGLIT literal ^( TEXT TEXT[$mystringliteral.text] ) )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(STRINGLIT, "STRINGLIT"), root_1);
				adaptor.addChild(root_1, stream_literal.nextTree());
				// JdbcGrammar.g:625:30: ^( TEXT TEXT[$mystringliteral.text] )
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(TEXT, "TEXT"), root_2);
				adaptor.addChild(root_2, (Object)adaptor.create(TEXT, input.toString(retval.start,input.LT(-1))));
				adaptor.addChild(root_1, root_2);
				}

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "mystringliteral"


	public static class literal_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "literal"
	// JdbcGrammar.g:627:1: literal : ( SINGLEQUOTE ! (~ ( SINGLEQUOTE | NL ) | ( ESCAPEDSINGLEQUOTE ) )* SINGLEQUOTE !) ;
	public final JdbcGrammarParser.literal_return literal() throws RecognitionException {
		JdbcGrammarParser.literal_return retval = new JdbcGrammarParser.literal_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token SINGLEQUOTE412=null;
		Token set413=null;
		Token SINGLEQUOTE414=null;

		Object SINGLEQUOTE412_tree=null;
		Object set413_tree=null;
		Object SINGLEQUOTE414_tree=null;

		try {
			// JdbcGrammar.g:628:2: ( ( SINGLEQUOTE ! (~ ( SINGLEQUOTE | NL ) | ( ESCAPEDSINGLEQUOTE ) )* SINGLEQUOTE !) )
			// JdbcGrammar.g:629:2: ( SINGLEQUOTE ! (~ ( SINGLEQUOTE | NL ) | ( ESCAPEDSINGLEQUOTE ) )* SINGLEQUOTE !)
			{
			root_0 = (Object)adaptor.nil();


			// JdbcGrammar.g:629:2: ( SINGLEQUOTE ! (~ ( SINGLEQUOTE | NL ) | ( ESCAPEDSINGLEQUOTE ) )* SINGLEQUOTE !)
			// JdbcGrammar.g:629:2: SINGLEQUOTE ! (~ ( SINGLEQUOTE | NL ) | ( ESCAPEDSINGLEQUOTE ) )* SINGLEQUOTE !
			{
			SINGLEQUOTE412=(Token)match(input,SINGLEQUOTE,FOLLOW_SINGLEQUOTE_in_literal3807); if (state.failed) return retval;
			// JdbcGrammar.g:629:15: (~ ( SINGLEQUOTE | NL ) | ( ESCAPEDSINGLEQUOTE ) )*
			loop74:
			while (true) {
				int alt74=2;
				int LA74_0 = input.LA(1);
				if ( ((LA74_0 >= A && LA74_0 <= NEGATION)||(LA74_0 >= NOTKEYWORD && LA74_0 <= SEMICOLON)||(LA74_0 >= SOURCETABLE && LA74_0 <= 139)) ) {
					alt74=1;
				}

				switch (alt74) {
				case 1 :
					// JdbcGrammar.g:
					{
					set413=input.LT(1);
					if ( (input.LA(1) >= A && input.LA(1) <= NEGATION)||(input.LA(1) >= NOTKEYWORD && input.LA(1) <= SEMICOLON)||(input.LA(1) >= SOURCETABLE && input.LA(1) <= 139) ) {
						input.consume();
						if ( state.backtracking==0 ) adaptor.addChild(root_0, (Object)adaptor.create(set413));
						state.errorRecovery=false;
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						throw mse;
					}
					}
					break;

				default :
					break loop74;
				}
			}

			SINGLEQUOTE414=(Token)match(input,SINGLEQUOTE,FOLLOW_SINGLEQUOTE_in_literal3829); if (state.failed) return retval;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "literal"


	public static class stringliteral_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "stringliteral"
	// JdbcGrammar.g:633:1: stringliteral : stringliteralnode -> ^( STRINGLIT stringliteralnode ) ;
	public final JdbcGrammarParser.stringliteral_return stringliteral() throws RecognitionException {
		JdbcGrammarParser.stringliteral_return retval = new JdbcGrammarParser.stringliteral_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope stringliteralnode415 =null;

		RewriteRuleSubtreeStream stream_stringliteralnode=new RewriteRuleSubtreeStream(adaptor,"rule stringliteralnode");

		try {
			// JdbcGrammar.g:634:3: ( stringliteralnode -> ^( STRINGLIT stringliteralnode ) )
			// JdbcGrammar.g:635:3: stringliteralnode
			{
			pushFollow(FOLLOW_stringliteralnode_in_stringliteral3844);
			stringliteralnode415=stringliteralnode();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_stringliteralnode.add(stringliteralnode415.getTree());
			// AST REWRITE
			// elements: stringliteralnode
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 635:21: -> ^( STRINGLIT stringliteralnode )
			{
				// JdbcGrammar.g:635:23: ^( STRINGLIT stringliteralnode )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(STRINGLIT, "STRINGLIT"), root_1);
				adaptor.addChild(root_1, stream_stringliteralnode.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "stringliteral"


	public static class stringliteralnode_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "stringliteralnode"
	// JdbcGrammar.g:638:1: stringliteralnode : ( ( ( SINGLEQUOTE ) ! (~ ( SINGLEQUOTE | NL ) | ( ESCAPEDSINGLEQUOTE ) )* ( SINGLEQUOTE ) !) | ( ( DOUBLEQUOTE ) ! (~ ( DOUBLEQUOTE | NL ) | ( ESCAPEDDOUBLEQUOTE ) )* ( DOUBLEQUOTE ) !) );
	public final JdbcGrammarParser.stringliteralnode_return stringliteralnode() throws RecognitionException {
		JdbcGrammarParser.stringliteralnode_return retval = new JdbcGrammarParser.stringliteralnode_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token SINGLEQUOTE416=null;
		Token set417=null;
		Token SINGLEQUOTE418=null;
		Token DOUBLEQUOTE419=null;
		Token set420=null;
		Token DOUBLEQUOTE421=null;

		Object SINGLEQUOTE416_tree=null;
		Object set417_tree=null;
		Object SINGLEQUOTE418_tree=null;
		Object DOUBLEQUOTE419_tree=null;
		Object set420_tree=null;
		Object DOUBLEQUOTE421_tree=null;

		try {
			// JdbcGrammar.g:638:18: ( ( ( SINGLEQUOTE ) ! (~ ( SINGLEQUOTE | NL ) | ( ESCAPEDSINGLEQUOTE ) )* ( SINGLEQUOTE ) !) | ( ( DOUBLEQUOTE ) ! (~ ( DOUBLEQUOTE | NL ) | ( ESCAPEDDOUBLEQUOTE ) )* ( DOUBLEQUOTE ) !) )
			int alt77=2;
			int LA77_0 = input.LA(1);
			if ( (LA77_0==SINGLEQUOTE) ) {
				alt77=1;
			}
			else if ( (LA77_0==DOUBLEQUOTE) ) {
				alt77=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 77, 0, input);
				throw nvae;
			}

			switch (alt77) {
				case 1 :
					// JdbcGrammar.g:639:2: ( ( SINGLEQUOTE ) ! (~ ( SINGLEQUOTE | NL ) | ( ESCAPEDSINGLEQUOTE ) )* ( SINGLEQUOTE ) !)
					{
					root_0 = (Object)adaptor.nil();


					// JdbcGrammar.g:639:2: ( ( SINGLEQUOTE ) ! (~ ( SINGLEQUOTE | NL ) | ( ESCAPEDSINGLEQUOTE ) )* ( SINGLEQUOTE ) !)
					// JdbcGrammar.g:639:2: ( SINGLEQUOTE ) ! (~ ( SINGLEQUOTE | NL ) | ( ESCAPEDSINGLEQUOTE ) )* ( SINGLEQUOTE ) !
					{
					// JdbcGrammar.g:639:2: ( SINGLEQUOTE )
					// JdbcGrammar.g:639:3: SINGLEQUOTE
					{
					SINGLEQUOTE416=(Token)match(input,SINGLEQUOTE,FOLLOW_SINGLEQUOTE_in_stringliteralnode3861); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					SINGLEQUOTE416_tree = (Object)adaptor.create(SINGLEQUOTE416);
					adaptor.addChild(root_0, SINGLEQUOTE416_tree);
					}

					}

					// JdbcGrammar.g:639:17: (~ ( SINGLEQUOTE | NL ) | ( ESCAPEDSINGLEQUOTE ) )*
					loop75:
					while (true) {
						int alt75=2;
						int LA75_0 = input.LA(1);
						if ( ((LA75_0 >= A && LA75_0 <= NEGATION)||(LA75_0 >= NOTKEYWORD && LA75_0 <= SEMICOLON)||(LA75_0 >= SOURCETABLE && LA75_0 <= 139)) ) {
							alt75=1;
						}

						switch (alt75) {
						case 1 :
							// JdbcGrammar.g:
							{
							set417=input.LT(1);
							if ( (input.LA(1) >= A && input.LA(1) <= NEGATION)||(input.LA(1) >= NOTKEYWORD && input.LA(1) <= SEMICOLON)||(input.LA(1) >= SOURCETABLE && input.LA(1) <= 139) ) {
								input.consume();
								if ( state.backtracking==0 ) adaptor.addChild(root_0, (Object)adaptor.create(set417));
								state.errorRecovery=false;
								state.failed=false;
							}
							else {
								if (state.backtracking>0) {state.failed=true; return retval;}
								MismatchedSetException mse = new MismatchedSetException(null,input);
								throw mse;
							}
							}
							break;

						default :
							break loop75;
						}
					}

					// JdbcGrammar.g:639:63: ( SINGLEQUOTE )
					// JdbcGrammar.g:639:64: SINGLEQUOTE
					{
					SINGLEQUOTE418=(Token)match(input,SINGLEQUOTE,FOLLOW_SINGLEQUOTE_in_stringliteralnode3884); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					SINGLEQUOTE418_tree = (Object)adaptor.create(SINGLEQUOTE418);
					adaptor.addChild(root_0, SINGLEQUOTE418_tree);
					}

					}

					}

					}
					break;
				case 2 :
					// JdbcGrammar.g:641:2: ( ( DOUBLEQUOTE ) ! (~ ( DOUBLEQUOTE | NL ) | ( ESCAPEDDOUBLEQUOTE ) )* ( DOUBLEQUOTE ) !)
					{
					root_0 = (Object)adaptor.nil();


					// JdbcGrammar.g:641:2: ( ( DOUBLEQUOTE ) ! (~ ( DOUBLEQUOTE | NL ) | ( ESCAPEDDOUBLEQUOTE ) )* ( DOUBLEQUOTE ) !)
					// JdbcGrammar.g:641:2: ( DOUBLEQUOTE ) ! (~ ( DOUBLEQUOTE | NL ) | ( ESCAPEDDOUBLEQUOTE ) )* ( DOUBLEQUOTE ) !
					{
					// JdbcGrammar.g:641:2: ( DOUBLEQUOTE )
					// JdbcGrammar.g:641:3: DOUBLEQUOTE
					{
					DOUBLEQUOTE419=(Token)match(input,DOUBLEQUOTE,FOLLOW_DOUBLEQUOTE_in_stringliteralnode3893); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					DOUBLEQUOTE419_tree = (Object)adaptor.create(DOUBLEQUOTE419);
					adaptor.addChild(root_0, DOUBLEQUOTE419_tree);
					}

					}

					// JdbcGrammar.g:641:17: (~ ( DOUBLEQUOTE | NL ) | ( ESCAPEDDOUBLEQUOTE ) )*
					loop76:
					while (true) {
						int alt76=2;
						int LA76_0 = input.LA(1);
						if ( ((LA76_0 >= A && LA76_0 <= DIVIDER)||(LA76_0 >= E && LA76_0 <= NEGATION)||(LA76_0 >= NOTKEYWORD && LA76_0 <= 139)) ) {
							alt76=1;
						}

						switch (alt76) {
						case 1 :
							// JdbcGrammar.g:
							{
							set420=input.LT(1);
							if ( (input.LA(1) >= A && input.LA(1) <= DIVIDER)||(input.LA(1) >= E && input.LA(1) <= NEGATION)||(input.LA(1) >= NOTKEYWORD && input.LA(1) <= 139) ) {
								input.consume();
								if ( state.backtracking==0 ) adaptor.addChild(root_0, (Object)adaptor.create(set420));
								state.errorRecovery=false;
								state.failed=false;
							}
							else {
								if (state.backtracking>0) {state.failed=true; return retval;}
								MismatchedSetException mse = new MismatchedSetException(null,input);
								throw mse;
							}
							}
							break;

						default :
							break loop76;
						}
					}

					// JdbcGrammar.g:641:63: ( DOUBLEQUOTE )
					// JdbcGrammar.g:641:64: DOUBLEQUOTE
					{
					DOUBLEQUOTE421=(Token)match(input,DOUBLEQUOTE,FOLLOW_DOUBLEQUOTE_in_stringliteralnode3916); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					DOUBLEQUOTE421_tree = (Object)adaptor.create(DOUBLEQUOTE421);
					adaptor.addChild(root_0, DOUBLEQUOTE421_tree);
					}

					}

					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "stringliteralnode"


	public static class jointypes_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "jointypes"
	// JdbcGrammar.g:645:1: jointypes : ( INNERKEYWORD | LEFT_KEYWORD | RIGHT_KEYWORD | FULl_KEYWORD ) ;
	public final JdbcGrammarParser.jointypes_return jointypes() throws RecognitionException {
		JdbcGrammarParser.jointypes_return retval = new JdbcGrammarParser.jointypes_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token set422=null;

		Object set422_tree=null;

		try {
			// JdbcGrammar.g:645:10: ( ( INNERKEYWORD | LEFT_KEYWORD | RIGHT_KEYWORD | FULl_KEYWORD ) )
			// JdbcGrammar.g:
			{
			root_0 = (Object)adaptor.nil();


			set422=input.LT(1);
			if ( input.LA(1)==FULl_KEYWORD||input.LA(1)==INNERKEYWORD||input.LA(1)==LEFT_KEYWORD||input.LA(1)==RIGHT_KEYWORD ) {
				input.consume();
				if ( state.backtracking==0 ) adaptor.addChild(root_0, (Object)adaptor.create(set422));
				state.errorRecovery=false;
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "jointypes"


	public static class comparisonoperator_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "comparisonoperator"
	// JdbcGrammar.g:650:1: comparisonoperator : comparisonoperatorbase -> ^( COMPARISONOPERATOR comparisonoperatorbase ) ;
	public final JdbcGrammarParser.comparisonoperator_return comparisonoperator() throws RecognitionException {
		JdbcGrammarParser.comparisonoperator_return retval = new JdbcGrammarParser.comparisonoperator_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope comparisonoperatorbase423 =null;

		RewriteRuleSubtreeStream stream_comparisonoperatorbase=new RewriteRuleSubtreeStream(adaptor,"rule comparisonoperatorbase");

		try {
			// JdbcGrammar.g:650:19: ( comparisonoperatorbase -> ^( COMPARISONOPERATOR comparisonoperatorbase ) )
			// JdbcGrammar.g:651:3: comparisonoperatorbase
			{
			pushFollow(FOLLOW_comparisonoperatorbase_in_comparisonoperator3958);
			comparisonoperatorbase423=comparisonoperatorbase();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_comparisonoperatorbase.add(comparisonoperatorbase423.getTree());
			// AST REWRITE
			// elements: comparisonoperatorbase
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 651:26: -> ^( COMPARISONOPERATOR comparisonoperatorbase )
			{
				// JdbcGrammar.g:651:28: ^( COMPARISONOPERATOR comparisonoperatorbase )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(COMPARISONOPERATOR, "COMPARISONOPERATOR"), root_1);
				adaptor.addChild(root_1, stream_comparisonoperatorbase.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "comparisonoperator"


	public static class comparisonoperatorbase_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "comparisonoperatorbase"
	// JdbcGrammar.g:654:1: comparisonoperatorbase : ( '=' | '<>' | '!=' | '>' | '<' | '>=' | '<=' ) ;
	public final JdbcGrammarParser.comparisonoperatorbase_return comparisonoperatorbase() throws RecognitionException {
		JdbcGrammarParser.comparisonoperatorbase_return retval = new JdbcGrammarParser.comparisonoperatorbase_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token set424=null;

		Object set424_tree=null;

		try {
			// JdbcGrammar.g:654:23: ( ( '=' | '<>' | '!=' | '>' | '<' | '>=' | '<=' ) )
			// JdbcGrammar.g:
			{
			root_0 = (Object)adaptor.nil();


			set424=input.LT(1);
			if ( input.LA(1)==128||(input.LA(1) >= 132 && input.LA(1) <= 137) ) {
				input.consume();
				if ( state.backtracking==0 ) adaptor.addChild(root_0, (Object)adaptor.create(set424));
				state.errorRecovery=false;
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "comparisonoperatorbase"


	public static class alias_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "alias"
	// JdbcGrammar.g:695:1: alias : ( ( ASKEYWORD IDENTIFIER -> ^( ALIAS IDENTIFIER ^( KEYWORD ASKEYWORD ) ^( TEXT TEXT[$alias.text] ) ) | ASKEYWORD aliasliteral -> ^( ALIAS aliasliteral ^( KEYWORD ASKEYWORD ) ^( TEXT TEXT[$alias.text] ) ) ) | ( IDENTIFIER -> ^( ALIAS IDENTIFIER ^( TEXT TEXT[$alias.text] ) ) | aliasliteral -> ^( ALIAS aliasliteral ^( TEXT TEXT[$alias.text] ) ) ) );
	public final JdbcGrammarParser.alias_return alias() throws RecognitionException {
		JdbcGrammarParser.alias_return retval = new JdbcGrammarParser.alias_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token ASKEYWORD425=null;
		Token IDENTIFIER426=null;
		Token ASKEYWORD427=null;
		Token IDENTIFIER429=null;
		ParserRuleReturnScope aliasliteral428 =null;
		ParserRuleReturnScope aliasliteral430 =null;

		Object ASKEYWORD425_tree=null;
		Object IDENTIFIER426_tree=null;
		Object ASKEYWORD427_tree=null;
		Object IDENTIFIER429_tree=null;
		RewriteRuleTokenStream stream_ASKEYWORD=new RewriteRuleTokenStream(adaptor,"token ASKEYWORD");
		RewriteRuleTokenStream stream_IDENTIFIER=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER");
		RewriteRuleSubtreeStream stream_aliasliteral=new RewriteRuleSubtreeStream(adaptor,"rule aliasliteral");

		try {
			// JdbcGrammar.g:696:6: ( ( ASKEYWORD IDENTIFIER -> ^( ALIAS IDENTIFIER ^( KEYWORD ASKEYWORD ) ^( TEXT TEXT[$alias.text] ) ) | ASKEYWORD aliasliteral -> ^( ALIAS aliasliteral ^( KEYWORD ASKEYWORD ) ^( TEXT TEXT[$alias.text] ) ) ) | ( IDENTIFIER -> ^( ALIAS IDENTIFIER ^( TEXT TEXT[$alias.text] ) ) | aliasliteral -> ^( ALIAS aliasliteral ^( TEXT TEXT[$alias.text] ) ) ) )
			int alt80=2;
			int LA80_0 = input.LA(1);
			if ( (LA80_0==ASKEYWORD) ) {
				alt80=1;
			}
			else if ( (LA80_0==DOUBLEQUOTE||LA80_0==IDENTIFIER||LA80_0==SINGLEQUOTE) ) {
				alt80=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 80, 0, input);
				throw nvae;
			}

			switch (alt80) {
				case 1 :
					// JdbcGrammar.g:698:6: ( ASKEYWORD IDENTIFIER -> ^( ALIAS IDENTIFIER ^( KEYWORD ASKEYWORD ) ^( TEXT TEXT[$alias.text] ) ) | ASKEYWORD aliasliteral -> ^( ALIAS aliasliteral ^( KEYWORD ASKEYWORD ) ^( TEXT TEXT[$alias.text] ) ) )
					{
					// JdbcGrammar.g:698:6: ( ASKEYWORD IDENTIFIER -> ^( ALIAS IDENTIFIER ^( KEYWORD ASKEYWORD ) ^( TEXT TEXT[$alias.text] ) ) | ASKEYWORD aliasliteral -> ^( ALIAS aliasliteral ^( KEYWORD ASKEYWORD ) ^( TEXT TEXT[$alias.text] ) ) )
					int alt78=2;
					int LA78_0 = input.LA(1);
					if ( (LA78_0==ASKEYWORD) ) {
						int LA78_1 = input.LA(2);
						if ( (LA78_1==IDENTIFIER) ) {
							alt78=1;
						}
						else if ( (LA78_1==DOUBLEQUOTE||LA78_1==SINGLEQUOTE) ) {
							alt78=2;
						}

						else {
							if (state.backtracking>0) {state.failed=true; return retval;}
							int nvaeMark = input.mark();
							try {
								input.consume();
								NoViableAltException nvae =
									new NoViableAltException("", 78, 1, input);
								throw nvae;
							} finally {
								input.rewind(nvaeMark);
							}
						}

					}

					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						NoViableAltException nvae =
							new NoViableAltException("", 78, 0, input);
						throw nvae;
					}

					switch (alt78) {
						case 1 :
							// JdbcGrammar.g:700:10: ASKEYWORD IDENTIFIER
							{
							ASKEYWORD425=(Token)match(input,ASKEYWORD,FOLLOW_ASKEYWORD_in_alias4228); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_ASKEYWORD.add(ASKEYWORD425);

							IDENTIFIER426=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_alias4230); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_IDENTIFIER.add(IDENTIFIER426);

							// AST REWRITE
							// elements: IDENTIFIER, ASKEYWORD
							// token labels: 
							// rule labels: retval
							// token list labels: 
							// rule list labels: 
							// wildcard labels: 
							if ( state.backtracking==0 ) {
							retval.tree = root_0;
							RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

							root_0 = (Object)adaptor.nil();
							// 700:31: -> ^( ALIAS IDENTIFIER ^( KEYWORD ASKEYWORD ) ^( TEXT TEXT[$alias.text] ) )
							{
								// JdbcGrammar.g:700:33: ^( ALIAS IDENTIFIER ^( KEYWORD ASKEYWORD ) ^( TEXT TEXT[$alias.text] ) )
								{
								Object root_1 = (Object)adaptor.nil();
								root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(ALIAS, "ALIAS"), root_1);
								adaptor.addChild(root_1, stream_IDENTIFIER.nextNode());
								// JdbcGrammar.g:700:52: ^( KEYWORD ASKEYWORD )
								{
								Object root_2 = (Object)adaptor.nil();
								root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(KEYWORD, "KEYWORD"), root_2);
								adaptor.addChild(root_2, stream_ASKEYWORD.nextNode());
								adaptor.addChild(root_1, root_2);
								}

								// JdbcGrammar.g:700:73: ^( TEXT TEXT[$alias.text] )
								{
								Object root_2 = (Object)adaptor.nil();
								root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(TEXT, "TEXT"), root_2);
								adaptor.addChild(root_2, (Object)adaptor.create(TEXT, input.toString(retval.start,input.LT(-1))));
								adaptor.addChild(root_1, root_2);
								}

								adaptor.addChild(root_0, root_1);
								}

							}


							retval.tree = root_0;
							}

							}
							break;
						case 2 :
							// JdbcGrammar.g:701:10: ASKEYWORD aliasliteral
							{
							ASKEYWORD427=(Token)match(input,ASKEYWORD,FOLLOW_ASKEYWORD_in_alias4261); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_ASKEYWORD.add(ASKEYWORD427);

							pushFollow(FOLLOW_aliasliteral_in_alias4263);
							aliasliteral428=aliasliteral();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_aliasliteral.add(aliasliteral428.getTree());
							// AST REWRITE
							// elements: ASKEYWORD, aliasliteral
							// token labels: 
							// rule labels: retval
							// token list labels: 
							// rule list labels: 
							// wildcard labels: 
							if ( state.backtracking==0 ) {
							retval.tree = root_0;
							RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

							root_0 = (Object)adaptor.nil();
							// 701:33: -> ^( ALIAS aliasliteral ^( KEYWORD ASKEYWORD ) ^( TEXT TEXT[$alias.text] ) )
							{
								// JdbcGrammar.g:701:35: ^( ALIAS aliasliteral ^( KEYWORD ASKEYWORD ) ^( TEXT TEXT[$alias.text] ) )
								{
								Object root_1 = (Object)adaptor.nil();
								root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(ALIAS, "ALIAS"), root_1);
								adaptor.addChild(root_1, stream_aliasliteral.nextTree());
								// JdbcGrammar.g:701:56: ^( KEYWORD ASKEYWORD )
								{
								Object root_2 = (Object)adaptor.nil();
								root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(KEYWORD, "KEYWORD"), root_2);
								adaptor.addChild(root_2, stream_ASKEYWORD.nextNode());
								adaptor.addChild(root_1, root_2);
								}

								// JdbcGrammar.g:701:77: ^( TEXT TEXT[$alias.text] )
								{
								Object root_2 = (Object)adaptor.nil();
								root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(TEXT, "TEXT"), root_2);
								adaptor.addChild(root_2, (Object)adaptor.create(TEXT, input.toString(retval.start,input.LT(-1))));
								adaptor.addChild(root_1, root_2);
								}

								adaptor.addChild(root_0, root_1);
								}

							}


							retval.tree = root_0;
							}

							}
							break;

					}

					}
					break;
				case 2 :
					// JdbcGrammar.g:704:6: ( IDENTIFIER -> ^( ALIAS IDENTIFIER ^( TEXT TEXT[$alias.text] ) ) | aliasliteral -> ^( ALIAS aliasliteral ^( TEXT TEXT[$alias.text] ) ) )
					{
					// JdbcGrammar.g:704:6: ( IDENTIFIER -> ^( ALIAS IDENTIFIER ^( TEXT TEXT[$alias.text] ) ) | aliasliteral -> ^( ALIAS aliasliteral ^( TEXT TEXT[$alias.text] ) ) )
					int alt79=2;
					int LA79_0 = input.LA(1);
					if ( (LA79_0==IDENTIFIER) ) {
						alt79=1;
					}
					else if ( (LA79_0==DOUBLEQUOTE||LA79_0==SINGLEQUOTE) ) {
						alt79=2;
					}

					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						NoViableAltException nvae =
							new NoViableAltException("", 79, 0, input);
						throw nvae;
					}

					switch (alt79) {
						case 1 :
							// JdbcGrammar.g:705:10: IDENTIFIER
							{
							IDENTIFIER429=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_alias4315); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_IDENTIFIER.add(IDENTIFIER429);

							// AST REWRITE
							// elements: IDENTIFIER
							// token labels: 
							// rule labels: retval
							// token list labels: 
							// rule list labels: 
							// wildcard labels: 
							if ( state.backtracking==0 ) {
							retval.tree = root_0;
							RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

							root_0 = (Object)adaptor.nil();
							// 705:21: -> ^( ALIAS IDENTIFIER ^( TEXT TEXT[$alias.text] ) )
							{
								// JdbcGrammar.g:705:23: ^( ALIAS IDENTIFIER ^( TEXT TEXT[$alias.text] ) )
								{
								Object root_1 = (Object)adaptor.nil();
								root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(ALIAS, "ALIAS"), root_1);
								adaptor.addChild(root_1, stream_IDENTIFIER.nextNode());
								// JdbcGrammar.g:705:42: ^( TEXT TEXT[$alias.text] )
								{
								Object root_2 = (Object)adaptor.nil();
								root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(TEXT, "TEXT"), root_2);
								adaptor.addChild(root_2, (Object)adaptor.create(TEXT, input.toString(retval.start,input.LT(-1))));
								adaptor.addChild(root_1, root_2);
								}

								adaptor.addChild(root_0, root_1);
								}

							}


							retval.tree = root_0;
							}

							}
							break;
						case 2 :
							// JdbcGrammar.g:706:10: aliasliteral
							{
							pushFollow(FOLLOW_aliasliteral_in_alias4340);
							aliasliteral430=aliasliteral();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_aliasliteral.add(aliasliteral430.getTree());
							// AST REWRITE
							// elements: aliasliteral
							// token labels: 
							// rule labels: retval
							// token list labels: 
							// rule list labels: 
							// wildcard labels: 
							if ( state.backtracking==0 ) {
							retval.tree = root_0;
							RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

							root_0 = (Object)adaptor.nil();
							// 706:23: -> ^( ALIAS aliasliteral ^( TEXT TEXT[$alias.text] ) )
							{
								// JdbcGrammar.g:706:25: ^( ALIAS aliasliteral ^( TEXT TEXT[$alias.text] ) )
								{
								Object root_1 = (Object)adaptor.nil();
								root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(ALIAS, "ALIAS"), root_1);
								adaptor.addChild(root_1, stream_aliasliteral.nextTree());
								// JdbcGrammar.g:706:46: ^( TEXT TEXT[$alias.text] )
								{
								Object root_2 = (Object)adaptor.nil();
								root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(TEXT, "TEXT"), root_2);
								adaptor.addChild(root_2, (Object)adaptor.create(TEXT, input.toString(retval.start,input.LT(-1))));
								adaptor.addChild(root_1, root_2);
								}

								adaptor.addChild(root_0, root_1);
								}

							}


							retval.tree = root_0;
							}

							}
							break;

					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "alias"


	public static class aliasliteral_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "aliasliteral"
	// JdbcGrammar.g:710:1: aliasliteral : ( ( SINGLEQUOTE (s1=~ ( SINGLEQUOTE ) )* SINGLEQUOTE ) | ( DOUBLEQUOTE IDENTIFIER DOUBLEQUOTE ) );
	public final JdbcGrammarParser.aliasliteral_return aliasliteral() throws RecognitionException {
		JdbcGrammarParser.aliasliteral_return retval = new JdbcGrammarParser.aliasliteral_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token s1=null;
		Token SINGLEQUOTE431=null;
		Token SINGLEQUOTE432=null;
		Token DOUBLEQUOTE433=null;
		Token IDENTIFIER434=null;
		Token DOUBLEQUOTE435=null;

		Object s1_tree=null;
		Object SINGLEQUOTE431_tree=null;
		Object SINGLEQUOTE432_tree=null;
		Object DOUBLEQUOTE433_tree=null;
		Object IDENTIFIER434_tree=null;
		Object DOUBLEQUOTE435_tree=null;

		try {
			// JdbcGrammar.g:711:2: ( ( SINGLEQUOTE (s1=~ ( SINGLEQUOTE ) )* SINGLEQUOTE ) | ( DOUBLEQUOTE IDENTIFIER DOUBLEQUOTE ) )
			int alt82=2;
			int LA82_0 = input.LA(1);
			if ( (LA82_0==SINGLEQUOTE) ) {
				alt82=1;
			}
			else if ( (LA82_0==DOUBLEQUOTE) ) {
				alt82=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 82, 0, input);
				throw nvae;
			}

			switch (alt82) {
				case 1 :
					// JdbcGrammar.g:712:2: ( SINGLEQUOTE (s1=~ ( SINGLEQUOTE ) )* SINGLEQUOTE )
					{
					root_0 = (Object)adaptor.nil();


					// JdbcGrammar.g:712:2: ( SINGLEQUOTE (s1=~ ( SINGLEQUOTE ) )* SINGLEQUOTE )
					// JdbcGrammar.g:712:2: SINGLEQUOTE (s1=~ ( SINGLEQUOTE ) )* SINGLEQUOTE
					{
					SINGLEQUOTE431=(Token)match(input,SINGLEQUOTE,FOLLOW_SINGLEQUOTE_in_aliasliteral4371); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					SINGLEQUOTE431_tree = (Object)adaptor.create(SINGLEQUOTE431);
					adaptor.addChild(root_0, SINGLEQUOTE431_tree);
					}

					// JdbcGrammar.g:712:16: (s1=~ ( SINGLEQUOTE ) )*
					loop81:
					while (true) {
						int alt81=2;
						int LA81_0 = input.LA(1);
						if ( ((LA81_0 >= A && LA81_0 <= SEMICOLON)||(LA81_0 >= SOURCETABLE && LA81_0 <= 139)) ) {
							alt81=1;
						}

						switch (alt81) {
						case 1 :
							// JdbcGrammar.g:712:16: s1=~ ( SINGLEQUOTE )
							{
							s1=input.LT(1);
							if ( (input.LA(1) >= A && input.LA(1) <= SEMICOLON)||(input.LA(1) >= SOURCETABLE && input.LA(1) <= 139) ) {
								input.consume();
								if ( state.backtracking==0 ) adaptor.addChild(root_0, (Object)adaptor.create(s1));
								state.errorRecovery=false;
								state.failed=false;
							}
							else {
								if (state.backtracking>0) {state.failed=true; return retval;}
								MismatchedSetException mse = new MismatchedSetException(null,input);
								throw mse;
							}
							}
							break;

						default :
							break loop81;
						}
					}

					SINGLEQUOTE432=(Token)match(input,SINGLEQUOTE,FOLLOW_SINGLEQUOTE_in_aliasliteral4381); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					SINGLEQUOTE432_tree = (Object)adaptor.create(SINGLEQUOTE432);
					adaptor.addChild(root_0, SINGLEQUOTE432_tree);
					}

					}

					}
					break;
				case 2 :
					// JdbcGrammar.g:713:3: ( DOUBLEQUOTE IDENTIFIER DOUBLEQUOTE )
					{
					root_0 = (Object)adaptor.nil();


					// JdbcGrammar.g:713:3: ( DOUBLEQUOTE IDENTIFIER DOUBLEQUOTE )
					// JdbcGrammar.g:713:4: DOUBLEQUOTE IDENTIFIER DOUBLEQUOTE
					{
					DOUBLEQUOTE433=(Token)match(input,DOUBLEQUOTE,FOLLOW_DOUBLEQUOTE_in_aliasliteral4387); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					DOUBLEQUOTE433_tree = (Object)adaptor.create(DOUBLEQUOTE433);
					adaptor.addChild(root_0, DOUBLEQUOTE433_tree);
					}

					IDENTIFIER434=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_aliasliteral4389); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					IDENTIFIER434_tree = (Object)adaptor.create(IDENTIFIER434);
					adaptor.addChild(root_0, IDENTIFIER434_tree);
					}

					DOUBLEQUOTE435=(Token)match(input,DOUBLEQUOTE,FOLLOW_DOUBLEQUOTE_in_aliasliteral4391); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					DOUBLEQUOTE435_tree = (Object)adaptor.create(DOUBLEQUOTE435);
					adaptor.addChild(root_0, DOUBLEQUOTE435_tree);
					}

					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "aliasliteral"


	public static class column_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "column"
	// JdbcGrammar.g:727:1: column : ( ( scopealias ( COLON | PUNCTUATION ) )* ( name | ( DOUBLEQUOTE name DOUBLEQUOTE ) ) ( alias )? ) -> ^( COLUMN ( scopealias )* name ( alias )? ^( TEXT TEXT[$column.text] ) ) ;
	public final JdbcGrammarParser.column_return column() throws RecognitionException {
		JdbcGrammarParser.column_return retval = new JdbcGrammarParser.column_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token COLON437=null;
		Token PUNCTUATION438=null;
		Token DOUBLEQUOTE440=null;
		Token DOUBLEQUOTE442=null;
		ParserRuleReturnScope scopealias436 =null;
		ParserRuleReturnScope name439 =null;
		ParserRuleReturnScope name441 =null;
		ParserRuleReturnScope alias443 =null;

		Object COLON437_tree=null;
		Object PUNCTUATION438_tree=null;
		Object DOUBLEQUOTE440_tree=null;
		Object DOUBLEQUOTE442_tree=null;
		RewriteRuleTokenStream stream_COLON=new RewriteRuleTokenStream(adaptor,"token COLON");
		RewriteRuleTokenStream stream_PUNCTUATION=new RewriteRuleTokenStream(adaptor,"token PUNCTUATION");
		RewriteRuleTokenStream stream_DOUBLEQUOTE=new RewriteRuleTokenStream(adaptor,"token DOUBLEQUOTE");
		RewriteRuleSubtreeStream stream_scopealias=new RewriteRuleSubtreeStream(adaptor,"rule scopealias");
		RewriteRuleSubtreeStream stream_alias=new RewriteRuleSubtreeStream(adaptor,"rule alias");
		RewriteRuleSubtreeStream stream_name=new RewriteRuleSubtreeStream(adaptor,"rule name");

		try {
			// JdbcGrammar.g:728:2: ( ( ( scopealias ( COLON | PUNCTUATION ) )* ( name | ( DOUBLEQUOTE name DOUBLEQUOTE ) ) ( alias )? ) -> ^( COLUMN ( scopealias )* name ( alias )? ^( TEXT TEXT[$column.text] ) ) )
			// JdbcGrammar.g:729:2: ( ( scopealias ( COLON | PUNCTUATION ) )* ( name | ( DOUBLEQUOTE name DOUBLEQUOTE ) ) ( alias )? )
			{
			// JdbcGrammar.g:729:2: ( ( scopealias ( COLON | PUNCTUATION ) )* ( name | ( DOUBLEQUOTE name DOUBLEQUOTE ) ) ( alias )? )
			// JdbcGrammar.g:730:5: ( scopealias ( COLON | PUNCTUATION ) )* ( name | ( DOUBLEQUOTE name DOUBLEQUOTE ) ) ( alias )?
			{
			// JdbcGrammar.g:730:5: ( scopealias ( COLON | PUNCTUATION ) )*
			loop84:
			while (true) {
				int alt84=2;
				switch ( input.LA(1) ) {
				case IDENTIFIER:
					{
					int LA84_1 = input.LA(2);
					if ( (LA84_1==COLON||LA84_1==PUNCTUATION) ) {
						alt84=1;
					}

					}
					break;
				case DOUBLEQUOTE:
					{
					int LA84_2 = input.LA(2);
					if ( (LA84_2==IDENTIFIER) ) {
						int LA84_5 = input.LA(3);
						if ( (LA84_5==DOUBLEQUOTE) ) {
							int LA84_6 = input.LA(4);
							if ( (LA84_6==COLON||LA84_6==PUNCTUATION) ) {
								alt84=1;
							}

						}

					}

					}
					break;
				case BACKSINGLEQUOTE:
					{
					alt84=1;
					}
					break;
				}
				switch (alt84) {
				case 1 :
					// JdbcGrammar.g:730:6: scopealias ( COLON | PUNCTUATION )
					{
					pushFollow(FOLLOW_scopealias_in_column4411);
					scopealias436=scopealias();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_scopealias.add(scopealias436.getTree());
					// JdbcGrammar.g:730:17: ( COLON | PUNCTUATION )
					int alt83=2;
					int LA83_0 = input.LA(1);
					if ( (LA83_0==COLON) ) {
						alt83=1;
					}
					else if ( (LA83_0==PUNCTUATION) ) {
						alt83=2;
					}

					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						NoViableAltException nvae =
							new NoViableAltException("", 83, 0, input);
						throw nvae;
					}

					switch (alt83) {
						case 1 :
							// JdbcGrammar.g:730:18: COLON
							{
							COLON437=(Token)match(input,COLON,FOLLOW_COLON_in_column4414); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_COLON.add(COLON437);

							}
							break;
						case 2 :
							// JdbcGrammar.g:730:26: PUNCTUATION
							{
							PUNCTUATION438=(Token)match(input,PUNCTUATION,FOLLOW_PUNCTUATION_in_column4418); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_PUNCTUATION.add(PUNCTUATION438);

							}
							break;

					}

					}
					break;

				default :
					break loop84;
				}
			}

			// JdbcGrammar.g:731:4: ( name | ( DOUBLEQUOTE name DOUBLEQUOTE ) )
			int alt85=2;
			int LA85_0 = input.LA(1);
			if ( (LA85_0==IDENTIFIER) ) {
				alt85=1;
			}
			else if ( (LA85_0==DOUBLEQUOTE) ) {
				alt85=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 85, 0, input);
				throw nvae;
			}

			switch (alt85) {
				case 1 :
					// JdbcGrammar.g:731:5: name
					{
					pushFollow(FOLLOW_name_in_column4428);
					name439=name();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_name.add(name439.getTree());
					}
					break;
				case 2 :
					// JdbcGrammar.g:731:12: ( DOUBLEQUOTE name DOUBLEQUOTE )
					{
					// JdbcGrammar.g:731:12: ( DOUBLEQUOTE name DOUBLEQUOTE )
					// JdbcGrammar.g:731:13: DOUBLEQUOTE name DOUBLEQUOTE
					{
					DOUBLEQUOTE440=(Token)match(input,DOUBLEQUOTE,FOLLOW_DOUBLEQUOTE_in_column4433); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_DOUBLEQUOTE.add(DOUBLEQUOTE440);

					pushFollow(FOLLOW_name_in_column4435);
					name441=name();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_name.add(name441.getTree());
					DOUBLEQUOTE442=(Token)match(input,DOUBLEQUOTE,FOLLOW_DOUBLEQUOTE_in_column4437); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_DOUBLEQUOTE.add(DOUBLEQUOTE442);

					}

					}
					break;

			}

			// JdbcGrammar.g:732:4: ( alias )?
			int alt86=2;
			int LA86_0 = input.LA(1);
			if ( (LA86_0==ASKEYWORD||LA86_0==DOUBLEQUOTE||LA86_0==IDENTIFIER||LA86_0==SINGLEQUOTE) ) {
				alt86=1;
			}
			switch (alt86) {
				case 1 :
					// JdbcGrammar.g:732:5: alias
					{
					pushFollow(FOLLOW_alias_in_column4445);
					alias443=alias();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_alias.add(alias443.getTree());
					}
					break;

			}

			}

			// AST REWRITE
			// elements: name, alias, scopealias
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 733:5: -> ^( COLUMN ( scopealias )* name ( alias )? ^( TEXT TEXT[$column.text] ) )
			{
				// JdbcGrammar.g:733:7: ^( COLUMN ( scopealias )* name ( alias )? ^( TEXT TEXT[$column.text] ) )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(COLUMN, "COLUMN"), root_1);
				// JdbcGrammar.g:733:16: ( scopealias )*
				while ( stream_scopealias.hasNext() ) {
					adaptor.addChild(root_1, stream_scopealias.nextTree());
				}
				stream_scopealias.reset();

				adaptor.addChild(root_1, stream_name.nextTree());
				// JdbcGrammar.g:733:33: ( alias )?
				if ( stream_alias.hasNext() ) {
					adaptor.addChild(root_1, stream_alias.nextTree());
				}
				stream_alias.reset();

				// JdbcGrammar.g:733:40: ^( TEXT TEXT[$column.text] )
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(TEXT, "TEXT"), root_2);
				adaptor.addChild(root_2, (Object)adaptor.create(TEXT, input.toString(retval.start,input.LT(-1))));
				adaptor.addChild(root_1, root_2);
				}

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "column"


	public static class columndivider_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "columndivider"
	// JdbcGrammar.g:737:1: columndivider : ( COLON | PUNCTUATION );
	public final JdbcGrammarParser.columndivider_return columndivider() throws RecognitionException {
		JdbcGrammarParser.columndivider_return retval = new JdbcGrammarParser.columndivider_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token set444=null;

		Object set444_tree=null;

		try {
			// JdbcGrammar.g:737:14: ( COLON | PUNCTUATION )
			// JdbcGrammar.g:
			{
			root_0 = (Object)adaptor.nil();


			set444=input.LT(1);
			if ( input.LA(1)==COLON||input.LA(1)==PUNCTUATION ) {
				input.consume();
				if ( state.backtracking==0 ) adaptor.addChild(root_0, (Object)adaptor.create(set444));
				state.errorRecovery=false;
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "columndivider"


	public static class likeclause_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "likeclause"
	// JdbcGrammar.g:743:1: likeclause : LIKEKEYWORD likesubclause -> ^( LIKEEXPRESSION likesubclause ) ;
	public final JdbcGrammarParser.likeclause_return likeclause() throws RecognitionException {
		JdbcGrammarParser.likeclause_return retval = new JdbcGrammarParser.likeclause_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token LIKEKEYWORD445=null;
		ParserRuleReturnScope likesubclause446 =null;

		Object LIKEKEYWORD445_tree=null;
		RewriteRuleTokenStream stream_LIKEKEYWORD=new RewriteRuleTokenStream(adaptor,"token LIKEKEYWORD");
		RewriteRuleSubtreeStream stream_likesubclause=new RewriteRuleSubtreeStream(adaptor,"rule likesubclause");

		try {
			// JdbcGrammar.g:744:5: ( LIKEKEYWORD likesubclause -> ^( LIKEEXPRESSION likesubclause ) )
			// JdbcGrammar.g:745:5: LIKEKEYWORD likesubclause
			{
			LIKEKEYWORD445=(Token)match(input,LIKEKEYWORD,FOLLOW_LIKEKEYWORD_in_likeclause4501); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_LIKEKEYWORD.add(LIKEKEYWORD445);

			pushFollow(FOLLOW_likesubclause_in_likeclause4503);
			likesubclause446=likesubclause();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_likesubclause.add(likesubclause446.getTree());
			// AST REWRITE
			// elements: likesubclause
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 745:31: -> ^( LIKEEXPRESSION likesubclause )
			{
				// JdbcGrammar.g:745:34: ^( LIKEEXPRESSION likesubclause )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(LIKEEXPRESSION, "LIKEEXPRESSION"), root_1);
				adaptor.addChild(root_1, stream_likesubclause.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "likeclause"


	public static class likesubclause_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "likesubclause"
	// JdbcGrammar.g:748:1: likesubclause : SINGLEQUOTE ! (~ ( SINGLEQUOTE | DOUBLEQUOTE | NL ) | ( ESCAPEDSINGLEQUOTE | ESCAPEDDOUBLEQUOTE ) )* SINGLEQUOTE !;
	public final JdbcGrammarParser.likesubclause_return likesubclause() throws RecognitionException {
		JdbcGrammarParser.likesubclause_return retval = new JdbcGrammarParser.likesubclause_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token SINGLEQUOTE447=null;
		Token set448=null;
		Token SINGLEQUOTE449=null;

		Object SINGLEQUOTE447_tree=null;
		Object set448_tree=null;
		Object SINGLEQUOTE449_tree=null;

		try {
			// JdbcGrammar.g:748:14: ( SINGLEQUOTE ! (~ ( SINGLEQUOTE | DOUBLEQUOTE | NL ) | ( ESCAPEDSINGLEQUOTE | ESCAPEDDOUBLEQUOTE ) )* SINGLEQUOTE !)
			// JdbcGrammar.g:749:12: SINGLEQUOTE ! (~ ( SINGLEQUOTE | DOUBLEQUOTE | NL ) | ( ESCAPEDSINGLEQUOTE | ESCAPEDDOUBLEQUOTE ) )* SINGLEQUOTE !
			{
			root_0 = (Object)adaptor.nil();


			SINGLEQUOTE447=(Token)match(input,SINGLEQUOTE,FOLLOW_SINGLEQUOTE_in_likesubclause4519); if (state.failed) return retval;
			// JdbcGrammar.g:749:14: (~ ( SINGLEQUOTE | DOUBLEQUOTE | NL ) | ( ESCAPEDSINGLEQUOTE | ESCAPEDDOUBLEQUOTE ) )*
			loop87:
			while (true) {
				int alt87=2;
				int LA87_0 = input.LA(1);
				if ( ((LA87_0 >= A && LA87_0 <= DIVIDER)||(LA87_0 >= E && LA87_0 <= NEGATION)||(LA87_0 >= NOTKEYWORD && LA87_0 <= SEMICOLON)||(LA87_0 >= SOURCETABLE && LA87_0 <= 139)) ) {
					alt87=1;
				}

				switch (alt87) {
				case 1 :
					// JdbcGrammar.g:
					{
					set448=input.LT(1);
					if ( (input.LA(1) >= A && input.LA(1) <= DIVIDER)||(input.LA(1) >= E && input.LA(1) <= NEGATION)||(input.LA(1) >= NOTKEYWORD && input.LA(1) <= SEMICOLON)||(input.LA(1) >= SOURCETABLE && input.LA(1) <= 139) ) {
						input.consume();
						if ( state.backtracking==0 ) adaptor.addChild(root_0, (Object)adaptor.create(set448));
						state.errorRecovery=false;
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						throw mse;
					}
					}
					break;

				default :
					break loop87;
				}
			}

			SINGLEQUOTE449=(Token)match(input,SINGLEQUOTE,FOLLOW_SINGLEQUOTE_in_likesubclause4547); if (state.failed) return retval;
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "likesubclause"


	public static class table_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "table"
	// JdbcGrammar.g:755:1: table : ( ( BACKQUOTE IDENTIFIER BACKQUOTE ) | ( SINGLEQUOTE IDENTIFIER SINGLEQUOTE ) | ( DOUBLEQUOTE IDENTIFIER DOUBLEQUOTE ) | ( ESCAPEDDOUBLEQUOTE IDENTIFIER ESCAPEDDOUBLEQUOTE ) | IDENTIFIER ) -> ^( TABLENAME ( SINGLEQUOTE )? ( DOUBLEQUOTE )? ( ESCAPEDDOUBLEQUOTE )? ( BACKQUOTE )? ^( NAME IDENTIFIER ) ( SINGLEQUOTE )? ( DOUBLEQUOTE )? ( ESCAPEDDOUBLEQUOTE )? ( BACKQUOTE )? ) ;
	public final JdbcGrammarParser.table_return table() throws RecognitionException {
		JdbcGrammarParser.table_return retval = new JdbcGrammarParser.table_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token BACKQUOTE450=null;
		Token IDENTIFIER451=null;
		Token BACKQUOTE452=null;
		Token SINGLEQUOTE453=null;
		Token IDENTIFIER454=null;
		Token SINGLEQUOTE455=null;
		Token DOUBLEQUOTE456=null;
		Token IDENTIFIER457=null;
		Token DOUBLEQUOTE458=null;
		Token ESCAPEDDOUBLEQUOTE459=null;
		Token IDENTIFIER460=null;
		Token ESCAPEDDOUBLEQUOTE461=null;
		Token IDENTIFIER462=null;

		Object BACKQUOTE450_tree=null;
		Object IDENTIFIER451_tree=null;
		Object BACKQUOTE452_tree=null;
		Object SINGLEQUOTE453_tree=null;
		Object IDENTIFIER454_tree=null;
		Object SINGLEQUOTE455_tree=null;
		Object DOUBLEQUOTE456_tree=null;
		Object IDENTIFIER457_tree=null;
		Object DOUBLEQUOTE458_tree=null;
		Object ESCAPEDDOUBLEQUOTE459_tree=null;
		Object IDENTIFIER460_tree=null;
		Object ESCAPEDDOUBLEQUOTE461_tree=null;
		Object IDENTIFIER462_tree=null;
		RewriteRuleTokenStream stream_BACKQUOTE=new RewriteRuleTokenStream(adaptor,"token BACKQUOTE");
		RewriteRuleTokenStream stream_DOUBLEQUOTE=new RewriteRuleTokenStream(adaptor,"token DOUBLEQUOTE");
		RewriteRuleTokenStream stream_SINGLEQUOTE=new RewriteRuleTokenStream(adaptor,"token SINGLEQUOTE");
		RewriteRuleTokenStream stream_IDENTIFIER=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER");
		RewriteRuleTokenStream stream_ESCAPEDDOUBLEQUOTE=new RewriteRuleTokenStream(adaptor,"token ESCAPEDDOUBLEQUOTE");

		try {
			// JdbcGrammar.g:756:4: ( ( ( BACKQUOTE IDENTIFIER BACKQUOTE ) | ( SINGLEQUOTE IDENTIFIER SINGLEQUOTE ) | ( DOUBLEQUOTE IDENTIFIER DOUBLEQUOTE ) | ( ESCAPEDDOUBLEQUOTE IDENTIFIER ESCAPEDDOUBLEQUOTE ) | IDENTIFIER ) -> ^( TABLENAME ( SINGLEQUOTE )? ( DOUBLEQUOTE )? ( ESCAPEDDOUBLEQUOTE )? ( BACKQUOTE )? ^( NAME IDENTIFIER ) ( SINGLEQUOTE )? ( DOUBLEQUOTE )? ( ESCAPEDDOUBLEQUOTE )? ( BACKQUOTE )? ) )
			// JdbcGrammar.g:757:4: ( ( BACKQUOTE IDENTIFIER BACKQUOTE ) | ( SINGLEQUOTE IDENTIFIER SINGLEQUOTE ) | ( DOUBLEQUOTE IDENTIFIER DOUBLEQUOTE ) | ( ESCAPEDDOUBLEQUOTE IDENTIFIER ESCAPEDDOUBLEQUOTE ) | IDENTIFIER )
			{
			// JdbcGrammar.g:757:4: ( ( BACKQUOTE IDENTIFIER BACKQUOTE ) | ( SINGLEQUOTE IDENTIFIER SINGLEQUOTE ) | ( DOUBLEQUOTE IDENTIFIER DOUBLEQUOTE ) | ( ESCAPEDDOUBLEQUOTE IDENTIFIER ESCAPEDDOUBLEQUOTE ) | IDENTIFIER )
			int alt88=5;
			switch ( input.LA(1) ) {
			case BACKQUOTE:
				{
				alt88=1;
				}
				break;
			case SINGLEQUOTE:
				{
				alt88=2;
				}
				break;
			case DOUBLEQUOTE:
				{
				alt88=3;
				}
				break;
			case ESCAPEDDOUBLEQUOTE:
				{
				alt88=4;
				}
				break;
			case IDENTIFIER:
				{
				alt88=5;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 88, 0, input);
				throw nvae;
			}
			switch (alt88) {
				case 1 :
					// JdbcGrammar.g:757:8: ( BACKQUOTE IDENTIFIER BACKQUOTE )
					{
					// JdbcGrammar.g:757:8: ( BACKQUOTE IDENTIFIER BACKQUOTE )
					// JdbcGrammar.g:757:9: BACKQUOTE IDENTIFIER BACKQUOTE
					{
					BACKQUOTE450=(Token)match(input,BACKQUOTE,FOLLOW_BACKQUOTE_in_table4567); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_BACKQUOTE.add(BACKQUOTE450);

					IDENTIFIER451=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_table4569); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IDENTIFIER.add(IDENTIFIER451);

					BACKQUOTE452=(Token)match(input,BACKQUOTE,FOLLOW_BACKQUOTE_in_table4571); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_BACKQUOTE.add(BACKQUOTE452);

					}

					}
					break;
				case 2 :
					// JdbcGrammar.g:758:7: ( SINGLEQUOTE IDENTIFIER SINGLEQUOTE )
					{
					// JdbcGrammar.g:758:7: ( SINGLEQUOTE IDENTIFIER SINGLEQUOTE )
					// JdbcGrammar.g:758:8: SINGLEQUOTE IDENTIFIER SINGLEQUOTE
					{
					SINGLEQUOTE453=(Token)match(input,SINGLEQUOTE,FOLLOW_SINGLEQUOTE_in_table4583); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_SINGLEQUOTE.add(SINGLEQUOTE453);

					IDENTIFIER454=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_table4585); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IDENTIFIER.add(IDENTIFIER454);

					SINGLEQUOTE455=(Token)match(input,SINGLEQUOTE,FOLLOW_SINGLEQUOTE_in_table4587); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_SINGLEQUOTE.add(SINGLEQUOTE455);

					}

					}
					break;
				case 3 :
					// JdbcGrammar.g:759:8: ( DOUBLEQUOTE IDENTIFIER DOUBLEQUOTE )
					{
					// JdbcGrammar.g:759:8: ( DOUBLEQUOTE IDENTIFIER DOUBLEQUOTE )
					// JdbcGrammar.g:759:9: DOUBLEQUOTE IDENTIFIER DOUBLEQUOTE
					{
					DOUBLEQUOTE456=(Token)match(input,DOUBLEQUOTE,FOLLOW_DOUBLEQUOTE_in_table4598); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_DOUBLEQUOTE.add(DOUBLEQUOTE456);

					IDENTIFIER457=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_table4600); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IDENTIFIER.add(IDENTIFIER457);

					DOUBLEQUOTE458=(Token)match(input,DOUBLEQUOTE,FOLLOW_DOUBLEQUOTE_in_table4602); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_DOUBLEQUOTE.add(DOUBLEQUOTE458);

					}

					}
					break;
				case 4 :
					// JdbcGrammar.g:760:8: ( ESCAPEDDOUBLEQUOTE IDENTIFIER ESCAPEDDOUBLEQUOTE )
					{
					// JdbcGrammar.g:760:8: ( ESCAPEDDOUBLEQUOTE IDENTIFIER ESCAPEDDOUBLEQUOTE )
					// JdbcGrammar.g:760:9: ESCAPEDDOUBLEQUOTE IDENTIFIER ESCAPEDDOUBLEQUOTE
					{
					ESCAPEDDOUBLEQUOTE459=(Token)match(input,ESCAPEDDOUBLEQUOTE,FOLLOW_ESCAPEDDOUBLEQUOTE_in_table4614); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_ESCAPEDDOUBLEQUOTE.add(ESCAPEDDOUBLEQUOTE459);

					IDENTIFIER460=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_table4616); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IDENTIFIER.add(IDENTIFIER460);

					ESCAPEDDOUBLEQUOTE461=(Token)match(input,ESCAPEDDOUBLEQUOTE,FOLLOW_ESCAPEDDOUBLEQUOTE_in_table4618); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_ESCAPEDDOUBLEQUOTE.add(ESCAPEDDOUBLEQUOTE461);

					}

					}
					break;
				case 5 :
					// JdbcGrammar.g:761:9: IDENTIFIER
					{
					IDENTIFIER462=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_table4630); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IDENTIFIER.add(IDENTIFIER462);

					}
					break;

			}

			// AST REWRITE
			// elements: BACKQUOTE, SINGLEQUOTE, SINGLEQUOTE, DOUBLEQUOTE, BACKQUOTE, ESCAPEDDOUBLEQUOTE, IDENTIFIER, ESCAPEDDOUBLEQUOTE, DOUBLEQUOTE
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 763:4: -> ^( TABLENAME ( SINGLEQUOTE )? ( DOUBLEQUOTE )? ( ESCAPEDDOUBLEQUOTE )? ( BACKQUOTE )? ^( NAME IDENTIFIER ) ( SINGLEQUOTE )? ( DOUBLEQUOTE )? ( ESCAPEDDOUBLEQUOTE )? ( BACKQUOTE )? )
			{
				// JdbcGrammar.g:763:6: ^( TABLENAME ( SINGLEQUOTE )? ( DOUBLEQUOTE )? ( ESCAPEDDOUBLEQUOTE )? ( BACKQUOTE )? ^( NAME IDENTIFIER ) ( SINGLEQUOTE )? ( DOUBLEQUOTE )? ( ESCAPEDDOUBLEQUOTE )? ( BACKQUOTE )? )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(TABLENAME, "TABLENAME"), root_1);
				// JdbcGrammar.g:763:18: ( SINGLEQUOTE )?
				if ( stream_SINGLEQUOTE.hasNext() ) {
					adaptor.addChild(root_1, stream_SINGLEQUOTE.nextNode());
				}
				stream_SINGLEQUOTE.reset();

				// JdbcGrammar.g:763:31: ( DOUBLEQUOTE )?
				if ( stream_DOUBLEQUOTE.hasNext() ) {
					adaptor.addChild(root_1, stream_DOUBLEQUOTE.nextNode());
				}
				stream_DOUBLEQUOTE.reset();

				// JdbcGrammar.g:763:44: ( ESCAPEDDOUBLEQUOTE )?
				if ( stream_ESCAPEDDOUBLEQUOTE.hasNext() ) {
					adaptor.addChild(root_1, stream_ESCAPEDDOUBLEQUOTE.nextNode());
				}
				stream_ESCAPEDDOUBLEQUOTE.reset();

				// JdbcGrammar.g:763:64: ( BACKQUOTE )?
				if ( stream_BACKQUOTE.hasNext() ) {
					adaptor.addChild(root_1, stream_BACKQUOTE.nextNode());
				}
				stream_BACKQUOTE.reset();

				// JdbcGrammar.g:763:75: ^( NAME IDENTIFIER )
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(NAME, "NAME"), root_2);
				adaptor.addChild(root_2, stream_IDENTIFIER.nextNode());
				adaptor.addChild(root_1, root_2);
				}

				// JdbcGrammar.g:763:94: ( SINGLEQUOTE )?
				if ( stream_SINGLEQUOTE.hasNext() ) {
					adaptor.addChild(root_1, stream_SINGLEQUOTE.nextNode());
				}
				stream_SINGLEQUOTE.reset();

				// JdbcGrammar.g:763:107: ( DOUBLEQUOTE )?
				if ( stream_DOUBLEQUOTE.hasNext() ) {
					adaptor.addChild(root_1, stream_DOUBLEQUOTE.nextNode());
				}
				stream_DOUBLEQUOTE.reset();

				// JdbcGrammar.g:763:120: ( ESCAPEDDOUBLEQUOTE )?
				if ( stream_ESCAPEDDOUBLEQUOTE.hasNext() ) {
					adaptor.addChild(root_1, stream_ESCAPEDDOUBLEQUOTE.nextNode());
				}
				stream_ESCAPEDDOUBLEQUOTE.reset();

				// JdbcGrammar.g:763:140: ( BACKQUOTE )?
				if ( stream_BACKQUOTE.hasNext() ) {
					adaptor.addChild(root_1, stream_BACKQUOTE.nextNode());
				}
				stream_BACKQUOTE.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "table"

	// $ANTLR start synpred1_JdbcGrammar
	public final void synpred1_JdbcGrammar_fragment() throws RecognitionException {
		// JdbcGrammar.g:174:3: ( disjunction )
		// JdbcGrammar.g:174:4: disjunction
		{
		pushFollow(FOLLOW_disjunction_in_synpred1_JdbcGrammar530);
		disjunction();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred1_JdbcGrammar

	// $ANTLR start synpred2_JdbcGrammar
	public final void synpred2_JdbcGrammar_fragment() throws RecognitionException {
		// JdbcGrammar.g:175:4: ( conjunction )
		// JdbcGrammar.g:175:5: conjunction
		{
		pushFollow(FOLLOW_conjunction_in_synpred2_JdbcGrammar539);
		conjunction();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred2_JdbcGrammar

	// $ANTLR start synpred3_JdbcGrammar
	public final void synpred3_JdbcGrammar_fragment() throws RecognitionException {
		// JdbcGrammar.g:176:4: ( subconjunction )
		// JdbcGrammar.g:176:5: subconjunction
		{
		pushFollow(FOLLOW_subconjunction_in_synpred3_JdbcGrammar548);
		subconjunction();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred3_JdbcGrammar

	// $ANTLR start synpred4_JdbcGrammar
	public final void synpred4_JdbcGrammar_fragment() throws RecognitionException {
		// JdbcGrammar.g:177:4: ( subdisjunction )
		// JdbcGrammar.g:177:5: subdisjunction
		{
		pushFollow(FOLLOW_subdisjunction_in_synpred4_JdbcGrammar557);
		subdisjunction();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred4_JdbcGrammar

	// $ANTLR start synpred5_JdbcGrammar
	public final void synpred5_JdbcGrammar_fragment() throws RecognitionException {
		// JdbcGrammar.g:178:4: ( exprlvl0 )
		// JdbcGrammar.g:178:5: exprlvl0
		{
		pushFollow(FOLLOW_exprlvl0_in_synpred5_JdbcGrammar566);
		exprlvl0();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred5_JdbcGrammar

	// $ANTLR start synpred6_JdbcGrammar
	public final void synpred6_JdbcGrammar_fragment() throws RecognitionException {
		// JdbcGrammar.g:183:3: ( NOTKEYWORD LPARAM subdisjunction RPARAM )
		// JdbcGrammar.g:183:4: NOTKEYWORD LPARAM subdisjunction RPARAM
		{
		match(input,NOTKEYWORD,FOLLOW_NOTKEYWORD_in_synpred6_JdbcGrammar581); if (state.failed) return;

		match(input,LPARAM,FOLLOW_LPARAM_in_synpred6_JdbcGrammar583); if (state.failed) return;

		pushFollow(FOLLOW_subdisjunction_in_synpred6_JdbcGrammar585);
		subdisjunction();
		state._fsp--;
		if (state.failed) return;

		match(input,RPARAM,FOLLOW_RPARAM_in_synpred6_JdbcGrammar587); if (state.failed) return;

		}

	}
	// $ANTLR end synpred6_JdbcGrammar

	// $ANTLR start synpred7_JdbcGrammar
	public final void synpred7_JdbcGrammar_fragment() throws RecognitionException {
		// JdbcGrammar.g:184:4: ( NOTKEYWORD LPARAM disjunction RPARAM )
		// JdbcGrammar.g:184:5: NOTKEYWORD LPARAM disjunction RPARAM
		{
		match(input,NOTKEYWORD,FOLLOW_NOTKEYWORD_in_synpred7_JdbcGrammar610); if (state.failed) return;

		match(input,LPARAM,FOLLOW_LPARAM_in_synpred7_JdbcGrammar612); if (state.failed) return;

		pushFollow(FOLLOW_disjunction_in_synpred7_JdbcGrammar614);
		disjunction();
		state._fsp--;
		if (state.failed) return;

		match(input,RPARAM,FOLLOW_RPARAM_in_synpred7_JdbcGrammar616); if (state.failed) return;

		}

	}
	// $ANTLR end synpred7_JdbcGrammar

	// $ANTLR start synpred8_JdbcGrammar
	public final void synpred8_JdbcGrammar_fragment() throws RecognitionException {
		// JdbcGrammar.g:185:4: ( LPARAM ! subdisjunction RPARAM !)
		// JdbcGrammar.g:185:5: LPARAM ! subdisjunction RPARAM !
		{
		match(input,LPARAM,FOLLOW_LPARAM_in_synpred8_JdbcGrammar639); if (state.failed) return;

		pushFollow(FOLLOW_subdisjunction_in_synpred8_JdbcGrammar642);
		subdisjunction();
		state._fsp--;
		if (state.failed) return;

		match(input,RPARAM,FOLLOW_RPARAM_in_synpred8_JdbcGrammar644); if (state.failed) return;

		}

	}
	// $ANTLR end synpred8_JdbcGrammar

	// $ANTLR start synpred9_JdbcGrammar
	public final void synpred9_JdbcGrammar_fragment() throws RecognitionException {
		// JdbcGrammar.g:186:4: ( LPARAM ! disjunction RPARAM !)
		// JdbcGrammar.g:186:5: LPARAM ! disjunction RPARAM !
		{
		match(input,LPARAM,FOLLOW_LPARAM_in_synpred9_JdbcGrammar662); if (state.failed) return;

		pushFollow(FOLLOW_disjunction_in_synpred9_JdbcGrammar665);
		disjunction();
		state._fsp--;
		if (state.failed) return;

		match(input,RPARAM,FOLLOW_RPARAM_in_synpred9_JdbcGrammar667); if (state.failed) return;

		}

	}
	// $ANTLR end synpred9_JdbcGrammar

	// $ANTLR start synpred10_JdbcGrammar
	public final void synpred10_JdbcGrammar_fragment() throws RecognitionException {
		// JdbcGrammar.g:191:3: ( conjunction )
		// JdbcGrammar.g:191:4: conjunction
		{
		pushFollow(FOLLOW_conjunction_in_synpred10_JdbcGrammar691);
		conjunction();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred10_JdbcGrammar

	// $ANTLR start synpred11_JdbcGrammar
	public final void synpred11_JdbcGrammar_fragment() throws RecognitionException {
		// JdbcGrammar.g:192:4: ( subconjunction )
		// JdbcGrammar.g:192:5: subconjunction
		{
		pushFollow(FOLLOW_subconjunction_in_synpred11_JdbcGrammar700);
		subconjunction();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred11_JdbcGrammar

	// $ANTLR start synpred12_JdbcGrammar
	public final void synpred12_JdbcGrammar_fragment() throws RecognitionException {
		// JdbcGrammar.g:193:4: ( subdisjunction )
		// JdbcGrammar.g:193:5: subdisjunction
		{
		pushFollow(FOLLOW_subdisjunction_in_synpred12_JdbcGrammar709);
		subdisjunction();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred12_JdbcGrammar

	// $ANTLR start synpred13_JdbcGrammar
	public final void synpred13_JdbcGrammar_fragment() throws RecognitionException {
		// JdbcGrammar.g:210:3: ( subconjunction )
		// JdbcGrammar.g:210:4: subconjunction
		{
		pushFollow(FOLLOW_subconjunction_in_synpred13_JdbcGrammar773);
		subconjunction();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred13_JdbcGrammar

	// $ANTLR start synpred14_JdbcGrammar
	public final void synpred14_JdbcGrammar_fragment() throws RecognitionException {
		// JdbcGrammar.g:211:4: ( subdisjunction )
		// JdbcGrammar.g:211:5: subdisjunction
		{
		pushFollow(FOLLOW_subdisjunction_in_synpred14_JdbcGrammar782);
		subdisjunction();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred14_JdbcGrammar

	// $ANTLR start synpred15_JdbcGrammar
	public final void synpred15_JdbcGrammar_fragment() throws RecognitionException {
		// JdbcGrammar.g:217:3: ( NOTKEYWORD LPARAM subconjunction RPARAM )
		// JdbcGrammar.g:217:4: NOTKEYWORD LPARAM subconjunction RPARAM
		{
		match(input,NOTKEYWORD,FOLLOW_NOTKEYWORD_in_synpred15_JdbcGrammar802); if (state.failed) return;

		match(input,LPARAM,FOLLOW_LPARAM_in_synpred15_JdbcGrammar804); if (state.failed) return;

		pushFollow(FOLLOW_subconjunction_in_synpred15_JdbcGrammar806);
		subconjunction();
		state._fsp--;
		if (state.failed) return;

		match(input,RPARAM,FOLLOW_RPARAM_in_synpred15_JdbcGrammar808); if (state.failed) return;

		}

	}
	// $ANTLR end synpred15_JdbcGrammar

	// $ANTLR start synpred16_JdbcGrammar
	public final void synpred16_JdbcGrammar_fragment() throws RecognitionException {
		// JdbcGrammar.g:218:4: ( NOTKEYWORD LPARAM conjunction RPARAM )
		// JdbcGrammar.g:218:5: NOTKEYWORD LPARAM conjunction RPARAM
		{
		match(input,NOTKEYWORD,FOLLOW_NOTKEYWORD_in_synpred16_JdbcGrammar831); if (state.failed) return;

		match(input,LPARAM,FOLLOW_LPARAM_in_synpred16_JdbcGrammar833); if (state.failed) return;

		pushFollow(FOLLOW_conjunction_in_synpred16_JdbcGrammar835);
		conjunction();
		state._fsp--;
		if (state.failed) return;

		match(input,RPARAM,FOLLOW_RPARAM_in_synpred16_JdbcGrammar837); if (state.failed) return;

		}

	}
	// $ANTLR end synpred16_JdbcGrammar

	// $ANTLR start synpred17_JdbcGrammar
	public final void synpred17_JdbcGrammar_fragment() throws RecognitionException {
		// JdbcGrammar.g:219:4: ( LPARAM ! subconjunction RPARAM !)
		// JdbcGrammar.g:219:5: LPARAM ! subconjunction RPARAM !
		{
		match(input,LPARAM,FOLLOW_LPARAM_in_synpred17_JdbcGrammar860); if (state.failed) return;

		pushFollow(FOLLOW_subconjunction_in_synpred17_JdbcGrammar863);
		subconjunction();
		state._fsp--;
		if (state.failed) return;

		match(input,RPARAM,FOLLOW_RPARAM_in_synpred17_JdbcGrammar865); if (state.failed) return;

		}

	}
	// $ANTLR end synpred17_JdbcGrammar

	// $ANTLR start synpred18_JdbcGrammar
	public final void synpred18_JdbcGrammar_fragment() throws RecognitionException {
		// JdbcGrammar.g:220:4: ( LPARAM ! conjunction RPARAM !)
		// JdbcGrammar.g:220:5: LPARAM ! conjunction RPARAM !
		{
		match(input,LPARAM,FOLLOW_LPARAM_in_synpred18_JdbcGrammar883); if (state.failed) return;

		pushFollow(FOLLOW_conjunction_in_synpred18_JdbcGrammar886);
		conjunction();
		state._fsp--;
		if (state.failed) return;

		match(input,RPARAM,FOLLOW_RPARAM_in_synpred18_JdbcGrammar888); if (state.failed) return;

		}

	}
	// $ANTLR end synpred18_JdbcGrammar

	// $ANTLR start synpred19_JdbcGrammar
	public final void synpred19_JdbcGrammar_fragment() throws RecognitionException {
		// JdbcGrammar.g:236:3: ( NOTKEYWORD LPARAM exprlvl0 RPARAM )
		// JdbcGrammar.g:236:4: NOTKEYWORD LPARAM exprlvl0 RPARAM
		{
		match(input,NOTKEYWORD,FOLLOW_NOTKEYWORD_in_synpred19_JdbcGrammar961); if (state.failed) return;

		match(input,LPARAM,FOLLOW_LPARAM_in_synpred19_JdbcGrammar963); if (state.failed) return;

		pushFollow(FOLLOW_exprlvl0_in_synpred19_JdbcGrammar965);
		exprlvl0();
		state._fsp--;
		if (state.failed) return;

		match(input,RPARAM,FOLLOW_RPARAM_in_synpred19_JdbcGrammar967); if (state.failed) return;

		}

	}
	// $ANTLR end synpred19_JdbcGrammar

	// $ANTLR start synpred20_JdbcGrammar
	public final void synpred20_JdbcGrammar_fragment() throws RecognitionException {
		// JdbcGrammar.g:237:4: ( NOTKEYWORD LPARAM exprlvl1 RPARAM )
		// JdbcGrammar.g:237:5: NOTKEYWORD LPARAM exprlvl1 RPARAM
		{
		match(input,NOTKEYWORD,FOLLOW_NOTKEYWORD_in_synpred20_JdbcGrammar990); if (state.failed) return;

		match(input,LPARAM,FOLLOW_LPARAM_in_synpred20_JdbcGrammar992); if (state.failed) return;

		pushFollow(FOLLOW_exprlvl1_in_synpred20_JdbcGrammar994);
		exprlvl1();
		state._fsp--;
		if (state.failed) return;

		match(input,RPARAM,FOLLOW_RPARAM_in_synpred20_JdbcGrammar996); if (state.failed) return;

		}

	}
	// $ANTLR end synpred20_JdbcGrammar

	// $ANTLR start synpred21_JdbcGrammar
	public final void synpred21_JdbcGrammar_fragment() throws RecognitionException {
		// JdbcGrammar.g:238:4: ( LPARAM ! exprlvl0 RPARAM !)
		// JdbcGrammar.g:238:5: LPARAM ! exprlvl0 RPARAM !
		{
		match(input,LPARAM,FOLLOW_LPARAM_in_synpred21_JdbcGrammar1019); if (state.failed) return;

		pushFollow(FOLLOW_exprlvl0_in_synpred21_JdbcGrammar1022);
		exprlvl0();
		state._fsp--;
		if (state.failed) return;

		match(input,RPARAM,FOLLOW_RPARAM_in_synpred21_JdbcGrammar1024); if (state.failed) return;

		}

	}
	// $ANTLR end synpred21_JdbcGrammar

	// $ANTLR start synpred22_JdbcGrammar
	public final void synpred22_JdbcGrammar_fragment() throws RecognitionException {
		// JdbcGrammar.g:239:4: ( LPARAM ! exprlvl1 RPARAM !)
		// JdbcGrammar.g:239:5: LPARAM ! exprlvl1 RPARAM !
		{
		match(input,LPARAM,FOLLOW_LPARAM_in_synpred22_JdbcGrammar1042); if (state.failed) return;

		pushFollow(FOLLOW_exprlvl1_in_synpred22_JdbcGrammar1045);
		exprlvl1();
		state._fsp--;
		if (state.failed) return;

		match(input,RPARAM,FOLLOW_RPARAM_in_synpred22_JdbcGrammar1047); if (state.failed) return;

		}

	}
	// $ANTLR end synpred22_JdbcGrammar

	// Delegated rules

	public final boolean synpred15_JdbcGrammar() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred15_JdbcGrammar_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred1_JdbcGrammar() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred1_JdbcGrammar_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred22_JdbcGrammar() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred22_JdbcGrammar_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred16_JdbcGrammar() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred16_JdbcGrammar_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred3_JdbcGrammar() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred3_JdbcGrammar_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred20_JdbcGrammar() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred20_JdbcGrammar_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred14_JdbcGrammar() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred14_JdbcGrammar_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred19_JdbcGrammar() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred19_JdbcGrammar_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred7_JdbcGrammar() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred7_JdbcGrammar_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred17_JdbcGrammar() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred17_JdbcGrammar_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred21_JdbcGrammar() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred21_JdbcGrammar_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred2_JdbcGrammar() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred2_JdbcGrammar_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred9_JdbcGrammar() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred9_JdbcGrammar_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred6_JdbcGrammar() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred6_JdbcGrammar_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred12_JdbcGrammar() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred12_JdbcGrammar_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred11_JdbcGrammar() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred11_JdbcGrammar_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred18_JdbcGrammar() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred18_JdbcGrammar_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred8_JdbcGrammar() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred8_JdbcGrammar_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred4_JdbcGrammar() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred4_JdbcGrammar_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred13_JdbcGrammar() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred13_JdbcGrammar_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred10_JdbcGrammar() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred10_JdbcGrammar_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred5_JdbcGrammar() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred5_JdbcGrammar_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}


	protected DFA22 dfa22 = new DFA22(this);
	protected DFA65 dfa65 = new DFA65(this);
	static final String DFA22_eotS =
		"\12\uffff";
	static final String DFA22_eofS =
		"\6\uffff\1\4\2\uffff\1\4";
	static final String DFA22_minS =
		"\1\13\1\uffff\1\4\2\uffff\1\4\1\6\2\4\1\6";
	static final String DFA22_maxS =
		"\1\157\1\uffff\1\u008b\2\uffff\1\u008b\1\u0089\2\u008b\1\u0089";
	static final String DFA22_acceptS =
		"\1\uffff\1\1\1\uffff\1\2\1\3\5\uffff";
	static final String DFA22_specialS =
		"\12\uffff}>";
	static final String[] DFA22_transitionS = {
			"\1\3\27\uffff\1\2\25\uffff\1\3\24\uffff\1\1\40\uffff\1\4",
			"",
			"\65\4\1\5\34\4\1\uffff\65\4",
			"",
			"",
			"\16\4\1\7\20\4\1\6\62\4\1\uffff\15\4\1\7\47\4",
			"\1\4\13\uffff\1\3\40\uffff\1\4\2\uffff\1\4\21\uffff\1\4\1\uffff\1\4"+
			"\21\uffff\1\4\3\uffff\1\4\3\uffff\1\3\4\uffff\1\4\4\uffff\1\4\21\uffff"+
			"\1\4\3\uffff\6\4",
			"\65\4\1\10\34\4\1\uffff\65\4",
			"\16\4\1\7\20\4\1\11\62\4\1\uffff\15\4\1\7\47\4",
			"\1\4\13\uffff\1\3\40\uffff\1\4\2\uffff\1\4\21\uffff\1\4\1\uffff\1\4"+
			"\21\uffff\1\4\3\uffff\1\4\10\uffff\1\4\4\uffff\1\4\21\uffff\1\4\3\uffff"+
			"\6\4"
	};

	static final short[] DFA22_eot = DFA.unpackEncodedString(DFA22_eotS);
	static final short[] DFA22_eof = DFA.unpackEncodedString(DFA22_eofS);
	static final char[] DFA22_min = DFA.unpackEncodedStringToUnsignedChars(DFA22_minS);
	static final char[] DFA22_max = DFA.unpackEncodedStringToUnsignedChars(DFA22_maxS);
	static final short[] DFA22_accept = DFA.unpackEncodedString(DFA22_acceptS);
	static final short[] DFA22_special = DFA.unpackEncodedString(DFA22_specialS);
	static final short[][] DFA22_transition;

	static {
		int numStates = DFA22_transitionS.length;
		DFA22_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA22_transition[i] = DFA.unpackEncodedString(DFA22_transitionS[i]);
		}
	}

	protected class DFA22 extends DFA {

		public DFA22(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 22;
			this.eot = DFA22_eot;
			this.eof = DFA22_eof;
			this.min = DFA22_min;
			this.max = DFA22_max;
			this.accept = DFA22_accept;
			this.special = DFA22_special;
			this.transition = DFA22_transition;
		}
		@Override
		public String getDescription() {
			return "270:1: exprconditioncore : ( subquery | columnforexpression | stringliteral );";
		}
	}

	static final String DFA65_eotS =
		"\15\uffff";
	static final String DFA65_eofS =
		"\15\uffff";
	static final String DFA65_minS =
		"\1\13\1\10\2\71\1\uffff\1\13\1\uffff\1\13\1\43\1\10\1\uffff\1\22\1\10";
	static final String DFA65_maxS =
		"\1\71\1\157\2\71\1\uffff\1\u0081\1\uffff\1\13\1\43\1\157\1\uffff\1\144"+
		"\1\157";
	static final String DFA65_acceptS =
		"\4\uffff\1\1\1\uffff\1\2\3\uffff\1\3\2\uffff";
	static final String DFA65_specialS =
		"\15\uffff}>";
	static final String[] DFA65_transitionS = {
			"\1\2\27\uffff\1\3\25\uffff\1\1",
			"\1\4\11\uffff\1\4\1\uffff\1\4\16\uffff\1\4\11\uffff\1\4\13\uffff\1\4"+
			"\24\uffff\1\6\25\uffff\1\5\12\uffff\1\4",
			"\1\7",
			"\1\10",
			"",
			"\1\2\27\uffff\1\3\25\uffff\1\11\107\uffff\1\12",
			"",
			"\1\13",
			"\1\14",
			"\1\4\11\uffff\1\4\1\uffff\1\4\16\uffff\1\4\11\uffff\1\4\13\uffff\1\4"+
			"\52\uffff\1\5\12\uffff\1\4",
			"",
			"\1\4\121\uffff\1\5",
			"\1\4\11\uffff\1\4\1\uffff\1\4\16\uffff\1\4\11\uffff\1\4\13\uffff\1\4"+
			"\52\uffff\1\5\12\uffff\1\4"
	};

	static final short[] DFA65_eot = DFA.unpackEncodedString(DFA65_eotS);
	static final short[] DFA65_eof = DFA.unpackEncodedString(DFA65_eofS);
	static final char[] DFA65_min = DFA.unpackEncodedStringToUnsignedChars(DFA65_minS);
	static final char[] DFA65_max = DFA.unpackEncodedStringToUnsignedChars(DFA65_maxS);
	static final short[] DFA65_accept = DFA.unpackEncodedString(DFA65_acceptS);
	static final short[] DFA65_special = DFA.unpackEncodedString(DFA65_specialS);
	static final short[][] DFA65_transition;

	static {
		int numStates = DFA65_transitionS.length;
		DFA65_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA65_transition[i] = DFA.unpackEncodedString(DFA65_transitionS[i]);
		}
	}

	protected class DFA65 extends DFA {

		public DFA65(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 65;
			this.eot = DFA65_eot;
			this.eof = DFA65_eof;
			this.min = DFA65_min;
			this.max = DFA65_max;
			this.accept = DFA65_accept;
			this.special = DFA65_special;
			this.transition = DFA65_transition;
		}
		@Override
		public String getDescription() {
			return "552:1: expressionpart : ( column | functioncall | multiplecolumn );";
		}
	}

	public static final BitSet FOLLOW_selectstatement_in_statement60 = new BitSet(new long[]{0x0000000000000000L,0x0000400000000000L});
	public static final BitSet FOLLOW_set_in_statement63 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SELECTKEYWORD_in_selectstatement84 = new BitSet(new long[]{0x0200000A00000800L,0x0000000000000000L,0x0000000000000002L});
	public static final BitSet FOLLOW_DISTINCT_in_selectstatement86 = new BitSet(new long[]{0x0200000800000800L,0x0000000000000000L,0x0000000000000002L});
	public static final BitSet FOLLOW_expression_in_selectstatement89 = new BitSet(new long[]{0x0000200000000000L});
	public static final BitSet FOLLOW_fromexpression_in_selectstatement91 = new BitSet(new long[]{0x0048000000000002L,0x0800000100000400L});
	public static final BitSet FOLLOW_whereexpression_in_selectstatement93 = new BitSet(new long[]{0x0048000000000002L,0x0000000100000400L});
	public static final BitSet FOLLOW_groupbyexpression_in_selectstatement96 = new BitSet(new long[]{0x0040000000000002L,0x0000000100000400L});
	public static final BitSet FOLLOW_havingexpression_in_selectstatement99 = new BitSet(new long[]{0x0000000000000002L,0x0000000100000400L});
	public static final BitSet FOLLOW_orderbyexpression_in_selectstatement102 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000400L});
	public static final BitSet FOLLOW_limitexpression_in_selectstatement105 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LIMITKEYWORD_in_limitexpression162 = new BitSet(new long[]{0x0000000000000000L,0x0000000001000000L});
	public static final BitSet FOLLOW_NUMBER_in_limitexpression164 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ORDERKEYWORD_in_orderbyexpression192 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_BYKEYWORD_in_orderbyexpression194 = new BitSet(new long[]{0x0200000800000800L});
	public static final BitSet FOLLOW_orderbycondition_in_orderbyexpression196 = new BitSet(new long[]{0x0000000000100002L});
	public static final BitSet FOLLOW_COMMA_in_orderbyexpression198 = new BitSet(new long[]{0x0200000800000800L});
	public static final BitSet FOLLOW_orderbycondition_in_orderbyexpression200 = new BitSet(new long[]{0x0000000000100002L});
	public static final BitSet FOLLOW_column_in_orderbycondition242 = new BitSet(new long[]{0x0000000040000082L});
	public static final BitSet FOLLOW_DESC_in_orderbycondition245 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ASC_in_orderbycondition247 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FROMKEYWORD_in_fromexpression282 = new BitSet(new long[]{0x0200004800000400L,0x0000800000004000L,0x0000000000000400L});
	public static final BitSet FOLLOW_datasource_in_fromexpression288 = new BitSet(new long[]{0x0000000000100002L});
	public static final BitSet FOLLOW_COMMA_in_fromexpression300 = new BitSet(new long[]{0x0200004800000400L,0x0000800000004000L,0x0000000000000400L});
	public static final BitSet FOLLOW_datasource_in_fromexpression315 = new BitSet(new long[]{0x0000000000100002L});
	public static final BitSet FOLLOW_andoperator_in_logicaloperator357 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_oroperator_in_logicaloperator359 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_AND_in_andoperator388 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_OR_in_oroperator399 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_WHEREKEYWORD_in_whereexpression416 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000008L});
	public static final BitSet FOLLOW_131_in_whereexpression418 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000080L});
	public static final BitSet FOLLOW_135_in_whereexpression420 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000008L});
	public static final BitSet FOLLOW_131_in_whereexpression422 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_WHEREKEYWORD_in_whereexpression433 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000008L});
	public static final BitSet FOLLOW_131_in_whereexpression435 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000080L});
	public static final BitSet FOLLOW_135_in_whereexpression437 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_130_in_whereexpression439 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_WHEREKEYWORD_in_whereexpression456 = new BitSet(new long[]{0x0200000800000800L,0x0000800000804000L});
	public static final BitSet FOLLOW_expr_in_whereexpression458 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_disjunction_in_expr533 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_conjunction_in_expr542 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_subconjunction_in_expr551 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_subdisjunction_in_expr560 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_exprlvl0_in_expr569 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NOTKEYWORD_in_subdisjunction591 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_subdisjunction593 = new BitSet(new long[]{0x0000000000000000L,0x0000000000804000L});
	public static final BitSet FOLLOW_subdisjunction_in_subdisjunction595 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_subdisjunction597 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NOTKEYWORD_in_subdisjunction620 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_subdisjunction622 = new BitSet(new long[]{0x0200000800000800L,0x0000800000804000L});
	public static final BitSet FOLLOW_disjunction_in_subdisjunction624 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_subdisjunction626 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPARAM_in_subdisjunction649 = new BitSet(new long[]{0x0000000000000000L,0x0000000000804000L});
	public static final BitSet FOLLOW_subdisjunction_in_subdisjunction652 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_subdisjunction654 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPARAM_in_subdisjunction672 = new BitSet(new long[]{0x0200000800000800L,0x0000800000804000L});
	public static final BitSet FOLLOW_disjunction_in_subdisjunction675 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_subdisjunction677 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_conjunction_in_disjunctionpart1694 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_subconjunction_in_disjunctionpart1703 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_subdisjunction_in_disjunctionpart1712 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_exprlvl0_in_disjunctionpart1717 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_disjunctionpart1_in_disjunction735 = new BitSet(new long[]{0x0000000000000000L,0x0000000010000000L});
	public static final BitSet FOLLOW_disjunctionpart2_in_disjunction737 = new BitSet(new long[]{0x0000000000000002L,0x0000000010000000L});
	public static final BitSet FOLLOW_oroperator_in_disjunctionpart2759 = new BitSet(new long[]{0x0200000800000800L,0x0000800000804000L});
	public static final BitSet FOLLOW_disjunctionpart1_in_disjunctionpart2761 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_subconjunction_in_conjunctionpart1776 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_subdisjunction_in_conjunctionpart1785 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_exprlvl0_in_conjunctionpart1790 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NOTKEYWORD_in_subconjunction812 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_subconjunction814 = new BitSet(new long[]{0x0000000000000000L,0x0000000000804000L});
	public static final BitSet FOLLOW_subconjunction_in_subconjunction816 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_subconjunction818 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NOTKEYWORD_in_subconjunction841 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_subconjunction843 = new BitSet(new long[]{0x0200000800000800L,0x0000800000804000L});
	public static final BitSet FOLLOW_conjunction_in_subconjunction845 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_subconjunction847 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPARAM_in_subconjunction870 = new BitSet(new long[]{0x0000000000000000L,0x0000000000804000L});
	public static final BitSet FOLLOW_subconjunction_in_subconjunction873 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_subconjunction875 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPARAM_in_subconjunction893 = new BitSet(new long[]{0x0200000800000800L,0x0000800000804000L});
	public static final BitSet FOLLOW_conjunction_in_subconjunction896 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_subconjunction898 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_conjunctionpart1_in_conjunction917 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_conjunctionpart2_in_conjunction919 = new BitSet(new long[]{0x0000000000000042L});
	public static final BitSet FOLLOW_andoperator_in_conjunctionpart2941 = new BitSet(new long[]{0x0200000800000800L,0x0000800000804000L});
	public static final BitSet FOLLOW_conjunctionpart1_in_conjunctionpart2943 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NOTKEYWORD_in_exprlvl0971 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_exprlvl0973 = new BitSet(new long[]{0x0200000800000800L,0x0000800000804000L});
	public static final BitSet FOLLOW_exprlvl0_in_exprlvl0975 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_exprlvl0977 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NOTKEYWORD_in_exprlvl01000 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_exprlvl01002 = new BitSet(new long[]{0x0200000800000800L,0x0000800000004000L});
	public static final BitSet FOLLOW_exprlvl1_in_exprlvl01004 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_exprlvl01006 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPARAM_in_exprlvl01029 = new BitSet(new long[]{0x0200000800000800L,0x0000800000804000L});
	public static final BitSet FOLLOW_exprlvl0_in_exprlvl01032 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_exprlvl01034 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPARAM_in_exprlvl01052 = new BitSet(new long[]{0x0200000800000800L,0x0000800000004000L});
	public static final BitSet FOLLOW_exprlvl1_in_exprlvl01055 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_exprlvl01057 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_exprlvl1_in_exprlvl01065 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_exprcondition_in_exprlvl11078 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_exprconditioncore_in_exprcondition1128 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000100L,0x00000000000003F1L});
	public static final BitSet FOLLOW_LIKEKEYWORD_in_exprcondition1149 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000100L});
	public static final BitSet FOLLOW_likeclause_in_exprcondition1151 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_comparisonoperator_in_exprcondition1165 = new BitSet(new long[]{0x0200000800000800L,0x0000800001004000L});
	public static final BitSet FOLLOW_exprconditioncore_in_exprcondition1184 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_number_in_exprcondition1188 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_subquery_in_exprconditioncore1273 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_columnforexpression_in_exprconditioncore1275 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_stringliteral_in_exprconditioncore1277 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_razoralias_in_columnforexpression1294 = new BitSet(new long[]{0x0000000000040000L});
	public static final BitSet FOLLOW_COLON_in_columnforexpression1296 = new BitSet(new long[]{0x0200000800000800L});
	public static final BitSet FOLLOW_scopealias_in_columnforexpression1304 = new BitSet(new long[]{0x0000000000040000L,0x0000001000000000L});
	public static final BitSet FOLLOW_COLON_in_columnforexpression1307 = new BitSet(new long[]{0x0200000800000800L});
	public static final BitSet FOLLOW_PUNCTUATION_in_columnforexpression1311 = new BitSet(new long[]{0x0200000800000800L});
	public static final BitSet FOLLOW_name_in_columnforexpression1320 = new BitSet(new long[]{0x0200000800000102L,0x0000800000000000L});
	public static final BitSet FOLLOW_alias_in_columnforexpression1329 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_HAVINGKEYWORD_in_havingexpression1376 = new BitSet(new long[]{0x0200000800000800L,0x0000800000804000L});
	public static final BitSet FOLLOW_expr_in_havingexpression1378 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_GROUPKEYWORD_in_groupbyexpression1463 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_BYKEYWORD_in_groupbyexpression1465 = new BitSet(new long[]{0x0200000800000800L});
	public static final BitSet FOLLOW_column_in_groupbyexpression1467 = new BitSet(new long[]{0x0000000000100002L});
	public static final BitSet FOLLOW_COMMA_in_groupbyexpression1470 = new BitSet(new long[]{0x0200000800000800L});
	public static final BitSet FOLLOW_column_in_groupbyexpression1472 = new BitSet(new long[]{0x0000000000100002L});
	public static final BitSet FOLLOW_column_in_condition1524 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x00000000000003F1L});
	public static final BitSet FOLLOW_comparisonoperator_in_condition1526 = new BitSet(new long[]{0x0200000800000800L});
	public static final BitSet FOLLOW_column_in_condition1530 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPARAM_in_subquery1581 = new BitSet(new long[]{0x0000000000000000L,0x0000100000000000L});
	public static final BitSet FOLLOW_selectstatement_in_subquery1590 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_subquery1596 = new BitSet(new long[]{0x0200000800000102L,0x0000800000000000L});
	public static final BitSet FOLLOW_alias_in_subquery1604 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_subquery_in_datasourceelement1668 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_sourcetable_in_datasourceelement1672 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_datasourcenoparen_in_datasource1684 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_datasourceparen_in_datasource1695 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_datasourceelement_in_datasourcenoparen1719 = new BitSet(new long[]{0x4400400000000002L,0x0000010000000040L});
	public static final BitSet FOLLOW_joinclause_in_datasourcenoparen1724 = new BitSet(new long[]{0x0200004800000400L,0x0000800000004000L,0x0000000000000400L});
	public static final BitSet FOLLOW_datasourceelement_in_datasourcenoparen1728 = new BitSet(new long[]{0x0000000000000000L,0x0000000008000000L});
	public static final BitSet FOLLOW_onclause_in_datasourcenoparen1732 = new BitSet(new long[]{0x4400400000000002L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourcenoparen1735 = new BitSet(new long[]{0x4400400000000002L,0x0000010000000040L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen1956 = new BitSet(new long[]{0x0200004800000400L,0x0000800000004000L,0x0000000000000400L});
	public static final BitSet FOLLOW_simplejoin_in_datasourceparen1958 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen1960 = new BitSet(new long[]{0x4400400000000002L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen1963 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen1978 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen1981 = new BitSet(new long[]{0x0200004800000400L,0x0000800000004000L,0x0000000000000400L});
	public static final BitSet FOLLOW_simplejoin_in_datasourceparen1983 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen1985 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen1988 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen1990 = new BitSet(new long[]{0x4400400000000002L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen1994 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2002 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2005 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2008 = new BitSet(new long[]{0x0200004800000400L,0x0000800000004000L,0x0000000000000400L});
	public static final BitSet FOLLOW_simplejoin_in_datasourceparen2010 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2012 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2015 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2017 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2021 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2023 = new BitSet(new long[]{0x4400400000000002L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2027 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2034 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2038 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2041 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2044 = new BitSet(new long[]{0x0200004800000400L,0x0000800000004000L,0x0000000000000400L});
	public static final BitSet FOLLOW_simplejoin_in_datasourceparen2046 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2048 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2051 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2053 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2057 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2059 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2063 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2065 = new BitSet(new long[]{0x4400400000000002L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2068 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2075 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2078 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2082 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2085 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2088 = new BitSet(new long[]{0x0200004800000400L,0x0000800000004000L,0x0000000000000400L});
	public static final BitSet FOLLOW_simplejoin_in_datasourceparen2090 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2092 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2095 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2097 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2101 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2103 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2107 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2109 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2112 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2114 = new BitSet(new long[]{0x4400400000000002L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2117 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2124 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2127 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2130 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2134 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2137 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2140 = new BitSet(new long[]{0x0200004800000400L,0x0000800000004000L,0x0000000000000400L});
	public static final BitSet FOLLOW_simplejoin_in_datasourceparen2142 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2144 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2147 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2149 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2153 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2155 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2159 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2161 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2164 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2166 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2169 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2171 = new BitSet(new long[]{0x4400400000000002L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2174 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2181 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2184 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2187 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2190 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2194 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2197 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2200 = new BitSet(new long[]{0x0200004800000400L,0x0000800000004000L,0x0000000000000400L});
	public static final BitSet FOLLOW_simplejoin_in_datasourceparen2202 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2204 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2207 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2209 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2213 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2215 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2219 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2221 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2224 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2226 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2229 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2231 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2234 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2236 = new BitSet(new long[]{0x4400400000000002L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2240 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2247 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2250 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2253 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2256 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2259 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2263 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2266 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2269 = new BitSet(new long[]{0x0200004800000400L,0x0000800000004000L,0x0000000000000400L});
	public static final BitSet FOLLOW_simplejoin_in_datasourceparen2271 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2273 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2276 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2278 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2282 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2284 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2288 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2290 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2293 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2295 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2298 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2300 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2303 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2305 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2309 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2311 = new BitSet(new long[]{0x4400400000000002L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2314 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2321 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2324 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2327 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2330 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2333 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2336 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2340 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2343 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2346 = new BitSet(new long[]{0x0200004800000400L,0x0000800000004000L,0x0000000000000400L});
	public static final BitSet FOLLOW_simplejoin_in_datasourceparen2348 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2350 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2353 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2355 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2359 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2361 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2365 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2367 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2370 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2372 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2375 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2377 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2380 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2382 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2386 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2388 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2391 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2393 = new BitSet(new long[]{0x4400400000000002L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2396 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2403 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2406 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2409 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2412 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2415 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2418 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2421 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2425 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2428 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_datasourceparen2431 = new BitSet(new long[]{0x0200004800000400L,0x0000800000004000L,0x0000000000000400L});
	public static final BitSet FOLLOW_simplejoin_in_datasourceparen2433 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2435 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2438 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2440 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2444 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2446 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2450 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2452 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2455 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2457 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2460 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2462 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2465 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2467 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2471 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2473 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2476 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2478 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2481 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourceparen2483 = new BitSet(new long[]{0x4400400000000002L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourceparen2486 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_datasourceelement_in_simplejoin2540 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_joinclause_in_simplejoin2544 = new BitSet(new long[]{0x0200004800000400L,0x0000800000004000L,0x0000000000000400L});
	public static final BitSet FOLLOW_datasourceelement_in_simplejoin2548 = new BitSet(new long[]{0x0000000000000000L,0x0000000008000000L});
	public static final BitSet FOLLOW_onclause_in_simplejoin2552 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_datasourceelement_in_datasourcerecur2601 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPARAM_in_datasourcerecur2611 = new BitSet(new long[]{0x0200004800000400L,0x0000800000004000L,0x0000000000000400L});
	public static final BitSet FOLLOW_datasourcerecur_in_datasourcerecur2613 = new BitSet(new long[]{0x4400400000000000L,0x0000010000000040L});
	public static final BitSet FOLLOW_multijoinexpression_in_datasourcerecur2615 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_datasourcerecur2617 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_joinclause_in_multijoinexpression2629 = new BitSet(new long[]{0x0200004800000400L,0x0000800000004000L,0x0000000000000400L});
	public static final BitSet FOLLOW_datasourceelement_in_multijoinexpression2631 = new BitSet(new long[]{0x0000000000000000L,0x0000000008000000L});
	public static final BitSet FOLLOW_onclause_in_multijoinexpression2633 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_jointypes_in_joinclause2740 = new BitSet(new long[]{0x4000000000000000L});
	public static final BitSet FOLLOW_JOINKEYWORD_in_joinclause2743 = new BitSet(new long[]{0x0000002000000002L});
	public static final BitSet FOLLOW_EACH_in_joinclause2745 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ONKEYWORD_in_onclause2780 = new BitSet(new long[]{0x0200000800000800L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_onclause2814 = new BitSet(new long[]{0x0200000800000800L});
	public static final BitSet FOLLOW_condition_in_onclause2816 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_onclause2818 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_condition_in_onclause2823 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_BACKQUOTE_in_dataset2883 = new BitSet(new long[]{0x0200000000000000L});
	public static final BitSet FOLLOW_name_in_dataset2885 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_BACKQUOTE_in_dataset2887 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SINGLEQUOTE_in_dataset2896 = new BitSet(new long[]{0x0200000000000000L});
	public static final BitSet FOLLOW_name_in_dataset2898 = new BitSet(new long[]{0x0000000000000000L,0x0000800000000000L});
	public static final BitSet FOLLOW_SINGLEQUOTE_in_dataset2900 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DOUBLEQUOTE_in_dataset2910 = new BitSet(new long[]{0x0200000000000000L});
	public static final BitSet FOLLOW_name_in_dataset2912 = new BitSet(new long[]{0x0000000800000000L});
	public static final BitSet FOLLOW_DOUBLEQUOTE_in_dataset2914 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ESCAPEDDOUBLEQUOTE_in_dataset2924 = new BitSet(new long[]{0x0200000000000000L});
	public static final BitSet FOLLOW_name_in_dataset2926 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_ESCAPEDDOUBLEQUOTE_in_dataset2928 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_name_in_dataset2937 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SINGLEQUOTE_in_project2979 = new BitSet(new long[]{0x0200000000000000L});
	public static final BitSet FOLLOW_name_in_project2982 = new BitSet(new long[]{0x0200000000040000L,0x0000801000000000L});
	public static final BitSet FOLLOW_projectdivider_in_project2984 = new BitSet(new long[]{0x0200000000000000L,0x0000800000000000L});
	public static final BitSet FOLLOW_SINGLEQUOTE_in_project2990 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DOUBLEQUOTE_in_project2999 = new BitSet(new long[]{0x0200000000000000L});
	public static final BitSet FOLLOW_name_in_project3002 = new BitSet(new long[]{0x0200000800040000L,0x0000001000000000L});
	public static final BitSet FOLLOW_projectdivider_in_project3004 = new BitSet(new long[]{0x0200000800000000L});
	public static final BitSet FOLLOW_DOUBLEQUOTE_in_project3010 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ESCAPEDDOUBLEQUOTE_in_project3020 = new BitSet(new long[]{0x0200000000000000L});
	public static final BitSet FOLLOW_name_in_project3023 = new BitSet(new long[]{0x0200004000040000L,0x0000001000000000L});
	public static final BitSet FOLLOW_projectdivider_in_project3025 = new BitSet(new long[]{0x0200004000000000L});
	public static final BitSet FOLLOW_ESCAPEDDOUBLEQUOTE_in_project3031 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_name_in_project3039 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_COLON_in_projectdivider3076 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_PUNCTUATION_in_projectdivider3080 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_138_in_sourcetable3112 = new BitSet(new long[]{0x0200004800000400L,0x0000800000000000L});
	public static final BitSet FOLLOW_project_in_sourcetable3123 = new BitSet(new long[]{0x0000000000040000L,0x0000001000000000L});
	public static final BitSet FOLLOW_columndivider_in_sourcetable3125 = new BitSet(new long[]{0x0200004800000400L,0x0000800000000000L});
	public static final BitSet FOLLOW_dataset_in_sourcetable3129 = new BitSet(new long[]{0x0000000000040000L,0x0000001000000000L});
	public static final BitSet FOLLOW_columndivider_in_sourcetable3131 = new BitSet(new long[]{0x0200004800000400L,0x0000800000000000L});
	public static final BitSet FOLLOW_srctablename_in_sourcetable3135 = new BitSet(new long[]{0x0200000800000102L,0x0000800000000000L,0x0000000000000800L});
	public static final BitSet FOLLOW_alias_in_sourcetable3147 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000000000800L});
	public static final BitSet FOLLOW_139_in_sourcetable3151 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SINGLEQUOTE_in_srctablename3199 = new BitSet(new long[]{0x0200000000000000L});
	public static final BitSet FOLLOW_name_in_srctablename3201 = new BitSet(new long[]{0x0000000000000000L,0x0000800000000000L});
	public static final BitSet FOLLOW_SINGLEQUOTE_in_srctablename3203 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DOUBLEQUOTE_in_srctablename3209 = new BitSet(new long[]{0x0200000000000000L});
	public static final BitSet FOLLOW_name_in_srctablename3211 = new BitSet(new long[]{0x0000000800000000L});
	public static final BitSet FOLLOW_DOUBLEQUOTE_in_srctablename3213 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ESCAPEDDOUBLEQUOTE_in_srctablename3219 = new BitSet(new long[]{0x0200000000000000L});
	public static final BitSet FOLLOW_name_in_srctablename3221 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_ESCAPEDDOUBLEQUOTE_in_srctablename3223 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_BACKQUOTE_in_srctablename3229 = new BitSet(new long[]{0x0200000000000000L});
	public static final BitSet FOLLOW_name_in_srctablename3231 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_BACKQUOTE_in_srctablename3233 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_name_in_srctablename3238 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDENTIFIER_in_name3252 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NUMBER_in_number3281 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_129_in_expression3308 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expressionpart_in_expression3353 = new BitSet(new long[]{0x0000000000100002L});
	public static final BitSet FOLLOW_COMMA_in_expression3368 = new BitSet(new long[]{0x0200000800000800L});
	public static final BitSet FOLLOW_expressionpart_in_expression3370 = new BitSet(new long[]{0x0000000000100002L});
	public static final BitSet FOLLOW_column_in_expressionpart3400 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_functioncall_in_expressionpart3404 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_multiplecolumn_in_expressionpart3408 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_scopealias_in_multiplecolumn3432 = new BitSet(new long[]{0x0000000000000000L,0x0000001000000000L});
	public static final BitSet FOLLOW_PUNCTUATION_in_multiplecolumn3434 = new BitSet(new long[]{0x0200000800000800L,0x0000000000000000L,0x0000000000000002L});
	public static final BitSet FOLLOW_129_in_multiplecolumn3443 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDENTIFIER_in_scopealias3485 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_BACKSINGLEQUOTE_in_scopealias3491 = new BitSet(new long[]{0x0200000000000000L});
	public static final BitSet FOLLOW_IDENTIFIER_in_scopealias3493 = new BitSet(new long[]{0x0000000000000800L});
	public static final BitSet FOLLOW_BACKSINGLEQUOTE_in_scopealias3495 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DOUBLEQUOTE_in_scopealias3501 = new BitSet(new long[]{0x0200000000000000L});
	public static final BitSet FOLLOW_IDENTIFIER_in_scopealias3503 = new BitSet(new long[]{0x0000000800000000L});
	public static final BitSet FOLLOW_DOUBLEQUOTE_in_scopealias3505 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DOUBLEQUOTE_in_razoralias3526 = new BitSet(new long[]{0x0200000000000000L});
	public static final BitSet FOLLOW_IDENTIFIER_in_razoralias3528 = new BitSet(new long[]{0x0000000000040000L,0x0000001000000000L});
	public static final BitSet FOLLOW_set_in_razoralias3531 = new BitSet(new long[]{0x0200000000000000L});
	public static final BitSet FOLLOW_IDENTIFIER_in_razoralias3539 = new BitSet(new long[]{0x0000000800040000L,0x0000001000000000L});
	public static final BitSet FOLLOW_DOUBLEQUOTE_in_razoralias3543 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_name_in_functioncall3569 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_functioncall3571 = new BitSet(new long[]{0x0200000A00000800L,0x0000820001000000L,0x0000000000000002L});
	public static final BitSet FOLLOW_DISTINCT_in_functioncall3573 = new BitSet(new long[]{0x0200000800000800L,0x0000820001000000L,0x0000000000000002L});
	public static final BitSet FOLLOW_functionparameters_in_functioncall3576 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_functioncall3578 = new BitSet(new long[]{0x0200000800000102L,0x0000800000000000L});
	public static final BitSet FOLLOW_alias_in_functioncall3585 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_functionparameterresume_in_functionparameters3663 = new BitSet(new long[]{0x0000000000100002L});
	public static final BitSet FOLLOW_COMMA_in_functionparameters3680 = new BitSet(new long[]{0x0200000800000800L,0x0000800001000000L});
	public static final BitSet FOLLOW_functionparameterresume_in_functionparameters3682 = new BitSet(new long[]{0x0000000000100002L});
	public static final BitSet FOLLOW_joker_in_functionparameters3708 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_129_in_joker3747 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_mystringliteral_in_functionparameterresume3764 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_column_in_functionparameterresume3768 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_number_in_functionparameterresume3772 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_literal_in_mystringliteral3785 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SINGLEQUOTE_in_literal3807 = new BitSet(new long[]{0xFFFFFFFFFFFFFFF0L,0xFFFFFFFFFFBFFFFFL,0x0000000000000FFFL});
	public static final BitSet FOLLOW_SINGLEQUOTE_in_literal3829 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_stringliteralnode_in_stringliteral3844 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SINGLEQUOTE_in_stringliteralnode3861 = new BitSet(new long[]{0xFFFFFFFFFFFFFFF0L,0xFFFFFFFFFFBFFFFFL,0x0000000000000FFFL});
	public static final BitSet FOLLOW_SINGLEQUOTE_in_stringliteralnode3884 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DOUBLEQUOTE_in_stringliteralnode3893 = new BitSet(new long[]{0xFFFFFFFFFFFFFFF0L,0xFFFFFFFFFFBFFFFFL,0x0000000000000FFFL});
	public static final BitSet FOLLOW_DOUBLEQUOTE_in_stringliteralnode3916 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_comparisonoperatorbase_in_comparisonoperator3958 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ASKEYWORD_in_alias4228 = new BitSet(new long[]{0x0200000000000000L});
	public static final BitSet FOLLOW_IDENTIFIER_in_alias4230 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ASKEYWORD_in_alias4261 = new BitSet(new long[]{0x0000000800000000L,0x0000800000000000L});
	public static final BitSet FOLLOW_aliasliteral_in_alias4263 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDENTIFIER_in_alias4315 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_aliasliteral_in_alias4340 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SINGLEQUOTE_in_aliasliteral4371 = new BitSet(new long[]{0xFFFFFFFFFFFFFFF0L,0xFFFFFFFFFFFFFFFFL,0x0000000000000FFFL});
	public static final BitSet FOLLOW_set_in_aliasliteral4375 = new BitSet(new long[]{0xFFFFFFFFFFFFFFF0L,0xFFFFFFFFFFFFFFFFL,0x0000000000000FFFL});
	public static final BitSet FOLLOW_SINGLEQUOTE_in_aliasliteral4381 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DOUBLEQUOTE_in_aliasliteral4387 = new BitSet(new long[]{0x0200000000000000L});
	public static final BitSet FOLLOW_IDENTIFIER_in_aliasliteral4389 = new BitSet(new long[]{0x0000000800000000L});
	public static final BitSet FOLLOW_DOUBLEQUOTE_in_aliasliteral4391 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_scopealias_in_column4411 = new BitSet(new long[]{0x0000000000040000L,0x0000001000000000L});
	public static final BitSet FOLLOW_COLON_in_column4414 = new BitSet(new long[]{0x0200000800000800L});
	public static final BitSet FOLLOW_PUNCTUATION_in_column4418 = new BitSet(new long[]{0x0200000800000800L});
	public static final BitSet FOLLOW_name_in_column4428 = new BitSet(new long[]{0x0200000800000102L,0x0000800000000000L});
	public static final BitSet FOLLOW_DOUBLEQUOTE_in_column4433 = new BitSet(new long[]{0x0200000000000000L});
	public static final BitSet FOLLOW_name_in_column4435 = new BitSet(new long[]{0x0000000800000000L});
	public static final BitSet FOLLOW_DOUBLEQUOTE_in_column4437 = new BitSet(new long[]{0x0200000800000102L,0x0000800000000000L});
	public static final BitSet FOLLOW_alias_in_column4445 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LIKEKEYWORD_in_likeclause4501 = new BitSet(new long[]{0x0000000000000000L,0x0000800000000000L});
	public static final BitSet FOLLOW_likesubclause_in_likeclause4503 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SINGLEQUOTE_in_likesubclause4519 = new BitSet(new long[]{0xFFFFFFF7FFFFFFF0L,0xFFFFFFFFFFBFFFFFL,0x0000000000000FFFL});
	public static final BitSet FOLLOW_SINGLEQUOTE_in_likesubclause4547 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_BACKQUOTE_in_table4567 = new BitSet(new long[]{0x0200000000000000L});
	public static final BitSet FOLLOW_IDENTIFIER_in_table4569 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_BACKQUOTE_in_table4571 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SINGLEQUOTE_in_table4583 = new BitSet(new long[]{0x0200000000000000L});
	public static final BitSet FOLLOW_IDENTIFIER_in_table4585 = new BitSet(new long[]{0x0000000000000000L,0x0000800000000000L});
	public static final BitSet FOLLOW_SINGLEQUOTE_in_table4587 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DOUBLEQUOTE_in_table4598 = new BitSet(new long[]{0x0200000000000000L});
	public static final BitSet FOLLOW_IDENTIFIER_in_table4600 = new BitSet(new long[]{0x0000000800000000L});
	public static final BitSet FOLLOW_DOUBLEQUOTE_in_table4602 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ESCAPEDDOUBLEQUOTE_in_table4614 = new BitSet(new long[]{0x0200000000000000L});
	public static final BitSet FOLLOW_IDENTIFIER_in_table4616 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_ESCAPEDDOUBLEQUOTE_in_table4618 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDENTIFIER_in_table4630 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_disjunction_in_synpred1_JdbcGrammar530 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_conjunction_in_synpred2_JdbcGrammar539 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_subconjunction_in_synpred3_JdbcGrammar548 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_subdisjunction_in_synpred4_JdbcGrammar557 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_exprlvl0_in_synpred5_JdbcGrammar566 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NOTKEYWORD_in_synpred6_JdbcGrammar581 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_synpred6_JdbcGrammar583 = new BitSet(new long[]{0x0000000000000000L,0x0000000000804000L});
	public static final BitSet FOLLOW_subdisjunction_in_synpred6_JdbcGrammar585 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_synpred6_JdbcGrammar587 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NOTKEYWORD_in_synpred7_JdbcGrammar610 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_synpred7_JdbcGrammar612 = new BitSet(new long[]{0x0200000800000800L,0x0000800000804000L});
	public static final BitSet FOLLOW_disjunction_in_synpred7_JdbcGrammar614 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_synpred7_JdbcGrammar616 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPARAM_in_synpred8_JdbcGrammar639 = new BitSet(new long[]{0x0000000000000000L,0x0000000000804000L});
	public static final BitSet FOLLOW_subdisjunction_in_synpred8_JdbcGrammar642 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_synpred8_JdbcGrammar644 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPARAM_in_synpred9_JdbcGrammar662 = new BitSet(new long[]{0x0200000800000800L,0x0000800000804000L});
	public static final BitSet FOLLOW_disjunction_in_synpred9_JdbcGrammar665 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_synpred9_JdbcGrammar667 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_conjunction_in_synpred10_JdbcGrammar691 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_subconjunction_in_synpred11_JdbcGrammar700 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_subdisjunction_in_synpred12_JdbcGrammar709 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_subconjunction_in_synpred13_JdbcGrammar773 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_subdisjunction_in_synpred14_JdbcGrammar782 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NOTKEYWORD_in_synpred15_JdbcGrammar802 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_synpred15_JdbcGrammar804 = new BitSet(new long[]{0x0000000000000000L,0x0000000000804000L});
	public static final BitSet FOLLOW_subconjunction_in_synpred15_JdbcGrammar806 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_synpred15_JdbcGrammar808 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NOTKEYWORD_in_synpred16_JdbcGrammar831 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_synpred16_JdbcGrammar833 = new BitSet(new long[]{0x0200000800000800L,0x0000800000804000L});
	public static final BitSet FOLLOW_conjunction_in_synpred16_JdbcGrammar835 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_synpred16_JdbcGrammar837 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPARAM_in_synpred17_JdbcGrammar860 = new BitSet(new long[]{0x0000000000000000L,0x0000000000804000L});
	public static final BitSet FOLLOW_subconjunction_in_synpred17_JdbcGrammar863 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_synpred17_JdbcGrammar865 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPARAM_in_synpred18_JdbcGrammar883 = new BitSet(new long[]{0x0200000800000800L,0x0000800000804000L});
	public static final BitSet FOLLOW_conjunction_in_synpred18_JdbcGrammar886 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_synpred18_JdbcGrammar888 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NOTKEYWORD_in_synpred19_JdbcGrammar961 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_synpred19_JdbcGrammar963 = new BitSet(new long[]{0x0200000800000800L,0x0000800000804000L});
	public static final BitSet FOLLOW_exprlvl0_in_synpred19_JdbcGrammar965 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_synpred19_JdbcGrammar967 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NOTKEYWORD_in_synpred20_JdbcGrammar990 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LPARAM_in_synpred20_JdbcGrammar992 = new BitSet(new long[]{0x0200000800000800L,0x0000800000004000L});
	public static final BitSet FOLLOW_exprlvl1_in_synpred20_JdbcGrammar994 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_synpred20_JdbcGrammar996 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPARAM_in_synpred21_JdbcGrammar1019 = new BitSet(new long[]{0x0200000800000800L,0x0000800000804000L});
	public static final BitSet FOLLOW_exprlvl0_in_synpred21_JdbcGrammar1022 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_synpred21_JdbcGrammar1024 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPARAM_in_synpred22_JdbcGrammar1042 = new BitSet(new long[]{0x0200000800000800L,0x0000800000004000L});
	public static final BitSet FOLLOW_exprlvl1_in_synpred22_JdbcGrammar1045 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
	public static final BitSet FOLLOW_RPARAM_in_synpred22_JdbcGrammar1047 = new BitSet(new long[]{0x0000000000000002L});
}

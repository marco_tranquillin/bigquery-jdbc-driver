package net.starschema.clouddb.jdbc;
// $ANTLR 3.5.2 JDBCTokens.g 2015-05-16 13:18:05

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

/**
 * Starschema Big Query JDBC Driver
 * Copyright (C) 2012, Starschema Ltd.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * This grammar implement the sql select statement
 *    @author Horvath Attila
 *    @author Balazs Gunics
 */
@SuppressWarnings("all")
public class JDBCTokens extends Lexer {
	public static final int EOF=-1;
	public static final int A=4;
	public static final int ALIAS=5;
	public static final int AND=6;
	public static final int ASC=7;
	public static final int ASKEYWORD=8;
	public static final int B=9;
	public static final int BACKQUOTE=10;
	public static final int BOOLEANEXPRESSION=11;
	public static final int BYKEYWORD=12;
	public static final int C=13;
	public static final int COLON=14;
	public static final int COLUMN=15;
	public static final int COMMA=16;
	public static final int COMMENT=17;
	public static final int COMPARISONOPERATOR=18;
	public static final int CONDITION=19;
	public static final int D=20;
	public static final int DATASETNAME=21;
	public static final int DATASOURCE=22;
	public static final int DESC=23;
	public static final int DIGIT=24;
	public static final int DISTINCT=25;
	public static final int DIVIDER=26;
	public static final int DOUBLEQUOTE=27;
	public static final int E=28;
	public static final int EACH=29;
	public static final int ESCAPEDDOUBLEQUOTE=30;
	public static final int ESCAPEDSINGLEQUOTE=31;
	public static final int EXPRESSION=32;
	public static final int EXPRESSIONTEXT=33;
	public static final int End_Comment=34;
	public static final int F=35;
	public static final int FROMEXPRESSION=36;
	public static final int FROMKEYWORD=37;
	public static final int FULl_KEYWORD=38;
	public static final int FUNCTIONCALL=39;
	public static final int FUNCTIONPARAMETERS=40;
	public static final int G=41;
	public static final int GROUPBYEXPRESSION=42;
	public static final int GROUPKEYWORD=43;
	public static final int H=44;
	public static final int HAVINGEXPRESSION=45;
	public static final int HAVINGKEYWORD=46;
	public static final int HIGHCHAR=47;
	public static final int I=48;
	public static final int IDENTIFIER=49;
	public static final int INNERKEYWORD=50;
	public static final int INTEGERPARAM=51;
	public static final int J=52;
	public static final int JOINEXPRESSION=53;
	public static final int JOINKEYWORD=54;
	public static final int JOINTYPE=55;
	public static final int K=56;
	public static final int L=57;
	public static final int LEFT_KEYWORD=58;
	public static final int LIKEKEYWORD=59;
	public static final int LIMITKEYWORD=60;
	public static final int LINE_COMMENT=61;
	public static final int LOGICALOPERATOR=62;
	public static final int LOWCHAR=63;
	public static final int LPARAM=64;
	public static final int Line_Comment=65;
	public static final int M=66;
	public static final int MULTIPLECALL=67;
	public static final int N=68;
	public static final int NAME=69;
	public static final int NL=70;
	public static final int NOTKEYWORD=71;
	public static final int NUMBER=72;
	public static final int O=73;
	public static final int ONCLAUSE=74;
	public static final int ONKEYWORD=75;
	public static final int OR=76;
	public static final int ORDERBYEXPRESSION=77;
	public static final int ORDERKEYWORD=78;
	public static final int P=79;
	public static final int PROJECTNAME=80;
	public static final int PUNCTUATION=81;
	public static final int Q=82;
	public static final int R=83;
	public static final int RIGHT_KEYWORD=84;
	public static final int RPARAM=85;
	public static final int S=86;
	public static final int SELECTKEYWORD=87;
	public static final int SELECTSTATEMENT=88;
	public static final int SEMICOLON=89;
	public static final int SINGLEQUOTE=90;
	public static final int SOURCETABLE=91;
	public static final int STRINGLIT=92;
	public static final int SUBQUERY=93;
	public static final int Start_Comment=94;
	public static final int T=95;
	public static final int TABLENAME=96;
	public static final int TEXT=97;
	public static final int U=98;
	public static final int V=99;
	public static final int W=100;
	public static final int WHEREEXPRESSION=101;
	public static final int WHEREKEYWORD=102;
	public static final int WS=103;
	public static final int X=104;
	public static final int Y=105;
	public static final int Z=106;

	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public JDBCTokens() {} 
	public JDBCTokens(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public JDBCTokens(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "JDBCTokens.g"; }

	// $ANTLR start "AND"
	public final void mAND() throws RecognitionException {
		try {
			int _type = AND;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:29:4: ( A N D )
			// JDBCTokens.g:29:9: A N D
			{
			mA(); 

			mN(); 

			mD(); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "AND"

	// $ANTLR start "ASKEYWORD"
	public final void mASKEYWORD() throws RecognitionException {
		try {
			int _type = ASKEYWORD;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:31:10: ( A S )
			// JDBCTokens.g:31:13: A S
			{
			mA(); 

			mS(); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ASKEYWORD"

	// $ANTLR start "ASC"
	public final void mASC() throws RecognitionException {
		try {
			int _type = ASC;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:33:4: ( ( A S C ) | ( A S C E N D I N G ) )
			int alt1=2;
			int LA1_0 = input.LA(1);
			if ( (LA1_0=='A'||LA1_0=='a') ) {
				int LA1_1 = input.LA(2);
				if ( (LA1_1=='S'||LA1_1=='s') ) {
					int LA1_2 = input.LA(3);
					if ( (LA1_2=='C'||LA1_2=='c') ) {
						int LA1_3 = input.LA(4);
						if ( (LA1_3=='E'||LA1_3=='e') ) {
							alt1=2;
						}

						else {
							alt1=1;
						}

					}

					else {
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 1, 2, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 1, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 1, 0, input);
				throw nvae;
			}

			switch (alt1) {
				case 1 :
					// JDBCTokens.g:33:17: ( A S C )
					{
					// JDBCTokens.g:33:17: ( A S C )
					// JDBCTokens.g:33:18: A S C
					{
					mA(); 

					mS(); 

					mC(); 

					}

					}
					break;
				case 2 :
					// JDBCTokens.g:33:27: ( A S C E N D I N G )
					{
					// JDBCTokens.g:33:27: ( A S C E N D I N G )
					// JDBCTokens.g:33:28: A S C E N D I N G
					{
					mA(); 

					mS(); 

					mC(); 

					mE(); 

					mN(); 

					mD(); 

					mI(); 

					mN(); 

					mG(); 

					}

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ASC"

	// $ANTLR start "BYKEYWORD"
	public final void mBYKEYWORD() throws RecognitionException {
		try {
			int _type = BYKEYWORD;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:36:10: ( B Y )
			// JDBCTokens.g:36:17: B Y
			{
			mB(); 

			mY(); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BYKEYWORD"

	// $ANTLR start "DESC"
	public final void mDESC() throws RecognitionException {
		try {
			int _type = DESC;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:39:5: ( ( D E S C ) | ( D E S C E N D I N G ) )
			int alt2=2;
			int LA2_0 = input.LA(1);
			if ( (LA2_0=='D'||LA2_0=='d') ) {
				int LA2_1 = input.LA(2);
				if ( (LA2_1=='E'||LA2_1=='e') ) {
					int LA2_2 = input.LA(3);
					if ( (LA2_2=='S'||LA2_2=='s') ) {
						int LA2_3 = input.LA(4);
						if ( (LA2_3=='C'||LA2_3=='c') ) {
							int LA2_4 = input.LA(5);
							if ( (LA2_4=='E'||LA2_4=='e') ) {
								alt2=2;
							}

							else {
								alt2=1;
							}

						}

						else {
							int nvaeMark = input.mark();
							try {
								for (int nvaeConsume = 0; nvaeConsume < 4 - 1; nvaeConsume++) {
									input.consume();
								}
								NoViableAltException nvae =
									new NoViableAltException("", 2, 3, input);
								throw nvae;
							} finally {
								input.rewind(nvaeMark);
							}
						}

					}

					else {
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 2, 2, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 2, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 2, 0, input);
				throw nvae;
			}

			switch (alt2) {
				case 1 :
					// JDBCTokens.g:39:17: ( D E S C )
					{
					// JDBCTokens.g:39:17: ( D E S C )
					// JDBCTokens.g:39:18: D E S C
					{
					mD(); 

					mE(); 

					mS(); 

					mC(); 

					}

					}
					break;
				case 2 :
					// JDBCTokens.g:39:28: ( D E S C E N D I N G )
					{
					// JDBCTokens.g:39:28: ( D E S C E N D I N G )
					// JDBCTokens.g:39:29: D E S C E N D I N G
					{
					mD(); 

					mE(); 

					mS(); 

					mC(); 

					mE(); 

					mN(); 

					mD(); 

					mI(); 

					mN(); 

					mG(); 

					}

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DESC"

	// $ANTLR start "DISTINCT"
	public final void mDISTINCT() throws RecognitionException {
		try {
			int _type = DISTINCT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:42:9: ( D I S T I N C T )
			// JDBCTokens.g:42:11: D I S T I N C T
			{
			mD(); 

			mI(); 

			mS(); 

			mT(); 

			mI(); 

			mN(); 

			mC(); 

			mT(); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DISTINCT"

	// $ANTLR start "EACH"
	public final void mEACH() throws RecognitionException {
		try {
			int _type = EACH;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:45:5: ( E A C H )
			// JDBCTokens.g:45:7: E A C H
			{
			mE(); 

			mA(); 

			mC(); 

			mH(); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EACH"

	// $ANTLR start "FULl_KEYWORD"
	public final void mFULl_KEYWORD() throws RecognitionException {
		try {
			int _type = FULl_KEYWORD;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:48:13: ( F U L L )
			// JDBCTokens.g:48:15: F U L L
			{
			mF(); 

			mU(); 

			mL(); 

			mL(); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FULl_KEYWORD"

	// $ANTLR start "FROMKEYWORD"
	public final void mFROMKEYWORD() throws RecognitionException {
		try {
			int _type = FROMKEYWORD;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:50:12: ( F R O M )
			// JDBCTokens.g:50:17: F R O M
			{
			mF(); 

			mR(); 

			mO(); 

			mM(); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FROMKEYWORD"

	// $ANTLR start "GROUPKEYWORD"
	public final void mGROUPKEYWORD() throws RecognitionException {
		try {
			int _type = GROUPKEYWORD;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:53:13: ( G R O U P )
			// JDBCTokens.g:53:17: G R O U P
			{
			mG(); 

			mR(); 

			mO(); 

			mU(); 

			mP(); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "GROUPKEYWORD"

	// $ANTLR start "HAVINGKEYWORD"
	public final void mHAVINGKEYWORD() throws RecognitionException {
		try {
			int _type = HAVINGKEYWORD;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:56:14: ( H A V I N G )
			// JDBCTokens.g:56:17: H A V I N G
			{
			mH(); 

			mA(); 

			mV(); 

			mI(); 

			mN(); 

			mG(); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "HAVINGKEYWORD"

	// $ANTLR start "INNERKEYWORD"
	public final void mINNERKEYWORD() throws RecognitionException {
		try {
			int _type = INNERKEYWORD;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:59:13: ( I N N E R )
			// JDBCTokens.g:59:17: I N N E R
			{
			mI(); 

			mN(); 

			mN(); 

			mE(); 

			mR(); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INNERKEYWORD"

	// $ANTLR start "JOINKEYWORD"
	public final void mJOINKEYWORD() throws RecognitionException {
		try {
			int _type = JOINKEYWORD;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:62:12: ( J O I N )
			// JDBCTokens.g:62:17: J O I N
			{
			mJ(); 

			mO(); 

			mI(); 

			mN(); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "JOINKEYWORD"

	// $ANTLR start "LEFT_KEYWORD"
	public final void mLEFT_KEYWORD() throws RecognitionException {
		try {
			int _type = LEFT_KEYWORD;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:65:13: ( L E F T )
			// JDBCTokens.g:65:15: L E F T
			{
			mL(); 

			mE(); 

			mF(); 

			mT(); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LEFT_KEYWORD"

	// $ANTLR start "LIKEKEYWORD"
	public final void mLIKEKEYWORD() throws RecognitionException {
		try {
			int _type = LIKEKEYWORD;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:67:12: ( L I K E )
			// JDBCTokens.g:67:17: L I K E
			{
			mL(); 

			mI(); 

			mK(); 

			mE(); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LIKEKEYWORD"

	// $ANTLR start "LIMITKEYWORD"
	public final void mLIMITKEYWORD() throws RecognitionException {
		try {
			int _type = LIMITKEYWORD;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:69:13: ( L I M I T )
			// JDBCTokens.g:69:15: L I M I T
			{
			mL(); 

			mI(); 

			mM(); 

			mI(); 

			mT(); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LIMITKEYWORD"

	// $ANTLR start "NOTKEYWORD"
	public final void mNOTKEYWORD() throws RecognitionException {
		try {
			int _type = NOTKEYWORD;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:72:11: ( N O T )
			// JDBCTokens.g:72:17: N O T
			{
			mN(); 

			mO(); 

			mT(); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NOTKEYWORD"

	// $ANTLR start "ONKEYWORD"
	public final void mONKEYWORD() throws RecognitionException {
		try {
			int _type = ONKEYWORD;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:75:10: ( O N )
			// JDBCTokens.g:75:17: O N
			{
			mO(); 

			mN(); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ONKEYWORD"

	// $ANTLR start "OR"
	public final void mOR() throws RecognitionException {
		try {
			int _type = OR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:77:3: ( O R )
			// JDBCTokens.g:77:9: O R
			{
			mO(); 

			mR(); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "OR"

	// $ANTLR start "ORDERKEYWORD"
	public final void mORDERKEYWORD() throws RecognitionException {
		try {
			int _type = ORDERKEYWORD;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:79:13: ( O R D E R )
			// JDBCTokens.g:79:17: O R D E R
			{
			mO(); 

			mR(); 

			mD(); 

			mE(); 

			mR(); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ORDERKEYWORD"

	// $ANTLR start "RIGHT_KEYWORD"
	public final void mRIGHT_KEYWORD() throws RecognitionException {
		try {
			int _type = RIGHT_KEYWORD;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:82:14: ( R I G H T )
			// JDBCTokens.g:82:16: R I G H T
			{
			mR(); 

			mI(); 

			mG(); 

			mH(); 

			mT(); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RIGHT_KEYWORD"

	// $ANTLR start "SELECTKEYWORD"
	public final void mSELECTKEYWORD() throws RecognitionException {
		try {
			int _type = SELECTKEYWORD;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:85:14: ( S E L E C T )
			// JDBCTokens.g:85:17: S E L E C T
			{
			mS(); 

			mE(); 

			mL(); 

			mE(); 

			mC(); 

			mT(); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SELECTKEYWORD"

	// $ANTLR start "WHEREKEYWORD"
	public final void mWHEREKEYWORD() throws RecognitionException {
		try {
			int _type = WHEREKEYWORD;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:88:13: ( W H E R E )
			// JDBCTokens.g:88:17: W H E R E
			{
			mW(); 

			mH(); 

			mE(); 

			mR(); 

			mE(); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WHEREKEYWORD"

	// $ANTLR start "BACKQUOTE"
	public final void mBACKQUOTE() throws RecognitionException {
		try {
			int _type = BACKQUOTE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:95:11: ( '`' )
			// JDBCTokens.g:95:15: '`'
			{
			match('`'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BACKQUOTE"

	// $ANTLR start "COLON"
	public final void mCOLON() throws RecognitionException {
		try {
			int _type = COLON;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:98:6: ( ':' )
			// JDBCTokens.g:98:15: ':'
			{
			match(':'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COLON"

	// $ANTLR start "COMMA"
	public final void mCOMMA() throws RecognitionException {
		try {
			int _type = COMMA;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:100:6: ( ',' )
			// JDBCTokens.g:100:15: ','
			{
			match(','); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMA"

	// $ANTLR start "DOUBLEQUOTE"
	public final void mDOUBLEQUOTE() throws RecognitionException {
		try {
			int _type = DOUBLEQUOTE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:103:12: ( '\\\"' )
			// JDBCTokens.g:103:15: '\\\"'
			{
			match('\"'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DOUBLEQUOTE"

	// $ANTLR start "ESCAPEDDOUBLEQUOTE"
	public final void mESCAPEDDOUBLEQUOTE() throws RecognitionException {
		try {
			int _type = ESCAPEDDOUBLEQUOTE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:106:20: ( '\\\\\"' )
			// JDBCTokens.g:106:24: '\\\\\"'
			{
			match("\\\""); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ESCAPEDDOUBLEQUOTE"

	// $ANTLR start "ESCAPEDSINGLEQUOTE"
	public final void mESCAPEDSINGLEQUOTE() throws RecognitionException {
		try {
			int _type = ESCAPEDSINGLEQUOTE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:108:19: ( '\\\\\\'' )
			// JDBCTokens.g:108:21: '\\\\\\''
			{
			match("\\'"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ESCAPEDSINGLEQUOTE"

	// $ANTLR start "LPARAM"
	public final void mLPARAM() throws RecognitionException {
		try {
			int _type = LPARAM;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:111:7: ( '(' )
			// JDBCTokens.g:111:15: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LPARAM"

	// $ANTLR start "PUNCTUATION"
	public final void mPUNCTUATION() throws RecognitionException {
		try {
			int _type = PUNCTUATION;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:114:12: ( '.' )
			// JDBCTokens.g:114:15: '.'
			{
			match('.'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PUNCTUATION"

	// $ANTLR start "RPARAM"
	public final void mRPARAM() throws RecognitionException {
		try {
			int _type = RPARAM;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:117:7: ( ')' )
			// JDBCTokens.g:117:15: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RPARAM"

	// $ANTLR start "SEMICOLON"
	public final void mSEMICOLON() throws RecognitionException {
		try {
			int _type = SEMICOLON;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:120:10: ( ';' )
			// JDBCTokens.g:120:15: ';'
			{
			match(';'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SEMICOLON"

	// $ANTLR start "SINGLEQUOTE"
	public final void mSINGLEQUOTE() throws RecognitionException {
		try {
			int _type = SINGLEQUOTE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:122:12: ( '\\'' )
			// JDBCTokens.g:122:15: '\\''
			{
			match('\''); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SINGLEQUOTE"

	// $ANTLR start "NUMBER"
	public final void mNUMBER() throws RecognitionException {
		try {
			int _type = NUMBER;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:128:1: ( ( DIGIT )+ )
			// JDBCTokens.g:129:1: ( DIGIT )+
			{
			// JDBCTokens.g:129:1: ( DIGIT )+
			int cnt3=0;
			loop3:
			while (true) {
				int alt3=2;
				int LA3_0 = input.LA(1);
				if ( ((LA3_0 >= '0' && LA3_0 <= '9')) ) {
					alt3=1;
				}

				switch (alt3) {
				case 1 :
					// JDBCTokens.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt3 >= 1 ) break loop3;
					EarlyExitException eee = new EarlyExitException(3, input);
					throw eee;
				}
				cnt3++;
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NUMBER"

	// $ANTLR start "IDENTIFIER"
	public final void mIDENTIFIER() throws RecognitionException {
		try {
			int _type = IDENTIFIER;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:134:5: ( ( LOWCHAR | HIGHCHAR | '_' | DIGIT ) ( LOWCHAR | HIGHCHAR | DIGIT | '_' | '%' )* )
			// JDBCTokens.g:135:5: ( LOWCHAR | HIGHCHAR | '_' | DIGIT ) ( LOWCHAR | HIGHCHAR | DIGIT | '_' | '%' )*
			{
			if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// JDBCTokens.g:135:39: ( LOWCHAR | HIGHCHAR | DIGIT | '_' | '%' )*
			loop4:
			while (true) {
				int alt4=2;
				int LA4_0 = input.LA(1);
				if ( (LA4_0=='%'||(LA4_0 >= '0' && LA4_0 <= '9')||(LA4_0 >= 'A' && LA4_0 <= 'Z')||LA4_0=='_'||(LA4_0 >= 'a' && LA4_0 <= 'z')) ) {
					alt4=1;
				}

				switch (alt4) {
				case 1 :
					// JDBCTokens.g:
					{
					if ( input.LA(1)=='%'||(input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop4;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "IDENTIFIER"

	// $ANTLR start "NL"
	public final void mNL() throws RecognitionException {
		try {
			// JDBCTokens.g:139:5: ( '\\r' | '\\n' )
			// JDBCTokens.g:
			{
			if ( input.LA(1)=='\n'||input.LA(1)=='\r' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NL"

	// $ANTLR start "WS"
	public final void mWS() throws RecognitionException {
		try {
			int _type = WS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:147:5: ( ( ' ' | '\\t' | '\\n' | '\\r' | '\\f' )+ )
			// JDBCTokens.g:148:5: ( ' ' | '\\t' | '\\n' | '\\r' | '\\f' )+
			{
			// JDBCTokens.g:148:5: ( ' ' | '\\t' | '\\n' | '\\r' | '\\f' )+
			int cnt5=0;
			loop5:
			while (true) {
				int alt5=2;
				int LA5_0 = input.LA(1);
				if ( ((LA5_0 >= '\t' && LA5_0 <= '\n')||(LA5_0 >= '\f' && LA5_0 <= '\r')||LA5_0==' ') ) {
					alt5=1;
				}

				switch (alt5) {
				case 1 :
					// JDBCTokens.g:
					{
					if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||(input.LA(1) >= '\f' && input.LA(1) <= '\r')||input.LA(1)==' ' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt5 >= 1 ) break loop5;
					EarlyExitException eee = new EarlyExitException(5, input);
					throw eee;
				}
				cnt5++;
			}

			_channel = HIDDEN;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WS"

	// $ANTLR start "COMMENT"
	public final void mCOMMENT() throws RecognitionException {
		try {
			int _type = COMMENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:153:5: ( ( Start_Comment ( options {greedy=false; } : . )* End_Comment )+ )
			// JDBCTokens.g:153:9: ( Start_Comment ( options {greedy=false; } : . )* End_Comment )+
			{
			// JDBCTokens.g:153:9: ( Start_Comment ( options {greedy=false; } : . )* End_Comment )+
			int cnt7=0;
			loop7:
			while (true) {
				int alt7=2;
				int LA7_0 = input.LA(1);
				if ( (LA7_0=='/') ) {
					alt7=1;
				}

				switch (alt7) {
				case 1 :
					// JDBCTokens.g:153:13: Start_Comment ( options {greedy=false; } : . )* End_Comment
					{
					mStart_Comment(); 

					// JDBCTokens.g:153:27: ( options {greedy=false; } : . )*
					loop6:
					while (true) {
						int alt6=2;
						int LA6_0 = input.LA(1);
						if ( (LA6_0=='*') ) {
							int LA6_1 = input.LA(2);
							if ( (LA6_1=='/') ) {
								alt6=2;
							}
							else if ( ((LA6_1 >= '\u0000' && LA6_1 <= '.')||(LA6_1 >= '0' && LA6_1 <= '\uFFFF')) ) {
								alt6=1;
							}

						}
						else if ( ((LA6_0 >= '\u0000' && LA6_0 <= ')')||(LA6_0 >= '+' && LA6_0 <= '\uFFFF')) ) {
							alt6=1;
						}

						switch (alt6) {
						case 1 :
							// JDBCTokens.g:153:55: .
							{
							matchAny(); 
							}
							break;

						default :
							break loop6;
						}
					}

					mEnd_Comment(); 

					}
					break;

				default :
					if ( cnt7 >= 1 ) break loop7;
					EarlyExitException eee = new EarlyExitException(7, input);
					throw eee;
				}
				cnt7++;
			}


			      _channel=HIDDEN;
			    
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMENT"

	// $ANTLR start "LINE_COMMENT"
	public final void mLINE_COMMENT() throws RecognitionException {
		try {
			int _type = LINE_COMMENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JDBCTokens.g:160:5: ( ( ( Line_Comment | '--' ) (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n' )+ )
			// JDBCTokens.g:160:9: ( ( Line_Comment | '--' ) (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n' )+
			{
			// JDBCTokens.g:160:9: ( ( Line_Comment | '--' ) (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n' )+
			int cnt11=0;
			loop11:
			while (true) {
				int alt11=2;
				int LA11_0 = input.LA(1);
				if ( (LA11_0=='-'||LA11_0=='/') ) {
					alt11=1;
				}

				switch (alt11) {
				case 1 :
					// JDBCTokens.g:160:13: ( Line_Comment | '--' ) (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n'
					{
					// JDBCTokens.g:160:13: ( Line_Comment | '--' )
					int alt8=2;
					int LA8_0 = input.LA(1);
					if ( (LA8_0=='/') ) {
						alt8=1;
					}
					else if ( (LA8_0=='-') ) {
						alt8=2;
					}

					else {
						NoViableAltException nvae =
							new NoViableAltException("", 8, 0, input);
						throw nvae;
					}

					switch (alt8) {
						case 1 :
							// JDBCTokens.g:160:15: Line_Comment
							{
							mLine_Comment(); 

							}
							break;
						case 2 :
							// JDBCTokens.g:160:30: '--'
							{
							match("--"); 

							}
							break;

					}

					// JDBCTokens.g:160:37: (~ ( '\\n' | '\\r' ) )*
					loop9:
					while (true) {
						int alt9=2;
						int LA9_0 = input.LA(1);
						if ( ((LA9_0 >= '\u0000' && LA9_0 <= '\t')||(LA9_0 >= '\u000B' && LA9_0 <= '\f')||(LA9_0 >= '\u000E' && LA9_0 <= '\uFFFF')) ) {
							alt9=1;
						}

						switch (alt9) {
						case 1 :
							// JDBCTokens.g:
							{
							if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '\uFFFF') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							break loop9;
						}
					}

					// JDBCTokens.g:160:51: ( '\\r' )?
					int alt10=2;
					int LA10_0 = input.LA(1);
					if ( (LA10_0=='\r') ) {
						alt10=1;
					}
					switch (alt10) {
						case 1 :
							// JDBCTokens.g:160:51: '\\r'
							{
							match('\r'); 
							}
							break;

					}

					match('\n'); 
					}
					break;

				default :
					if ( cnt11 >= 1 ) break loop11;
					EarlyExitException eee = new EarlyExitException(11, input);
					throw eee;
				}
				cnt11++;
			}


			      _channel=HIDDEN;
			    
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LINE_COMMENT"

	// $ANTLR start "LOWCHAR"
	public final void mLOWCHAR() throws RecognitionException {
		try {
			// JDBCTokens.g:169:5: ( 'a' .. 'z' )
			// JDBCTokens.g:
			{
			if ( (input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LOWCHAR"

	// $ANTLR start "HIGHCHAR"
	public final void mHIGHCHAR() throws RecognitionException {
		try {
			// JDBCTokens.g:172:5: ( 'A' .. 'Z' )
			// JDBCTokens.g:
			{
			if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "HIGHCHAR"

	// $ANTLR start "DIGIT"
	public final void mDIGIT() throws RecognitionException {
		try {
			// JDBCTokens.g:175:5: ( '0' .. '9' )
			// JDBCTokens.g:
			{
			if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DIGIT"

	// $ANTLR start "ALIAS"
	public final void mALIAS() throws RecognitionException {
		try {
			// JDBCTokens.g:179:15: ()
			// JDBCTokens.g:179:16: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ALIAS"

	// $ANTLR start "BOOLEANEXPRESSION"
	public final void mBOOLEANEXPRESSION() throws RecognitionException {
		try {
			// JDBCTokens.g:180:27: ()
			// JDBCTokens.g:180:28: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BOOLEANEXPRESSION"

	// $ANTLR start "COLUMN"
	public final void mCOLUMN() throws RecognitionException {
		try {
			// JDBCTokens.g:181:16: ()
			// JDBCTokens.g:181:17: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COLUMN"

	// $ANTLR start "COMPARISONOPERATOR"
	public final void mCOMPARISONOPERATOR() throws RecognitionException {
		try {
			// JDBCTokens.g:182:28: ()
			// JDBCTokens.g:182:29: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMPARISONOPERATOR"

	// $ANTLR start "CONDITION"
	public final void mCONDITION() throws RecognitionException {
		try {
			// JDBCTokens.g:183:19: ()
			// JDBCTokens.g:183:20: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CONDITION"

	// $ANTLR start "DATASETNAME"
	public final void mDATASETNAME() throws RecognitionException {
		try {
			// JDBCTokens.g:184:21: ()
			// JDBCTokens.g:184:22: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DATASETNAME"

	// $ANTLR start "DATASOURCE"
	public final void mDATASOURCE() throws RecognitionException {
		try {
			// JDBCTokens.g:185:20: ()
			// JDBCTokens.g:185:21: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DATASOURCE"

	// $ANTLR start "DIVIDER"
	public final void mDIVIDER() throws RecognitionException {
		try {
			// JDBCTokens.g:186:17: ()
			// JDBCTokens.g:186:18: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DIVIDER"

	// $ANTLR start "EXPRESSION"
	public final void mEXPRESSION() throws RecognitionException {
		try {
			// JDBCTokens.g:187:20: ()
			// JDBCTokens.g:187:21: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EXPRESSION"

	// $ANTLR start "FROMEXPRESSION"
	public final void mFROMEXPRESSION() throws RecognitionException {
		try {
			// JDBCTokens.g:188:24: ()
			// JDBCTokens.g:188:25: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FROMEXPRESSION"

	// $ANTLR start "FUNCTIONCALL"
	public final void mFUNCTIONCALL() throws RecognitionException {
		try {
			// JDBCTokens.g:189:22: ()
			// JDBCTokens.g:189:23: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FUNCTIONCALL"

	// $ANTLR start "FUNCTIONPARAMETERS"
	public final void mFUNCTIONPARAMETERS() throws RecognitionException {
		try {
			// JDBCTokens.g:190:28: ()
			// JDBCTokens.g:190:29: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FUNCTIONPARAMETERS"

	// $ANTLR start "GROUPBYEXPRESSION"
	public final void mGROUPBYEXPRESSION() throws RecognitionException {
		try {
			// JDBCTokens.g:191:27: ()
			// JDBCTokens.g:191:28: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "GROUPBYEXPRESSION"

	// $ANTLR start "HAVINGEXPRESSION"
	public final void mHAVINGEXPRESSION() throws RecognitionException {
		try {
			// JDBCTokens.g:192:26: ()
			// JDBCTokens.g:192:27: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "HAVINGEXPRESSION"

	// $ANTLR start "INTEGERPARAM"
	public final void mINTEGERPARAM() throws RecognitionException {
		try {
			// JDBCTokens.g:193:22: ()
			// JDBCTokens.g:193:23: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INTEGERPARAM"

	// $ANTLR start "JOINEXPRESSION"
	public final void mJOINEXPRESSION() throws RecognitionException {
		try {
			// JDBCTokens.g:194:24: ()
			// JDBCTokens.g:194:25: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "JOINEXPRESSION"

	// $ANTLR start "JOINTYPE"
	public final void mJOINTYPE() throws RecognitionException {
		try {
			// JDBCTokens.g:195:18: ()
			// JDBCTokens.g:195:19: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "JOINTYPE"

	// $ANTLR start "LOGICALOPERATOR"
	public final void mLOGICALOPERATOR() throws RecognitionException {
		try {
			// JDBCTokens.g:196:25: ()
			// JDBCTokens.g:196:26: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LOGICALOPERATOR"

	// $ANTLR start "MULTIPLECALL"
	public final void mMULTIPLECALL() throws RecognitionException {
		try {
			// JDBCTokens.g:197:22: ()
			// JDBCTokens.g:197:23: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MULTIPLECALL"

	// $ANTLR start "NAME"
	public final void mNAME() throws RecognitionException {
		try {
			// JDBCTokens.g:198:14: ()
			// JDBCTokens.g:198:15: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NAME"

	// $ANTLR start "ONCLAUSE"
	public final void mONCLAUSE() throws RecognitionException {
		try {
			// JDBCTokens.g:199:18: ()
			// JDBCTokens.g:199:19: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ONCLAUSE"

	// $ANTLR start "ORDERBYEXPRESSION"
	public final void mORDERBYEXPRESSION() throws RecognitionException {
		try {
			// JDBCTokens.g:200:27: ()
			// JDBCTokens.g:200:28: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ORDERBYEXPRESSION"

	// $ANTLR start "PROJECTNAME"
	public final void mPROJECTNAME() throws RecognitionException {
		try {
			// JDBCTokens.g:201:21: ()
			// JDBCTokens.g:201:22: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PROJECTNAME"

	// $ANTLR start "SELECTSTATEMENT"
	public final void mSELECTSTATEMENT() throws RecognitionException {
		try {
			// JDBCTokens.g:202:25: ()
			// JDBCTokens.g:202:26: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SELECTSTATEMENT"

	// $ANTLR start "SOURCETABLE"
	public final void mSOURCETABLE() throws RecognitionException {
		try {
			// JDBCTokens.g:203:21: ()
			// JDBCTokens.g:203:22: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SOURCETABLE"

	// $ANTLR start "STRINGLIT"
	public final void mSTRINGLIT() throws RecognitionException {
		try {
			// JDBCTokens.g:204:19: ()
			// JDBCTokens.g:204:20: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "STRINGLIT"

	// $ANTLR start "SUBQUERY"
	public final void mSUBQUERY() throws RecognitionException {
		try {
			// JDBCTokens.g:205:18: ()
			// JDBCTokens.g:205:19: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SUBQUERY"

	// $ANTLR start "TABLENAME"
	public final void mTABLENAME() throws RecognitionException {
		try {
			// JDBCTokens.g:206:19: ()
			// JDBCTokens.g:206:20: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "TABLENAME"

	// $ANTLR start "TEXT"
	public final void mTEXT() throws RecognitionException {
		try {
			// JDBCTokens.g:207:14: ()
			// JDBCTokens.g:207:15: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "TEXT"

	// $ANTLR start "WHEREEXPRESSION"
	public final void mWHEREEXPRESSION() throws RecognitionException {
		try {
			// JDBCTokens.g:208:25: ()
			// JDBCTokens.g:208:26: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WHEREEXPRESSION"

	// $ANTLR start "EXPRESSIONTEXT"
	public final void mEXPRESSIONTEXT() throws RecognitionException {
		try {
			// JDBCTokens.g:209:24: ()
			// JDBCTokens.g:209:25: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EXPRESSIONTEXT"

	// $ANTLR start "A"
	public final void mA() throws RecognitionException {
		try {
			// JDBCTokens.g:213:11: ( ( 'a' | 'A' ) )
			// JDBCTokens.g:
			{
			if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "A"

	// $ANTLR start "B"
	public final void mB() throws RecognitionException {
		try {
			// JDBCTokens.g:214:11: ( ( 'b' | 'B' ) )
			// JDBCTokens.g:
			{
			if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "B"

	// $ANTLR start "C"
	public final void mC() throws RecognitionException {
		try {
			// JDBCTokens.g:215:11: ( ( 'c' | 'C' ) )
			// JDBCTokens.g:
			{
			if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "C"

	// $ANTLR start "D"
	public final void mD() throws RecognitionException {
		try {
			// JDBCTokens.g:216:11: ( ( 'd' | 'D' ) )
			// JDBCTokens.g:
			{
			if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "D"

	// $ANTLR start "E"
	public final void mE() throws RecognitionException {
		try {
			// JDBCTokens.g:217:11: ( ( 'e' | 'E' ) )
			// JDBCTokens.g:
			{
			if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "E"

	// $ANTLR start "F"
	public final void mF() throws RecognitionException {
		try {
			// JDBCTokens.g:218:11: ( ( 'f' | 'F' ) )
			// JDBCTokens.g:
			{
			if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "F"

	// $ANTLR start "G"
	public final void mG() throws RecognitionException {
		try {
			// JDBCTokens.g:219:11: ( ( 'g' | 'G' ) )
			// JDBCTokens.g:
			{
			if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "G"

	// $ANTLR start "H"
	public final void mH() throws RecognitionException {
		try {
			// JDBCTokens.g:220:11: ( ( 'h' | 'H' ) )
			// JDBCTokens.g:
			{
			if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "H"

	// $ANTLR start "I"
	public final void mI() throws RecognitionException {
		try {
			// JDBCTokens.g:221:11: ( ( 'i' | 'I' ) )
			// JDBCTokens.g:
			{
			if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "I"

	// $ANTLR start "J"
	public final void mJ() throws RecognitionException {
		try {
			// JDBCTokens.g:222:11: ( ( 'j' | 'J' ) )
			// JDBCTokens.g:
			{
			if ( input.LA(1)=='J'||input.LA(1)=='j' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "J"

	// $ANTLR start "K"
	public final void mK() throws RecognitionException {
		try {
			// JDBCTokens.g:223:11: ( ( 'k' | 'K' ) )
			// JDBCTokens.g:
			{
			if ( input.LA(1)=='K'||input.LA(1)=='k' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "K"

	// $ANTLR start "L"
	public final void mL() throws RecognitionException {
		try {
			// JDBCTokens.g:224:11: ( ( 'l' | 'L' ) )
			// JDBCTokens.g:
			{
			if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "L"

	// $ANTLR start "M"
	public final void mM() throws RecognitionException {
		try {
			// JDBCTokens.g:225:11: ( ( 'm' | 'M' ) )
			// JDBCTokens.g:
			{
			if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "M"

	// $ANTLR start "N"
	public final void mN() throws RecognitionException {
		try {
			// JDBCTokens.g:226:11: ( ( 'n' | 'N' ) )
			// JDBCTokens.g:
			{
			if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "N"

	// $ANTLR start "O"
	public final void mO() throws RecognitionException {
		try {
			// JDBCTokens.g:227:11: ( ( 'o' | 'O' ) )
			// JDBCTokens.g:
			{
			if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "O"

	// $ANTLR start "P"
	public final void mP() throws RecognitionException {
		try {
			// JDBCTokens.g:228:11: ( ( 'p' | 'P' ) )
			// JDBCTokens.g:
			{
			if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "P"

	// $ANTLR start "Q"
	public final void mQ() throws RecognitionException {
		try {
			// JDBCTokens.g:229:11: ( ( 'q' | 'Q' ) )
			// JDBCTokens.g:
			{
			if ( input.LA(1)=='Q'||input.LA(1)=='q' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "Q"

	// $ANTLR start "R"
	public final void mR() throws RecognitionException {
		try {
			// JDBCTokens.g:230:11: ( ( 'r' | 'R' ) )
			// JDBCTokens.g:
			{
			if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "R"

	// $ANTLR start "S"
	public final void mS() throws RecognitionException {
		try {
			// JDBCTokens.g:231:11: ( ( 's' | 'S' ) )
			// JDBCTokens.g:
			{
			if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "S"

	// $ANTLR start "T"
	public final void mT() throws RecognitionException {
		try {
			// JDBCTokens.g:232:11: ( ( 't' | 'T' ) )
			// JDBCTokens.g:
			{
			if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T"

	// $ANTLR start "U"
	public final void mU() throws RecognitionException {
		try {
			// JDBCTokens.g:233:11: ( ( 'u' | 'U' ) )
			// JDBCTokens.g:
			{
			if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "U"

	// $ANTLR start "V"
	public final void mV() throws RecognitionException {
		try {
			// JDBCTokens.g:234:11: ( ( 'v' | 'V' ) )
			// JDBCTokens.g:
			{
			if ( input.LA(1)=='V'||input.LA(1)=='v' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "V"

	// $ANTLR start "W"
	public final void mW() throws RecognitionException {
		try {
			// JDBCTokens.g:235:11: ( ( 'w' | 'W' ) )
			// JDBCTokens.g:
			{
			if ( input.LA(1)=='W'||input.LA(1)=='w' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "W"

	// $ANTLR start "X"
	public final void mX() throws RecognitionException {
		try {
			// JDBCTokens.g:236:11: ( ( 'x' | 'X' ) )
			// JDBCTokens.g:
			{
			if ( input.LA(1)=='X'||input.LA(1)=='x' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "X"

	// $ANTLR start "Y"
	public final void mY() throws RecognitionException {
		try {
			// JDBCTokens.g:237:11: ( ( 'y' | 'Y' ) )
			// JDBCTokens.g:
			{
			if ( input.LA(1)=='Y'||input.LA(1)=='y' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "Y"

	// $ANTLR start "Z"
	public final void mZ() throws RecognitionException {
		try {
			// JDBCTokens.g:238:11: ( ( 'z' | 'Z' ) )
			// JDBCTokens.g:
			{
			if ( input.LA(1)=='Z'||input.LA(1)=='z' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "Z"

	// $ANTLR start "Start_Comment"
	public final void mStart_Comment() throws RecognitionException {
		try {
			// JDBCTokens.g:243:17: ( '/*' )
			// JDBCTokens.g:243:19: '/*'
			{
			match("/*"); 

			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "Start_Comment"

	// $ANTLR start "End_Comment"
	public final void mEnd_Comment() throws RecognitionException {
		try {
			// JDBCTokens.g:246:17: ( '*/' )
			// JDBCTokens.g:246:19: '*/'
			{
			match("*/"); 

			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "End_Comment"

	// $ANTLR start "Line_Comment"
	public final void mLine_Comment() throws RecognitionException {
		try {
			// JDBCTokens.g:249:17: ( '//' )
			// JDBCTokens.g:249:19: '//'
			{
			match("//"); 

			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "Line_Comment"

	@Override
	public void mTokens() throws RecognitionException {
		// JDBCTokens.g:1:8: ( AND | ASKEYWORD | ASC | BYKEYWORD | DESC | DISTINCT | EACH | FULl_KEYWORD | FROMKEYWORD | GROUPKEYWORD | HAVINGKEYWORD | INNERKEYWORD | JOINKEYWORD | LEFT_KEYWORD | LIKEKEYWORD | LIMITKEYWORD | NOTKEYWORD | ONKEYWORD | OR | ORDERKEYWORD | RIGHT_KEYWORD | SELECTKEYWORD | WHEREKEYWORD | BACKQUOTE | COLON | COMMA | DOUBLEQUOTE | ESCAPEDDOUBLEQUOTE | ESCAPEDSINGLEQUOTE | LPARAM | PUNCTUATION | RPARAM | SEMICOLON | SINGLEQUOTE | NUMBER | IDENTIFIER | WS | COMMENT | LINE_COMMENT )
		int alt12=39;
		alt12 = dfa12.predict(input);
		switch (alt12) {
			case 1 :
				// JDBCTokens.g:1:10: AND
				{
				mAND(); 

				}
				break;
			case 2 :
				// JDBCTokens.g:1:14: ASKEYWORD
				{
				mASKEYWORD(); 

				}
				break;
			case 3 :
				// JDBCTokens.g:1:24: ASC
				{
				mASC(); 

				}
				break;
			case 4 :
				// JDBCTokens.g:1:28: BYKEYWORD
				{
				mBYKEYWORD(); 

				}
				break;
			case 5 :
				// JDBCTokens.g:1:38: DESC
				{
				mDESC(); 

				}
				break;
			case 6 :
				// JDBCTokens.g:1:43: DISTINCT
				{
				mDISTINCT(); 

				}
				break;
			case 7 :
				// JDBCTokens.g:1:52: EACH
				{
				mEACH(); 

				}
				break;
			case 8 :
				// JDBCTokens.g:1:57: FULl_KEYWORD
				{
				mFULl_KEYWORD(); 

				}
				break;
			case 9 :
				// JDBCTokens.g:1:70: FROMKEYWORD
				{
				mFROMKEYWORD(); 

				}
				break;
			case 10 :
				// JDBCTokens.g:1:82: GROUPKEYWORD
				{
				mGROUPKEYWORD(); 

				}
				break;
			case 11 :
				// JDBCTokens.g:1:95: HAVINGKEYWORD
				{
				mHAVINGKEYWORD(); 

				}
				break;
			case 12 :
				// JDBCTokens.g:1:109: INNERKEYWORD
				{
				mINNERKEYWORD(); 

				}
				break;
			case 13 :
				// JDBCTokens.g:1:122: JOINKEYWORD
				{
				mJOINKEYWORD(); 

				}
				break;
			case 14 :
				// JDBCTokens.g:1:134: LEFT_KEYWORD
				{
				mLEFT_KEYWORD(); 

				}
				break;
			case 15 :
				// JDBCTokens.g:1:147: LIKEKEYWORD
				{
				mLIKEKEYWORD(); 

				}
				break;
			case 16 :
				// JDBCTokens.g:1:159: LIMITKEYWORD
				{
				mLIMITKEYWORD(); 

				}
				break;
			case 17 :
				// JDBCTokens.g:1:172: NOTKEYWORD
				{
				mNOTKEYWORD(); 

				}
				break;
			case 18 :
				// JDBCTokens.g:1:183: ONKEYWORD
				{
				mONKEYWORD(); 

				}
				break;
			case 19 :
				// JDBCTokens.g:1:193: OR
				{
				mOR(); 

				}
				break;
			case 20 :
				// JDBCTokens.g:1:196: ORDERKEYWORD
				{
				mORDERKEYWORD(); 

				}
				break;
			case 21 :
				// JDBCTokens.g:1:209: RIGHT_KEYWORD
				{
				mRIGHT_KEYWORD(); 

				}
				break;
			case 22 :
				// JDBCTokens.g:1:223: SELECTKEYWORD
				{
				mSELECTKEYWORD(); 

				}
				break;
			case 23 :
				// JDBCTokens.g:1:237: WHEREKEYWORD
				{
				mWHEREKEYWORD(); 

				}
				break;
			case 24 :
				// JDBCTokens.g:1:250: BACKQUOTE
				{
				mBACKQUOTE(); 

				}
				break;
			case 25 :
				// JDBCTokens.g:1:260: COLON
				{
				mCOLON(); 

				}
				break;
			case 26 :
				// JDBCTokens.g:1:266: COMMA
				{
				mCOMMA(); 

				}
				break;
			case 27 :
				// JDBCTokens.g:1:272: DOUBLEQUOTE
				{
				mDOUBLEQUOTE(); 

				}
				break;
			case 28 :
				// JDBCTokens.g:1:284: ESCAPEDDOUBLEQUOTE
				{
				mESCAPEDDOUBLEQUOTE(); 

				}
				break;
			case 29 :
				// JDBCTokens.g:1:303: ESCAPEDSINGLEQUOTE
				{
				mESCAPEDSINGLEQUOTE(); 

				}
				break;
			case 30 :
				// JDBCTokens.g:1:322: LPARAM
				{
				mLPARAM(); 

				}
				break;
			case 31 :
				// JDBCTokens.g:1:329: PUNCTUATION
				{
				mPUNCTUATION(); 

				}
				break;
			case 32 :
				// JDBCTokens.g:1:341: RPARAM
				{
				mRPARAM(); 

				}
				break;
			case 33 :
				// JDBCTokens.g:1:348: SEMICOLON
				{
				mSEMICOLON(); 

				}
				break;
			case 34 :
				// JDBCTokens.g:1:358: SINGLEQUOTE
				{
				mSINGLEQUOTE(); 

				}
				break;
			case 35 :
				// JDBCTokens.g:1:370: NUMBER
				{
				mNUMBER(); 

				}
				break;
			case 36 :
				// JDBCTokens.g:1:377: IDENTIFIER
				{
				mIDENTIFIER(); 

				}
				break;
			case 37 :
				// JDBCTokens.g:1:388: WS
				{
				mWS(); 

				}
				break;
			case 38 :
				// JDBCTokens.g:1:391: COMMENT
				{
				mCOMMENT(); 

				}
				break;
			case 39 :
				// JDBCTokens.g:1:399: LINE_COMMENT
				{
				mLINE_COMMENT(); 

				}
				break;

		}
	}


	protected DFA12 dfa12 = new DFA12(this);
	static final String DFA12_eotS =
		"\1\uffff\17\33\12\uffff\1\65\4\uffff\1\33\1\71\1\73\14\33\1\111\1\112"+
		"\3\33\3\uffff\1\65\1\uffff\1\117\1\uffff\1\120\1\uffff\14\33\1\136\2\uffff"+
		"\4\33\2\uffff\1\33\1\144\1\33\1\147\1\150\1\151\3\33\1\155\1\156\1\157"+
		"\1\33\1\uffff\5\33\1\uffff\2\33\3\uffff\1\170\1\33\1\172\3\uffff\1\173"+
		"\1\174\1\175\1\33\1\177\3\33\1\uffff\1\u0083\4\uffff\1\u0084\1\uffff\3"+
		"\33\2\uffff\2\33\1\u008a\1\120\1\33\1\uffff\1\144";
	static final String DFA12_eofS =
		"\u008c\uffff";
	static final String DFA12_minS =
		"\1\11\1\116\1\131\1\105\1\101\2\122\1\101\1\116\1\117\1\105\1\117\1\116"+
		"\1\111\1\105\1\110\4\uffff\1\42\5\uffff\1\45\2\uffff\1\52\1\uffff\1\104"+
		"\2\45\2\123\1\103\1\114\2\117\1\126\1\116\1\111\1\106\1\113\1\124\2\45"+
		"\1\107\1\114\1\105\3\uffff\1\45\1\uffff\1\45\1\uffff\1\45\1\uffff\1\103"+
		"\1\124\1\110\1\114\1\115\1\125\1\111\1\105\1\116\1\124\1\105\1\111\1\45"+
		"\2\uffff\1\105\1\110\1\105\1\122\2\uffff\1\116\1\45\1\111\3\45\1\120\1"+
		"\116\1\122\3\45\1\124\1\uffff\1\122\1\124\1\103\1\105\1\104\1\uffff\2"+
		"\116\3\uffff\1\45\1\107\1\45\3\uffff\3\45\1\124\1\45\1\111\1\104\1\103"+
		"\1\uffff\1\45\4\uffff\1\45\1\uffff\1\116\1\111\1\124\2\uffff\1\107\1\116"+
		"\2\45\1\107\1\uffff\1\45";
	static final String DFA12_maxS =
		"\1\172\1\163\1\171\1\151\1\141\1\165\1\162\1\141\1\156\1\157\1\151\1\157"+
		"\1\162\1\151\1\145\1\150\4\uffff\1\47\5\uffff\1\172\2\uffff\1\57\1\uffff"+
		"\1\144\2\172\2\163\1\143\1\154\2\157\1\166\1\156\1\151\1\146\1\155\1\164"+
		"\2\172\1\147\1\154\1\145\3\uffff\1\172\1\uffff\1\172\1\uffff\1\172\1\uffff"+
		"\1\143\1\164\1\150\1\154\1\155\1\165\1\151\1\145\1\156\1\164\1\145\1\151"+
		"\1\172\2\uffff\1\145\1\150\1\145\1\162\2\uffff\1\156\1\172\1\151\3\172"+
		"\1\160\1\156\1\162\3\172\1\164\1\uffff\1\162\1\164\1\143\1\145\1\144\1"+
		"\uffff\2\156\3\uffff\1\172\1\147\1\172\3\uffff\3\172\1\164\1\172\1\151"+
		"\1\144\1\143\1\uffff\1\172\4\uffff\1\172\1\uffff\1\156\1\151\1\164\2\uffff"+
		"\1\147\1\156\2\172\1\147\1\uffff\1\172";
	static final String DFA12_acceptS =
		"\20\uffff\1\30\1\31\1\32\1\33\1\uffff\1\36\1\37\1\40\1\41\1\42\1\uffff"+
		"\1\44\1\45\1\uffff\1\47\24\uffff\1\34\1\35\1\43\1\uffff\1\46\1\uffff\1"+
		"\2\1\uffff\1\4\15\uffff\1\22\1\23\4\uffff\1\1\1\3\15\uffff\1\21\5\uffff"+
		"\1\5\2\uffff\1\7\1\10\1\11\3\uffff\1\15\1\16\1\17\10\uffff\1\12\1\uffff"+
		"\1\14\1\20\1\24\1\25\1\uffff\1\27\3\uffff\1\13\1\26\5\uffff\1\6\1\uffff";
	static final String DFA12_specialS =
		"\u008c\uffff}>";
	static final String[] DFA12_transitionS = {
			"\2\34\1\uffff\2\34\22\uffff\1\34\1\uffff\1\23\4\uffff\1\31\1\25\1\27"+
			"\2\uffff\1\22\1\36\1\26\1\35\12\32\1\21\1\30\5\uffff\1\1\1\2\1\33\1\3"+
			"\1\4\1\5\1\6\1\7\1\10\1\11\1\33\1\12\1\33\1\13\1\14\2\33\1\15\1\16\3"+
			"\33\1\17\3\33\1\uffff\1\24\2\uffff\1\33\1\20\1\1\1\2\1\33\1\3\1\4\1\5"+
			"\1\6\1\7\1\10\1\11\1\33\1\12\1\33\1\13\1\14\2\33\1\15\1\16\3\33\1\17"+
			"\3\33",
			"\1\37\4\uffff\1\40\32\uffff\1\37\4\uffff\1\40",
			"\1\41\37\uffff\1\41",
			"\1\42\3\uffff\1\43\33\uffff\1\42\3\uffff\1\43",
			"\1\44\37\uffff\1\44",
			"\1\46\2\uffff\1\45\34\uffff\1\46\2\uffff\1\45",
			"\1\47\37\uffff\1\47",
			"\1\50\37\uffff\1\50",
			"\1\51\37\uffff\1\51",
			"\1\52\37\uffff\1\52",
			"\1\53\3\uffff\1\54\33\uffff\1\53\3\uffff\1\54",
			"\1\55\37\uffff\1\55",
			"\1\56\3\uffff\1\57\33\uffff\1\56\3\uffff\1\57",
			"\1\60\37\uffff\1\60",
			"\1\61\37\uffff\1\61",
			"\1\62\37\uffff\1\62",
			"",
			"",
			"",
			"",
			"\1\63\4\uffff\1\64",
			"",
			"",
			"",
			"",
			"",
			"\1\33\12\uffff\12\66\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"",
			"",
			"\1\67\4\uffff\1\36",
			"",
			"\1\70\37\uffff\1\70",
			"\1\33\12\uffff\12\33\7\uffff\2\33\1\72\27\33\4\uffff\1\33\1\uffff\2"+
			"\33\1\72\27\33",
			"\1\33\12\uffff\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\1\74\37\uffff\1\74",
			"\1\75\37\uffff\1\75",
			"\1\76\37\uffff\1\76",
			"\1\77\37\uffff\1\77",
			"\1\100\37\uffff\1\100",
			"\1\101\37\uffff\1\101",
			"\1\102\37\uffff\1\102",
			"\1\103\37\uffff\1\103",
			"\1\104\37\uffff\1\104",
			"\1\105\37\uffff\1\105",
			"\1\106\1\uffff\1\107\35\uffff\1\106\1\uffff\1\107",
			"\1\110\37\uffff\1\110",
			"\1\33\12\uffff\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\1\33\12\uffff\12\33\7\uffff\3\33\1\113\26\33\4\uffff\1\33\1\uffff\3"+
			"\33\1\113\26\33",
			"\1\114\37\uffff\1\114",
			"\1\115\37\uffff\1\115",
			"\1\116\37\uffff\1\116",
			"",
			"",
			"",
			"\1\33\12\uffff\12\66\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"",
			"\1\33\12\uffff\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"",
			"\1\33\12\uffff\12\33\7\uffff\4\33\1\121\25\33\4\uffff\1\33\1\uffff\4"+
			"\33\1\121\25\33",
			"",
			"\1\122\37\uffff\1\122",
			"\1\123\37\uffff\1\123",
			"\1\124\37\uffff\1\124",
			"\1\125\37\uffff\1\125",
			"\1\126\37\uffff\1\126",
			"\1\127\37\uffff\1\127",
			"\1\130\37\uffff\1\130",
			"\1\131\37\uffff\1\131",
			"\1\132\37\uffff\1\132",
			"\1\133\37\uffff\1\133",
			"\1\134\37\uffff\1\134",
			"\1\135\37\uffff\1\135",
			"\1\33\12\uffff\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"",
			"",
			"\1\137\37\uffff\1\137",
			"\1\140\37\uffff\1\140",
			"\1\141\37\uffff\1\141",
			"\1\142\37\uffff\1\142",
			"",
			"",
			"\1\143\37\uffff\1\143",
			"\1\33\12\uffff\12\33\7\uffff\4\33\1\145\25\33\4\uffff\1\33\1\uffff\4"+
			"\33\1\145\25\33",
			"\1\146\37\uffff\1\146",
			"\1\33\12\uffff\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\1\33\12\uffff\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\1\33\12\uffff\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\1\152\37\uffff\1\152",
			"\1\153\37\uffff\1\153",
			"\1\154\37\uffff\1\154",
			"\1\33\12\uffff\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\1\33\12\uffff\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\1\33\12\uffff\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\1\160\37\uffff\1\160",
			"",
			"\1\161\37\uffff\1\161",
			"\1\162\37\uffff\1\162",
			"\1\163\37\uffff\1\163",
			"\1\164\37\uffff\1\164",
			"\1\165\37\uffff\1\165",
			"",
			"\1\166\37\uffff\1\166",
			"\1\167\37\uffff\1\167",
			"",
			"",
			"",
			"\1\33\12\uffff\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\1\171\37\uffff\1\171",
			"\1\33\12\uffff\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"",
			"",
			"",
			"\1\33\12\uffff\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\1\33\12\uffff\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\1\33\12\uffff\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\1\176\37\uffff\1\176",
			"\1\33\12\uffff\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\1\u0080\37\uffff\1\u0080",
			"\1\u0081\37\uffff\1\u0081",
			"\1\u0082\37\uffff\1\u0082",
			"",
			"\1\33\12\uffff\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"",
			"",
			"",
			"",
			"\1\33\12\uffff\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"",
			"\1\u0085\37\uffff\1\u0085",
			"\1\u0086\37\uffff\1\u0086",
			"\1\u0087\37\uffff\1\u0087",
			"",
			"",
			"\1\u0088\37\uffff\1\u0088",
			"\1\u0089\37\uffff\1\u0089",
			"\1\33\12\uffff\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\1\33\12\uffff\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\1\u008b\37\uffff\1\u008b",
			"",
			"\1\33\12\uffff\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33"
	};

	static final short[] DFA12_eot = DFA.unpackEncodedString(DFA12_eotS);
	static final short[] DFA12_eof = DFA.unpackEncodedString(DFA12_eofS);
	static final char[] DFA12_min = DFA.unpackEncodedStringToUnsignedChars(DFA12_minS);
	static final char[] DFA12_max = DFA.unpackEncodedStringToUnsignedChars(DFA12_maxS);
	static final short[] DFA12_accept = DFA.unpackEncodedString(DFA12_acceptS);
	static final short[] DFA12_special = DFA.unpackEncodedString(DFA12_specialS);
	static final short[][] DFA12_transition;

	static {
		int numStates = DFA12_transitionS.length;
		DFA12_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA12_transition[i] = DFA.unpackEncodedString(DFA12_transitionS[i]);
		}
	}

	protected class DFA12 extends DFA {

		public DFA12(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 12;
			this.eot = DFA12_eot;
			this.eof = DFA12_eof;
			this.min = DFA12_min;
			this.max = DFA12_max;
			this.accept = DFA12_accept;
			this.special = DFA12_special;
			this.transition = DFA12_transition;
		}
		@Override
		public String getDescription() {
			return "1:1: Tokens : ( AND | ASKEYWORD | ASC | BYKEYWORD | DESC | DISTINCT | EACH | FULl_KEYWORD | FROMKEYWORD | GROUPKEYWORD | HAVINGKEYWORD | INNERKEYWORD | JOINKEYWORD | LEFT_KEYWORD | LIKEKEYWORD | LIMITKEYWORD | NOTKEYWORD | ONKEYWORD | OR | ORDERKEYWORD | RIGHT_KEYWORD | SELECTKEYWORD | WHEREKEYWORD | BACKQUOTE | COLON | COMMA | DOUBLEQUOTE | ESCAPEDDOUBLEQUOTE | ESCAPEDSINGLEQUOTE | LPARAM | PUNCTUATION | RPARAM | SEMICOLON | SINGLEQUOTE | NUMBER | IDENTIFIER | WS | COMMENT | LINE_COMMENT );";
		}
	}

}

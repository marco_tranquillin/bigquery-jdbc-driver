// $ANTLR 3.5.2 JdbcGrammar.g 2015-05-16 13:18:14
/**
 * Starschema Big Query JDBC Driver
 * Copyright (C) 2012, Starschema Ltd.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * This grammar implement the sql select statement
 *    @author Horvath Attila
 *    @author Balazs Gunics
 */
  package net.starschema.clouddb.jdbc;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class JdbcGrammarLexer extends Lexer {
	public static final int EOF=-1;
	public static final int T__128=128;
	public static final int T__129=129;
	public static final int T__130=130;
	public static final int T__131=131;
	public static final int T__132=132;
	public static final int T__133=133;
	public static final int T__134=134;
	public static final int T__135=135;
	public static final int T__136=136;
	public static final int T__137=137;
	public static final int T__138=138;
	public static final int T__139=139;
	public static final int A=4;
	public static final int ALIAS=5;
	public static final int AND=6;
	public static final int ASC=7;
	public static final int ASKEYWORD=8;
	public static final int B=9;
	public static final int BACKQUOTE=10;
	public static final int BACKSINGLEQUOTE=11;
	public static final int BOOLEANEXPRESSION=12;
	public static final int BOOLEANEXPRESSIONITEM=13;
	public static final int BOOLEANEXPRESSIONITEMLEFT=14;
	public static final int BOOLEANEXPRESSIONITEMRIGHT=15;
	public static final int BYKEYWORD=16;
	public static final int C=17;
	public static final int COLON=18;
	public static final int COLUMN=19;
	public static final int COMMA=20;
	public static final int COMMENT=21;
	public static final int COMPARISONOPERATOR=22;
	public static final int CONDITION=23;
	public static final int CONDITIONLEFT=24;
	public static final int CONDITIONRIGHT=25;
	public static final int CONJUNCTION=26;
	public static final int D=27;
	public static final int DATASETNAME=28;
	public static final int DATASOURCE=29;
	public static final int DESC=30;
	public static final int DIGIT=31;
	public static final int DISJUNCTION=32;
	public static final int DISTINCT=33;
	public static final int DIVIDER=34;
	public static final int DOUBLEQUOTE=35;
	public static final int E=36;
	public static final int EACH=37;
	public static final int ESCAPEDDOUBLEQUOTE=38;
	public static final int ESCAPEDSINGLEQUOTE=39;
	public static final int EXPRESSION=40;
	public static final int EXPRESSIONTEXT=41;
	public static final int End_Comment=42;
	public static final int F=43;
	public static final int FROMEXPRESSION=44;
	public static final int FROMKEYWORD=45;
	public static final int FULl_KEYWORD=46;
	public static final int FUNCTIONCALL=47;
	public static final int FUNCTIONPARAMETERS=48;
	public static final int G=49;
	public static final int GROUPBYEXPRESSION=50;
	public static final int GROUPKEYWORD=51;
	public static final int H=52;
	public static final int HAVINGEXPRESSION=53;
	public static final int HAVINGKEYWORD=54;
	public static final int HIGHCHAR=55;
	public static final int I=56;
	public static final int IDENTIFIER=57;
	public static final int INNERKEYWORD=58;
	public static final int INTEGERPARAM=59;
	public static final int J=60;
	public static final int JOINEXPRESSION=61;
	public static final int JOINKEYWORD=62;
	public static final int JOINTYPE=63;
	public static final int JOKER=64;
	public static final int JOKERCALL=65;
	public static final int K=66;
	public static final int KEYWORD=67;
	public static final int L=68;
	public static final int LEFTEXPR=69;
	public static final int LEFT_KEYWORD=70;
	public static final int LIKEEXPRESSION=71;
	public static final int LIKEKEYWORD=72;
	public static final int LIMITEXPRESSION=73;
	public static final int LIMITKEYWORD=74;
	public static final int LINE_COMMENT=75;
	public static final int LOGICALOPERATOR=76;
	public static final int LOWCHAR=77;
	public static final int LPARAM=78;
	public static final int Line_Comment=79;
	public static final int M=80;
	public static final int MULTIJOINEXPRESSION=81;
	public static final int MULTIPLECALL=82;
	public static final int N=83;
	public static final int NAME=84;
	public static final int NEGATION=85;
	public static final int NL=86;
	public static final int NOTKEYWORD=87;
	public static final int NUMBER=88;
	public static final int O=89;
	public static final int ONCLAUSE=90;
	public static final int ONKEYWORD=91;
	public static final int OR=92;
	public static final int ORDERBYCLAUSE=93;
	public static final int ORDERBYEXPRESSION=94;
	public static final int ORDERBYORDER=95;
	public static final int ORDERKEYWORD=96;
	public static final int P=97;
	public static final int PARENJOIN=98;
	public static final int PROJECTNAME=99;
	public static final int PUNCTUATION=100;
	public static final int Q=101;
	public static final int R=102;
	public static final int RIGHTEXPR=103;
	public static final int RIGHT_KEYWORD=104;
	public static final int RPARAM=105;
	public static final int S=106;
	public static final int SCOPE=107;
	public static final int SELECTKEYWORD=108;
	public static final int SELECTSTATEMENT=109;
	public static final int SEMICOLON=110;
	public static final int SINGLEQUOTE=111;
	public static final int SOURCETABLE=112;
	public static final int STRINGLIT=113;
	public static final int SUBQUERY=114;
	public static final int Start_Comment=115;
	public static final int T=116;
	public static final int TABLENAME=117;
	public static final int TEXT=118;
	public static final int U=119;
	public static final int V=120;
	public static final int W=121;
	public static final int WHEREEXPRESSION=122;
	public static final int WHEREKEYWORD=123;
	public static final int WS=124;
	public static final int X=125;
	public static final int Y=126;
	public static final int Z=127;
	public static final int Tokens=140;

	// delegates
	public JdbcGrammar_JDBCTokens gJDBCTokens;
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {gJDBCTokens};
	}

	public JdbcGrammarLexer() {} 
	public JdbcGrammarLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public JdbcGrammarLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
		gJDBCTokens = new JdbcGrammar_JDBCTokens(input, state, this);
	}
	@Override public String getGrammarFileName() { return "JdbcGrammar.g"; }

	// $ANTLR start "T__128"
	public final void mT__128() throws RecognitionException {
		try {
			int _type = T__128;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JdbcGrammar.g:32:8: ( '!=' )
			// JdbcGrammar.g:32:10: '!='
			{
			match("!="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__128"

	// $ANTLR start "T__129"
	public final void mT__129() throws RecognitionException {
		try {
			int _type = T__129;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JdbcGrammar.g:33:8: ( '*' )
			// JdbcGrammar.g:33:10: '*'
			{
			match('*'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__129"

	// $ANTLR start "T__130"
	public final void mT__130() throws RecognitionException {
		try {
			int _type = T__130;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JdbcGrammar.g:34:8: ( '0' )
			// JdbcGrammar.g:34:10: '0'
			{
			match('0'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__130"

	// $ANTLR start "T__131"
	public final void mT__131() throws RecognitionException {
		try {
			int _type = T__131;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JdbcGrammar.g:35:8: ( '1' )
			// JdbcGrammar.g:35:10: '1'
			{
			match('1'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__131"

	// $ANTLR start "T__132"
	public final void mT__132() throws RecognitionException {
		try {
			int _type = T__132;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JdbcGrammar.g:36:8: ( '<' )
			// JdbcGrammar.g:36:10: '<'
			{
			match('<'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__132"

	// $ANTLR start "T__133"
	public final void mT__133() throws RecognitionException {
		try {
			int _type = T__133;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JdbcGrammar.g:37:8: ( '<=' )
			// JdbcGrammar.g:37:10: '<='
			{
			match("<="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__133"

	// $ANTLR start "T__134"
	public final void mT__134() throws RecognitionException {
		try {
			int _type = T__134;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JdbcGrammar.g:38:8: ( '<>' )
			// JdbcGrammar.g:38:10: '<>'
			{
			match("<>"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__134"

	// $ANTLR start "T__135"
	public final void mT__135() throws RecognitionException {
		try {
			int _type = T__135;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JdbcGrammar.g:39:8: ( '=' )
			// JdbcGrammar.g:39:10: '='
			{
			match('='); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__135"

	// $ANTLR start "T__136"
	public final void mT__136() throws RecognitionException {
		try {
			int _type = T__136;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JdbcGrammar.g:40:8: ( '>' )
			// JdbcGrammar.g:40:10: '>'
			{
			match('>'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__136"

	// $ANTLR start "T__137"
	public final void mT__137() throws RecognitionException {
		try {
			int _type = T__137;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JdbcGrammar.g:41:8: ( '>=' )
			// JdbcGrammar.g:41:10: '>='
			{
			match(">="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__137"

	// $ANTLR start "T__138"
	public final void mT__138() throws RecognitionException {
		try {
			int _type = T__138;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JdbcGrammar.g:42:8: ( '[' )
			// JdbcGrammar.g:42:10: '['
			{
			match('['); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__138"

	// $ANTLR start "T__139"
	public final void mT__139() throws RecognitionException {
		try {
			int _type = T__139;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JdbcGrammar.g:43:8: ( ']' )
			// JdbcGrammar.g:43:10: ']'
			{
			match(']'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__139"

	// $ANTLR start "LIMITEXPRESSION"
	public final void mLIMITEXPRESSION() throws RecognitionException {
		try {
			// JdbcGrammar.g:96:25: ()
			// JdbcGrammar.g:96:26: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LIMITEXPRESSION"

	// $ANTLR start "ORDERBYCLAUSE"
	public final void mORDERBYCLAUSE() throws RecognitionException {
		try {
			// JdbcGrammar.g:115:23: ()
			// JdbcGrammar.g:115:24: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ORDERBYCLAUSE"

	// $ANTLR start "ORDERBYORDER"
	public final void mORDERBYORDER() throws RecognitionException {
		try {
			// JdbcGrammar.g:116:22: ()
			// JdbcGrammar.g:116:23: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ORDERBYORDER"

	// $ANTLR start "DISJUNCTION"
	public final void mDISJUNCTION() throws RecognitionException {
		try {
			// JdbcGrammar.g:197:21: ()
			// JdbcGrammar.g:197:22: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DISJUNCTION"

	// $ANTLR start "CONJUNCTION"
	public final void mCONJUNCTION() throws RecognitionException {
		try {
			// JdbcGrammar.g:222:21: ()
			// JdbcGrammar.g:222:22: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CONJUNCTION"

	// $ANTLR start "NEGATION"
	public final void mNEGATION() throws RecognitionException {
		try {
			// JdbcGrammar.g:233:18: ()
			// JdbcGrammar.g:233:19: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NEGATION"

	// $ANTLR start "BOOLEANEXPRESSIONITEMLEFT"
	public final void mBOOLEANEXPRESSIONITEMLEFT() throws RecognitionException {
		try {
			// JdbcGrammar.g:248:35: ()
			// JdbcGrammar.g:248:36: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BOOLEANEXPRESSIONITEMLEFT"

	// $ANTLR start "BOOLEANEXPRESSIONITEMRIGHT"
	public final void mBOOLEANEXPRESSIONITEMRIGHT() throws RecognitionException {
		try {
			// JdbcGrammar.g:249:36: ()
			// JdbcGrammar.g:249:37: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BOOLEANEXPRESSIONITEMRIGHT"

	// $ANTLR start "LIKEEXPRESSION"
	public final void mLIKEEXPRESSION() throws RecognitionException {
		try {
			// JdbcGrammar.g:250:24: ()
			// JdbcGrammar.g:250:25: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LIKEEXPRESSION"

	// $ANTLR start "BOOLEANEXPRESSIONITEM"
	public final void mBOOLEANEXPRESSIONITEM() throws RecognitionException {
		try {
			// JdbcGrammar.g:251:31: ()
			// JdbcGrammar.g:251:32: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BOOLEANEXPRESSIONITEM"

	// $ANTLR start "CONDITIONLEFT"
	public final void mCONDITIONLEFT() throws RecognitionException {
		try {
			// JdbcGrammar.g:307:23: ()
			// JdbcGrammar.g:307:24: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CONDITIONLEFT"

	// $ANTLR start "CONDITIONRIGHT"
	public final void mCONDITIONRIGHT() throws RecognitionException {
		try {
			// JdbcGrammar.g:308:24: ()
			// JdbcGrammar.g:308:25: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CONDITIONRIGHT"

	// $ANTLR start "LEFTEXPR"
	public final void mLEFTEXPR() throws RecognitionException {
		try {
			// JdbcGrammar.g:336:18: ()
			// JdbcGrammar.g:336:19: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LEFTEXPR"

	// $ANTLR start "RIGHTEXPR"
	public final void mRIGHTEXPR() throws RecognitionException {
		try {
			// JdbcGrammar.g:337:19: ()
			// JdbcGrammar.g:337:20: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RIGHTEXPR"

	// $ANTLR start "MULTIJOINEXPRESSION"
	public final void mMULTIJOINEXPRESSION() throws RecognitionException {
		try {
			// JdbcGrammar.g:338:29: ()
			// JdbcGrammar.g:338:30: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MULTIJOINEXPRESSION"

	// $ANTLR start "PARENJOIN"
	public final void mPARENJOIN() throws RecognitionException {
		try {
			// JdbcGrammar.g:339:19: ()
			// JdbcGrammar.g:339:20: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PARENJOIN"

	// $ANTLR start "JOKERCALL"
	public final void mJOKERCALL() throws RecognitionException {
		try {
			// JdbcGrammar.g:538:19: ()
			// JdbcGrammar.g:538:20: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "JOKERCALL"

	// $ANTLR start "SCOPE"
	public final void mSCOPE() throws RecognitionException {
		try {
			// JdbcGrammar.g:567:15: ()
			// JdbcGrammar.g:567:16: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SCOPE"

	// $ANTLR start "BACKSINGLEQUOTE"
	public final void mBACKSINGLEQUOTE() throws RecognitionException {
		try {
			int _type = BACKSINGLEQUOTE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JdbcGrammar.g:582:16: ( '`' )
			// JdbcGrammar.g:583:1: '`'
			{
			match('`'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BACKSINGLEQUOTE"

	// $ANTLR start "JOKER"
	public final void mJOKER() throws RecognitionException {
		try {
			// JdbcGrammar.g:611:15: ()
			// JdbcGrammar.g:611:16: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "JOKER"

	// $ANTLR start "WS"
	public final void mWS() throws RecognitionException {
		try {
			int _type = WS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JdbcGrammar.g:674:5: ( ( ' ' | '\\t' | '\\n' | '\\r' | '\\f' | '%n' )+ )
			// JdbcGrammar.g:675:5: ( ' ' | '\\t' | '\\n' | '\\r' | '\\f' | '%n' )+
			{
			// JdbcGrammar.g:675:5: ( ' ' | '\\t' | '\\n' | '\\r' | '\\f' | '%n' )+
			int cnt1=0;
			loop1:
			while (true) {
				int alt1=7;
				switch ( input.LA(1) ) {
				case ' ':
					{
					alt1=1;
					}
					break;
				case '\t':
					{
					alt1=2;
					}
					break;
				case '\n':
					{
					alt1=3;
					}
					break;
				case '\r':
					{
					alt1=4;
					}
					break;
				case '\f':
					{
					alt1=5;
					}
					break;
				case '%':
					{
					alt1=6;
					}
					break;
				}
				switch (alt1) {
				case 1 :
					// JdbcGrammar.g:675:6: ' '
					{
					match(' '); 
					}
					break;
				case 2 :
					// JdbcGrammar.g:675:12: '\\t'
					{
					match('\t'); 
					}
					break;
				case 3 :
					// JdbcGrammar.g:675:19: '\\n'
					{
					match('\n'); 
					}
					break;
				case 4 :
					// JdbcGrammar.g:675:26: '\\r'
					{
					match('\r'); 
					}
					break;
				case 5 :
					// JdbcGrammar.g:675:33: '\\f'
					{
					match('\f'); 
					}
					break;
				case 6 :
					// JdbcGrammar.g:675:40: '%n'
					{
					match("%n"); 

					}
					break;

				default :
					if ( cnt1 >= 1 ) break loop1;
					EarlyExitException eee = new EarlyExitException(1, input);
					throw eee;
				}
				cnt1++;
			}

			 _channel = HIDDEN;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WS"

	// $ANTLR start "COMMENT"
	public final void mCOMMENT() throws RecognitionException {
		try {
			int _type = COMMENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JdbcGrammar.g:680:5: ( ( Start_Comment ( options {greedy=false; } : . )* End_Comment )+ )
			// JdbcGrammar.g:680:9: ( Start_Comment ( options {greedy=false; } : . )* End_Comment )+
			{
			// JdbcGrammar.g:680:9: ( Start_Comment ( options {greedy=false; } : . )* End_Comment )+
			int cnt3=0;
			loop3:
			while (true) {
				int alt3=2;
				int LA3_0 = input.LA(1);
				if ( (LA3_0=='/') ) {
					alt3=1;
				}

				switch (alt3) {
				case 1 :
					// JdbcGrammar.g:680:13: Start_Comment ( options {greedy=false; } : . )* End_Comment
					{
					gJDBCTokens.mStart_Comment(); 

					// JdbcGrammar.g:680:27: ( options {greedy=false; } : . )*
					loop2:
					while (true) {
						int alt2=2;
						int LA2_0 = input.LA(1);
						if ( (LA2_0=='*') ) {
							int LA2_1 = input.LA(2);
							if ( (LA2_1=='/') ) {
								alt2=2;
							}
							else if ( ((LA2_1 >= '\u0000' && LA2_1 <= '.')||(LA2_1 >= '0' && LA2_1 <= '\uFFFF')) ) {
								alt2=1;
							}

						}
						else if ( ((LA2_0 >= '\u0000' && LA2_0 <= ')')||(LA2_0 >= '+' && LA2_0 <= '\uFFFF')) ) {
							alt2=1;
						}

						switch (alt2) {
						case 1 :
							// JdbcGrammar.g:680:55: .
							{
							matchAny(); 
							}
							break;

						default :
							break loop2;
						}
					}

					gJDBCTokens.mEnd_Comment(); 

					}
					break;

				default :
					if ( cnt3 >= 1 ) break loop3;
					EarlyExitException eee = new EarlyExitException(3, input);
					throw eee;
				}
				cnt3++;
			}


			      _channel=HIDDEN;
			    
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMENT"

	// $ANTLR start "LINE_COMMENT"
	public final void mLINE_COMMENT() throws RecognitionException {
		try {
			int _type = LINE_COMMENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// JdbcGrammar.g:687:5: ( ( ( Line_Comment | '--' ) (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n' )+ )
			// JdbcGrammar.g:687:9: ( ( Line_Comment | '--' ) (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n' )+
			{
			// JdbcGrammar.g:687:9: ( ( Line_Comment | '--' ) (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n' )+
			int cnt7=0;
			loop7:
			while (true) {
				int alt7=2;
				int LA7_0 = input.LA(1);
				if ( (LA7_0=='-'||LA7_0=='/') ) {
					alt7=1;
				}

				switch (alt7) {
				case 1 :
					// JdbcGrammar.g:687:13: ( Line_Comment | '--' ) (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n'
					{
					// JdbcGrammar.g:687:13: ( Line_Comment | '--' )
					int alt4=2;
					int LA4_0 = input.LA(1);
					if ( (LA4_0=='/') ) {
						alt4=1;
					}
					else if ( (LA4_0=='-') ) {
						alt4=2;
					}

					else {
						NoViableAltException nvae =
							new NoViableAltException("", 4, 0, input);
						throw nvae;
					}

					switch (alt4) {
						case 1 :
							// JdbcGrammar.g:687:15: Line_Comment
							{
							gJDBCTokens.mLine_Comment(); 

							}
							break;
						case 2 :
							// JdbcGrammar.g:687:30: '--'
							{
							match("--"); 

							}
							break;

					}

					// JdbcGrammar.g:687:37: (~ ( '\\n' | '\\r' ) )*
					loop5:
					while (true) {
						int alt5=2;
						int LA5_0 = input.LA(1);
						if ( ((LA5_0 >= '\u0000' && LA5_0 <= '\t')||(LA5_0 >= '\u000B' && LA5_0 <= '\f')||(LA5_0 >= '\u000E' && LA5_0 <= '\uFFFF')) ) {
							alt5=1;
						}

						switch (alt5) {
						case 1 :
							// JdbcGrammar.g:
							{
							if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '\uFFFF') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							break loop5;
						}
					}

					// JdbcGrammar.g:687:51: ( '\\r' )?
					int alt6=2;
					int LA6_0 = input.LA(1);
					if ( (LA6_0=='\r') ) {
						alt6=1;
					}
					switch (alt6) {
						case 1 :
							// JdbcGrammar.g:687:51: '\\r'
							{
							match('\r'); 
							}
							break;

					}

					match('\n'); 
					}
					break;

				default :
					if ( cnt7 >= 1 ) break loop7;
					EarlyExitException eee = new EarlyExitException(7, input);
					throw eee;
				}
				cnt7++;
			}


			      _channel=HIDDEN;
			    
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LINE_COMMENT"

	// $ANTLR start "KEYWORD"
	public final void mKEYWORD() throws RecognitionException {
		try {
			// JdbcGrammar.g:691:17: ()
			// JdbcGrammar.g:691:18: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "KEYWORD"

	@Override
	public void mTokens() throws RecognitionException {
		// JdbcGrammar.g:1:8: ( T__128 | T__129 | T__130 | T__131 | T__132 | T__133 | T__134 | T__135 | T__136 | T__137 | T__138 | T__139 | BACKSINGLEQUOTE | WS | COMMENT | LINE_COMMENT | JDBCTokens. Tokens )
		int alt8=17;
		alt8 = dfa8.predict(input);
		switch (alt8) {
			case 1 :
				// JdbcGrammar.g:1:10: T__128
				{
				mT__128(); 

				}
				break;
			case 2 :
				// JdbcGrammar.g:1:17: T__129
				{
				mT__129(); 

				}
				break;
			case 3 :
				// JdbcGrammar.g:1:24: T__130
				{
				mT__130(); 

				}
				break;
			case 4 :
				// JdbcGrammar.g:1:31: T__131
				{
				mT__131(); 

				}
				break;
			case 5 :
				// JdbcGrammar.g:1:38: T__132
				{
				mT__132(); 

				}
				break;
			case 6 :
				// JdbcGrammar.g:1:45: T__133
				{
				mT__133(); 

				}
				break;
			case 7 :
				// JdbcGrammar.g:1:52: T__134
				{
				mT__134(); 

				}
				break;
			case 8 :
				// JdbcGrammar.g:1:59: T__135
				{
				mT__135(); 

				}
				break;
			case 9 :
				// JdbcGrammar.g:1:66: T__136
				{
				mT__136(); 

				}
				break;
			case 10 :
				// JdbcGrammar.g:1:73: T__137
				{
				mT__137(); 

				}
				break;
			case 11 :
				// JdbcGrammar.g:1:80: T__138
				{
				mT__138(); 

				}
				break;
			case 12 :
				// JdbcGrammar.g:1:87: T__139
				{
				mT__139(); 

				}
				break;
			case 13 :
				// JdbcGrammar.g:1:94: BACKSINGLEQUOTE
				{
				mBACKSINGLEQUOTE(); 

				}
				break;
			case 14 :
				// JdbcGrammar.g:1:110: WS
				{
				mWS(); 

				}
				break;
			case 15 :
				// JdbcGrammar.g:1:113: COMMENT
				{
				mCOMMENT(); 

				}
				break;
			case 16 :
				// JdbcGrammar.g:1:121: LINE_COMMENT
				{
				mLINE_COMMENT(); 

				}
				break;
			case 17 :
				// JdbcGrammar.g:1:134: JDBCTokens. Tokens
				{
				gJDBCTokens.mTokens(); 

				}
				break;

		}
	}


	protected DFA8 dfa8 = new DFA8(this);
	static final String DFA8_eotS =
		"\3\uffff\1\24\1\25\1\30\1\uffff\1\32\3\uffff\5\34\15\uffff\1\34\7\uffff"+
		"\1\47\1\51\5\uffff";
	static final String DFA8_eofS =
		"\54\uffff";
	static final String DFA8_minS =
		"\1\11\2\uffff\2\45\1\75\1\uffff\1\75\3\uffff\5\11\1\156\1\52\1\55\12\uffff"+
		"\1\11\6\0\1\12\1\55\1\0\1\uffff\1\57\1\uffff\2\0";
	static final String DFA8_maxS =
		"\1\172\2\uffff\2\172\1\76\1\uffff\1\75\3\uffff\5\45\1\156\1\57\1\55\12"+
		"\uffff\1\45\6\uffff\1\12\1\57\1\uffff\1\uffff\1\57\1\uffff\2\uffff";
	static final String DFA8_acceptS =
		"\1\uffff\1\1\1\2\3\uffff\1\10\1\uffff\1\13\1\14\1\15\10\uffff\1\21\1\3"+
		"\1\4\1\6\1\7\1\5\1\12\1\11\1\15\1\16\12\uffff\1\20\1\uffff\1\17\2\uffff";
	static final String DFA8_specialS =
		"\36\uffff\1\6\1\2\1\7\1\1\1\5\1\10\2\uffff\1\0\3\uffff\1\4\1\3}>";
	static final String[] DFA8_transitionS = {
			"\1\14\1\15\1\uffff\1\17\1\16\22\uffff\1\13\1\1\1\23\2\uffff\1\20\1\uffff"+
			"\3\23\1\2\1\uffff\1\23\1\22\1\23\1\21\1\3\1\4\12\23\1\5\1\6\1\7\2\uffff"+
			"\32\23\1\10\1\23\1\11\1\uffff\1\23\1\12\32\23",
			"",
			"",
			"\1\23\12\uffff\12\23\7\uffff\32\23\4\uffff\1\23\1\uffff\32\23",
			"\1\23\12\uffff\12\23\7\uffff\32\23\4\uffff\1\23\1\uffff\32\23",
			"\1\26\1\27",
			"",
			"\1\31",
			"",
			"",
			"",
			"\1\14\1\15\1\uffff\1\17\1\16\22\uffff\1\13\4\uffff\1\20",
			"\1\14\1\15\1\uffff\1\17\1\16\22\uffff\1\13\4\uffff\1\20",
			"\1\14\1\15\1\uffff\1\17\1\16\22\uffff\1\13\4\uffff\1\20",
			"\1\14\1\15\1\uffff\1\17\1\16\22\uffff\1\13\4\uffff\1\20",
			"\1\14\1\15\1\uffff\1\17\1\16\22\uffff\1\13\4\uffff\1\20",
			"\1\35",
			"\1\36\4\uffff\1\37",
			"\1\40",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\14\1\15\1\uffff\1\17\1\16\22\uffff\1\13\4\uffff\1\20",
			"\52\42\1\41\uffd5\42",
			"\12\43\1\45\2\43\1\44\ufff2\43",
			"\12\43\1\45\2\43\1\44\ufff2\43",
			"\52\42\1\41\4\42\1\46\uffd0\42",
			"\52\42\1\41\uffd5\42",
			"\12\43\1\45\2\43\1\44\ufff2\43",
			"\1\45",
			"\1\22\1\uffff\1\50",
			"\52\42\1\41\4\42\1\52\uffd0\42",
			"",
			"\1\37",
			"",
			"\52\42\1\53\uffd5\42",
			"\52\42\1\41\4\42\1\46\uffd0\42"
	};

	static final short[] DFA8_eot = DFA.unpackEncodedString(DFA8_eotS);
	static final short[] DFA8_eof = DFA.unpackEncodedString(DFA8_eofS);
	static final char[] DFA8_min = DFA.unpackEncodedStringToUnsignedChars(DFA8_minS);
	static final char[] DFA8_max = DFA.unpackEncodedStringToUnsignedChars(DFA8_maxS);
	static final short[] DFA8_accept = DFA.unpackEncodedString(DFA8_acceptS);
	static final short[] DFA8_special = DFA.unpackEncodedString(DFA8_specialS);
	static final short[][] DFA8_transition;

	static {
		int numStates = DFA8_transitionS.length;
		DFA8_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA8_transition[i] = DFA.unpackEncodedString(DFA8_transitionS[i]);
		}
	}

	protected class DFA8 extends DFA {

		public DFA8(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 8;
			this.eot = DFA8_eot;
			this.eof = DFA8_eof;
			this.min = DFA8_min;
			this.max = DFA8_max;
			this.accept = DFA8_accept;
			this.special = DFA8_special;
			this.transition = DFA8_transition;
		}
		@Override
		public String getDescription() {
			return "1:1: Tokens : ( T__128 | T__129 | T__130 | T__131 | T__132 | T__133 | T__134 | T__135 | T__136 | T__137 | T__138 | T__139 | BACKSINGLEQUOTE | WS | COMMENT | LINE_COMMENT | JDBCTokens. Tokens );";
		}
		@Override
		public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
			IntStream input = _input;
			int _s = s;
			switch ( s ) {
					case 0 : 
						int LA8_38 = input.LA(1);
						s = -1;
						if ( (LA8_38=='/') ) {s = 42;}
						else if ( (LA8_38=='*') ) {s = 33;}
						else if ( ((LA8_38 >= '\u0000' && LA8_38 <= ')')||(LA8_38 >= '+' && LA8_38 <= '.')||(LA8_38 >= '0' && LA8_38 <= '\uFFFF')) ) {s = 34;}
						else s = 41;
						if ( s>=0 ) return s;
						break;

					case 1 : 
						int LA8_33 = input.LA(1);
						s = -1;
						if ( (LA8_33=='/') ) {s = 38;}
						else if ( (LA8_33=='*') ) {s = 33;}
						else if ( ((LA8_33 >= '\u0000' && LA8_33 <= ')')||(LA8_33 >= '+' && LA8_33 <= '.')||(LA8_33 >= '0' && LA8_33 <= '\uFFFF')) ) {s = 34;}
						if ( s>=0 ) return s;
						break;

					case 2 : 
						int LA8_31 = input.LA(1);
						s = -1;
						if ( ((LA8_31 >= '\u0000' && LA8_31 <= '\t')||(LA8_31 >= '\u000B' && LA8_31 <= '\f')||(LA8_31 >= '\u000E' && LA8_31 <= '\uFFFF')) ) {s = 35;}
						else if ( (LA8_31=='\r') ) {s = 36;}
						else if ( (LA8_31=='\n') ) {s = 37;}
						if ( s>=0 ) return s;
						break;

					case 3 : 
						int LA8_43 = input.LA(1);
						s = -1;
						if ( (LA8_43=='/') ) {s = 38;}
						else if ( (LA8_43=='*') ) {s = 33;}
						else if ( ((LA8_43 >= '\u0000' && LA8_43 <= ')')||(LA8_43 >= '+' && LA8_43 <= '.')||(LA8_43 >= '0' && LA8_43 <= '\uFFFF')) ) {s = 34;}
						if ( s>=0 ) return s;
						break;

					case 4 : 
						int LA8_42 = input.LA(1);
						s = -1;
						if ( (LA8_42=='*') ) {s = 43;}
						else if ( ((LA8_42 >= '\u0000' && LA8_42 <= ')')||(LA8_42 >= '+' && LA8_42 <= '\uFFFF')) ) {s = 34;}
						if ( s>=0 ) return s;
						break;

					case 5 : 
						int LA8_34 = input.LA(1);
						s = -1;
						if ( (LA8_34=='*') ) {s = 33;}
						else if ( ((LA8_34 >= '\u0000' && LA8_34 <= ')')||(LA8_34 >= '+' && LA8_34 <= '\uFFFF')) ) {s = 34;}
						if ( s>=0 ) return s;
						break;

					case 6 : 
						int LA8_30 = input.LA(1);
						s = -1;
						if ( (LA8_30=='*') ) {s = 33;}
						else if ( ((LA8_30 >= '\u0000' && LA8_30 <= ')')||(LA8_30 >= '+' && LA8_30 <= '\uFFFF')) ) {s = 34;}
						if ( s>=0 ) return s;
						break;

					case 7 : 
						int LA8_32 = input.LA(1);
						s = -1;
						if ( ((LA8_32 >= '\u0000' && LA8_32 <= '\t')||(LA8_32 >= '\u000B' && LA8_32 <= '\f')||(LA8_32 >= '\u000E' && LA8_32 <= '\uFFFF')) ) {s = 35;}
						else if ( (LA8_32=='\r') ) {s = 36;}
						else if ( (LA8_32=='\n') ) {s = 37;}
						if ( s>=0 ) return s;
						break;

					case 8 : 
						int LA8_35 = input.LA(1);
						s = -1;
						if ( (LA8_35=='\r') ) {s = 36;}
						else if ( (LA8_35=='\n') ) {s = 37;}
						else if ( ((LA8_35 >= '\u0000' && LA8_35 <= '\t')||(LA8_35 >= '\u000B' && LA8_35 <= '\f')||(LA8_35 >= '\u000E' && LA8_35 <= '\uFFFF')) ) {s = 35;}
						if ( s>=0 ) return s;
						break;
			}
			NoViableAltException nvae =
				new NoViableAltException(getDescription(), 8, _s, input);
			error(nvae);
			throw nvae;
		}
	}

}
